const { task, src, dest, watch, series } = require("gulp");
const less = require("gulp-less");
 
task("less", function(){
	return src("./assets/styles/*.less")
        .pipe(less())
        .pipe(dest("./assets/styles"));
});

task("watch", function () {
	watch("./assets/styles/*.less", series("less"));
});

task("default", series("less", "watch"));