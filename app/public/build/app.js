(self["webpackChunk"] = self["webpackChunk"] || []).push([["app"],{

/***/ "./assets/controllers sync recursive ./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js! \\.(j|t)sx?$":
/*!*****************************************************************************************************************!*\
  !*** ./assets/controllers/ sync ./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js! \.(j|t)sx?$ ***!
  \*****************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./hello_controller.js": "./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js!./assets/controllers/hello_controller.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./assets/controllers sync recursive ./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js! \\.(j|t)sx?$";

/***/ }),

/***/ "./node_modules/@symfony/stimulus-bridge/dist/webpack/loader.js!./assets/controllers.json":
/*!************************************************************************************************!*\
  !*** ./node_modules/@symfony/stimulus-bridge/dist/webpack/loader.js!./assets/controllers.json ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
});

/***/ }),

/***/ "./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js!./assets/controllers/hello_controller.js":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js!./assets/controllers/hello_controller.js ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ _default)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_object_set_prototype_of_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.object.set-prototype-of.js */ "./node_modules/core-js/modules/es.object.set-prototype-of.js");
/* harmony import */ var core_js_modules_es_object_set_prototype_of_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_set_prototype_of_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_object_get_prototype_of_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.object.get-prototype-of.js */ "./node_modules/core-js/modules/es.object.get-prototype-of.js");
/* harmony import */ var core_js_modules_es_object_get_prototype_of_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_get_prototype_of_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_reflect_construct_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.reflect.construct.js */ "./node_modules/core-js/modules/es.reflect.construct.js");
/* harmony import */ var core_js_modules_es_reflect_construct_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_reflect_construct_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_object_create_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.object.create.js */ "./node_modules/core-js/modules/es.object.create.js");
/* harmony import */ var core_js_modules_es_object_create_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_create_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_object_define_property_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.object.define-property.js */ "./node_modules/core-js/modules/es.object.define-property.js");
/* harmony import */ var core_js_modules_es_object_define_property_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_define_property_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var stimulus__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! stimulus */ "./node_modules/stimulus/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }














function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */

var _default = /*#__PURE__*/function (_Controller) {
  _inherits(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    _classCallCheck(this, _default);

    return _super.apply(this, arguments);
  }

  _createClass(_default, [{
    key: "connect",
    value: function connect() {
      this.element.textContent = 'Hello Stimulus! Edit me in assets/controllers/hello_controller.js';
    }
  }]);

  return _default;
}(stimulus__WEBPACK_IMPORTED_MODULE_12__.Controller);



/***/ }),

/***/ "./assets/app.js":
/*!***********************!*\
  !*** ./assets/app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "toggleShippingAccordion": () => (/* binding */ toggleShippingAccordion),
/* harmony export */   "validateForm": () => (/* binding */ validateForm),
/* harmony export */   "getCart": () => (/* binding */ getCart),
/* harmony export */   "validateReduceQuantity": () => (/* binding */ validateReduceQuantity),
/* harmony export */   "validateIncreaseQuantity": () => (/* binding */ validateIncreaseQuantity),
/* harmony export */   "changeQuantity": () => (/* binding */ changeQuantity),
/* harmony export */   "validateQuantity": () => (/* binding */ validateQuantity),
/* harmony export */   "blurPage": () => (/* binding */ blurPage),
/* harmony export */   "focusPage": () => (/* binding */ focusPage),
/* harmony export */   "removeItem": () => (/* binding */ removeItem),
/* harmony export */   "toggleExpandedTotalAndTaxTable": () => (/* binding */ toggleExpandedTotalAndTaxTable),
/* harmony export */   "productAccordion": () => (/* binding */ productAccordion),
/* harmony export */   "accordionButtonClick": () => (/* binding */ accordionButtonClick),
/* harmony export */   "toggle": () => (/* binding */ toggle),
/* harmony export */   "productFeaturesAccordionButtonClick": () => (/* binding */ productFeaturesAccordionButtonClick),
/* harmony export */   "handleNextLink": () => (/* binding */ handleNextLink),
/* harmony export */   "myFunction": () => (/* binding */ myFunction),
/* harmony export */   "loadProduct": () => (/* binding */ loadProduct),
/* harmony export */   "loadProducts": () => (/* binding */ loadProducts),
/* harmony export */   "openNav": () => (/* binding */ openNav),
/* harmony export */   "closeNav": () => (/* binding */ closeNav)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.parse-int.js */ "./node_modules/core-js/modules/es.parse-int.js");
/* harmony import */ var core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_parse_int_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.array.concat.js */ "./node_modules/core-js/modules/es.array.concat.js");
/* harmony import */ var core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_concat_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.array.for-each.js */ "./node_modules/core-js/modules/es.array.for-each.js");
/* harmony import */ var core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_for_each_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/web.dom-collections.for-each.js */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");
/* harmony import */ var core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_for_each_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/web.timers.js */ "./node_modules/core-js/modules/web.timers.js");
/* harmony import */ var core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_timers_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _styles_app_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./styles/app.css */ "./assets/styles/app.css");
/* harmony import */ var _bootstrap_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./bootstrap.js */ "./assets/bootstrap.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var _components_App_jsx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/App.jsx */ "./assets/components/App.jsx");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_14__);










/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)
 // start the Stimulus application






react_dom__WEBPACK_IMPORTED_MODULE_12__.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_11__.createElement(_components_App_jsx__WEBPACK_IMPORTED_MODULE_13__.default, null), document.getElementById('root')); //Footer accordion

var acc = document.getElementsByClassName("accordion_button");

if (acc) {
  for (var i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
      this.classList.toggle("open");
      var panel = this.nextElementSibling;

      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    });
  }
} //Shipping accordion


function toggleShippingAccordion() {
  var accordionButtons = document.getElementsByClassName('accordion_button');
  var panels = document.getElementsByClassName('shipping-accordion-panel');

  for (var _i = 0; _i < accordionButtons.length; _i++) {
    accordionButtons[_i].classList.toggle("open");

    if (panels[_i].style.maxHeight) {
      panels[_i].style.maxHeight = null;
    } else {
      panels[_i].style.maxHeight = panels[_i].scrollHeight + "px";
    }
  }
} // Shipping form validation

function validateForm() {
  var valid = true;
  $('input').each(function (index, element) {
    if (!element.checkValidity()) {
      valid = false;
      $(element).next().text('Vaadittu kenttä.');
      $(element).css('border', '1px solid #e1771f');
      $(element).prev().css('color', '#e1771f');
    } else {
      $(element).next().text('');
      $(element).css('border', '1px solid lightgray');
      $(element).prev().css('color', '#5f5f5f');
    }
  });
  return valid;
} // Shopping cart

function getCart() {
  $.get("/api/cart/get", function (data) {
    if (data.items.length > 0) {
      $(".shopping-cart-summary").removeClass("invisible");
      $(".shopping-cart-items-count").text(data.items.length);
      $(".shopping-cart-total").html(data.total.toFixed(2) + "&nbsp;&euro;");
    } else {
      $(".shopping-cart-summary").addClass("invisible");
    }
  }, 'json');
}
function validateReduceQuantity() {
  var input = $('.quantity-input').val();

  if (parseInt(input) > 0) {
    $('.quantity-input').val(parseInt(input) - 1);
    return true;
  }

  if (parseInt(input.value) === 0) {
    $('.nonpositive-quantity-warning').removeClass('d-none');
  }
}
function validateIncreaseQuantity() {
  var input = $('.quantity-input').val();

  if (parseInt(input) < 100) {
    $('.quantity-input').val(parseInt(input) + 1);
    return true;
  }
}
function changeQuantity() {
  var input = $('.quantity-input').val();
  var url = $('.url-input').val();
  $.post('api/cart/change', {
    quantity: parseInt(input),
    url: url
  }).then(function () {
    return focusPage();
  });
}
function validateQuantity() {
  var input = $('.quantity-input').val();

  if (parseInt(input) > 0 && parseInt(input) < 100) {
    addToCart();
    $('.nonpositive-quantity-warning').addClass('d-none');
    $('.quantity-warning').addClass('d-none');
    return true;
  } else if (parseInt(input) < 1) {
    $('.nonpositive-quantity-warning').removeClass('d-none');
    $(".add-cart-confirmed").addClass("d-none");
  } else {
    $('.quantity-warning').removeClass('d-none');
    $(".add-cart-confirmed").addClass("d-none");
  }
}
function blurPage() {
  $('.page-container').css('opacity', 0.5);
  $('.spinner-border').removeClass('d-none');
}
function focusPage() {
  $('.page-container').css('opacity', 1);
  $('.spinner-border').addClass('d-none');
}

function addToCart() {
  $(".order-btn").css("opacity", 0.5);
  $(".order-btn-spinner-border").removeClass("d-none");
  $.post("/api/product/add", {
    'product': $('.url-input').val(),
    'quantity': $('.quantity-input').val()
  }, function () {
    $(".add-cart-confirmed").removeClass("d-none");
    $(".order-btn").css("opacity", 1);
    $(".order-btn-spinner-border").addClass("d-none");
    return true;
  });
}

function removeItem(url) {
  axios__WEBPACK_IMPORTED_MODULE_14___default().get("api/cart/remove/" + url).then(function () {
    return focusPage();
  });
}
function toggleExpandedTotalAndTaxTable() {
  if ($('.expanded-total-and-tax-table-container').hasClass('d-none')) {
    $('.expanded-total-and-tax-table-container').removeClass('d-none');
    $('.shipping-form-container').addClass('d-none');
    $('.shipping-progress-indicator-container').addClass('d-none');
    $('.shipping-mobile-header-container').addClass('d-none');
    $('.shipping-main-content-container').removeClass('mt-4');
  } else {
    $('.expanded-total-and-tax-table-container').addClass('d-none');
    $('.shipping-form-container').removeClass('d-none');
    $('.shipping-progress-indicator-container').removeClass('d-none');
    $('.shipping-mobile-header-container').removeClass('d-none');
    $('.shipping-main-content-container').addClass('mt-4');
  }
} // Product accordion and tabs

function productAccordion() {
  var productAcc = document.getElementsByClassName("product_accordion_button");

  for (var _i2 = 0; _i2 < productAcc.length; _i2++) {
    productAcc[_i2].addEventListener("click", accordionButtonClick);
  }

  var productFeaturesAcc = document.getElementsByClassName("product_features_accordion_button");

  var _loop = function _loop(_i3) {
    productFeaturesAcc[_i3].addEventListener("click", function () {
      return productFeaturesAccordionButtonClick(_i3);
    });
  };

  for (var _i3 = 0; _i3 < productFeaturesAcc.length; _i3++) {
    _loop(_i3);
  }
}
function accordionButtonClick(accordionButton) {
  var productAcc = document.getElementsByClassName("product_accordion_button");
  var tabList = document.getElementById('tab-list');
  tabList.className = '';
  var productPanels = document.getElementsByClassName("product_panel");
  var tabs = document.getElementsByClassName("tab");

  for (var _i4 = 0; _i4 < productAcc.length; _i4++) {
    if (accordionButton !== productAcc[_i4] && accordionButton.parentElement !== productAcc[_i4]) {
      tabs[_i4].classList.add('d-none');

      productPanels[_i4].style.maxHeight = null;

      productAcc[_i4].classList.remove('open');
    } else {
      tabList.classList.add('tab' + productAcc[_i4].id.substring('product-accordion-button'.length));
      document.getElementById('tab' + productAcc[_i4].id.substring('product-accordion-button'.length)).classList.remove('d-none');
      productPanels[_i4].style.maxHeight = productPanels[_i4].scrollHeight + "px";

      productAcc[_i4].classList.add('open');
    }
  }
}
function toggle(tab) {
  var tabList = document.getElementById('tab-list');
  var productPanels = document.getElementsByClassName('product_panel');
  var tabs = document.getElementsByClassName('tab');
  var productAcc = document.getElementsByClassName("product_accordion_button");
  tabList.className = "";
  tabList.classList.add(tab);

  for (var _i5 = 0; _i5 < tabs.length; _i5++) {
    tabs[_i5].classList.add('d-none');

    if (productAcc[_i5].id !== 'product-accordion-button' + tab.substring('tab'.length)) {
      productPanels[_i5].style.maxHeight = null;

      productAcc[_i5].classList.remove('open');
    } else {
      productPanels[_i5].style.maxHeight = productPanels[_i5].scrollHeight + "px";

      productAcc[_i5].classList.add('open');
    }
  }

  document.getElementById(tab).classList.remove('d-none');
} // Product features accordion

function productFeaturesAccordionButtonClick() {
  document.getElementsByClassName("product_features_accordion_button")[0].classList.toggle("open");
  document.getElementsByClassName("product_features_accordion_button")[1].classList.toggle("open");
  var productFeaturesPanel1 = document.getElementsByClassName("product_features_accordion_button")[0].nextElementSibling;
  var productPanel1 = document.getElementsByClassName("product_features_accordion_button")[0].parentElement.parentElement;
  var productFeaturesPanel2 = document.getElementsByClassName("product_features_accordion_button")[1].nextElementSibling;
  var productPanel2 = document.getElementsByClassName("product_features_accordion_button")[1].parentElement.parentElement;

  if (productFeaturesPanel1.style.maxHeight) {
    productFeaturesPanel1.style.maxHeight = null;
    productFeaturesPanel2.style.maxHeight = null;
  } else {
    if ($(".product-features-table").html() === "") {
      $(".product-features-spinner-border").removeClass("d-none");
      var url = window.location.pathname.split("/").pop();
      $.get("api/product/" + url, {}, function (data) {
        $(".product-features-spinner-border").addClass("d-none");
        $.each(data.features, function (key, value) {
          $(".product-features-table").html("<tr><td>".concat(key, "</td><td>").concat(value, "</td></tr>"));
        });
        productFeaturesPanel1.style.maxHeight = productFeaturesPanel1.scrollHeight + "px";
        productPanel1.style.maxHeight = productFeaturesPanel1.scrollHeight + productPanel1.scrollHeight + "px";
        productFeaturesPanel2.style.maxHeight = productFeaturesPanel1.scrollHeight + "px";
        productPanel2.style.maxHeight = productFeaturesPanel1.scrollHeight + productPanel1.scrollHeight + "px";
      }, 'json');
    }

    productFeaturesPanel1.style.maxHeight = productFeaturesPanel1.scrollHeight + "px";
    productPanel1.style.maxHeight = productFeaturesPanel1.scrollHeight + productPanel1.scrollHeight + "px";
    productFeaturesPanel2.style.maxHeight = productFeaturesPanel1.scrollHeight + "px";
    productPanel2.style.maxHeight = productFeaturesPanel1.scrollHeight + productPanel1.scrollHeight + "px";
  }
} //Product carousel

var minPerSlide;
function handleNextLink(nextLink) {
  var items = document.querySelectorAll('#productCarousel .carousel-item');
  var currentIndex = $('#productCarousel .active').index();

  if (currentIndex + 1 === items.length - minPerSlide) {
    nextLink.classList.add("disabled");
  } else {
    nextLink.classList.remove("disabled");
  }
}
function myFunction() {
  var xs = window.matchMedia("(max-width: 575px)");
  var sm = window.matchMedia("(min-width: 576px) and (max-width: 767px)");
  var md = window.matchMedia("(min-width: 768px) and (max-width: 991px)");
  var lg = window.matchMedia("(min-width: 992px)");
  xs.addListener(myFunction2);
  sm.addListener(myFunction2);
  md.addListener(myFunction2);
  lg.addListener(myFunction2);
  document.querySelector('#productCarousel .carousel-item').classList.add('active');
  myFunction2(xs);
  myFunction2(sm);
  myFunction2(md);
  myFunction2(lg);
}

function myFunction2(x) {
  if (x.matches) {
    if (x.media == "(max-width: 575px)") {
      minPerSlide = 2;
    } else if (x.media == "(min-width: 576px) and (max-width: 767px)") {
      minPerSlide = 3;
    } else if (x.media == "(min-width: 768px) and (max-width: 991px)") {
      minPerSlide = 4;
    } else if (x.media == "(min-width: 992px)") {
      minPerSlide = 5;
    }

    myFunction3(minPerSlide);
  }
}

function myFunction3(minPerSlide) {
  var nextLink = $('.carousel-control-next');
  var items = document.querySelectorAll('#productCarousel .carousel-item');
  var numPerSlide = items[0].childElementCount;

  if (numPerSlide === 1) {
    items.forEach(function (el) {
      var next = el.nextElementSibling;

      for (var _i6 = 1; _i6 < minPerSlide; _i6++) {
        if (!next) {
          next = items[0];
        }

        var cloneChildren = $(next).children();
        var cloneChild = $(cloneChildren).first().clone(true);
        cloneChild.appendTo(el);
        next = next.nextElementSibling;
      }
    });
  } else if (numPerSlide < minPerSlide) {
    items.forEach(function (el) {
      var next = el.nextElementSibling;

      if (!next) {
        next = items[0];
      }

      var i = numPerSlide;

      while (i < minPerSlide) {
        var cloneChild = next.cloneNode(true);
        var j = numPerSlide - 1;
        var nextChild = cloneChild.children[j];

        while (nextChild && i < minPerSlide) {
          el.appendChild(nextChild);
          nextChild = cloneChild.children[++j];
          i++;
        }

        next = next.nextElementSibling;

        if (!next) {
          next = items[0];
        }
      }
    });
    $('#productCarousel .active')[0].classList.remove('active');
    $('#productCarousel .carousel-item')[0].classList.add('active');
    nextLink[0].classList.remove("disabled");
  } else if (numPerSlide > minPerSlide) {
    items.forEach(function (el) {
      for (var _i7 = numPerSlide - 1; _i7 > minPerSlide - 1; _i7--) {
        el.removeChild(el.children[_i7]);
      }
    });
    $('#productCarousel .active')[0].classList.remove('active');
    $('#productCarousel .carousel-item')[0].classList.add('active');
    nextLink[0].classList.remove("disabled");
  }
} // Product details


function loadProduct(product) {
  setTimeout(function () {
    $(".add-to-cart-form").removeClass("invisible");
    $(".order-btn").css("opacity", 1);

    if ($(".in-store")) {
      $(".in-store-circle").addClass("fas fa-circle").css("opacity", 1);
      $(".in-store").text(product.inStore).css("opacity", 1);
      $(".product-details-wait-spinner-border").addClass("d-none");
    }

    $(".vat").text("Sis. ALV:n (".concat(parseInt(product.vat * 100), "%)"));
    $(".price").html(product.price + " &euro;");
  }, 1000);
} // Product list details

function loadProducts(products) {
  products.map(function (product) {
    $(".price." + product.url).html(product.price + " &euro;");
    $(".vat." + product.url).text("Sis. ALV:n (".concat(parseInt(product.vat * 100), "%)"));

    if ($(".in-store")) {
      $(".in-store-circle." + product.url).addClass("fas fa-circle").css("opacity", 1);
      $(".in-store." + product.url).text(product.inStore).css("opacity", 1);
    }
  });
} // Side navigation

function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
  $("body").css("overflow-y", "hidden");
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  $("body").css("overflow-y", "auto");
}

/***/ }),

/***/ "./assets/bootstrap.js":
/*!*****************************!*\
  !*** ./assets/bootstrap.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "app": () => (/* binding */ app)
/* harmony export */ });
/* harmony import */ var _symfony_stimulus_bridge__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @symfony/stimulus-bridge */ "./node_modules/@symfony/stimulus-bridge/dist/index.js");
 // Registers Stimulus controllers from controllers.json and in the controllers/ directory

var app = (0,_symfony_stimulus_bridge__WEBPACK_IMPORTED_MODULE_0__.startStimulusApp)(__webpack_require__("./assets/controllers sync recursive ./node_modules/@symfony/stimulus-bridge/lazy-controller-loader.js! \\.(j|t)sx?$")); // register any custom, 3rd party controllers here
// app.register('some_controller_name', SomeImportedController);

/***/ }),

/***/ "./assets/components/App.jsx":
/*!***********************************!*\
  !*** ./assets/components/App.jsx ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.array.is-array.js */ "./node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.array.slice.js */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.array.from.js */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _Layout1__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./Layout1 */ "./assets/components/Layout1.jsx");
/* harmony import */ var _Layout2__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./Layout2 */ "./assets/components/Layout2.jsx");
/* harmony import */ var _Cart__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./Cart */ "./assets/components/Cart.jsx");
/* harmony import */ var _Confirmation__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./Confirmation */ "./assets/components/Confirmation.jsx");
/* harmony import */ var _Home__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./Home */ "./assets/components/Home.jsx");
/* harmony import */ var _Product__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./Product */ "./assets/components/Product.jsx");
/* harmony import */ var _Products__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./Products */ "./assets/components/Products.jsx");
/* harmony import */ var _Shipping__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./Shipping */ "./assets/components/Shipping.jsx");
/* harmony import */ var _NotFound__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./NotFound */ "./assets/components/NotFound.jsx");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_22___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_22__);













function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }














var App = function App() {
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_12__.useState)(),
      _useState2 = _slicedToArray(_useState, 2),
      products = _useState2[0],
      setProducts = _useState2[1];

  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_12__.useState)(true),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var _useState5 = (0,react__WEBPACK_IMPORTED_MODULE_12__.useState)(true),
      _useState6 = _slicedToArray(_useState5, 2),
      loadingCart = _useState6[0],
      setLoadingCart = _useState6[1];

  var _useState7 = (0,react__WEBPACK_IMPORTED_MODULE_12__.useState)(),
      _useState8 = _slicedToArray(_useState7, 2),
      cart = _useState8[0],
      setCart = _useState8[1];

  var _useState9 = (0,react__WEBPACK_IMPORTED_MODULE_12__.useState)(false),
      _useState10 = _slicedToArray(_useState9, 2),
      update = _useState10[0],
      setUpdate = _useState10[1];

  function callback(_update) {
    setUpdate(_update);
  }

  (0,react__WEBPACK_IMPORTED_MODULE_12__.useEffect)(function () {
    axios__WEBPACK_IMPORTED_MODULE_22___default().get("/api/product/products").then(function (response) {
      setProducts(response.data);
      setLoading(false);
    });
    return function () {
      setLoading();
      setProducts();
      setCart();
    };
  }, []);
  (0,react__WEBPACK_IMPORTED_MODULE_12__.useEffect)(function () {
    axios__WEBPACK_IMPORTED_MODULE_22___default().get("/api/cart/get").then(function (response) {
      setCart(response.data);
      setLoadingCart(false);

      if (response.data.items.length > 0) {
        $(".shopping-cart-summary").removeClass("invisible");
        $(".shopping-cart-items-count").text(response.data.items.length);
        $(".shopping-cart-total").html(response.data.total.toFixed(2) + "&nbsp;&euro;");
      } else {
        $(".shopping-cart-summary").addClass("invisible");
      }
    });
    return function () {
      setLoading();
      setLoadingCart();
      setUpdate();
      setCart();
    }, [update];
  });
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_23__.BrowserRouter, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Switch, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: ["/", "/checkout", "/products", "/:url"]
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Layout1__WEBPACK_IMPORTED_MODULE_13__.default, {
    cart: cart
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Switch, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Home__WEBPACK_IMPORTED_MODULE_17__.default, {
    products: products,
    loading: loading
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: "/products"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Products__WEBPACK_IMPORTED_MODULE_19__.default, {
    products: products,
    loading: loading
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: "/checkout"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Cart__WEBPACK_IMPORTED_MODULE_15__.default, {
    cart: cart,
    loading: loadingCart,
    callback: callback
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: "/:url"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Product__WEBPACK_IMPORTED_MODULE_18__.default, {
    callback: callback
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    path: "*",
    component: _NotFound__WEBPACK_IMPORTED_MODULE_21__.default
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    path: ["/checkout/shipping", "/checkout/confirmation"]
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Layout2__WEBPACK_IMPORTED_MODULE_14__.default, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Switch, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: "/checkout/shipping"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(_Shipping__WEBPACK_IMPORTED_MODULE_20__.default, {
    cart: cart,
    loading: loadingCart
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_12__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_24__.Route, {
    exact: true,
    path: "/checkout/confirmation",
    component: _Confirmation__WEBPACK_IMPORTED_MODULE_16__.default
  }))))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (App);

/***/ }),

/***/ "./assets/components/Cart.jsx":
/*!************************************!*\
  !*** ./assets/components/Cart.jsx ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app */ "./assets/app.js");
/* harmony import */ var _Loading__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Loading */ "./assets/components/Loading.jsx");
/* harmony import */ var _EmptyCart__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./EmptyCart */ "./assets/components/EmptyCart.jsx");








var Cart = function Cart(_ref) {
  var cart = _ref.cart,
      loading = _ref.loading,
      callback = _ref.callback;
  var history = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_6__.useHistory)();
  return loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(_Loading__WEBPACK_IMPORTED_MODULE_4__.default, null) : cart.items.length === 0 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(_EmptyCart__WEBPACK_IMPORTED_MODULE_5__.default, null) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("main", null, cart.total < 30 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "min-order-amount-warning bg-white p-4 mb-2 ms-3 me-3 mt-4 d-flex justify-content-between",
    style: {
      fontSize: "12px",
      border: "1px solid #e77110",
      position: "relative"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
    style: {
      color: "#e77110",
      position: "absolute",
      left: "30px",
      top: "27px"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "20",
    height: "20",
    fill: "currentColor",
    className: "bi bi-exclamation-triangle",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
    d: "M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.146.146 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.163.163 0 0 1-.054.06.116.116 0 0 1-.066.017H1.146a.115.115 0 0 1-.066-.017.163.163 0 0 1-.054-.06.176.176 0 0 1 .002-.183L7.884 2.073a.147.147 0 0 1 .054-.057zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
    d: "M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
    style: {
      color: "lightgray"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "32",
    height: "32",
    fill: "currentColor",
    className: "bi bi-circle-fill",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("circle", {
    cx: "8",
    cy: "8",
    r: "8"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
    className: "ps-4",
    style: {
      color: "#e77110",
      position: "absolute",
      left: "60px",
      lineHeight: "32px"
    }
  }, "Pienin tilausm\xE4\xE4r\xE4 on 30,00 \u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__.Link, {
    to: "/checkout",
    style: {
      cursor: "pointer"
    },
    onClick: function onClick() {
      document.getElementsByClassName("min-order-amount-warning")[0].classList.add("d-none");
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
    style: {
      color: "#e77110"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "32",
    height: "32",
    fill: "currentColor",
    className: "bi bi-x",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
    d: "M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"
  })))) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "checkout-top-container",
    style: {
      fontSize: "12px",
      color: "#5f5f5f"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-inline-block checkout-main-container",
    style: {
      height: "fitContent"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("h2", {
    className: "cart-details-title mt-2 p-4 mb-0 bg-white fw-bold"
  }, "OSTOKORI"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "cart-details-container p-4 bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "cart-details"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-flex align-items-center p-3",
    style: {
      backgroundColor: "lightgray",
      height: "70px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      width: "40%"
    },
    className: "cart-contents-heading"
  }, "Tuote"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      width: "15%"
    },
    className: "cart-contents-heading"
  }, "Yksikk\xF6hinta"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      width: "30%"
    },
    className: "cart-contents-heading"
  }, "Kpl/tn"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      width: "15%"
    },
    className: "cart-contents-heading"
  }, "Kokonaishinta ilman ALV:t\xE4")), cart.items.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      className: "d-flex align-items-top pt-3 pb-3",
      style: {
        borderBottom: "1px solid lightgray"
      },
      key: index
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        width: "40%"
      },
      className: "cart-contents-item d-flex"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__.Link, {
      to: {
        pathname: item.product.url,
        state: item.product
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("img", {
      style: {
        width: "72px",
        height: "72px"
      },
      src: item.product.imageUrl,
      alt: ""
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      className: "d-inline"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_7__.Link, {
      to: {
        pathname: item.product.url,
        state: item.product
      },
      style: {
        textDecoration: "none",
        color: "#5f5f5f"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, item.product.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        color: "lightgray",
        fontSize: "10px"
      }
    }, item.product.imageUrl)))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        width: "15%"
      },
      className: "cart-contents-item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, (item.product.price * (1 - item.product.vat)).toFixed(2), " ", "\u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        color: "lightgray",
        fontSize: "10px"
      }
    }, "Ilman ALV:t\xE4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, item.product.price, " \u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        color: "lightgray",
        fontSize: "10px"
      }
    }, "Sis. ALV:n (14 %)")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        width: "30%"
      },
      className: "cart-contents-item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("form", {
      className: "d-flex"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("input", {
      className: "url-input",
      name: "url",
      type: "hidden",
      defaultValue: item.product.url
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
      className: "quantity-change-btn text-center",
      style: {
        marginRight: "10px",
        lineHeight: "40px"
      },
      onClick: function onClick() {
        (0,_app__WEBPACK_IMPORTED_MODULE_3__.blurPage)();
        (0,_app__WEBPACK_IMPORTED_MODULE_3__.validateReduceQuantity)() ? (0,_app__WEBPACK_IMPORTED_MODULE_3__.changeQuantity)() : null;
        callback(true);
      }
    }, "-"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("input", {
      name: "quantity",
      type: "number",
      defaultValue: item.quantity,
      className: "quantity-input d-inline form-control text-center",
      style: {
        fontSize: "12px"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
      className: "quantity-change-btn text-center",
      style: {
        marginLeft: "10px",
        lineHeight: "40px"
      },
      onClick: function onClick() {
        (0,_app__WEBPACK_IMPORTED_MODULE_3__.blurPage)();
        (0,_app__WEBPACK_IMPORTED_MODULE_3__.validateIncreaseQuantity)() ? (0,_app__WEBPACK_IMPORTED_MODULE_3__.changeQuantity)() : null;
        callback(true);
      }
    }, "+"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        width: "40%"
      },
      className: "d-inline d-flex justify-content-center align-items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
      style: {
        color: "#5f5f5f",
        cursor: "pointer"
      },
      xmlns: "http://www.w3.org/2000/svg",
      width: "16",
      height: "16",
      fill: "currentColor",
      className: "bi bi-trash",
      viewBox: "0 0 16 16",
      onClick: function onClick() {
        (0,_app__WEBPACK_IMPORTED_MODULE_3__.blurPage)();
        (0,_app__WEBPACK_IMPORTED_MODULE_3__.removeItem)(item.product.url);
        callback(true);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
      d: "M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
      fillRule: "evenodd",
      d: "M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "10px",
        height: "48px",
        width: "140px",
        backgroundColor: "lightgray",
        color: "#b55f18"
      },
      className: "nonpositive-quantity-warning text-center p-2 mt-2 mb-2 d-none"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, "M\xE4\xE4r\xE4 ei ole sallitun"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, "rajan sis\xE4ll\xE4."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        width: "15%"
      },
      className: "cart-contents-item"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, (item.total * (1 - item.product.vat)).toFixed(2), " \u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        color: "lightgray",
        fontSize: "10px"
      }
    }, "Ilman ALV:t\xE4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, item.total.toFixed(2), " \u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        color: "lightgray",
        fontSize: "10px"
      }
    }, "Sis. ALV:n (14 %)")));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "promo-code-container ps-2 pe-2 pt-3 pb-3 mt-2",
    style: {
      border: "1px solid lightgray"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
    className: "mb-3",
    style: {
      color: "#5f5f5f",
      fontWeight: "bold"
    }
  }, "Onko sinulla alennuskoodi?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("form", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("input", {
    style: {
      boxShadow: "none",
      borderRadius: "0",
      fontSize: "13px",
      color: "lightgray"
    },
    className: "form-control fw-bold",
    type: "text",
    defaultValue: "Sy\xF6t\xE4 alennuskoodi"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "checkout-page-btn w-100 mt-1",
    style: {
      border: "none",
      textAlign: "center",
      cursor: "pointer"
    }
  }, "K\xE4yte alennusta"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      color: "#5f5f5f",
      height: "fit-content"
    },
    className: "total-and-tax-container mb-2 me-3 p-4 bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("h6", {
    className: "fw-bold text-left"
  }, "TILAUKSESI"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("table", {
    className: "total-and-tax-table w-100"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, "Kokonaishinta ilman ALV:t\xE4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, (cart.total - cart.totalVat).toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, "Kokonaishinta ALV:\xE4n kanssa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, cart.total.toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, "Verot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, cart.totalVat.toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, "Toimituskulut"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, "Ilmainen")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tr", {
    style: {
      borderTop: "1px solid lightgray",
      fontWeight: "bold"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, "Kokonaishinta ALV:\xE4n kanssa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, cart.total.toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("tr", {
    className: "d-lg-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("td", null, cart.total < 30 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      opacity: "0.5",
      cursor: "pointer"
    },
    className: "checkout-page-btn proceed-to-checkout-btn w-100 mt-1"
  }, "Slirry kassalle") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    onClick: function onClick() {
      return history.push("/checkout/shipping");
    },
    style: {
      cursor: "pointer",
      textAlign: "center"
    },
    className: "checkout-page-btn proceed-to-checkout-btn w-100 mt-1"
  }, "Slirry kassalle"))))), cart.total < 30 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      cursor: "pointer",
      opacity: "0.5"
    },
    className: "d-none d-lg-block checkout-page-btn proceed-to-checkout-btn mt-3"
  }, "Slirry kassalle") : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    onClick: function onClick() {
      return history.push("/checkout/shipping");
    },
    style: {
      cursor: "pointer",
      textAlign: "center"
    },
    className: "d-none d-lg-block checkout-page-btn proceed-to-checkout-btn mt-3"
  }, "Slirry kassalle"))), ";");
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Cart);

/***/ }),

/***/ "./assets/components/Confirmation.jsx":
/*!********************************************!*\
  !*** ./assets/components/Confirmation.jsx ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);




var Confirmation = function Confirmation() {
  var location = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_2__.useLocation)();
  var shippingInfo = location.state;
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    return axios__WEBPACK_IMPORTED_MODULE_1___default().get("/api/cart/clear");
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("main", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "shipping-progress-indicator-container d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-flex mt-4 ps-4 pe-4 justify-content-around justify-content-lg-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      position: "relative"
    },
    className: "ms-auto ms-lg-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("svg", {
    style: {
      color: "#5f5f5f"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    fill: "currentColor",
    className: "bi bi-circle",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("path", {
    d: "M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      color: "#5f5f5f",
      position: "absolute",
      left: "8px",
      top: "4px",
      fontSize: "12px"
    }
  }, "1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "13px",
      color: "#5f5f5f",
      height: "24px",
      lineHeight: "24px"
    },
    className: "ms-3 me-3"
  }, "Toimituskulut")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-none d-md-inline-block me-3",
    style: {
      height: "24px",
      lineHeight: "24px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "/img/line.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      position: "relative"
    },
    className: "me-auto me-lg-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("svg", {
    style: {
      color: "#5f5f5f"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    fill: "currentColor",
    className: "bi bi-circle-fill",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("circle", {
    cx: "8",
    cy: "8",
    r: "8"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      color: "white",
      position: "absolute",
      left: "8px",
      top: "4px",
      fontSize: "12px"
    }
  }, "2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "13px",
      color: "#5f5f5f",
      height: "24px",
      lineHeight: "24px"
    },
    className: "ms-3 me-3 fw-bold"
  }, "Tilausvahvistus")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "mt-4 bg-white",
    style: {
      paddingBottom: "250px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "w-100 text-center fs-3 p-4"
  }, "Kiitos tilauksestasi! Tilauksesi l\xE4hetet\xE4\xE4n osoitteeseen:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      marginLeft: "50%"
    }
  }, shippingInfo.company ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.company) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.firstName, " ", shippingInfo.lastName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.streetAddress1), shippingInfo.streetAddress2 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.streetAddress2) : null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.zipCode), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.city), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.telephone), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, shippingInfo.email))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Confirmation);

/***/ }),

/***/ "./assets/components/EmptyCart.jsx":
/*!*****************************************!*\
  !*** ./assets/components/EmptyCart.jsx ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var EmptyCart = function EmptyCart(_ref) {
  var products = _ref.products;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("main", {
    className: "p-4",
    style: {
      width: '100%',
      height: '500px',
      backgroundColor: 'lightgray',
      lineHeight: '6px'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null, "Ostoskorisi on tyhj\xE4."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null, "Jatka ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__.Link, {
    to: "/",
    style: {
      textDecoration: 'none',
      color: 'black'
    }
  }, "t\xE4st\xE4"), " ostoksiasi."));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (EmptyCart);

/***/ }),

/***/ "./assets/components/Home.jsx":
/*!************************************!*\
  !*** ./assets/components/Home.jsx ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _MainCarousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./MainCarousel */ "./assets/components/MainCarousel.jsx");
/* harmony import */ var _ProductCarousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductCarousel */ "./assets/components/ProductCarousel.jsx");
/* harmony import */ var _Loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Loading */ "./assets/components/Loading.jsx");





var Home = function Home(_ref) {
  var products = _ref.products,
      loading = _ref.loading;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("main", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      backgroundColor: "#e77110",
      color: "white",
      fontSize: "13px",
      fontWeight: "bold"
    },
    className: "banner text-center text-break"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", null, "Rehut suoraan ovellesi - maksuton toimitus kaikkiin tilauksiin! Minimitilausm\xE4\xE4r\xE4 2 tuotetta.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("main", null, loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_Loading__WEBPACK_IMPORTED_MODULE_3__.default, null) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_MainCarousel__WEBPACK_IMPORTED_MODULE_1__.default, null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(_ProductCarousel__WEBPACK_IMPORTED_MODULE_2__.default, {
    products: products
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      background: "url('img/pikkusankari_norrahorse_banneri_1800x500_3f9936cd-b48f-4758-89be-42a69913fe26.jpg')",
      height: "300px",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      position: "relative"
    },
    className: "row mt-5 front-page-image-1 front-page-image-1-container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "front-page-image-1-overlay w-100"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h2", {
    className: "mb-3",
    style: {
      color: "white",
      fontSize: "40px"
    }
  }, "PIKKU SANKARI"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-flex align-items-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "carousel-btn mt3"
  }, "PONIEN OMA REHU, TUTUSTU JA OSTA", " ")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "row mt-5 mb-5 front-page-image-2-container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "col-lg-6"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    className: "img-fluid mt-3 mb-3",
    src: "img/norra_menu_logo_vaaka_1080x.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "col-lg-6 hidden"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h3", {
    className: "mt-4 mb-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "22px",
      color: "#5f5f5f",
      fontWeight: "bold"
    }
  }, " ", "OPTIMOI HEVOSESI RUOKINTA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    style: {
      fontSize: "16px",
      color: "#5f5f5f"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", null, "Maksuttomalla"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    href: "https://norramenu.lantmannenagro.fi/",
    title: "https://norramenu.lantmannenfeed.fi/",
    target: "_blank",
    rel: "noopener"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", null, "Norra Menu -ruokintalaskurilla")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", null, "teet helposti juuri sinun hevosellesi sopivan, turvallisen ja t\xE4ysipainoisen ruokintasuunnitelman. Harkiten suunniteltu ruokinta tukee hevosen hyvinvointia ja suorituskyky\xE4.")))))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Home);

/***/ }),

/***/ "./assets/components/Layout1.jsx":
/*!***************************************!*\
  !*** ./assets/components/Layout1.jsx ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app */ "./assets/app.js");




var Layout1 = function Layout1(_ref) {
  var children = _ref.children,
      cart = _ref.cart;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "page-container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("header", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "spinner-border d-none",
    role: "status",
    style: {
      color: "#e77110",
      width: "50px",
      height: "50px",
      position: "absolute",
      top: "50%",
      left: "50%",
      zIndex: "1040"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "sr-only"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "header-mobile"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "toggle-container"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "toggle",
    style: {
      fontSize: "25px",
      cursor: "context-menu",
      color: "white"
    },
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.openNav
  }, "\u2630")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "logo",
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "img/norra_logo_musta_laatikko_1.png",
    alt: ""
  })), cart ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "shopping-cart d-flex",
    style: {
      textDecoration: "none",
      position: "relative"
    },
    to: "/checkout"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "img/shopping-cart.png",
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "shopping-cart-summary invisible",
    style: {
      marginTop: "-10px",
      marginLeft: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "shopping-cart-items-count text-center"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "shopping-cart-total fw-bold"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "10px",
      color: "white"
    },
    className: "d-block text-end"
  }, "Sis. ALV:n")))) : null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "header-desktop"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "logo",
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "img/norra_logo_musta_laatikko_1.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "shopping-cart d-flex",
    style: {
      textDecoration: "none",
      position: "relative"
    },
    to: "/checkout"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "img/shopping-cart.png",
    alt: ""
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "shopping-cart-summary invisible",
    style: {
      marginTop: "-10px",
      marginLeft: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "shopping-cart-items-count text-center"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "shopping-cart-total fw-bold"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "10px",
      color: "white"
    },
    className: "d-block text-end"
  }, "Sis. ALV:n"))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "navigation-bar"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/"
  }, "RUOKINTALASKURI"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/"
  }, "RUOKINTAVINKIT"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/products"
  }, "TUOTTEET"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/"
  }, "YHTEYSTIEDOT"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/"
  }, "YRITYS")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    id: "mySidenav",
    className: "sidenav d-lg-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      paddingTop: "6px",
      paddingBottom: "6px",
      borderBottom: "2px solid lightgray"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    style: {
      cursor: "context-menu"
    },
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "32px",
      color: "#e77110",
      paddingBottom: "6px"
    }
  }, "\xD7"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "12px",
      color: "#5f5f5f"
    }
  }, " ", "VALIKKO", " "))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/",
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, "RUOKINTALASKURI")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/",
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, "RUOKINTAVINKIT")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/products",
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, "TUOTTEET")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/",
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, "YHTEYSTIEDOT")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "fw-bold",
    to: "/",
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, "YRITYS")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    style: {
      fontSize: "12px"
    },
    to: "/",
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.closeNav
  }, "PIKATILAUS")))), children, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("footer", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-lg-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      fontSize: "13px"
    },
    className: "accordion_button d-flex align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("b", null, "VERKKOKAUPPA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "plus-icon white-plus-icon"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "panel"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "pt-4 pr-4 pb-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    style: {
      fontSize: "13px"
    },
    className: "d-block",
    href: "/"
  }, "Myynti- ja toimitusehdot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    style: {
      fontSize: "13px"
    },
    className: "d-block",
    href: "/"
  }, "Palautukset"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    style: {
      fontSize: "13px"
    },
    className: "d-block",
    href: "/"
  }, "K\xE4ytt\xF6ehdot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    style: {
      fontSize: "13px"
    },
    className: "d-block",
    href: "/"
  }, "Yhteystiedot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    style: {
      fontSize: "13px"
    },
    className: "d-block",
    href: "/"
  }, "Yritys"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      fontSize: "13px"
    },
    className: "accordion_button d-flex align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("b", null, "SEURAA MEIT\xC4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "plus-icon white-plus-icon"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "panel"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "pt-4 pr-4 pb-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("a", {
    href: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "img/f.png",
    alt: ""
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      backgroundColor: "#020c10"
    },
    className: "w-100 row p-3 m-0 d-none d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline-block col-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "mt-3 pb-2",
    style: {
      fontSize: "13px",
      color: "white"
    },
    to: "/"
  }, "VERKKOKAUPPA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "pr-4 pb-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "d-block",
    style: {
      fontSize: "13px"
    },
    to: "/"
  }, "Myynti- ja toimitusehdot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "d-block",
    style: {
      fontSize: "13px"
    },
    to: "/"
  }, "Palautukset"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "d-block",
    style: {
      fontSize: "13px"
    },
    to: "/"
  }, "K\xE4ytt\xF6ehdot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "d-block",
    style: {
      fontSize: "13px"
    },
    to: "/"
  }, "Yhteystiedot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "d-block",
    style: {
      fontSize: "13px"
    },
    to: "/"
  }, "Yritys"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline-block col-2 align-top"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      fontSize: "13px",
      color: "white"
    },
    className: "mt-3 pb-2"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("b", null, "SEURAA MEIT\xC4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    src: "img/f.png",
    style: {
      width: "15px",
      height: "15px"
    },
    alt: ""
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    style: {
      backgroundColor: "#020c10",
      color: "white",
      fontSize: "12px"
    },
    className: "w-100 text-center pt-3 pb-4 mb-0"
  }, "\xA9 2020 Lantm\xE4nnen \xA0 ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    to: "/"
  }, "Tietosuojapolitiikka"), " ", "\xA0 ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    to: "/"
  }, "Ev\xE4stek\xE4yt\xE4nt\xF6"), "\xA0 ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    to: "/"
  }, "Ev\xE4steasetukset"))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Layout1);

/***/ }),

/***/ "./assets/components/Layout2.jsx":
/*!***************************************!*\
  !*** ./assets/components/Layout2.jsx ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app */ "./assets/app.js");




var Layout2 = function Layout2(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("header", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-none d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      height: '74px',
      width: '100%',
      backgroundColor: '#020c10',
      color: 'white'
    },
    className: "d-flex justify-content-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "logo h-100",
    style: {
      lineHeight: '74px'
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    style: {
      height: '60px'
    },
    className: "ml-3 mt-1",
    src: "/img/norra_logo_musta_laatikko_1.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "fs-3 fw-bold h-100",
    style: {
      lineHeight: '70px'
    }
  }, "KASSALLE"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline me-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    style: {
      color: 'white',
      fontSize: '13px',
      textDecoration: 'none',
      lineHeight: '74px'
    },
    to: "/"
  }, "Jatka ostoksia"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "16",
    height: "16",
    fill: "currentColor",
    className: "d-inline bi bi-chevron-right",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("path", {
    fillRule: "evenodd",
    d: "M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
  }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "shipping-mobile-header-container d-lg-none"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      height: '148px',
      width: '100%',
      backgroundColor: '#020c10',
      color: 'white'
    },
    className: "d-flex justify-content-between"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline d-flex ms-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "mt-auto"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "16",
    height: "16",
    fill: "currentColor",
    className: "bi bi-chevron-left",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("path", {
    fillRule: "evenodd",
    d: "M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    style: {
      color: 'white',
      fontSize: '13px',
      textDecoration: 'none',
      lineHeight: '74px'
    },
    to: "/"
  }, "Jatka ostoksia"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "logo h-100 ps-2",
    style: {
      lineHeight: '74px'
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    style: {
      height: '60px'
    },
    className: "mt-1",
    src: "/img/norra_logo_musta_laatikko_1.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "fs-6 fw-bold text-center",
    style: {
      lineHeight: '70px'
    }
  }, "KASSALLE")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "d-inline d-flex me-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "mt-auto",
    style: {
      cursor: 'pointer'
    },
    onClick: _app__WEBPACK_IMPORTED_MODULE_1__.toggleExpandedTotalAndTaxTable
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      color: 'white',
      fontSize: '13px',
      textDecoration: 'none',
      lineHeight: '74px'
    }
  }, "Tilauksesi"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "16",
    height: "16",
    fill: "currentColor",
    className: "d-inline bi bi-chevron-right",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("path", {
    fillRule: "evenodd",
    d: "M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
  })))))), props.children);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Layout2);

/***/ }),

/***/ "./assets/components/Loading.jsx":
/*!***************************************!*\
  !*** ./assets/components/Loading.jsx ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");


var Loading = function Loading() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("main", {
    style: {
      height: '500px',
      width: '100%'
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "spinner-border",
    role: "status",
    style: {
      color: "#e77110",
      width: "50px",
      height: "50px",
      position: "absolute",
      top: "50%",
      left: "50%",
      zIndex: "1040"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    className: "sr-only"
  })));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Loading);

/***/ }),

/***/ "./assets/components/MainCarousel.jsx":
/*!********************************************!*\
  !*** ./assets/components/MainCarousel.jsx ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");



var MainCarousel = function MainCarousel() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    id: "mainCarousel",
    className: "carousel slide ml-2 mr-2",
    "data-bs-ride": "carousel"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-indicators"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    type: "button",
    "data-bs-target": "#mainCarousel",
    "data-bs-slide-to": "0",
    className: "active",
    "aria-current": "true",
    "aria-label": "Slide 1"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    type: "button",
    "data-bs-target": "#mainCarousel",
    "data-bs-slide-to": "1",
    "aria-label": "Slide 2"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    type: "button",
    "data-bs-target": "#mainCarousel",
    "data-bs-slide-to": "2",
    "aria-label": "Slide 3"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    type: "button",
    "data-bs-target": "#mainCarousel",
    "data-bs-slide-to": "3",
    "aria-label": "Slide 4"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    type: "button",
    "data-bs-target": "#mainCarousel",
    "data-bs-slide-to": "4",
    "aria-label": "Slide 5"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-inner"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      background: "url('img/norra-old.jpg')",
      height: "504px",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover"
    },
    className: "main-carousel-image carousel-item active"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__.Link, {
    className: "carousel-caption d-block",
    style: {
      marginBottom: "30px"
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-caption d-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    style: {
      marginBottom: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "36px",
      color: "white",
      fontWeight: "bold"
    }
  }, "RUOKI HEVOSESI HYVINVONTIA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "carousel-btn"
  }, " ", "KOTIMAISET HEVOSREHUT POHJOISIIN OLOIHIN", " ")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      background: "url('img/norrslide2.jpg')",
      height: "504px",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover"
    },
    className: "main-carousel-image carousel-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__.Link, {
    className: "carousel-caption d-block",
    style: {
      marginBottom: "30px"
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-caption d-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    style: {
      marginBottom: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "36px",
      color: "white",
      fontWeight: "bold"
    }
  }, "RUOKI HEVOSESI HYVINVONTIA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "carousel-btn"
  }, " ", "KOTIMAISET HEVOSREHUT POHJOISIIN OLOIHIN", " ")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      background: "url('img/norra_karusellikuva_nuoriso_3000x1100_fed485d3-f2e1-42d9-84c1-0afe9abac6f6_4472x.jpg')",
      height: "504px",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover"
    },
    className: "main-carousel-image carousel-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__.Link, {
    className: "carousel-caption d-block",
    style: {
      marginBottom: "30px"
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-caption d-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    style: {
      marginBottom: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "36px",
      color: "white",
      fontWeight: "bold"
    }
  }, "RUOKI HEVOSESI HYVINVONTIA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "carousel-btn"
  }, " ", "KOTIMAISET HEVOSREHUT POHJOISIIN OLOIHIN", " ")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      background: "url('img/norra_menu_1920x704.jpg')",
      height: "504px",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover"
    },
    className: "main-carousel-image carousel-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__.Link, {
    className: "carousel-caption d-block",
    style: {
      marginBottom: "30px"
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-caption d-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    style: {
      marginBottom: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "36px",
      color: "white",
      fontWeight: "bold"
    }
  }, "RUOKI HEVOSESI HYVINVONTIA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "carousel-btn"
  }, " ", "KOTIMAISET HEVOSREHUT POHJOISIIN OLOIHIN", " ")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      background: "url('img/black-horse.jpg')",
      height: "504px",
      backgroundPosition: "center",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover"
    },
    className: "main-carousel-image carousel-item"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__.Link, {
    className: "carousel-caption d-block",
    style: {
      marginBottom: "30px"
    },
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-caption d-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h1", {
    style: {
      marginBottom: "30px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "36px",
      color: "white",
      fontWeight: "bold"
    }
  }, "RUOKI HEVOSESI HYVINVONTIA")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("button", {
    className: "carousel-btn"
  }, " ", "KOTIMAISET HEVOSREHUT POHJOISIIN OLOIHIN", " "))))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MainCarousel);

/***/ }),

/***/ "./assets/components/NotFound.jsx":
/*!****************************************!*\
  !*** ./assets/components/NotFound.jsx ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");


var NotFound = function NotFound() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("main", null, "The requested resource was not found.");
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NotFound);

/***/ }),

/***/ "./assets/components/Product.jsx":
/*!***************************************!*\
  !*** ./assets/components/Product.jsx ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.regexp.exec.js */ "./node_modules/core-js/modules/es.regexp.exec.js");
/* harmony import */ var core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_regexp_exec_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.string.split.js */ "./node_modules/core-js/modules/es.string.split.js");
/* harmony import */ var core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! core-js/modules/es.string.replace.js */ "./node_modules/core-js/modules/es.string.replace.js");
/* harmony import */ var core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_replace_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var core_js_modules_es_object_keys_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! core-js/modules/es.object.keys.js */ "./node_modules/core-js/modules/es.object.keys.js");
/* harmony import */ var core_js_modules_es_object_keys_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_keys_js__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var core_js_modules_es_object_entries_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! core-js/modules/es.object.entries.js */ "./node_modules/core-js/modules/es.object.entries.js");
/* harmony import */ var core_js_modules_es_object_entries_js__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_entries_js__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! core-js/modules/es.array.is-array.js */ "./node_modules/core-js/modules/es.array.is-array.js");
/* harmony import */ var core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_is_array_js__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! core-js/modules/es.symbol.js */ "./node_modules/core-js/modules/es.symbol.js");
/* harmony import */ var core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_js__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! core-js/modules/es.symbol.description.js */ "./node_modules/core-js/modules/es.symbol.description.js");
/* harmony import */ var core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_description_js__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! core-js/modules/es.object.to-string.js */ "./node_modules/core-js/modules/es.object.to-string.js");
/* harmony import */ var core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_object_to_string_js__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! core-js/modules/es.symbol.iterator.js */ "./node_modules/core-js/modules/es.symbol.iterator.js");
/* harmony import */ var core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_symbol_iterator_js__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! core-js/modules/es.array.iterator.js */ "./node_modules/core-js/modules/es.array.iterator.js");
/* harmony import */ var core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator_js__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! core-js/modules/es.string.iterator.js */ "./node_modules/core-js/modules/es.string.iterator.js");
/* harmony import */ var core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_iterator_js__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator.js */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");
/* harmony import */ var core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator_js__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! core-js/modules/es.array.slice.js */ "./node_modules/core-js/modules/es.array.slice.js");
/* harmony import */ var core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_slice_js__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! core-js/modules/es.function.name.js */ "./node_modules/core-js/modules/es.function.name.js");
/* harmony import */ var core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_function_name_js__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! core-js/modules/es.array.from.js */ "./node_modules/core-js/modules/es.array.from.js");
/* harmony import */ var core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_from_js__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router/esm/react-router.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../app */ "./assets/app.js");
/* harmony import */ var _NotFound_jsx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./NotFound.jsx */ "./assets/components/NotFound.jsx");
/* harmony import */ var _Loading_jsx__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./Loading.jsx */ "./assets/components/Loading.jsx");


















function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }








var Product = function Product(_ref) {
  var callback = _ref.callback;
  var location = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_22__.useLocation)();
  var _product = location.state;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_17__.useState)(),
      _useState2 = _slicedToArray(_useState, 2),
      product = _useState2[0],
      setProduct = _useState2[1];

  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_17__.useState)(true),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  (0,react__WEBPACK_IMPORTED_MODULE_17__.useEffect)(function () {
    if (_product) {
      setProduct(_product);
      (0,_app__WEBPACK_IMPORTED_MODULE_19__.loadProduct)(_product);
      setLoading(false);
    } else {
      axios__WEBPACK_IMPORTED_MODULE_18___default().get("api/product/".concat(window.location.href.split("/").pop())).then(function (response) {
        setProduct(response.data);
        setLoading(false);
      });
    }

    return function () {
      setProduct();
      setLoading();
    };
  }, []);

  if (loading) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement(_Loading_jsx__WEBPACK_IMPORTED_MODULE_21__.default, null);
  } else if (product) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("main", {
      className: "main-container pb-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "d-none d-lg-block mb-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("a", {
      className: "align-top",
      style: {
        color: "#5f5f5f",
        textDecoration: "none",
        fontSize: "small"
      },
      href: "#"
    }, "Etusivu"), "\xA0 / \xA0", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "align-top",
      style: {
        color: "#5f5f5f",
        textDecoration: "none",
        fontSize: "small"
      }
    }, product.title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "add-cart-confirmed bg-white p-4 mb-1 d-flex justify-content-between d-none",
      style: {
        fontSize: "12px",
        border: "1px solid green"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      style: {
        color: "lightgray",
        position: "absolute"
      },
      xmlns: "http://www.w3.org/2000/svg",
      width: "32",
      height: "32",
      fill: "currentColor",
      className: "bi bi-circle-fill",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("circle", {
      cx: "8",
      cy: "8",
      r: "8"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      style: {
        color: "green",
        position: "relative"
      },
      xmlns: "http://www.w3.org/2000/svg",
      width: "32",
      height: "32",
      fill: "currentColor",
      className: "bi bi-check",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
      d: "M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        color: "green"
      }
    }, "\xA0\xA0\xA0 Lis\xE4sit tuotteen ", product.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("a", {
      style: {
        color: "#5f5f5f",
        textDecoration: "none",
        fontWeight: "bold"
      },
      href: "checkout/cart"
    }, "ostoskoriisi.")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_23__.Link, {
      to: {
        pathname: product.url,
        state: product
      },
      style: {
        cursor: "pointer"
      },
      onClick: function onClick() {
        $(".add-cart-confirmed")[0].classList.add("d-none");
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      style: {
        color: "green"
      },
      xmlns: "http://www.w3.org/2000/svg",
      width: "32",
      height: "32",
      fill: "currentColor",
      className: "bi bi-x",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
      d: "M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "d-flex w-100 justify-content-between p-4 bg-white d-none d-lg-flex"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        paddingLeft: "100px",
        width: "55%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("a", {
      href: ""
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      src: product.imageUrl,
      className: "img-fluid w-100"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        width: "45%",
        paddingRight: "40px"
      },
      className: "d-inline pt-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        color: "#5f5f5f",
        fontSize: "12px"
      }
    }, "Tuotenumero \xA0", product.imageUrl.replace(".jpg", "").replace("/img/", "")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("h4", {
      style: {
        color: "#5f5f5f"
      },
      className: "product-title fw-bold"
    }, product.title.toUpperCase())), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "price-box d-flex justify-content-between align-items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        color: "#5f5f5f",
        height: "46px"
      },
      className: "fs-2 fw-bold price ".concat(product.url)
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        fontSize: "x-small"
      },
      className: "text-muted vat ".concat(product.vat)
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("hr", {
      style: {
        marginTop: "6px",
        marginBottom: "48px"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("form", {
      style: {
        height: "40px"
      },
      className: "d-flex invisible add-to-cart-form"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("input", {
      className: "url-input",
      type: "hidden",
      defaultValue: product.url
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "quantity-change-btn text-center",
      style: {
        marginRight: "10px"
      },
      onClick: _app__WEBPACK_IMPORTED_MODULE_19__.validateReduceQuantity
    }, "-"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("input", {
      type: "number",
      defaultValue: "1",
      className: "quantity-input d-inline form-control text-center",
      style: {
        fontSize: "12px"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "quantity-change-btn text-center",
      style: {
        marginLeft: "10px"
      },
      onClick: _app__WEBPACK_IMPORTED_MODULE_19__.validateIncreaseQuantity
    }, "+"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      onClick: function onClick() {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.validateQuantity)() ? callback(true) : null;
      },
      className: "order-btn d-inline text-center w-100",
      style: {
        marginLeft: "20px",
        transition: "0.5s ease-in"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        lineHeight: "40px",
        cursor: "pointer",
        position: "relative"
      },
      className: "d-flex justify-content-center"
    }, "Lis\xE4\xE4 ostokoriin", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "order-btn-spinner-border d-none",
      role: "status",
      style: {
        color: "#e77110",
        width: "20px",
        height: "20px",
        position: "absolute"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "sr-only"
    }))))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        color: "green",
        fontSize: "12px",
        height: "18px"
      },
      className: "mt-3 in-store-box"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("i", {
      style: {
        opacity: "0",
        transition: "0.5s ease-in"
      },
      className: "in-store-circle"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        opacity: "0",
        transition: "0.5s ease-in"
      },
      className: "in-store"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "spinner-border product-details-wait-spinner-border",
      role: "status",
      style: {
        color: "#e77110",
        width: "20px",
        height: "20px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "sr-only"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("p", {
      style: {
        fontSize: "small",
        color: "#5f5f5f",
        marginTop: "16px"
      }
    }, product.shortDescription), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("hr", null), function () {
      return product.sellingPoints.map(function (sellingPoint, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "d-flex"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "d-flex align-items-center"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("i", {
          style: {
            color: "#e77110",
            fontSize: "small"
          },
          className: "fas fa-check"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          style: {
            fontSize: "small",
            color: "#5f5f5f"
          },
          className: "d-inline p-1 fw-bold"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", null, sellingPoint))));
      });
    }(), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/facebook.jpg",
      alt: ""
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/pinterest.jpg",
      alt: ""
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/twitter.jpg",
      alt: ""
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline",
      src: "img/email.png",
      alt: ""
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "md-product-container w-100 bg-white pb-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "pt-4 ps-3"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        color: "#5f5f5f",
        fontSize: "12px"
      }
    }, "Tuotenumero \xA0", product.imageUrl.replace(".jpg", "").replace("/img/", "")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("h4", {
      style: {
        color: "#5f5f5f"
      },
      className: "fw-bold"
    }, product.title.toUpperCase())), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        paddingLeft: "20%",
        paddingRight: "20%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("a", {
      href: ""
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      src: product.imageUrl,
      className: "img-fluid w-100"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        paddingLeft: "20%",
        paddingRight: "20%"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        height: "43px"
      },
      className: "price-box d-flex justify-content-between align-items-center"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        color: "#5f5f5f"
      },
      className: "fs-2 fw-bold price ".concat(product.url)
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        fontSize: "x-small"
      },
      className: "text-muted vat ".concat(product.vat)
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("hr", {
      style: {
        marginTop: "6px",
        marginBottom: "48px"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("form", {
      style: {
        height: "40px"
      },
      className: "d-flex invisible add-to-cart-form"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("input", {
      className: "url-input",
      type: "hidden",
      defaultValue: product.url
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "quantity-change-btn text-center",
      style: {
        marginRight: "10px"
      },
      onClick: function onClick(event) {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.validateReduceQuantity)(event);
      }
    }, "-"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("input", {
      type: "number",
      defaultValue: "1",
      className: "quantity-input d-inline form-control text-center",
      style: {
        fontSize: "12px"
      }
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "quantity-change-btn text-center",
      style: {
        marginLeft: "10px"
      },
      onClick: function onClick(event) {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.validateIncreaseQuantity)(event);
      }
    }, "+"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      onClick: function onClick() {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.validateQuantity)() ? callback(true) : null;
      },
      className: "order-btn d-inline text-center w-100",
      style: {
        marginLeft: "20px",
        transition: "0.5s ease-in"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "order-btn-spinner-border d-none",
      role: "status",
      style: {
        color: "#e77110",
        width: "20px",
        height: "20px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      className: "sr-only"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        lineHeight: "40px",
        cursor: "pointer"
      }
    }, "Lis\xE4\xE4 ostokoriin"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        color: "green",
        fontSize: "12px",
        height: "18px"
      },
      className: "mt-3 in-store-box"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("i", {
      style: {
        opacity: "0",
        transition: "0.5s ease-in"
      },
      className: "in-store-circle"
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
      style: {
        opacity: "0",
        transition: "0.5s ease-in"
      },
      className: "in-store"
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("p", {
      style: {
        fontSize: "small",
        color: "#5f5f5f",
        marginTop: "16px"
      }
    }, product.shortDescription), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("hr", null), function () {
      return product.sellingPoints.map(function (sellingPoint, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          key: index,
          className: "d-flex"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "d-flex align-items-center"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("i", {
          style: {
            color: "#e77110",
            fontSize: "small"
          },
          className: "fas fa-solid fa-check"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          style: {
            fontSize: "small",
            color: "#5f5f5f"
          },
          className: "d-inline p-1 fw-bold"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", null, sellingPoint)));
      });
    }(), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/facebook.jpg",
      alt: ""
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/pinterest.jpg",
      alt: ""
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/twitter.jpg",
      alt: ""
    }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("img", {
      style: {
        width: "22px",
        height: "22px"
      },
      className: "d-inline me-1",
      src: "img/email.png",
      alt: ""
    })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        fontSize: "12px",
        color: "#5f5f5f"
      },
      className: "product-tab-list"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "p-3 bg-white w-100 mt-2"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      id: "tab-list",
      className: "tab-long-description"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "40px",
        cursor: "pointer"
      },
      className: "d-inline fw-bold pt-3 pb-3",
      onClick: function onClick() {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.toggle)("tab-long-description");
      }
    }, "TUOTEKUVAUS"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "40px",
        cursor: "pointer"
      },
      className: "d-inline fw-bold pt-3 pb-3",
      onClick: function onClick() {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.toggle)("tab-contents");
      }
    }, "SIS\xC4LT\xD6"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "40px",
        cursor: "pointer"
      },
      className: "d-inline fw-bold pt-3 pb-3",
      onClick: function onClick() {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.toggle)("tab-more-information");
      }
    }, "LIS\xC4TIETOA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "40px",
        cursor: "pointer"
      },
      className: "d-inline fw-bold pt-3 pb-3",
      onClick: function onClick() {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.toggle)("tab-usage-rate");
      }
    }, "K\xC4YTT\xD6M\xC4\xC4R\xC4"), function () {
      if (product.features) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          style: {
            cursor: "pointer"
          },
          className: "d-inline fw-bold pt-3 pb-3",
          onClick: function onClick() {
            return (0,_app__WEBPACK_IMPORTED_MODULE_19__.toggle)("tab-features");
          }
        }, "OMINAISUUDET");
      }
    }())), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "p-3 bg-white"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "370px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("p", {
      id: "tab-long-description",
      className: "tab"
    }, product.longDescription)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "370px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
      id: "tab-contents",
      className: "tab table table-striped table-borderless d-none"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tbody", null, function () {
      return Object.keys(product.contents).map(function (key, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tr", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, key), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, product.contents[key]));
      });
    }()))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "370px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
      id: "tab-more-information",
      className: "table table-borderless tab d-none"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tbody", null, function () {
      return Object.keys(product.moreInformation).map(function (key, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tr", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, key), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, product.moreInformation[key]));
      });
    }()))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginRight: "370px"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
      id: "tab-usage-rate",
      className: "table table-striped table-borderless tab d-none"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tbody", null, function () {
      return Object.entries(product.usageRate).map(function (key, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tr", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, key), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, product.usageRate[key]));
      });
    }()))), function () {
      if (product.features) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          id: "tab-features",
          className: "tab d-none"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "product_features_accordion_button d-flex align-items-center fw-bold pt-4",
          onClick: function onClick(event) {
            return (0,_app__WEBPACK_IMPORTED_MODULE_19__.productFeaturesAccordionButtonClick)(event.target);
          }
        }, "Tuoten ominaisuudet", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "product_features_plus_icon"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "product_features_panel"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "spinner-border product-features-spinner-border",
          role: "status",
          style: {
            color: "#e77110",
            width: "20px",
            height: "20px"
          }
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
          className: "sr-only"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
          className: "table table-striped table-borderless product-features-table"
        }))));
      }
    }())), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      style: {
        marginTop: "1px"
      },
      className: "product-accordion"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      id: "product-accordion-button-long-description",
      className: "product_accordion_button d-flex align-items-center",
      onClick: function onClick(event) {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.accordionButtonClick)(event.target);
      }
    }, "Tuotekuvaus", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      width: "16",
      height: "16",
      fill: "currentColor",
      className: "bi bi-chevron-down",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
      fillRule: "evenodd",
      d: "M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "product_panel"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "pt-4 pr-4 pb-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("p", null, product.longDescription))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      id: "product-accordion-button-contents",
      className: "product_accordion_button d-flex align-items-center",
      onClick: function onClick(event) {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.accordionButtonClick)(event.target);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("b", null, "Sis\xE4lt\xF6"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      width: "16",
      height: "16",
      fill: "currentColor",
      className: "bi bi-chevron-down",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
      fillRule: "evenodd",
      d: "M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "product_panel"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "pt-4 pr-4 pb-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
      className: "table table-striped table-borderless"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tbody", null, function () {
      return Object.keys(product.contents).map(function (key, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tr", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, key), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, product.contents[key]));
      });
    }())))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      id: "product-accordion-button-more-information",
      className: "product_accordion_button d-flex align-items-center",
      onClick: function onClick(event) {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.accordionButtonClick)(event.target);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("b", null, "Lis\xE4tietoa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      width: "16",
      height: "16",
      fill: "currentColor",
      className: "bi bi-chevron-down",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
      fillRule: "evenodd",
      d: "M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "product_panel"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "pt-4 pr-4 pb-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
      id: "accordion-more-information",
      className: "table table-borderless",
      onClick: function onClick(event) {
        return productAccordionButtonClick(event.target);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tbody", null, function () {
      return Object.keys(product.moreInformation).map(function (key, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tr", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, key), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, product.moreInformation[key]));
      });
    }())))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      id: "product-accordion-button-usage-rate",
      className: "product_accordion_button d-flex align-items-center",
      onClick: function onClick(event) {
        return (0,_app__WEBPACK_IMPORTED_MODULE_19__.accordionButtonClick)(event.target);
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("b", null, "K\xE4ytt\xF6m\xE4\xE4r\xE4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      width: "16",
      height: "16",
      fill: "currentColor",
      className: "bi bi-chevron-down",
      viewBox: "0 0 16 16"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
      fillRule: "evenodd",
      d: "M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
    }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "product_panel"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
      className: "pt-4 pr-4 pb-4"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
      className: "table table-striped table-borderless"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tbody", null, function () {
      return Object.keys(product.usageRate).map(function (key, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("tr", {
          key: index
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, key), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("td", null, product.usageRate[key]));
      });
    }())))), function () {
      if (product.features) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement(react__WEBPACK_IMPORTED_MODULE_17__.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          id: "product-accordion-button-features",
          className: "product_accordion_button d-flex align-items-center",
          onClick: function onClick(event) {
            return (0,_app__WEBPACK_IMPORTED_MODULE_19__.accordionButtonClick)(event.target);
          }
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("b", null, "Ominaisuudet"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("svg", {
          xmlns: "http://www.w3.org/2000/svg",
          width: "16",
          height: "16",
          fill: "currentColor",
          className: "bi bi-chevron-down",
          viewBox: "0 0 16 16"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("path", {
          fillRule: "evenodd",
          d: "M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
        }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "product_panel"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "pt-4 pr-4 pb-4"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          style: {
            fontSize: "12px"
          },
          className: "product_features_accordion_button d-flex align-items-center fw-bold pt-4",
          onClick: function onClick(event) {
            return (0,_app__WEBPACK_IMPORTED_MODULE_19__.productFeaturesAccordionButtonClick)(event.target);
          }
        }, "Tuoten ominaisuudet", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "product_features_plus_icon"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "product_features_panel"
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("div", {
          className: "spinner-border product-features-spinner-border",
          role: "status",
          style: {
            color: "#e77110",
            width: "20px",
            height: "20px"
          }
        }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("span", {
          className: "sr-only"
        })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement("table", {
          className: "table table-striped table-borderless pb-2 product-features-table"
        })))));
      }
    }()));
  } else {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_17__.createElement(_NotFound_jsx__WEBPACK_IMPORTED_MODULE_20__.default, null);
  }
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Product);

/***/ }),

/***/ "./assets/components/ProductCard.jsx":
/*!*******************************************!*\
  !*** ./assets/components/ProductCard.jsx ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app */ "./assets/app.js");




var ProductCard = function ProductCard(_ref) {
  var product = _ref.product;
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    return (0,_app__WEBPACK_IMPORTED_MODULE_1__.loadProduct)(product);
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "col-6 col-lg-4 p-0 mb-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "card h-100"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "card-img d-flex justify-content-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    to: {
      pathname: product.url,
      state: product
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    style: {
      height: '150px'
    },
    src: product.imageUrl,
    className: "img-fluid"
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "card-body d-flex flex-column"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "card-title",
    to: {
      pathname: product.url,
      state: product
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h5", {
    style: {
      color: '#5f5f5f'
    },
    className: "card-title text-center"
  }, product.title)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", {
    style: {
      fontSize: '12px',
      color: '#5f5f5f'
    }
  }, product.shortDescription), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "h-100"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      height: '35px'
    },
    className: "price-box d-flex justify-content-between align-items-center mb-3"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      color: '#5f5f5f'
    },
    className: "fs-4 price fw-bold"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: 'x-small'
    },
    className: "text-muted vat"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      height: '42px'
    },
    className: "btn-box d-flex align-items-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__.Link, {
    className: "card-btn",
    style: {
      textDecoration: 'none'
    },
    to: {
      pathname: product.url,
      state: product
    }
  }, "Lue lis\xE4\xE4")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    style: {
      color: 'green',
      fontSize: '12px',
      height: '18px'
    },
    className: "mt-3 in-store-box"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("i", {
    style: {
      opacity: '0',
      transition: '0.5s ease-in'
    },
    className: "in-store-circle"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      opacity: '0',
      transition: '0.5s ease-in'
    },
    className: "in-store"
  })))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCard);

/***/ }),

/***/ "./assets/components/ProductCarousel.jsx":
/*!***********************************************!*\
  !*** ./assets/components/ProductCarousel.jsx ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _ProductCarouselItem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductCarouselItem */ "./assets/components/ProductCarouselItem.jsx");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app */ "./assets/app.js");






var ProductCarousel = function ProductCarousel(_ref) {
  var products = _ref.products;
  var nextLink = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);
  var previousLink = (0,react__WEBPACK_IMPORTED_MODULE_1__.useRef)(null);

  function handlePreviousClick() {
    nextLink.current.classList.remove("disabled");
    previousLink.current.blur();
  }

  function handleNextClick() {
    (0,_app__WEBPACK_IMPORTED_MODULE_3__.handleNextLink)(nextLink.current);
    nextLink.current.blur();
  }

  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(function () {
    (0,_app__WEBPACK_IMPORTED_MODULE_3__.loadProducts)(products);
    (0,_app__WEBPACK_IMPORTED_MODULE_3__.myFunction)();
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    className: "row mx-auto my-auto justify-content-center mt-5"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    id: "productCarousel",
    className: "carousel slide",
    "data-bs-ride": "carousel",
    "data-bs-interval": "false",
    "data-bs-wrap": "false"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    className: "carousel-inner"
  }, function () {
    if (products) {
      return products.map(function (product, index) {
        return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_ProductCarouselItem__WEBPACK_IMPORTED_MODULE_2__.default, {
          product: product,
          key: index
        });
      });
    }
  }()), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("a", {
    href: "#productCarousel",
    className: "carousel-control-prev bg-transparent",
    "data-bs-slide": "prev",
    role: "button",
    onClick: handlePreviousClick,
    tabIndex: -1,
    ref: previousLink
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("span", {
    className: "carousel-control-prev-icon",
    "aria-hidden": "true"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("a", {
    href: "#productCarousel",
    className: "carousel-control-next bg-transparent",
    "data-bs-slide": "next",
    role: "button",
    onClick: handleNextClick,
    tabIndex: -1,
    ref: nextLink
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("span", {
    className: "carousel-control-next-icon",
    "aria-hidden": "true"
  })))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCarousel);

/***/ }),

/***/ "./assets/components/ProductCarouselItem.jsx":
/*!***************************************************!*\
  !*** ./assets/components/ProductCarouselItem.jsx ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router/esm/react-router.js");



var ProductCarouselItem = function ProductCarouselItem(_ref) {
  var product = _ref.product;
  var history = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_1__.useHistory)();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    return $('.card-btn, .card-title, .img-fluid').click(function (event) {
      var _product = JSON.parse(event.target.id);

      history.push({
        pathname: _product.url,
        state: _product
      });
    });
  }, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "carousel-item",
    style: {
      cursor: "pointer"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "col"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "card"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "card-img d-flex justify-content-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("img", {
    style: {
      height: "150px"
    },
    src: product.imageUrl,
    className: "img-fluid",
    id: JSON.stringify(product)
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "card-body d-flex flex-column"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("h5", {
    id: JSON.stringify(product),
    style: {
      color: "#5f5f5f"
    },
    className: "card-title text-center"
  }, product.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "price-box d-flex justify-content-between align-items-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      color: "#5f5f5f"
    },
    className: "fs-5 price ".concat(product.url)
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      fontSize: "x-small"
    },
    className: "text-muted vat ".concat(product.url)
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", {
    className: "btn-box d-flex align-items-end"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    id: JSON.stringify(product),
    style: {
      textDecoration: "none"
    },
    className: "card-btn"
  }, "Lue lis\xE4\xE4"))))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCarouselItem);

/***/ }),

/***/ "./assets/components/Products.jsx":
/*!****************************************!*\
  !*** ./assets/components/Products.jsx ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _ProductCard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductCard */ "./assets/components/ProductCard.jsx");
/* harmony import */ var _Loading__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Loading */ "./assets/components/Loading.jsx");
/* harmony import */ var _NotFound__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./NotFound */ "./assets/components/NotFound.jsx");







var Products = function Products(_ref) {
  var products = _ref.products,
      loading = _ref.loading;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("main", null, loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_Loading__WEBPACK_IMPORTED_MODULE_3__.default, null) : products ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    className: "d-lg-none p-3 bg-white"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Link, {
    to: "/"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("i", {
    className: "fas fa-chevron-left",
    style: {
      color: "#5f5f5f"
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("span", {
    style: {
      color: "#5f5f5f",
      fontSize: "16px",
      fontWeight: "bold"
    }
  }, "TUOTTEET")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    className: "p-4"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    style: {
      width: "calc(100% - 6px)",
      marginLeft: "3px",
      marginRight: "-3px"
    },
    className: "navigation-links p-4 bg-white d-none d-lg-block mb-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__.Link, {
    style: {
      color: "#5f5f5f",
      textDecoration: "none",
      fontSize: "12px"
    },
    to: "/"
  }, "Etusivu \xA0 / \xA0"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("span", {
    style: {
      color: "#5f5f5f",
      fontSize: "12px"
    }
  }, "TUOTTEET")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    className: "pt-3",
    style: {
      color: "#5f5f5f",
      fontSize: "x-large",
      fontWeight: "bold"
    }
  }, "TUOTTEET")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("div", {
    className: "row m-0"
  }, products.map(function (product, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_ProductCard__WEBPACK_IMPORTED_MODULE_2__.default, {
      product: product,
      key: index
    });
  })))) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement(_NotFound__WEBPACK_IMPORTED_MODULE_4__.default, null));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Products);

/***/ }),

/***/ "./assets/components/Shipping.jsx":
/*!****************************************!*\
  !*** ./assets/components/Shipping.jsx ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.map.js */ "./node_modules/core-js/modules/es.array.map.js");
/* harmony import */ var core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_map_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _ShippingForm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ShippingForm */ "./assets/components/ShippingForm.jsx");
/* harmony import */ var _TotalAndTaxTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./TotalAndTaxTable */ "./assets/components/TotalAndTaxTable.jsx");
/* harmony import */ var _Loading__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Loading */ "./assets/components/Loading.jsx");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app */ "./assets/app.js");








var Shipping = function Shipping(_ref) {
  var cart = _ref.cart,
      loading = _ref.loading;
  return loading ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(_Loading__WEBPACK_IMPORTED_MODULE_5__.default, null) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("main", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "shipping-progress-indicator-container d-lg-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-flex mt-4 ps-4 pe-4 justify-content-around justify-content-lg-center"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      position: "relative"
    },
    className: "me-auto me-lg-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
    style: {
      color: "#5f5f5f"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    fill: "currentColor",
    className: "bi bi-circle-fill",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("circle", {
    cx: "8",
    cy: "8",
    r: "8"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
    style: {
      color: "white",
      position: "absolute",
      left: "8px",
      top: "4px",
      fontSize: "12px"
    }
  }, "1"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
    style: {
      fontSize: "13px",
      color: "#5f5f5f",
      height: "24px",
      lineHeight: "24px"
    },
    className: "ms-3 me-3 fw-bold"
  }, "Toimituskulut")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-none d-md-inline-block me-3",
    style: {
      height: "24px",
      lineHeight: "24px"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("img", {
    src: "/img/line.png",
    alt: ""
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      position: "relative"
    },
    className: "ms-auto ms-lg-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
    style: {
      color: "#5f5f5f"
    },
    xmlns: "http://www.w3.org/2000/svg",
    width: "24",
    height: "24",
    fill: "currentColor",
    className: "bi bi-circle",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
    d: "M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
    style: {
      color: "#5f5f5f",
      position: "absolute",
      left: "8px",
      top: "4px",
      fontSize: "12px"
    }
  }, "2"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("span", {
    style: {
      fontSize: "13px",
      color: "#5f5f5f",
      height: "24px",
      lineHeight: "24px"
    },
    className: "ms-3 me-3"
  }, "Vilausvahvistus")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "shipping-main-content-container d-flex m-lg-4 mt-4",
    style: {
      color: "#5f5f5f"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "shipping-form-container d-lg-inline-block bg-white p-4 me-lg-4 flex-grow-1"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "fw-bold fs-6 w-100 pb-4 mb-4",
    style: {
      borderBottom: "1px solid lightgray"
    }
  }, "Toimitusosoite"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(_ShippingForm__WEBPACK_IMPORTED_MODULE_3__.default, {
    cart: cart
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      height: "fit-content"
    },
    className: "total-and-tax-table-container mb-2 p-4 bg-white d-none d-lg-inline-block d-flex"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-inline-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("h6", {
    className: "fw-bold text-left"
  }, "TILAUKSESI")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(_TotalAndTaxTable__WEBPACK_IMPORTED_MODULE_4__.default, {
    classList: "total-and-tax-table w-100",
    cart: cart
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-none d-lg-inline-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("button", {
    style: {
      fontSize: "12px",
      backgroundColor: "white",
      color: "#5f5f5f",
      borderTop: "1px solid lightgray",
      borderLeft: "1px solid lightgray",
      borderRight: "1px solid lightgray"
    },
    className: "accordion_button d-flex align-items-center",
    onClick: _app__WEBPACK_IMPORTED_MODULE_6__.toggleShippingAccordion
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("b", null, cart.items.length, " TUOTE OSTOSKORISSA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "plus-icon gray-plus-icon"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      borderLeft: "1px solid lightgray",
      borderRight: "1px solid lightgray",
      borderBottom: "1px solid lightgray",
      backgroundColor: "white",
      color: "#5f5f5f"
    },
    className: "shipping-accordion-panel panel"
  }, cart.items.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      key: index,
      className: "pt-3 pr-3 pb-3 d-flex"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      className: "d-inline-block"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("img", {
      style: {
        height: "75px"
      },
      src: item.product.imageUrl,
      alt: ""
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      className: "inline-block"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "12px"
      },
      className: "fw-bold"
    }, item.product.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "12px"
      }
    }, "Kpl/tn: ", item.quantity), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "10px"
      }
    }, "Verollinen: ", item.total.toFixed(2), " \u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "10px"
      }
    }, "Veroton:", " ", (item.total * (1 - item.product.vat)).toFixed(2), " \u20AC")));
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "expanded-total-and-tax-table-container bg-white p-4 d-none d-lg-none w-100 h-100"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      height: "fit-content"
    },
    className: ""
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "w-100 d-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-inline-block"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("h6", {
    className: "fw-bold text-left"
  }, "TILAUKSESI")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "d-flex d-inline-block",
    onClick: _app__WEBPACK_IMPORTED_MODULE_6__.toggleExpandedTotalAndTaxTable
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "16",
    height: "16",
    fill: "currentColor",
    className: "bi bi-x-lg ms-auto",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("path", {
    d: "M1.293 1.293a1 1 0 0 1 1.414 0L8 6.586l5.293-5.293a1 1 0 1 1 1.414 1.414L9.414 8l5.293 5.293a1 1 0 0 1-1.414 1.414L8 9.414l-5.293 5.293a1 1 0 0 1-1.414-1.414L6.586 8 1.293 2.707a1 1 0 0 1 0-1.414z"
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("hr", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement(_TotalAndTaxTable__WEBPACK_IMPORTED_MODULE_4__.default, {
    classList: "d-block total-and-tax-table w-100",
    cart: cart
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      fontSize: "12px",
      backgroundColor: "white",
      color: "#5f5f5f",
      borderTop: "1px solid lightgray",
      borderLeft: "1px solid lightgray",
      borderRight: "1px solid lightgray"
    },
    className: "accordion_button d-flex align-items-center",
    onClick: _app__WEBPACK_IMPORTED_MODULE_6__.toggleShippingAccordion
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("b", null, cart.items.length, " TUOTE OSTOSKORISSA"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    className: "plus-icon gray-plus-icon"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
    style: {
      borderLeft: "1px solid lightgray",
      borderRight: "1px solid lightgray",
      borderBottom: "1px solid lightgray",
      backgroundColor: "white",
      color: "#5f5f5f"
    },
    className: "shipping-accordion-panel panel"
  }, cart.items.map(function (item, index) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      key: index,
      className: "pt-3 pr-3 pb-3 d-flex"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      className: "d-inline-block"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("img", {
      style: {
        height: "75px"
      },
      src: item.product.imageUrl,
      alt: ""
    })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      className: "inline-block"
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "12px"
      },
      className: "fw-bold"
    }, item.product.title), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "12px"
      }
    }, "Kpl/tn: ", item.quantity), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "10px"
      }
    }, "Verollinen: ", item.total.toFixed(2), " \u20AC"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2__.createElement("div", {
      style: {
        fontSize: "10px"
      }
    }, "Veroton:", " ", (item.total * (1 - item.product.vat)).toFixed(2), " ", "\u20AC")));
  })))))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Shipping);

/***/ }),

/***/ "./assets/components/ShippingForm.jsx":
/*!********************************************!*\
  !*** ./assets/components/ShippingForm.jsx ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app */ "./assets/app.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router/esm/react-router.js");




var ShippingForm = function ShippingForm(_ref) {
  var cart = _ref.cart;
  var history = (0,react_router_dom__WEBPACK_IMPORTED_MODULE_2__.useHistory)();

  function handleSubmit() {
    var valid = (0,_app__WEBPACK_IMPORTED_MODULE_1__.validateForm)();

    if (valid && cart.items.length > 0) {
      var shippingInfo = {
        firstName: $("#firstName").val(),
        lastName: $("#lastName").val(),
        company: $("#company").val(),
        streetAddress1: $("#streetAddress1").val(),
        streetAddress2: $("#streetAddress2").val(),
        zipCode: $("#zipCode").val(),
        city: $("#city").val(),
        telephone: $("#telephone").val(),
        email: $("#email").val(),
        marketing: $("#marketing").val(),
        deliveryInfo: $("#deliveryInfo").val()
      };
      history.push({
        pathname: "/checkout/confirmation",
        state: shippingInfo
      });
    }
  }

  ;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("form", {
    className: "shipping-form"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Etunimi *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    id: "firstName",
    className: "form-control",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Sukunumi *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "lastName",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Yritys"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "company"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Katuosoite *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "streetAddress1",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "streetAddress2"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Postinumero *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "zipCode",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Paikkakunta *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "city",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "Puhelinumero *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "telephone",
    defaultValue: "+358",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", null, "S\xE4kh\xF6postiosoite *"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    type: "text",
    className: "form-control",
    id: "email",
    required: true
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("p", null)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("input", {
    style: {
      width: "20px",
      height: "20px",
      cursor: "pointer",
      borderRadius: "0"
    },
    type: "checkbox",
    id: "marketing"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", {
    style: {
      color: "#5f5f5f"
    },
    className: "align-top"
  }, "Minulle saa l\xE4hett\xE4\xE4 Norra Horsen markkinointimateriaalia")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("label", {
    style: {
      color: "#5f5f5f"
    }
  }, "Toimituksen lis\xE4tiedot "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("textarea", {
    style: {
      borderRadius: "0"
    },
    className: "form-control",
    id: "deliveryInfo"
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", null, "Viestiin mahtuu maksimissaan 235 merkki\xE4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0__.createElement("span", {
    style: {
      width: "220px",
      height: "50px",
      cursor: "pointer",
      textAlign: "center"
    },
    className: "checkout-page-btn float-end mt-5 p-3",
    onClick: handleSubmit
  }, "Seuraava"));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ShippingForm);

/***/ }),

/***/ "./assets/components/TotalAndTaxTable.jsx":
/*!************************************************!*\
  !*** ./assets/components/TotalAndTaxTable.jsx ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.number.to-fixed.js */ "./node_modules/core-js/modules/es.number.to-fixed.js");
/* harmony import */ var core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_number_to_fixed_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");



var TotalAndTaxTable = function TotalAndTaxTable(_ref) {
  var classList = _ref.classList,
      cart = _ref.cart;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("table", {
    className: classList
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("tbody", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, "Kokonaishinta ilman ALV:t\xE4"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, (cart.total - cart.totalVat).toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, "Kokonaishinta ALV:\xE4n kanssa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, cart.total.toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, "Verot"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, cart.totalVat.toFixed(2), " \u20AC")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("tr", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, "Toimituskulut"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, "Ilmainen")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("tr", {
    className: "fw-bold"
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, "Kokonaishinta ALV:\xE4n kanssa"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1__.createElement("td", null, cart.total.toFixed(2), " \u20AC"))));
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TotalAndTaxTable);

/***/ }),

/***/ "./assets/styles/app.css":
/*!*******************************!*\
  !*** ./assets/styles/app.css ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendors-node_modules_symfony_stimulus-bridge_dist_index_js-node_modules_axios_index_js-node_m-aa4481"], () => (__webpack_exec__("./assets/app.js")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEk7Ozs7Ozs7Ozs7Ozs7OztBQ3RCQSxpRUFBZTtBQUNmLENBQUMsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDREQ7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7OztXQUVJLG1CQUFVO0FBQ04sV0FBS0MsT0FBTCxDQUFhQyxXQUFiLEdBQTJCLG1FQUEzQjtBQUNIOzs7O0VBSHdCRixpRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWDdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0NBR0E7O0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUVBSSw4Q0FBQSxlQUFnQixrREFBQyx5REFBRCxPQUFoQixFQUF5QkksUUFBUSxDQUFDQyxjQUFULENBQXdCLE1BQXhCLENBQXpCLEUsQ0FFQTs7QUFFQSxJQUFJQyxHQUFHLEdBQUdGLFFBQVEsQ0FBQ0csc0JBQVQsQ0FBZ0Msa0JBQWhDLENBQVY7O0FBRUEsSUFBSUQsR0FBSixFQUFTO0FBQ0wsT0FBSyxJQUFJRSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRixHQUFHLENBQUNHLE1BQXhCLEVBQWdDRCxDQUFDLEVBQWpDLEVBQXFDO0FBRWpDRixJQUFBQSxHQUFHLENBQUNFLENBQUQsQ0FBSCxDQUFPRSxnQkFBUCxDQUF3QixPQUF4QixFQUFpQyxZQUFZO0FBRXpDLFdBQUtDLFNBQUwsQ0FBZUMsTUFBZixDQUFzQixNQUF0QjtBQUVBLFVBQUlDLEtBQUssR0FBRyxLQUFLQyxrQkFBakI7O0FBRUEsVUFBSUQsS0FBSyxDQUFDRSxLQUFOLENBQVlDLFNBQWhCLEVBQTJCO0FBRXZCSCxRQUFBQSxLQUFLLENBQUNFLEtBQU4sQ0FBWUMsU0FBWixHQUF3QixJQUF4QjtBQUVILE9BSkQsTUFJTztBQUVISCxRQUFBQSxLQUFLLENBQUNFLEtBQU4sQ0FBWUMsU0FBWixHQUF3QkgsS0FBSyxDQUFDSSxZQUFOLEdBQXFCLElBQTdDO0FBQ0g7QUFDSixLQWREO0FBZUg7QUFDSixDLENBRUQ7OztBQUVPLFNBQVNDLHVCQUFULEdBQW1DO0FBRXRDLE1BQUlDLGdCQUFnQixHQUFHZixRQUFRLENBQUNHLHNCQUFULENBQWdDLGtCQUFoQyxDQUF2QjtBQUVBLE1BQUlhLE1BQU0sR0FBR2hCLFFBQVEsQ0FBQ0csc0JBQVQsQ0FBZ0MsMEJBQWhDLENBQWI7O0FBRUEsT0FBSyxJQUFJQyxFQUFDLEdBQUcsQ0FBYixFQUFnQkEsRUFBQyxHQUFHVyxnQkFBZ0IsQ0FBQ1YsTUFBckMsRUFBNkNELEVBQUMsRUFBOUMsRUFBa0Q7QUFFOUNXLElBQUFBLGdCQUFnQixDQUFDWCxFQUFELENBQWhCLENBQW9CRyxTQUFwQixDQUE4QkMsTUFBOUIsQ0FBcUMsTUFBckM7O0FBRUEsUUFBSVEsTUFBTSxDQUFDWixFQUFELENBQU4sQ0FBVU8sS0FBVixDQUFnQkMsU0FBcEIsRUFBK0I7QUFFM0JJLE1BQUFBLE1BQU0sQ0FBQ1osRUFBRCxDQUFOLENBQVVPLEtBQVYsQ0FBZ0JDLFNBQWhCLEdBQTRCLElBQTVCO0FBQ0gsS0FIRCxNQUdPO0FBRUhJLE1BQUFBLE1BQU0sQ0FBQ1osRUFBRCxDQUFOLENBQVVPLEtBQVYsQ0FBZ0JDLFNBQWhCLEdBQTRCSSxNQUFNLENBQUNaLEVBQUQsQ0FBTixDQUFVUyxZQUFWLEdBQXlCLElBQXJEO0FBQ0g7QUFDSjtBQUNKLEMsQ0FFRDs7QUFFTyxTQUFTSSxZQUFULEdBQXdCO0FBRTNCLE1BQUlDLEtBQUssR0FBRyxJQUFaO0FBRUFDLEVBQUFBLENBQUMsQ0FBQyxPQUFELENBQUQsQ0FBV0MsSUFBWCxDQUFnQixVQUFDQyxLQUFELEVBQVE1QixPQUFSLEVBQW9CO0FBRWhDLFFBQUksQ0FBQ0EsT0FBTyxDQUFDNkIsYUFBUixFQUFMLEVBQThCO0FBRTFCSixNQUFBQSxLQUFLLEdBQUcsS0FBUjtBQUVBQyxNQUFBQSxDQUFDLENBQUMxQixPQUFELENBQUQsQ0FBVzhCLElBQVgsR0FBa0JDLElBQWxCLENBQXVCLGtCQUF2QjtBQUVBTCxNQUFBQSxDQUFDLENBQUMxQixPQUFELENBQUQsQ0FBV2dDLEdBQVgsQ0FBZSxRQUFmLEVBQXlCLG1CQUF6QjtBQUVBTixNQUFBQSxDQUFDLENBQUMxQixPQUFELENBQUQsQ0FBV2lDLElBQVgsR0FBa0JELEdBQWxCLENBQXNCLE9BQXRCLEVBQStCLFNBQS9CO0FBRUgsS0FWRCxNQVVPO0FBRUhOLE1BQUFBLENBQUMsQ0FBQzFCLE9BQUQsQ0FBRCxDQUFXOEIsSUFBWCxHQUFrQkMsSUFBbEIsQ0FBdUIsRUFBdkI7QUFFQUwsTUFBQUEsQ0FBQyxDQUFDMUIsT0FBRCxDQUFELENBQVdnQyxHQUFYLENBQWUsUUFBZixFQUF5QixxQkFBekI7QUFFQU4sTUFBQUEsQ0FBQyxDQUFDMUIsT0FBRCxDQUFELENBQVdpQyxJQUFYLEdBQWtCRCxHQUFsQixDQUFzQixPQUF0QixFQUErQixTQUEvQjtBQUNIO0FBQ0osR0FwQkQ7QUFzQkEsU0FBT1AsS0FBUDtBQUNILEMsQ0FFRDs7QUFFTyxTQUFTUyxPQUFULEdBQW1CO0FBRXRCUixFQUFBQSxDQUFDLENBQUNTLEdBQUYsQ0FBTSxlQUFOLEVBQXVCLFVBQUFDLElBQUksRUFBSTtBQUUzQixRQUFJQSxJQUFJLENBQUNDLEtBQUwsQ0FBV3pCLE1BQVgsR0FBb0IsQ0FBeEIsRUFBMkI7QUFFdkJjLE1BQUFBLENBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCWSxXQUE1QixDQUF3QyxXQUF4QztBQUVBWixNQUFBQSxDQUFDLENBQUMsNEJBQUQsQ0FBRCxDQUFnQ0ssSUFBaEMsQ0FBcUNLLElBQUksQ0FBQ0MsS0FBTCxDQUFXekIsTUFBaEQ7QUFFQWMsTUFBQUEsQ0FBQyxDQUFDLHNCQUFELENBQUQsQ0FBMEJhLElBQTFCLENBQStCSCxJQUFJLENBQUNJLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQixDQUFuQixJQUF3QixjQUF2RDtBQUVILEtBUkQsTUFRTztBQUVIZixNQUFBQSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QmdCLFFBQTVCLENBQXFDLFdBQXJDO0FBQ0g7QUFFSixHQWZELEVBZUcsTUFmSDtBQWdCSDtBQUVNLFNBQVNDLHNCQUFULEdBQWtDO0FBRXJDLE1BQUlDLEtBQUssR0FBR2xCLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCbUIsR0FBckIsRUFBWjs7QUFFQSxNQUFJQyxRQUFRLENBQUNGLEtBQUQsQ0FBUixHQUFrQixDQUF0QixFQUF5QjtBQUVyQmxCLElBQUFBLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCbUIsR0FBckIsQ0FBeUJDLFFBQVEsQ0FBQ0YsS0FBRCxDQUFSLEdBQWtCLENBQTNDO0FBRUEsV0FBTyxJQUFQO0FBQ0g7O0FBQ0QsTUFBSUUsUUFBUSxDQUFDRixLQUFLLENBQUNHLEtBQVAsQ0FBUixLQUEwQixDQUE5QixFQUFpQztBQUU3QnJCLElBQUFBLENBQUMsQ0FBQywrQkFBRCxDQUFELENBQW1DWSxXQUFuQyxDQUErQyxRQUEvQztBQUNIO0FBQ0o7QUFFTSxTQUFTVSx3QkFBVCxHQUFvQztBQUV2QyxNQUFJSixLQUFLLEdBQUdsQixDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQm1CLEdBQXJCLEVBQVo7O0FBRUEsTUFBSUMsUUFBUSxDQUFDRixLQUFELENBQVIsR0FBa0IsR0FBdEIsRUFBMkI7QUFFdkJsQixJQUFBQSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQm1CLEdBQXJCLENBQXlCQyxRQUFRLENBQUNGLEtBQUQsQ0FBUixHQUFrQixDQUEzQztBQUVBLFdBQU8sSUFBUDtBQUNIO0FBQ0o7QUFFTSxTQUFTSyxjQUFULEdBQTBCO0FBRTdCLE1BQUlMLEtBQUssR0FBR2xCLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCbUIsR0FBckIsRUFBWjtBQUVBLE1BQUlLLEdBQUcsR0FBR3hCLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JtQixHQUFoQixFQUFWO0FBRUFuQixFQUFBQSxDQUFDLENBQUN5QixJQUFGLENBQU8saUJBQVAsRUFBMEI7QUFBRUMsSUFBQUEsUUFBUSxFQUFFTixRQUFRLENBQUNGLEtBQUQsQ0FBcEI7QUFBNkJNLElBQUFBLEdBQUcsRUFBRUE7QUFBbEMsR0FBMUIsRUFDQ0csSUFERCxDQUNNO0FBQUEsV0FBTUMsU0FBUyxFQUFmO0FBQUEsR0FETjtBQUVIO0FBRU0sU0FBU0MsZ0JBQVQsR0FBNEI7QUFFL0IsTUFBSVgsS0FBSyxHQUFHbEIsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJtQixHQUFyQixFQUFaOztBQUVBLE1BQUlDLFFBQVEsQ0FBQ0YsS0FBRCxDQUFSLEdBQWtCLENBQWxCLElBQXVCRSxRQUFRLENBQUNGLEtBQUQsQ0FBUixHQUFrQixHQUE3QyxFQUFrRDtBQUU5Q1ksSUFBQUEsU0FBUztBQUVUOUIsSUFBQUEsQ0FBQyxDQUFDLCtCQUFELENBQUQsQ0FBbUNnQixRQUFuQyxDQUE0QyxRQUE1QztBQUNBaEIsSUFBQUEsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJnQixRQUF2QixDQUFnQyxRQUFoQztBQUVBLFdBQU8sSUFBUDtBQUVILEdBVEQsTUFTTyxJQUFJSSxRQUFRLENBQUNGLEtBQUQsQ0FBUixHQUFrQixDQUF0QixFQUF5QjtBQUU1QmxCLElBQUFBLENBQUMsQ0FBQywrQkFBRCxDQUFELENBQW1DWSxXQUFuQyxDQUErQyxRQUEvQztBQUNBWixJQUFBQSxDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QmdCLFFBQXpCLENBQWtDLFFBQWxDO0FBRUgsR0FMTSxNQUtBO0FBRUhoQixJQUFBQSxDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QlksV0FBdkIsQ0FBbUMsUUFBbkM7QUFDQVosSUFBQUEsQ0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJnQixRQUF6QixDQUFrQyxRQUFsQztBQUVIO0FBQ0o7QUFFTSxTQUFTZSxRQUFULEdBQW9CO0FBRXZCL0IsRUFBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJNLEdBQXJCLENBQXlCLFNBQXpCLEVBQW9DLEdBQXBDO0FBRUFOLEVBQUFBLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCWSxXQUFyQixDQUFpQyxRQUFqQztBQUNIO0FBRU0sU0FBU2dCLFNBQVQsR0FBcUI7QUFFeEI1QixFQUFBQSxDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQk0sR0FBckIsQ0FBeUIsU0FBekIsRUFBb0MsQ0FBcEM7QUFDQU4sRUFBQUEsQ0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJnQixRQUFyQixDQUE4QixRQUE5QjtBQUNIOztBQUVELFNBQVNjLFNBQVQsR0FBcUI7QUFFakI5QixFQUFBQSxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCTSxHQUFoQixDQUFvQixTQUFwQixFQUErQixHQUEvQjtBQUNBTixFQUFBQSxDQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQlksV0FBL0IsQ0FBMkMsUUFBM0M7QUFFQVosRUFBQUEsQ0FBQyxDQUFDeUIsSUFBRixDQUFPLGtCQUFQLEVBQ0k7QUFBRSxlQUFXekIsQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQm1CLEdBQWhCLEVBQWI7QUFBb0MsZ0JBQVluQixDQUFDLENBQUMsaUJBQUQsQ0FBRCxDQUFxQm1CLEdBQXJCO0FBQWhELEdBREosRUFFSSxZQUFNO0FBRUZuQixJQUFBQSxDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QlksV0FBekIsQ0FBcUMsUUFBckM7QUFDQVosSUFBQUEsQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQk0sR0FBaEIsQ0FBb0IsU0FBcEIsRUFBK0IsQ0FBL0I7QUFDQU4sSUFBQUEsQ0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JnQixRQUEvQixDQUF3QyxRQUF4QztBQUVBLFdBQU8sSUFBUDtBQUNILEdBVEw7QUFVSDs7QUFFTSxTQUFTZ0IsVUFBVCxDQUFvQlIsR0FBcEIsRUFBeUI7QUFFNUI3QyxFQUFBQSxpREFBQSxDQUFVLHFCQUFxQjZDLEdBQS9CLEVBQW9DRyxJQUFwQyxDQUF5QztBQUFBLFdBQU1DLFNBQVMsRUFBZjtBQUFBLEdBQXpDO0FBQ0g7QUFFTSxTQUFTSyw4QkFBVCxHQUEwQztBQUU3QyxNQUFJakMsQ0FBQyxDQUFDLHlDQUFELENBQUQsQ0FBNkNrQyxRQUE3QyxDQUFzRCxRQUF0RCxDQUFKLEVBQXFFO0FBRWpFbEMsSUFBQUEsQ0FBQyxDQUFDLHlDQUFELENBQUQsQ0FBNkNZLFdBQTdDLENBQXlELFFBQXpEO0FBQ0FaLElBQUFBLENBQUMsQ0FBQywwQkFBRCxDQUFELENBQThCZ0IsUUFBOUIsQ0FBdUMsUUFBdkM7QUFDQWhCLElBQUFBLENBQUMsQ0FBQyx3Q0FBRCxDQUFELENBQTRDZ0IsUUFBNUMsQ0FBcUQsUUFBckQ7QUFDQWhCLElBQUFBLENBQUMsQ0FBQyxtQ0FBRCxDQUFELENBQXVDZ0IsUUFBdkMsQ0FBZ0QsUUFBaEQ7QUFDQWhCLElBQUFBLENBQUMsQ0FBQyxrQ0FBRCxDQUFELENBQXNDWSxXQUF0QyxDQUFrRCxNQUFsRDtBQUVILEdBUkQsTUFRTztBQUVIWixJQUFBQSxDQUFDLENBQUMseUNBQUQsQ0FBRCxDQUE2Q2dCLFFBQTdDLENBQXNELFFBQXREO0FBQ0FoQixJQUFBQSxDQUFDLENBQUMsMEJBQUQsQ0FBRCxDQUE4QlksV0FBOUIsQ0FBMEMsUUFBMUM7QUFDQVosSUFBQUEsQ0FBQyxDQUFDLHdDQUFELENBQUQsQ0FBNENZLFdBQTVDLENBQXdELFFBQXhEO0FBQ0FaLElBQUFBLENBQUMsQ0FBQyxtQ0FBRCxDQUFELENBQXVDWSxXQUF2QyxDQUFtRCxRQUFuRDtBQUNBWixJQUFBQSxDQUFDLENBQUMsa0NBQUQsQ0FBRCxDQUFzQ2dCLFFBQXRDLENBQStDLE1BQS9DO0FBQ0g7QUFDSixDLENBRUQ7O0FBRU8sU0FBU21CLGdCQUFULEdBQTRCO0FBRS9CLE1BQUlDLFVBQVUsR0FBR3ZELFFBQVEsQ0FBQ0csc0JBQVQsQ0FDYiwwQkFEYSxDQUFqQjs7QUFJQSxPQUFLLElBQUlDLEdBQUMsR0FBRyxDQUFiLEVBQWdCQSxHQUFDLEdBQUdtRCxVQUFVLENBQUNsRCxNQUEvQixFQUF1Q0QsR0FBQyxFQUF4QyxFQUE0QztBQUN4Q21ELElBQUFBLFVBQVUsQ0FBQ25ELEdBQUQsQ0FBVixDQUFjRSxnQkFBZCxDQUErQixPQUEvQixFQUF3Q2tELG9CQUF4QztBQUNIOztBQUVELE1BQUlDLGtCQUFrQixHQUFHekQsUUFBUSxDQUFDRyxzQkFBVCxDQUNyQixtQ0FEcUIsQ0FBekI7O0FBVitCLDZCQWN0QkMsR0Fkc0I7QUFlM0JxRCxJQUFBQSxrQkFBa0IsQ0FBQ3JELEdBQUQsQ0FBbEIsQ0FBc0JFLGdCQUF0QixDQUF1QyxPQUF2QyxFQUFnRDtBQUFBLGFBQzVDb0QsbUNBQW1DLENBQUN0RCxHQUFELENBRFM7QUFBQSxLQUFoRDtBQWYyQjs7QUFjL0IsT0FBSyxJQUFJQSxHQUFDLEdBQUcsQ0FBYixFQUFnQkEsR0FBQyxHQUFHcUQsa0JBQWtCLENBQUNwRCxNQUF2QyxFQUErQ0QsR0FBQyxFQUFoRCxFQUFvRDtBQUFBLFVBQTNDQSxHQUEyQztBQUduRDtBQUNKO0FBRU0sU0FBU29ELG9CQUFULENBQThCRyxlQUE5QixFQUErQztBQUVsRCxNQUFJSixVQUFVLEdBQUd2RCxRQUFRLENBQUNHLHNCQUFULENBQ2IsMEJBRGEsQ0FBakI7QUFJQSxNQUFJeUQsT0FBTyxHQUFHNUQsUUFBUSxDQUFDQyxjQUFULENBQXdCLFVBQXhCLENBQWQ7QUFDQTJELEVBQUFBLE9BQU8sQ0FBQ0MsU0FBUixHQUFvQixFQUFwQjtBQUNBLE1BQUlDLGFBQWEsR0FBRzlELFFBQVEsQ0FBQ0csc0JBQVQsQ0FBZ0MsZUFBaEMsQ0FBcEI7QUFDQSxNQUFJNEQsSUFBSSxHQUFHL0QsUUFBUSxDQUFDRyxzQkFBVCxDQUFnQyxLQUFoQyxDQUFYOztBQUVBLE9BQUssSUFBSUMsR0FBQyxHQUFHLENBQWIsRUFBZ0JBLEdBQUMsR0FBR21ELFVBQVUsQ0FBQ2xELE1BQS9CLEVBQXVDRCxHQUFDLEVBQXhDLEVBQTRDO0FBRXhDLFFBQUl1RCxlQUFlLEtBQUtKLFVBQVUsQ0FBQ25ELEdBQUQsQ0FBOUIsSUFBcUN1RCxlQUFlLENBQUNLLGFBQWhCLEtBQWtDVCxVQUFVLENBQUNuRCxHQUFELENBQXJGLEVBQTBGO0FBRXRGMkQsTUFBQUEsSUFBSSxDQUFDM0QsR0FBRCxDQUFKLENBQVFHLFNBQVIsQ0FBa0IwRCxHQUFsQixDQUFzQixRQUF0Qjs7QUFDQUgsTUFBQUEsYUFBYSxDQUFDMUQsR0FBRCxDQUFiLENBQWlCTyxLQUFqQixDQUF1QkMsU0FBdkIsR0FBbUMsSUFBbkM7O0FBQ0EyQyxNQUFBQSxVQUFVLENBQUNuRCxHQUFELENBQVYsQ0FBY0csU0FBZCxDQUF3QjJELE1BQXhCLENBQStCLE1BQS9CO0FBRUgsS0FORCxNQU1PO0FBRUhOLE1BQUFBLE9BQU8sQ0FBQ3JELFNBQVIsQ0FBa0IwRCxHQUFsQixDQUFzQixRQUFRVixVQUFVLENBQUNuRCxHQUFELENBQVYsQ0FBYytELEVBQWQsQ0FBaUJDLFNBQWpCLENBQTJCLDJCQUEyQi9ELE1BQXRELENBQTlCO0FBQ0FMLE1BQUFBLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixRQUFRc0QsVUFBVSxDQUFDbkQsR0FBRCxDQUFWLENBQWMrRCxFQUFkLENBQWlCQyxTQUFqQixDQUEyQiwyQkFBMkIvRCxNQUF0RCxDQUFoQyxFQUErRkUsU0FBL0YsQ0FBeUcyRCxNQUF6RyxDQUFnSCxRQUFoSDtBQUNBSixNQUFBQSxhQUFhLENBQUMxRCxHQUFELENBQWIsQ0FBaUJPLEtBQWpCLENBQXVCQyxTQUF2QixHQUFtQ2tELGFBQWEsQ0FBQzFELEdBQUQsQ0FBYixDQUFpQlMsWUFBakIsR0FBZ0MsSUFBbkU7O0FBQ0EwQyxNQUFBQSxVQUFVLENBQUNuRCxHQUFELENBQVYsQ0FBY0csU0FBZCxDQUF3QjBELEdBQXhCLENBQTRCLE1BQTVCO0FBQ0g7QUFDSjtBQUNKO0FBRU0sU0FBU3pELE1BQVQsQ0FBZ0I2RCxHQUFoQixFQUFxQjtBQUV4QixNQUFJVCxPQUFPLEdBQUc1RCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsVUFBeEIsQ0FBZDtBQUNBLE1BQUk2RCxhQUFhLEdBQUc5RCxRQUFRLENBQUNHLHNCQUFULENBQWdDLGVBQWhDLENBQXBCO0FBQ0EsTUFBSTRELElBQUksR0FBRy9ELFFBQVEsQ0FBQ0csc0JBQVQsQ0FBZ0MsS0FBaEMsQ0FBWDtBQUNBLE1BQUlvRCxVQUFVLEdBQUd2RCxRQUFRLENBQUNHLHNCQUFULENBQ2IsMEJBRGEsQ0FBakI7QUFJQXlELEVBQUFBLE9BQU8sQ0FBQ0MsU0FBUixHQUFvQixFQUFwQjtBQUNBRCxFQUFBQSxPQUFPLENBQUNyRCxTQUFSLENBQWtCMEQsR0FBbEIsQ0FBc0JJLEdBQXRCOztBQUVBLE9BQUssSUFBSWpFLEdBQUMsR0FBRyxDQUFiLEVBQWdCQSxHQUFDLEdBQUcyRCxJQUFJLENBQUMxRCxNQUF6QixFQUFpQ0QsR0FBQyxFQUFsQyxFQUFzQztBQUVsQzJELElBQUFBLElBQUksQ0FBQzNELEdBQUQsQ0FBSixDQUFRRyxTQUFSLENBQWtCMEQsR0FBbEIsQ0FBc0IsUUFBdEI7O0FBRUEsUUFBSVYsVUFBVSxDQUFDbkQsR0FBRCxDQUFWLENBQWMrRCxFQUFkLEtBQXFCLDZCQUE2QkUsR0FBRyxDQUFDRCxTQUFKLENBQWMsTUFBTS9ELE1BQXBCLENBQXRELEVBQW1GO0FBRS9FeUQsTUFBQUEsYUFBYSxDQUFDMUQsR0FBRCxDQUFiLENBQWlCTyxLQUFqQixDQUF1QkMsU0FBdkIsR0FBbUMsSUFBbkM7O0FBQ0EyQyxNQUFBQSxVQUFVLENBQUNuRCxHQUFELENBQVYsQ0FBY0csU0FBZCxDQUF3QjJELE1BQXhCLENBQStCLE1BQS9CO0FBRUgsS0FMRCxNQUtPO0FBRUhKLE1BQUFBLGFBQWEsQ0FBQzFELEdBQUQsQ0FBYixDQUFpQk8sS0FBakIsQ0FBdUJDLFNBQXZCLEdBQW1Da0QsYUFBYSxDQUFDMUQsR0FBRCxDQUFiLENBQWlCUyxZQUFqQixHQUFnQyxJQUFuRTs7QUFDQTBDLE1BQUFBLFVBQVUsQ0FBQ25ELEdBQUQsQ0FBVixDQUFjRyxTQUFkLENBQXdCMEQsR0FBeEIsQ0FBNEIsTUFBNUI7QUFDSDtBQUNKOztBQUVEakUsRUFBQUEsUUFBUSxDQUFDQyxjQUFULENBQXdCb0UsR0FBeEIsRUFBNkI5RCxTQUE3QixDQUF1QzJELE1BQXZDLENBQThDLFFBQTlDO0FBQ0gsQyxDQUdEOztBQUVPLFNBQVNSLG1DQUFULEdBQStDO0FBRWxEMUQsRUFBQUEsUUFBUSxDQUFDRyxzQkFBVCxDQUFnQyxtQ0FBaEMsRUFBcUUsQ0FBckUsRUFBd0VJLFNBQXhFLENBQWtGQyxNQUFsRixDQUF5RixNQUF6RjtBQUVBUixFQUFBQSxRQUFRLENBQUNHLHNCQUFULENBQWdDLG1DQUFoQyxFQUFxRSxDQUFyRSxFQUF3RUksU0FBeEUsQ0FBa0ZDLE1BQWxGLENBQXlGLE1BQXpGO0FBRUEsTUFBSThELHFCQUFxQixHQUFHdEUsUUFBUSxDQUFDRyxzQkFBVCxDQUFnQyxtQ0FBaEMsRUFBcUUsQ0FBckUsRUFBd0VPLGtCQUFwRztBQUNBLE1BQUk2RCxhQUFhLEdBQUd2RSxRQUFRLENBQUNHLHNCQUFULENBQWdDLG1DQUFoQyxFQUFxRSxDQUFyRSxFQUF3RTZELGFBQXhFLENBQXNGQSxhQUExRztBQUNBLE1BQUlRLHFCQUFxQixHQUFHeEUsUUFBUSxDQUFDRyxzQkFBVCxDQUFnQyxtQ0FBaEMsRUFBcUUsQ0FBckUsRUFBd0VPLGtCQUFwRztBQUNBLE1BQUkrRCxhQUFhLEdBQUd6RSxRQUFRLENBQUNHLHNCQUFULENBQWdDLG1DQUFoQyxFQUFxRSxDQUFyRSxFQUF3RTZELGFBQXhFLENBQXNGQSxhQUExRzs7QUFFQSxNQUFJTSxxQkFBcUIsQ0FBQzNELEtBQXRCLENBQTRCQyxTQUFoQyxFQUEyQztBQUV2QzBELElBQUFBLHFCQUFxQixDQUFDM0QsS0FBdEIsQ0FBNEJDLFNBQTVCLEdBQXdDLElBQXhDO0FBQ0E0RCxJQUFBQSxxQkFBcUIsQ0FBQzdELEtBQXRCLENBQTRCQyxTQUE1QixHQUF3QyxJQUF4QztBQUVILEdBTEQsTUFLTztBQUVILFFBQUlPLENBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCYSxJQUE3QixPQUF3QyxFQUE1QyxFQUFnRDtBQUU1Q2IsTUFBQUEsQ0FBQyxDQUFDLGtDQUFELENBQUQsQ0FBc0NZLFdBQXRDLENBQWtELFFBQWxEO0FBRUEsVUFBSVksR0FBRyxHQUFHK0IsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxRQUFoQixDQUF5QkMsS0FBekIsQ0FBK0IsR0FBL0IsRUFBb0NDLEdBQXBDLEVBQVY7QUFFQTNELE1BQUFBLENBQUMsQ0FBQ1MsR0FBRixDQUFNLGlCQUFpQmUsR0FBdkIsRUFBNEIsRUFBNUIsRUFBZ0MsVUFBVWQsSUFBVixFQUFnQjtBQUU1Q1YsUUFBQUEsQ0FBQyxDQUFDLGtDQUFELENBQUQsQ0FBc0NnQixRQUF0QyxDQUErQyxRQUEvQztBQUVBaEIsUUFBQUEsQ0FBQyxDQUFDQyxJQUFGLENBQU9TLElBQUksQ0FBQ2tELFFBQVosRUFBc0IsVUFBVUMsR0FBVixFQUFleEMsS0FBZixFQUFzQjtBQUV4Q3JCLFVBQUFBLENBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCYSxJQUE3QixtQkFBNkNnRCxHQUE3QyxzQkFBNER4QyxLQUE1RDtBQUNILFNBSEQ7QUFLQThCLFFBQUFBLHFCQUFxQixDQUFDM0QsS0FBdEIsQ0FBNEJDLFNBQTVCLEdBQXdDMEQscUJBQXFCLENBQUN6RCxZQUF0QixHQUFxQyxJQUE3RTtBQUNBMEQsUUFBQUEsYUFBYSxDQUFDNUQsS0FBZCxDQUFvQkMsU0FBcEIsR0FBZ0MwRCxxQkFBcUIsQ0FBQ3pELFlBQXRCLEdBQXFDMEQsYUFBYSxDQUFDMUQsWUFBbkQsR0FBa0UsSUFBbEc7QUFDQTJELFFBQUFBLHFCQUFxQixDQUFDN0QsS0FBdEIsQ0FBNEJDLFNBQTVCLEdBQXdDMEQscUJBQXFCLENBQUN6RCxZQUF0QixHQUFxQyxJQUE3RTtBQUNBNEQsUUFBQUEsYUFBYSxDQUFDOUQsS0FBZCxDQUFvQkMsU0FBcEIsR0FBZ0MwRCxxQkFBcUIsQ0FBQ3pELFlBQXRCLEdBQXFDMEQsYUFBYSxDQUFDMUQsWUFBbkQsR0FBa0UsSUFBbEc7QUFDSCxPQWJELEVBYUcsTUFiSDtBQWNIOztBQUVEeUQsSUFBQUEscUJBQXFCLENBQUMzRCxLQUF0QixDQUE0QkMsU0FBNUIsR0FBd0MwRCxxQkFBcUIsQ0FBQ3pELFlBQXRCLEdBQXFDLElBQTdFO0FBQ0EwRCxJQUFBQSxhQUFhLENBQUM1RCxLQUFkLENBQW9CQyxTQUFwQixHQUFnQzBELHFCQUFxQixDQUFDekQsWUFBdEIsR0FBcUMwRCxhQUFhLENBQUMxRCxZQUFuRCxHQUFrRSxJQUFsRztBQUNBMkQsSUFBQUEscUJBQXFCLENBQUM3RCxLQUF0QixDQUE0QkMsU0FBNUIsR0FBd0MwRCxxQkFBcUIsQ0FBQ3pELFlBQXRCLEdBQXFDLElBQTdFO0FBQ0E0RCxJQUFBQSxhQUFhLENBQUM5RCxLQUFkLENBQW9CQyxTQUFwQixHQUFnQzBELHFCQUFxQixDQUFDekQsWUFBdEIsR0FBcUMwRCxhQUFhLENBQUMxRCxZQUFuRCxHQUFrRSxJQUFsRztBQUNIO0FBQ0osQyxDQUVEOztBQUVBLElBQUlvRSxXQUFKO0FBRU8sU0FBU0MsY0FBVCxDQUF3QkMsUUFBeEIsRUFBa0M7QUFFckMsTUFBSXJELEtBQUssR0FBRzlCLFFBQVEsQ0FBQ29GLGdCQUFULENBQTBCLGlDQUExQixDQUFaO0FBRUEsTUFBSUMsWUFBWSxHQUFHbEUsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEJFLEtBQTlCLEVBQW5COztBQUVBLE1BQUlnRSxZQUFZLEdBQUcsQ0FBZixLQUFxQnZELEtBQUssQ0FBQ3pCLE1BQU4sR0FBZTRFLFdBQXhDLEVBQXFEO0FBRWpERSxJQUFBQSxRQUFRLENBQUM1RSxTQUFULENBQW1CMEQsR0FBbkIsQ0FBdUIsVUFBdkI7QUFDSCxHQUhELE1BR087QUFFSGtCLElBQUFBLFFBQVEsQ0FBQzVFLFNBQVQsQ0FBbUIyRCxNQUFuQixDQUEwQixVQUExQjtBQUNIO0FBQ0o7QUFFTSxTQUFTb0IsVUFBVCxHQUFzQjtBQUV6QixNQUFJQyxFQUFFLEdBQUdiLE1BQU0sQ0FBQ2MsVUFBUCxDQUFrQixvQkFBbEIsQ0FBVDtBQUNBLE1BQUlDLEVBQUUsR0FBR2YsTUFBTSxDQUFDYyxVQUFQLENBQWtCLDJDQUFsQixDQUFUO0FBQ0EsTUFBSUUsRUFBRSxHQUFHaEIsTUFBTSxDQUFDYyxVQUFQLENBQWtCLDJDQUFsQixDQUFUO0FBQ0EsTUFBSUcsRUFBRSxHQUFHakIsTUFBTSxDQUFDYyxVQUFQLENBQWtCLG9CQUFsQixDQUFUO0FBRUFELEVBQUFBLEVBQUUsQ0FBQ0ssV0FBSCxDQUFlQyxXQUFmO0FBQ0FKLEVBQUFBLEVBQUUsQ0FBQ0csV0FBSCxDQUFlQyxXQUFmO0FBQ0FILEVBQUFBLEVBQUUsQ0FBQ0UsV0FBSCxDQUFlQyxXQUFmO0FBQ0FGLEVBQUFBLEVBQUUsQ0FBQ0MsV0FBSCxDQUFlQyxXQUFmO0FBRUE3RixFQUFBQSxRQUFRLENBQUM4RixhQUFULENBQXVCLGlDQUF2QixFQUEwRHZGLFNBQTFELENBQW9FMEQsR0FBcEUsQ0FBd0UsUUFBeEU7QUFFQTRCLEVBQUFBLFdBQVcsQ0FBQ04sRUFBRCxDQUFYO0FBQ0FNLEVBQUFBLFdBQVcsQ0FBQ0osRUFBRCxDQUFYO0FBQ0FJLEVBQUFBLFdBQVcsQ0FBQ0gsRUFBRCxDQUFYO0FBQ0FHLEVBQUFBLFdBQVcsQ0FBQ0YsRUFBRCxDQUFYO0FBQ0g7O0FBRUQsU0FBU0UsV0FBVCxDQUFxQkUsQ0FBckIsRUFBd0I7QUFFcEIsTUFBSUEsQ0FBQyxDQUFDQyxPQUFOLEVBQWU7QUFFWCxRQUFJRCxDQUFDLENBQUNFLEtBQUYsSUFBVyxvQkFBZixFQUFxQztBQUVqQ2hCLE1BQUFBLFdBQVcsR0FBRyxDQUFkO0FBRUgsS0FKRCxNQUlPLElBQUljLENBQUMsQ0FBQ0UsS0FBRixJQUFXLDJDQUFmLEVBQTREO0FBRS9EaEIsTUFBQUEsV0FBVyxHQUFHLENBQWQ7QUFFSCxLQUpNLE1BSUEsSUFBSWMsQ0FBQyxDQUFDRSxLQUFGLElBQVcsMkNBQWYsRUFBNEQ7QUFFL0RoQixNQUFBQSxXQUFXLEdBQUcsQ0FBZDtBQUVILEtBSk0sTUFJQSxJQUFJYyxDQUFDLENBQUNFLEtBQUYsSUFBVyxvQkFBZixFQUFxQztBQUV4Q2hCLE1BQUFBLFdBQVcsR0FBRyxDQUFkO0FBRUg7O0FBRURpQixJQUFBQSxXQUFXLENBQUNqQixXQUFELENBQVg7QUFDSDtBQUNKOztBQUVELFNBQVNpQixXQUFULENBQXFCakIsV0FBckIsRUFBa0M7QUFFOUIsTUFBSUUsUUFBUSxHQUFHaEUsQ0FBQyxDQUFDLHdCQUFELENBQWhCO0FBRUEsTUFBSVcsS0FBSyxHQUFHOUIsUUFBUSxDQUFDb0YsZ0JBQVQsQ0FBMEIsaUNBQTFCLENBQVo7QUFFQSxNQUFJZSxXQUFXLEdBQUdyRSxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNzRSxpQkFBM0I7O0FBRUEsTUFBSUQsV0FBVyxLQUFLLENBQXBCLEVBQXVCO0FBRW5CckUsSUFBQUEsS0FBSyxDQUFDdUUsT0FBTixDQUFjLFVBQUNDLEVBQUQsRUFBUTtBQUVsQixVQUFJL0UsSUFBSSxHQUFHK0UsRUFBRSxDQUFDNUYsa0JBQWQ7O0FBRUEsV0FBSyxJQUFJTixHQUFDLEdBQUcsQ0FBYixFQUFnQkEsR0FBQyxHQUFHNkUsV0FBcEIsRUFBaUM3RSxHQUFDLEVBQWxDLEVBQXNDO0FBRWxDLFlBQUksQ0FBQ21CLElBQUwsRUFBVztBQUVQQSxVQUFBQSxJQUFJLEdBQUdPLEtBQUssQ0FBQyxDQUFELENBQVo7QUFDSDs7QUFFRCxZQUFJeUUsYUFBYSxHQUFHcEYsQ0FBQyxDQUFDSSxJQUFELENBQUQsQ0FBUWlGLFFBQVIsRUFBcEI7QUFDQSxZQUFJQyxVQUFVLEdBQUd0RixDQUFDLENBQUNvRixhQUFELENBQUQsQ0FBaUJHLEtBQWpCLEdBQXlCQyxLQUF6QixDQUErQixJQUEvQixDQUFqQjtBQUNBRixRQUFBQSxVQUFVLENBQUNHLFFBQVgsQ0FBb0JOLEVBQXBCO0FBQ0EvRSxRQUFBQSxJQUFJLEdBQUdBLElBQUksQ0FBQ2Isa0JBQVo7QUFDSDtBQUNKLEtBaEJEO0FBaUJILEdBbkJELE1BbUJPLElBQUl5RixXQUFXLEdBQUdsQixXQUFsQixFQUErQjtBQUVsQ25ELElBQUFBLEtBQUssQ0FBQ3VFLE9BQU4sQ0FBYyxVQUFDQyxFQUFELEVBQVE7QUFDbEIsVUFBSS9FLElBQUksR0FBRytFLEVBQUUsQ0FBQzVGLGtCQUFkOztBQUVBLFVBQUksQ0FBQ2EsSUFBTCxFQUFXO0FBRVBBLFFBQUFBLElBQUksR0FBR08sS0FBSyxDQUFDLENBQUQsQ0FBWjtBQUNIOztBQUVELFVBQUkxQixDQUFDLEdBQUcrRixXQUFSOztBQUVBLGFBQU8vRixDQUFDLEdBQUc2RSxXQUFYLEVBQXdCO0FBRXBCLFlBQUl3QixVQUFVLEdBQUdsRixJQUFJLENBQUNzRixTQUFMLENBQWUsSUFBZixDQUFqQjtBQUNBLFlBQUlDLENBQUMsR0FBR1gsV0FBVyxHQUFHLENBQXRCO0FBQ0EsWUFBSVksU0FBUyxHQUFHTixVQUFVLENBQUNELFFBQVgsQ0FBb0JNLENBQXBCLENBQWhCOztBQUVBLGVBQU9DLFNBQVMsSUFBSTNHLENBQUMsR0FBRzZFLFdBQXhCLEVBQXFDO0FBRWpDcUIsVUFBQUEsRUFBRSxDQUFDVSxXQUFILENBQWVELFNBQWY7QUFDQUEsVUFBQUEsU0FBUyxHQUFHTixVQUFVLENBQUNELFFBQVgsQ0FBb0IsRUFBRU0sQ0FBdEIsQ0FBWjtBQUNBMUcsVUFBQUEsQ0FBQztBQUNKOztBQUVEbUIsUUFBQUEsSUFBSSxHQUFHQSxJQUFJLENBQUNiLGtCQUFaOztBQUVBLFlBQUksQ0FBQ2EsSUFBTCxFQUFXO0FBRVBBLFVBQUFBLElBQUksR0FBR08sS0FBSyxDQUFDLENBQUQsQ0FBWjtBQUNIO0FBQ0o7QUFDSixLQTlCRDtBQWdDQVgsSUFBQUEsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEIsQ0FBOUIsRUFBaUNaLFNBQWpDLENBQTJDMkQsTUFBM0MsQ0FBa0QsUUFBbEQ7QUFDQS9DLElBQUFBLENBQUMsQ0FBQyxpQ0FBRCxDQUFELENBQXFDLENBQXJDLEVBQXdDWixTQUF4QyxDQUFrRDBELEdBQWxELENBQXNELFFBQXREO0FBRUFrQixJQUFBQSxRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVk1RSxTQUFaLENBQXNCMkQsTUFBdEIsQ0FBNkIsVUFBN0I7QUFFSCxHQXZDTSxNQXVDQSxJQUFJaUMsV0FBVyxHQUFHbEIsV0FBbEIsRUFBK0I7QUFFbENuRCxJQUFBQSxLQUFLLENBQUN1RSxPQUFOLENBQWMsVUFBQ0MsRUFBRCxFQUFRO0FBRWxCLFdBQUssSUFBSWxHLEdBQUMsR0FBRytGLFdBQVcsR0FBRyxDQUEzQixFQUE4Qi9GLEdBQUMsR0FBRzZFLFdBQVcsR0FBRyxDQUFoRCxFQUFtRDdFLEdBQUMsRUFBcEQsRUFBd0Q7QUFFcERrRyxRQUFBQSxFQUFFLENBQUNXLFdBQUgsQ0FBZVgsRUFBRSxDQUFDRSxRQUFILENBQVlwRyxHQUFaLENBQWY7QUFDSDtBQUNKLEtBTkQ7QUFRQWUsSUFBQUEsQ0FBQyxDQUFDLDBCQUFELENBQUQsQ0FBOEIsQ0FBOUIsRUFBaUNaLFNBQWpDLENBQTJDMkQsTUFBM0MsQ0FBa0QsUUFBbEQ7QUFDQS9DLElBQUFBLENBQUMsQ0FBQyxpQ0FBRCxDQUFELENBQXFDLENBQXJDLEVBQXdDWixTQUF4QyxDQUFrRDBELEdBQWxELENBQXNELFFBQXREO0FBRUFrQixJQUFBQSxRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVk1RSxTQUFaLENBQXNCMkQsTUFBdEIsQ0FBNkIsVUFBN0I7QUFDSDtBQUNKLEMsQ0FFRDs7O0FBRU8sU0FBU2dELFdBQVQsQ0FBcUJDLE9BQXJCLEVBQThCO0FBRWpDQyxFQUFBQSxVQUFVLENBQUMsWUFBTTtBQUViakcsSUFBQUEsQ0FBQyxDQUFDLG1CQUFELENBQUQsQ0FBdUJZLFdBQXZCLENBQW1DLFdBQW5DO0FBQ0FaLElBQUFBLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JNLEdBQWhCLENBQW9CLFNBQXBCLEVBQStCLENBQS9COztBQUVBLFFBQUlOLENBQUMsQ0FBQyxXQUFELENBQUwsRUFBb0I7QUFFaEJBLE1BQUFBLENBQUMsQ0FBQyxrQkFBRCxDQUFELENBQXNCZ0IsUUFBdEIsQ0FBK0IsZUFBL0IsRUFBZ0RWLEdBQWhELENBQW9ELFNBQXBELEVBQStELENBQS9EO0FBQ0FOLE1BQUFBLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZUssSUFBZixDQUFvQjJGLE9BQU8sQ0FBQ0UsT0FBNUIsRUFBcUM1RixHQUFyQyxDQUF5QyxTQUF6QyxFQUFvRCxDQUFwRDtBQUNBTixNQUFBQSxDQUFDLENBQUMsc0NBQUQsQ0FBRCxDQUEwQ2dCLFFBQTFDLENBQW1ELFFBQW5EO0FBQ0g7O0FBRURoQixJQUFBQSxDQUFDLENBQUMsTUFBRCxDQUFELENBQVVLLElBQVYsdUJBQThCZSxRQUFRLENBQUM0RSxPQUFPLENBQUNHLEdBQVIsR0FBYyxHQUFmLENBQXRDO0FBQ0FuRyxJQUFBQSxDQUFDLENBQUMsUUFBRCxDQUFELENBQVlhLElBQVosQ0FBaUJtRixPQUFPLENBQUNJLEtBQVIsR0FBZ0IsU0FBakM7QUFFSCxHQWZTLEVBZVAsSUFmTyxDQUFWO0FBaUJILEMsQ0FFRDs7QUFFTyxTQUFTQyxZQUFULENBQXNCQyxRQUF0QixFQUFnQztBQUVuQ0EsRUFBQUEsUUFBUSxDQUFDQyxHQUFULENBQWMsVUFBQVAsT0FBTyxFQUFJO0FBRXJCaEcsSUFBQUEsQ0FBQyxDQUFDLFlBQVlnRyxPQUFPLENBQUN4RSxHQUFyQixDQUFELENBQTJCWCxJQUEzQixDQUFnQ21GLE9BQU8sQ0FBQ0ksS0FBUixHQUFnQixTQUFoRDtBQUNBcEcsSUFBQUEsQ0FBQyxDQUFDLFVBQVVnRyxPQUFPLENBQUN4RSxHQUFuQixDQUFELENBQXlCbkIsSUFBekIsdUJBQTZDZSxRQUFRLENBQUM0RSxPQUFPLENBQUNHLEdBQVIsR0FBYyxHQUFmLENBQXJEOztBQUVBLFFBQUluRyxDQUFDLENBQUMsV0FBRCxDQUFMLEVBQW9CO0FBRWhCQSxNQUFBQSxDQUFDLENBQUMsc0JBQXNCZ0csT0FBTyxDQUFDeEUsR0FBL0IsQ0FBRCxDQUFxQ1IsUUFBckMsQ0FBOEMsZUFBOUMsRUFBK0RWLEdBQS9ELENBQW1FLFNBQW5FLEVBQThFLENBQTlFO0FBQ0FOLE1BQUFBLENBQUMsQ0FBQyxlQUFlZ0csT0FBTyxDQUFDeEUsR0FBeEIsQ0FBRCxDQUE4Qm5CLElBQTlCLENBQW1DMkYsT0FBTyxDQUFDRSxPQUEzQyxFQUFvRDVGLEdBQXBELENBQXdELFNBQXhELEVBQW1FLENBQW5FO0FBQ0g7QUFDSixHQVZEO0FBV0gsQyxDQUVEOztBQUVPLFNBQVNrRyxPQUFULEdBQW1CO0FBRXRCM0gsRUFBQUEsUUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDVSxLQUFyQyxDQUEyQ2lILEtBQTNDLEdBQW1ELE1BQW5EO0FBQ0F6RyxFQUFBQSxDQUFDLENBQUMsTUFBRCxDQUFELENBQVVNLEdBQVYsQ0FBYyxZQUFkLEVBQTRCLFFBQTVCO0FBQ0g7QUFFTSxTQUFTb0csUUFBVCxHQUFvQjtBQUV2QjdILEVBQUFBLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixXQUF4QixFQUFxQ1UsS0FBckMsQ0FBMkNpSCxLQUEzQyxHQUFtRCxHQUFuRDtBQUNBekcsRUFBQUEsQ0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVTSxHQUFWLENBQWMsWUFBZCxFQUE0QixNQUE1QjtBQUNILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Q0MxakJEOztBQUNPLElBQU1zRyxHQUFHLEdBQUdELDBFQUFnQixDQUFDRSwwSUFBRCxDQUE1QixDLENBTVA7QUFDQSxnRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU1uSSxHQUFHLEdBQUcsU0FBTkEsR0FBTSxHQUFNO0FBQ2hCLGtCQUFnQ3lJLGdEQUFRLEVBQXhDO0FBQUE7QUFBQSxNQUFPYixRQUFQO0FBQUEsTUFBaUJ3QixXQUFqQjs7QUFDQSxtQkFBOEJYLGdEQUFRLENBQUMsSUFBRCxDQUF0QztBQUFBO0FBQUEsTUFBT1ksT0FBUDtBQUFBLE1BQWdCQyxVQUFoQjs7QUFDQSxtQkFBc0NiLGdEQUFRLENBQUMsSUFBRCxDQUE5QztBQUFBO0FBQUEsTUFBT2MsV0FBUDtBQUFBLE1BQW9CQyxjQUFwQjs7QUFDQSxtQkFBd0JmLGdEQUFRLEVBQWhDO0FBQUE7QUFBQSxNQUFPZ0IsSUFBUDtBQUFBLE1BQWFDLE9BQWI7O0FBQ0EsbUJBQTRCakIsZ0RBQVEsQ0FBQyxLQUFELENBQXBDO0FBQUE7QUFBQSxNQUFPa0IsTUFBUDtBQUFBLE1BQWVDLFNBQWY7O0FBRUEsV0FBU0MsUUFBVCxDQUFrQkMsT0FBbEIsRUFBMkI7QUFDekJGLElBQUFBLFNBQVMsQ0FBQ0UsT0FBRCxDQUFUO0FBQ0Q7O0FBRURwQixFQUFBQSxpREFBUyxDQUFDLFlBQU07QUFDZHpJLElBQUFBLGlEQUFBLENBQVUsdUJBQVYsRUFBbUNnRCxJQUFuQyxDQUF3QyxVQUFDOEcsUUFBRCxFQUFjO0FBQ3BEWCxNQUFBQSxXQUFXLENBQUNXLFFBQVEsQ0FBQy9ILElBQVYsQ0FBWDtBQUNBc0gsTUFBQUEsVUFBVSxDQUFDLEtBQUQsQ0FBVjtBQUNELEtBSEQ7QUFLQSxXQUFPLFlBQU07QUFDWEEsTUFBQUEsVUFBVTtBQUNWRixNQUFBQSxXQUFXO0FBQ1hNLE1BQUFBLE9BQU87QUFDUixLQUpEO0FBS0QsR0FYUSxFQVdOLEVBWE0sQ0FBVDtBQWFBaEIsRUFBQUEsaURBQVMsQ0FBQyxZQUFNO0FBQ2R6SSxJQUFBQSxpREFBQSxDQUFVLGVBQVYsRUFBMkJnRCxJQUEzQixDQUFnQyxVQUFDOEcsUUFBRCxFQUFjO0FBQzVDTCxNQUFBQSxPQUFPLENBQUNLLFFBQVEsQ0FBQy9ILElBQVYsQ0FBUDtBQUNBd0gsTUFBQUEsY0FBYyxDQUFDLEtBQUQsQ0FBZDs7QUFFQSxVQUFJTyxRQUFRLENBQUMvSCxJQUFULENBQWNDLEtBQWQsQ0FBb0J6QixNQUFwQixHQUE2QixDQUFqQyxFQUFvQztBQUNsQ2MsUUFBQUEsQ0FBQyxDQUFDLHdCQUFELENBQUQsQ0FBNEJZLFdBQTVCLENBQXdDLFdBQXhDO0FBRUFaLFFBQUFBLENBQUMsQ0FBQyw0QkFBRCxDQUFELENBQWdDSyxJQUFoQyxDQUFxQ29JLFFBQVEsQ0FBQy9ILElBQVQsQ0FBY0MsS0FBZCxDQUFvQnpCLE1BQXpEO0FBRUFjLFFBQUFBLENBQUMsQ0FBQyxzQkFBRCxDQUFELENBQTBCYSxJQUExQixDQUNFNEgsUUFBUSxDQUFDL0gsSUFBVCxDQUFjSSxLQUFkLENBQW9CQyxPQUFwQixDQUE0QixDQUE1QixJQUFpQyxjQURuQztBQUdELE9BUkQsTUFRTztBQUNMZixRQUFBQSxDQUFDLENBQUMsd0JBQUQsQ0FBRCxDQUE0QmdCLFFBQTVCLENBQXFDLFdBQXJDO0FBQ0Q7QUFDRixLQWZEO0FBaUJBLFdBQ0UsWUFBTTtBQUNKZ0gsTUFBQUEsVUFBVTtBQUNWRSxNQUFBQSxjQUFjO0FBQ2RJLE1BQUFBLFNBQVM7QUFDVEYsTUFBQUEsT0FBTztBQUNSLEtBTEQsRUFNQSxDQUFDQyxNQUFELENBUEY7QUFTRCxHQTNCUSxDQUFUO0FBNkJBLHNCQUNFLGtEQUFDLDREQUFELHFCQUNFLGtEQUFDLHFEQUFELHFCQUNFLGtEQUFDLG9EQUFEO0FBQU8sU0FBSyxNQUFaO0FBQWEsUUFBSSxFQUFFLENBQUMsR0FBRCxFQUFNLFdBQU4sRUFBbUIsV0FBbkIsRUFBZ0MsT0FBaEM7QUFBbkIsa0JBQ0Usa0RBQUMsOENBQUQ7QUFBUyxRQUFJLEVBQUVGO0FBQWYsa0JBQ0Usa0RBQUMscURBQUQscUJBQ0Usa0RBQUMsb0RBQUQ7QUFBTyxTQUFLLE1BQVo7QUFBYSxRQUFJLEVBQUM7QUFBbEIsa0JBQ0Usa0RBQUMsMkNBQUQ7QUFBTSxZQUFRLEVBQUU3QixRQUFoQjtBQUEwQixXQUFPLEVBQUV5QjtBQUFuQyxJQURGLENBREYsZUFJRSxrREFBQyxvREFBRDtBQUFPLFNBQUssTUFBWjtBQUFhLFFBQUksRUFBQztBQUFsQixrQkFDRSxrREFBQywrQ0FBRDtBQUFVLFlBQVEsRUFBRXpCLFFBQXBCO0FBQThCLFdBQU8sRUFBRXlCO0FBQXZDLElBREYsQ0FKRixlQU9FLGtEQUFDLG9EQUFEO0FBQU8sU0FBSyxNQUFaO0FBQWEsUUFBSSxFQUFDO0FBQWxCLGtCQUNFLGtEQUFDLDJDQUFEO0FBQU0sUUFBSSxFQUFFSSxJQUFaO0FBQWtCLFdBQU8sRUFBRUYsV0FBM0I7QUFBd0MsWUFBUSxFQUFFTTtBQUFsRCxJQURGLENBUEYsZUFVRSxrREFBQyxvREFBRDtBQUFPLFNBQUssTUFBWjtBQUFhLFFBQUksRUFBQztBQUFsQixrQkFDRSxrREFBQyw4Q0FBRDtBQUFTLFlBQVEsRUFBRUE7QUFBbkIsSUFERixDQVZGLGVBYUUsa0RBQUMsb0RBQUQ7QUFBTyxRQUFJLEVBQUMsR0FBWjtBQUFnQixhQUFTLEVBQUVWLCtDQUFRQTtBQUFuQyxJQWJGLENBREYsQ0FERixDQURGLGVBb0JFLGtEQUFDLG9EQUFEO0FBQU8sUUFBSSxFQUFFLENBQUMsb0JBQUQsRUFBdUIsd0JBQXZCO0FBQWIsa0JBQ0Usa0RBQUMsOENBQUQscUJBQ0Usa0RBQUMscURBQUQscUJBQ0Usa0RBQUMsb0RBQUQ7QUFBTyxTQUFLLE1BQVo7QUFBYSxRQUFJLEVBQUM7QUFBbEIsa0JBQ0Usa0RBQUMsK0NBQUQ7QUFBVSxRQUFJLEVBQUVNLElBQWhCO0FBQXNCLFdBQU8sRUFBRUY7QUFBL0IsSUFERixDQURGLGVBSUUsa0RBQUMsb0RBQUQ7QUFDRSxTQUFLLE1BRFA7QUFFRSxRQUFJLEVBQUMsd0JBRlA7QUFHRSxhQUFTLEVBQUVULG1EQUFZQTtBQUh6QixJQUpGLENBREYsQ0FERixDQXBCRixDQURGLENBREY7QUF1Q0QsQ0E1RkQ7O0FBNkZBLGlFQUFlOUksR0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxR0E7QUFDQTtBQUNBO0FBT0E7QUFDQTs7QUFFQSxJQUFNNkksSUFBSSxHQUFHLFNBQVBBLElBQU8sT0FBaUM7QUFBQSxNQUE5QlksSUFBOEIsUUFBOUJBLElBQThCO0FBQUEsTUFBeEJKLE9BQXdCLFFBQXhCQSxPQUF3QjtBQUFBLE1BQWZRLFFBQWUsUUFBZkEsUUFBZTtBQUM1QyxNQUFJTyxPQUFPLEdBQUdILDREQUFVLEVBQXhCO0FBRUEsU0FBT1osT0FBTyxnQkFDWixpREFBQyw2Q0FBRCxPQURZLEdBRVZJLElBQUksQ0FBQ3hILEtBQUwsQ0FBV3pCLE1BQVgsS0FBc0IsQ0FBdEIsZ0JBQ0YsaURBQUMsK0NBQUQsT0FERSxnQkFHRiwrREFDR2lKLElBQUksQ0FBQ3JILEtBQUwsR0FBYSxFQUFiLGdCQUNDO0FBQ0UsYUFBUyxFQUFDLDBGQURaO0FBRUUsU0FBSyxFQUFFO0FBQ0xpSSxNQUFBQSxRQUFRLEVBQUUsTUFETDtBQUVMQyxNQUFBQSxNQUFNLEVBQUUsbUJBRkg7QUFHTEMsTUFBQUEsUUFBUSxFQUFFO0FBSEw7QUFGVCxrQkFRRTtBQUNFLFNBQUssRUFBRTtBQUNMQyxNQUFBQSxLQUFLLEVBQUUsU0FERjtBQUVMRCxNQUFBQSxRQUFRLEVBQUUsVUFGTDtBQUdMRSxNQUFBQSxJQUFJLEVBQUUsTUFIRDtBQUlMQyxNQUFBQSxHQUFHLEVBQUU7QUFKQSxLQURUO0FBT0UsU0FBSyxFQUFDLDRCQVBSO0FBUUUsU0FBSyxFQUFDLElBUlI7QUFTRSxVQUFNLEVBQUMsSUFUVDtBQVVFLFFBQUksRUFBQyxjQVZQO0FBV0UsYUFBUyxFQUFDLDRCQVhaO0FBWUUsV0FBTyxFQUFDO0FBWlYsa0JBY0U7QUFBTSxLQUFDLEVBQUM7QUFBUixJQWRGLGVBZUU7QUFBTSxLQUFDLEVBQUM7QUFBUixJQWZGLENBUkYsZUF5QkU7QUFDRSxTQUFLLEVBQUU7QUFBRUYsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FEVDtBQUVFLFNBQUssRUFBQyw0QkFGUjtBQUdFLFNBQUssRUFBQyxJQUhSO0FBSUUsVUFBTSxFQUFDLElBSlQ7QUFLRSxRQUFJLEVBQUMsY0FMUDtBQU1FLGFBQVMsRUFBQyxtQkFOWjtBQU9FLFdBQU8sRUFBQztBQVBWLGtCQVNFO0FBQVEsTUFBRSxFQUFDLEdBQVg7QUFBZSxNQUFFLEVBQUMsR0FBbEI7QUFBc0IsS0FBQyxFQUFDO0FBQXhCLElBVEYsQ0F6QkYsZUFvQ0U7QUFDRSxhQUFTLEVBQUMsTUFEWjtBQUVFLFNBQUssRUFBRTtBQUNMQSxNQUFBQSxLQUFLLEVBQUUsU0FERjtBQUVMRCxNQUFBQSxRQUFRLEVBQUUsVUFGTDtBQUdMRSxNQUFBQSxJQUFJLEVBQUUsTUFIRDtBQUlMRSxNQUFBQSxVQUFVLEVBQUU7QUFKUDtBQUZULG1EQXBDRixlQStDRSxpREFBQyxrREFBRDtBQUNFLE1BQUUsRUFBQyxXQURMO0FBRUUsU0FBSyxFQUFFO0FBQUVDLE1BQUFBLE1BQU0sRUFBRTtBQUFWLEtBRlQ7QUFHRSxXQUFPLEVBQUUsbUJBQU07QUFDYnpLLE1BQUFBLFFBQVEsQ0FDTEcsc0JBREgsQ0FDMEIsMEJBRDFCLEVBQ3NELENBRHRELEVBRUdJLFNBRkgsQ0FFYTBELEdBRmIsQ0FFaUIsUUFGakI7QUFHRDtBQVBILGtCQVNFO0FBQ0UsU0FBSyxFQUFFO0FBQUVvRyxNQUFBQSxLQUFLLEVBQUU7QUFBVCxLQURUO0FBRUUsU0FBSyxFQUFDLDRCQUZSO0FBR0UsU0FBSyxFQUFDLElBSFI7QUFJRSxVQUFNLEVBQUMsSUFKVDtBQUtFLFFBQUksRUFBQyxjQUxQO0FBTUUsYUFBUyxFQUFDLFNBTlo7QUFPRSxXQUFPLEVBQUM7QUFQVixrQkFTRTtBQUFNLEtBQUMsRUFBQztBQUFSLElBVEYsQ0FURixDQS9DRixDQURELEdBc0VHLElBdkVOLGVBd0VFO0FBQ0UsYUFBUyxFQUFDLHdCQURaO0FBRUUsU0FBSyxFQUFFO0FBQUVILE1BQUFBLFFBQVEsRUFBRSxNQUFaO0FBQW9CRyxNQUFBQSxLQUFLLEVBQUU7QUFBM0I7QUFGVCxrQkFJRTtBQUNFLGFBQVMsRUFBQyx3Q0FEWjtBQUVFLFNBQUssRUFBRTtBQUFFSyxNQUFBQSxNQUFNLEVBQUU7QUFBVjtBQUZULGtCQUlFO0FBQUksYUFBUyxFQUFDO0FBQWQsZ0JBSkYsZUFPRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxhQUFTLEVBQUMsK0JBRFo7QUFFRSxTQUFLLEVBQUU7QUFBRUMsTUFBQUEsZUFBZSxFQUFFLFdBQW5CO0FBQWdDRCxNQUFBQSxNQUFNLEVBQUU7QUFBeEM7QUFGVCxrQkFJRTtBQUFLLFNBQUssRUFBRTtBQUFFOUMsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FBWjtBQUE4QixhQUFTLEVBQUM7QUFBeEMsYUFKRixlQU9FO0FBQUssU0FBSyxFQUFFO0FBQUVBLE1BQUFBLEtBQUssRUFBRTtBQUFULEtBQVo7QUFBOEIsYUFBUyxFQUFDO0FBQXhDLHVCQVBGLGVBVUU7QUFBSyxTQUFLLEVBQUU7QUFBRUEsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FBWjtBQUE4QixhQUFTLEVBQUM7QUFBeEMsY0FWRixlQWFFO0FBQUssU0FBSyxFQUFFO0FBQUVBLE1BQUFBLEtBQUssRUFBRTtBQUFULEtBQVo7QUFBOEIsYUFBUyxFQUFDO0FBQXhDLHFDQWJGLENBREYsRUFrQkcwQixJQUFJLENBQUN4SCxLQUFMLENBQVc0RixHQUFYLENBQWUsVUFBQ2tELElBQUQsRUFBT3ZKLEtBQVA7QUFBQSx3QkFDZDtBQUNFLGVBQVMsRUFBQyxrQ0FEWjtBQUVFLFdBQUssRUFBRTtBQUFFd0osUUFBQUEsWUFBWSxFQUFFO0FBQWhCLE9BRlQ7QUFHRSxTQUFHLEVBQUV4SjtBQUhQLG9CQUtFO0FBQ0UsV0FBSyxFQUFFO0FBQUV1RyxRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQURUO0FBRUUsZUFBUyxFQUFDO0FBRlosb0JBSUUsaURBQUMsa0RBQUQ7QUFDRSxRQUFFLEVBQUU7QUFBRWhELFFBQUFBLFFBQVEsRUFBRWdHLElBQUksQ0FBQ3pELE9BQUwsQ0FBYXhFLEdBQXpCO0FBQThCbUksUUFBQUEsS0FBSyxFQUFFRixJQUFJLENBQUN6RDtBQUExQztBQUROLG9CQUdFO0FBQ0UsV0FBSyxFQUFFO0FBQUVTLFFBQUFBLEtBQUssRUFBRSxNQUFUO0FBQWlCOEMsUUFBQUEsTUFBTSxFQUFFO0FBQXpCLE9BRFQ7QUFFRSxTQUFHLEVBQUVFLElBQUksQ0FBQ3pELE9BQUwsQ0FBYTRELFFBRnBCO0FBR0UsU0FBRyxFQUFDO0FBSE4sTUFIRixDQUpGLGVBYUU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRSxpREFBQyxrREFBRDtBQUNFLFFBQUUsRUFBRTtBQUFFbkcsUUFBQUEsUUFBUSxFQUFFZ0csSUFBSSxDQUFDekQsT0FBTCxDQUFheEUsR0FBekI7QUFBOEJtSSxRQUFBQSxLQUFLLEVBQUVGLElBQUksQ0FBQ3pEO0FBQTFDLE9BRE47QUFFRSxXQUFLLEVBQUU7QUFBRTZELFFBQUFBLGNBQWMsRUFBRSxNQUFsQjtBQUEwQlgsUUFBQUEsS0FBSyxFQUFFO0FBQWpDO0FBRlQsb0JBSUUsOERBQU1PLElBQUksQ0FBQ3pELE9BQUwsQ0FBYThELEtBQW5CLENBSkYsZUFLRTtBQUFLLFdBQUssRUFBRTtBQUFFWixRQUFBQSxLQUFLLEVBQUUsV0FBVDtBQUFzQkgsUUFBQUEsUUFBUSxFQUFFO0FBQWhDO0FBQVosT0FDR1UsSUFBSSxDQUFDekQsT0FBTCxDQUFhNEQsUUFEaEIsQ0FMRixDQURGLENBYkYsQ0FMRixlQThCRTtBQUFLLFdBQUssRUFBRTtBQUFFbkQsUUFBQUEsS0FBSyxFQUFFO0FBQVQsT0FBWjtBQUE4QixlQUFTLEVBQUM7QUFBeEMsb0JBQ0UsOERBQ0csQ0FBQ2dELElBQUksQ0FBQ3pELE9BQUwsQ0FBYUksS0FBYixJQUFzQixJQUFJcUQsSUFBSSxDQUFDekQsT0FBTCxDQUFhRyxHQUF2QyxDQUFELEVBQThDcEYsT0FBOUMsQ0FBc0QsQ0FBdEQsQ0FESCxFQUM2RCxHQUQ3RCxXQURGLGVBS0U7QUFBSyxXQUFLLEVBQUU7QUFBRW1JLFFBQUFBLEtBQUssRUFBRSxXQUFUO0FBQXNCSCxRQUFBQSxRQUFRLEVBQUU7QUFBaEM7QUFBWix5QkFMRixlQVFFLDhEQUFNVSxJQUFJLENBQUN6RCxPQUFMLENBQWFJLEtBQW5CLFlBUkYsZUFTRTtBQUFLLFdBQUssRUFBRTtBQUFFOEMsUUFBQUEsS0FBSyxFQUFFLFdBQVQ7QUFBc0JILFFBQUFBLFFBQVEsRUFBRTtBQUFoQztBQUFaLDJCQVRGLENBOUJGLGVBMkNFO0FBQUssV0FBSyxFQUFFO0FBQUV0QyxRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQUFaO0FBQThCLGVBQVMsRUFBQztBQUF4QyxvQkFDRTtBQUFNLGVBQVMsRUFBQztBQUFoQixvQkFDRTtBQUNFLGVBQVMsRUFBQyxXQURaO0FBRUUsVUFBSSxFQUFDLEtBRlA7QUFHRSxVQUFJLEVBQUMsUUFIUDtBQUlFLGtCQUFZLEVBQUVnRCxJQUFJLENBQUN6RCxPQUFMLENBQWF4RTtBQUo3QixNQURGLGVBT0U7QUFDRSxlQUFTLEVBQUMsaUNBRFo7QUFFRSxXQUFLLEVBQUU7QUFBRXVJLFFBQUFBLFdBQVcsRUFBRSxNQUFmO0FBQXVCVixRQUFBQSxVQUFVLEVBQUU7QUFBbkMsT0FGVDtBQUdFLGFBQU8sRUFBRSxtQkFBTTtBQUNidEgsUUFBQUEsOENBQVE7QUFDUmQsUUFBQUEsNERBQXNCLEtBQUtNLG9EQUFjLEVBQW5CLEdBQXdCLElBQTlDO0FBQ0FnSCxRQUFBQSxRQUFRLENBQUMsSUFBRCxDQUFSO0FBQ0Q7QUFQSCxXQVBGLGVBa0JFO0FBQ0UsVUFBSSxFQUFDLFVBRFA7QUFFRSxVQUFJLEVBQUMsUUFGUDtBQUdFLGtCQUFZLEVBQUVrQixJQUFJLENBQUMvSCxRQUhyQjtBQUlFLGVBQVMsRUFBQyxrREFKWjtBQUtFLFdBQUssRUFBRTtBQUFFcUgsUUFBQUEsUUFBUSxFQUFFO0FBQVo7QUFMVCxNQWxCRixlQXlCRTtBQUNFLGVBQVMsRUFBQyxpQ0FEWjtBQUVFLFdBQUssRUFBRTtBQUFFaUIsUUFBQUEsVUFBVSxFQUFFLE1BQWQ7QUFBc0JYLFFBQUFBLFVBQVUsRUFBRTtBQUFsQyxPQUZUO0FBR0UsYUFBTyxFQUFFLG1CQUFNO0FBQ2J0SCxRQUFBQSw4Q0FBUTtBQUNSVCxRQUFBQSw4REFBd0IsS0FBS0Msb0RBQWMsRUFBbkIsR0FBd0IsSUFBaEQ7QUFDQWdILFFBQUFBLFFBQVEsQ0FBQyxJQUFELENBQVI7QUFDRDtBQVBILFdBekJGLGVBb0NFO0FBQ0UsV0FBSyxFQUFFO0FBQUU5QixRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQURUO0FBRUUsZUFBUyxFQUFDO0FBRlosb0JBSUU7QUFDRSxXQUFLLEVBQUU7QUFBRXlDLFFBQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CSSxRQUFBQSxNQUFNLEVBQUU7QUFBNUIsT0FEVDtBQUVFLFdBQUssRUFBQyw0QkFGUjtBQUdFLFdBQUssRUFBQyxJQUhSO0FBSUUsWUFBTSxFQUFDLElBSlQ7QUFLRSxVQUFJLEVBQUMsY0FMUDtBQU1FLGVBQVMsRUFBQyxhQU5aO0FBT0UsYUFBTyxFQUFDLFdBUFY7QUFRRSxhQUFPLEVBQUUsbUJBQU07QUFDYnZILFFBQUFBLDhDQUFRO0FBQ1JDLFFBQUFBLGdEQUFVLENBQUN5SCxJQUFJLENBQUN6RCxPQUFMLENBQWF4RSxHQUFkLENBQVY7QUFDQStHLFFBQUFBLFFBQVEsQ0FBQyxJQUFELENBQVI7QUFDRDtBQVpILG9CQWNFO0FBQU0sT0FBQyxFQUFDO0FBQVIsTUFkRixlQWVFO0FBQ0UsY0FBUSxFQUFDLFNBRFg7QUFFRSxPQUFDLEVBQUM7QUFGSixNQWZGLENBSkYsQ0FwQ0YsQ0FERixlQStERTtBQUNFLFdBQUssRUFBRTtBQUNMUSxRQUFBQSxRQUFRLEVBQUUsTUFETDtBQUVMUSxRQUFBQSxNQUFNLEVBQUUsTUFGSDtBQUdMOUMsUUFBQUEsS0FBSyxFQUFFLE9BSEY7QUFJTCtDLFFBQUFBLGVBQWUsRUFBRSxXQUpaO0FBS0xOLFFBQUFBLEtBQUssRUFBRTtBQUxGLE9BRFQ7QUFRRSxlQUFTLEVBQUM7QUFSWixvQkFVRSwrRkFWRixlQVdFLHFGQVhGLENBL0RGLENBM0NGLGVBd0hFO0FBQUssV0FBSyxFQUFFO0FBQUV6QyxRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQUFaO0FBQThCLGVBQVMsRUFBQztBQUF4QyxvQkFDRSw4REFDRyxDQUFDZ0QsSUFBSSxDQUFDM0ksS0FBTCxJQUFjLElBQUkySSxJQUFJLENBQUN6RCxPQUFMLENBQWFHLEdBQS9CLENBQUQsRUFBc0NwRixPQUF0QyxDQUE4QyxDQUE5QyxDQURILFlBREYsZUFJRTtBQUFLLFdBQUssRUFBRTtBQUFFbUksUUFBQUEsS0FBSyxFQUFFLFdBQVQ7QUFBc0JILFFBQUFBLFFBQVEsRUFBRTtBQUFoQztBQUFaLHlCQUpGLGVBT0UsOERBQU1VLElBQUksQ0FBQzNJLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQixDQUFuQixDQUFOLFlBUEYsZUFRRTtBQUFLLFdBQUssRUFBRTtBQUFFbUksUUFBQUEsS0FBSyxFQUFFLFdBQVQ7QUFBc0JILFFBQUFBLFFBQVEsRUFBRTtBQUFoQztBQUFaLDJCQVJGLENBeEhGLENBRGM7QUFBQSxHQUFmLENBbEJILENBREYsZUEySkU7QUFDRSxhQUFTLEVBQUMsK0NBRFo7QUFFRSxTQUFLLEVBQUU7QUFBRUMsTUFBQUEsTUFBTSxFQUFFO0FBQVY7QUFGVCxrQkFJRTtBQUNFLGFBQVMsRUFBQyxNQURaO0FBRUUsU0FBSyxFQUFFO0FBQUVFLE1BQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CZSxNQUFBQSxVQUFVLEVBQUU7QUFBaEM7QUFGVCxrQ0FKRixlQVVFLDRFQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xDLE1BQUFBLFNBQVMsRUFBRSxNQUROO0FBRUxDLE1BQUFBLFlBQVksRUFBRSxHQUZUO0FBR0xwQixNQUFBQSxRQUFRLEVBQUUsTUFITDtBQUlMRyxNQUFBQSxLQUFLLEVBQUU7QUFKRixLQURUO0FBT0UsYUFBUyxFQUFDLHNCQVBaO0FBUUUsUUFBSSxFQUFDLE1BUlA7QUFTRSxnQkFBWSxFQUFDO0FBVGYsSUFERixlQVlFO0FBQ0UsYUFBUyxFQUFDLDhCQURaO0FBRUUsU0FBSyxFQUFFO0FBQ0xGLE1BQUFBLE1BQU0sRUFBRSxNQURIO0FBRUxvQixNQUFBQSxTQUFTLEVBQUUsUUFGTjtBQUdMZCxNQUFBQSxNQUFNLEVBQUU7QUFISDtBQUZULDBCQVpGLENBVkYsQ0EzSkYsQ0FQRixDQUpGLGVBME1FO0FBQ0UsU0FBSyxFQUFFO0FBQUVKLE1BQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CSyxNQUFBQSxNQUFNLEVBQUU7QUFBNUIsS0FEVDtBQUVFLGFBQVMsRUFBQztBQUZaLGtCQUlFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBSkYsZUFLRSw0REFMRixlQU1FO0FBQU8sYUFBUyxFQUFDO0FBQWpCLGtCQUNFLDZFQUNFLDBFQUNFLDZGQURGLGVBRUUsNkRBQUssQ0FBQ3BCLElBQUksQ0FBQ3JILEtBQUwsR0FBYXFILElBQUksQ0FBQ2tDLFFBQW5CLEVBQTZCdEosT0FBN0IsQ0FBcUMsQ0FBckMsQ0FBTCxZQUZGLENBREYsZUFLRSwwRUFDRSw4RkFERixlQUVFLDZEQUFLb0gsSUFBSSxDQUFDckgsS0FBTCxDQUFXQyxPQUFYLENBQW1CLENBQW5CLENBQUwsWUFGRixDQUxGLGVBU0UsMEVBQ0UscUVBREYsZUFFRSw2REFBS29ILElBQUksQ0FBQ2tDLFFBQUwsQ0FBY3RKLE9BQWQsQ0FBc0IsQ0FBdEIsQ0FBTCxZQUZGLENBVEYsZUFhRSwwRUFDRSw2RUFERixlQUVFLHdFQUZGLENBYkYsZUFpQkU7QUFDRSxTQUFLLEVBQUU7QUFBRXVKLE1BQUFBLFNBQVMsRUFBRSxxQkFBYjtBQUFvQ0wsTUFBQUEsVUFBVSxFQUFFO0FBQWhEO0FBRFQsa0JBR0UsOEZBSEYsZUFJRSw2REFBSzlCLElBQUksQ0FBQ3JILEtBQUwsQ0FBV0MsT0FBWCxDQUFtQixDQUFuQixDQUFMLFlBSkYsQ0FqQkYsZUF1QkU7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRSw0REFERixlQUVFLDZEQUNHb0gsSUFBSSxDQUFDckgsS0FBTCxHQUFhLEVBQWIsZ0JBQ0M7QUFDRSxTQUFLLEVBQUU7QUFBRXlKLE1BQUFBLE9BQU8sRUFBRSxLQUFYO0FBQWtCakIsTUFBQUEsTUFBTSxFQUFFO0FBQTFCLEtBRFQ7QUFFRSxhQUFTLEVBQUM7QUFGWix1QkFERCxnQkFRQztBQUNFLFdBQU8sRUFBRTtBQUFBLGFBQ1BSLE9BQU8sQ0FBQzBCLElBQVIsQ0FBYSxvQkFBYixDQURPO0FBQUEsS0FEWDtBQUlFLFNBQUssRUFBRTtBQUFFbEIsTUFBQUEsTUFBTSxFQUFFLFNBQVY7QUFBcUJjLE1BQUFBLFNBQVMsRUFBRTtBQUFoQyxLQUpUO0FBS0UsYUFBUyxFQUFDO0FBTFosdUJBVEosQ0FGRixDQXZCRixDQURGLENBTkYsRUF3REdqQyxJQUFJLENBQUNySCxLQUFMLEdBQWEsRUFBYixnQkFDQztBQUNFLFNBQUssRUFBRTtBQUFFd0ksTUFBQUEsTUFBTSxFQUFFLFNBQVY7QUFBcUJpQixNQUFBQSxPQUFPLEVBQUU7QUFBOUIsS0FEVDtBQUVFLGFBQVMsRUFBQztBQUZaLHVCQURELGdCQVFDO0FBQ0UsV0FBTyxFQUFFO0FBQUEsYUFDUHpCLE9BQU8sQ0FBQzBCLElBQVIsQ0FBYSxvQkFBYixDQURPO0FBQUEsS0FEWDtBQUlFLFNBQUssRUFBRTtBQUFFbEIsTUFBQUEsTUFBTSxFQUFFLFNBQVY7QUFBcUJjLE1BQUFBLFNBQVMsRUFBRTtBQUFoQyxLQUpUO0FBS0UsYUFBUyxFQUFDO0FBTFosdUJBaEVKLENBMU1GLENBeEVGLE1BTEY7QUFzV0QsQ0F6V0Q7O0FBMFdBLGlFQUFlN0MsSUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdFhBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxHQUFNO0FBQ3pCLE1BQU1oRSxRQUFRLEdBQUdpSCw2REFBVyxFQUE1QjtBQUNBLE1BQU1DLFlBQVksR0FBR2xILFFBQVEsQ0FBQ21HLEtBQTlCO0FBRUF2QyxFQUFBQSxnREFBUyxDQUFDO0FBQUEsV0FBTXpJLGdEQUFBLENBQVUsaUJBQVYsQ0FBTjtBQUFBLEdBQUQsRUFBcUMsRUFBckMsQ0FBVDtBQUVBLHNCQUNFLDRFQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLFNBQUssRUFBRTtBQUFFc0ssTUFBQUEsUUFBUSxFQUFFO0FBQVosS0FBWjtBQUFzQyxhQUFTLEVBQUM7QUFBaEQsa0JBQ0U7QUFDRSxTQUFLLEVBQUU7QUFBRUMsTUFBQUEsS0FBSyxFQUFFO0FBQVQsS0FEVDtBQUVFLFNBQUssRUFBQyw0QkFGUjtBQUdFLFNBQUssRUFBQyxJQUhSO0FBSUUsVUFBTSxFQUFDLElBSlQ7QUFLRSxRQUFJLEVBQUMsY0FMUDtBQU1FLGFBQVMsRUFBQyxjQU5aO0FBT0UsV0FBTyxFQUFDO0FBUFYsa0JBU0U7QUFBTSxLQUFDLEVBQUM7QUFBUixJQVRGLENBREYsZUFZRTtBQUNFLFNBQUssRUFBRTtBQUNMQSxNQUFBQSxLQUFLLEVBQUUsU0FERjtBQUVMRCxNQUFBQSxRQUFRLEVBQUUsVUFGTDtBQUdMRSxNQUFBQSxJQUFJLEVBQUUsS0FIRDtBQUlMQyxNQUFBQSxHQUFHLEVBQUUsS0FKQTtBQUtMTCxNQUFBQSxRQUFRLEVBQUU7QUFMTDtBQURULFNBWkYsZUF1QkU7QUFDRSxTQUFLLEVBQUU7QUFDTEEsTUFBQUEsUUFBUSxFQUFFLE1BREw7QUFFTEcsTUFBQUEsS0FBSyxFQUFFLFNBRkY7QUFHTEssTUFBQUEsTUFBTSxFQUFFLE1BSEg7QUFJTEYsTUFBQUEsVUFBVSxFQUFFO0FBSlAsS0FEVDtBQU9FLGFBQVMsRUFBQztBQVBaLHFCQXZCRixDQURGLGVBb0NFO0FBQ0UsYUFBUyxFQUFDLCtCQURaO0FBRUUsU0FBSyxFQUFFO0FBQUVFLE1BQUFBLE1BQU0sRUFBRSxNQUFWO0FBQWtCRixNQUFBQSxVQUFVLEVBQUU7QUFBOUI7QUFGVCxrQkFJRTtBQUFLLE9BQUcsRUFBQyxlQUFUO0FBQXlCLE9BQUcsRUFBQztBQUE3QixJQUpGLENBcENGLGVBMENFO0FBQUssU0FBSyxFQUFFO0FBQUVKLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQVo7QUFBc0MsYUFBUyxFQUFDO0FBQWhELGtCQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQUVDLE1BQUFBLEtBQUssRUFBRTtBQUFULEtBRFQ7QUFFRSxTQUFLLEVBQUMsNEJBRlI7QUFHRSxTQUFLLEVBQUMsSUFIUjtBQUlFLFVBQU0sRUFBQyxJQUpUO0FBS0UsUUFBSSxFQUFDLGNBTFA7QUFNRSxhQUFTLEVBQUMsbUJBTlo7QUFPRSxXQUFPLEVBQUM7QUFQVixrQkFTRTtBQUFRLE1BQUUsRUFBQyxHQUFYO0FBQWUsTUFBRSxFQUFDLEdBQWxCO0FBQXNCLEtBQUMsRUFBQztBQUF4QixJQVRGLENBREYsZUFZRTtBQUNFLFNBQUssRUFBRTtBQUNMQSxNQUFBQSxLQUFLLEVBQUUsT0FERjtBQUVMRCxNQUFBQSxRQUFRLEVBQUUsVUFGTDtBQUdMRSxNQUFBQSxJQUFJLEVBQUUsS0FIRDtBQUlMQyxNQUFBQSxHQUFHLEVBQUUsS0FKQTtBQUtMTCxNQUFBQSxRQUFRLEVBQUU7QUFMTDtBQURULFNBWkYsZUF1QkU7QUFDRSxTQUFLLEVBQUU7QUFDTEEsTUFBQUEsUUFBUSxFQUFFLE1BREw7QUFFTEcsTUFBQUEsS0FBSyxFQUFFLFNBRkY7QUFHTEssTUFBQUEsTUFBTSxFQUFFLE1BSEg7QUFJTEYsTUFBQUEsVUFBVSxFQUFFO0FBSlAsS0FEVDtBQU9FLGFBQVMsRUFBQztBQVBaLHVCQXZCRixDQTFDRixDQURGLENBREYsZUFpRkU7QUFBSyxhQUFTLEVBQUMsZUFBZjtBQUErQixTQUFLLEVBQUU7QUFBRXNCLE1BQUFBLGFBQWEsRUFBRTtBQUFqQjtBQUF0QyxrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLDBFQURGLGVBS0U7QUFBSyxTQUFLLEVBQUU7QUFBRVgsTUFBQUEsVUFBVSxFQUFFO0FBQWQ7QUFBWixLQUNHVSxZQUFZLENBQUNFLE9BQWIsZ0JBQXVCLDhEQUFNRixZQUFZLENBQUNFLE9BQW5CLENBQXZCLEdBQTJELElBRDlELGVBR0UsOERBQ0dGLFlBQVksQ0FBQ0csU0FEaEIsT0FDNEJILFlBQVksQ0FBQ0ksUUFEekMsQ0FIRixlQU1FLDhEQUFNSixZQUFZLENBQUNLLGNBQW5CLENBTkYsRUFRR0wsWUFBWSxDQUFDTSxjQUFiLGdCQUNDLDhEQUFNTixZQUFZLENBQUNNLGNBQW5CLENBREQsR0FFRyxJQVZOLGVBWUUsOERBQU1OLFlBQVksQ0FBQ08sT0FBbkIsQ0FaRixlQWFFLDhEQUFNUCxZQUFZLENBQUNRLElBQW5CLENBYkYsZUFjRSw4REFBTVIsWUFBWSxDQUFDUyxTQUFuQixDQWRGLGVBZUUsOERBQU1ULFlBQVksQ0FBQ1UsS0FBbkIsQ0FmRixDQUxGLENBakZGLENBREY7QUEyR0QsQ0FqSEQ7O0FBbUhBLGlFQUFlNUQsWUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZIQTtBQUNBOztBQUNBLElBQU1xQixTQUFTLEdBQUcsU0FBWkEsU0FBWTtBQUFBLE1BQUV2QyxRQUFGLFFBQUVBLFFBQUY7QUFBQSxzQkFFZDtBQUFNLGFBQVMsRUFBQyxLQUFoQjtBQUFzQixTQUFLLEVBQUU7QUFBQ0csTUFBQUEsS0FBSyxFQUFDLE1BQVA7QUFBZThDLE1BQUFBLE1BQU0sRUFBQyxPQUF0QjtBQUErQkMsTUFBQUEsZUFBZSxFQUFDLFdBQS9DO0FBQTRESCxNQUFBQSxVQUFVLEVBQUM7QUFBdkU7QUFBN0Isa0JBQ0csdUZBREgsZUFFRyxtRkFBUyxpREFBQyxrREFBRDtBQUFNLE1BQUUsRUFBQyxHQUFUO0FBQWEsU0FBSyxFQUFFO0FBQUNRLE1BQUFBLGNBQWMsRUFBQyxNQUFoQjtBQUF3QlgsTUFBQUEsS0FBSyxFQUFDO0FBQTlCO0FBQXBCLG1CQUFULGlCQUZILENBRmM7QUFBQSxDQUFsQjs7QUFRQSxpRUFBZUwsU0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVkE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTXBCLElBQUksR0FBRyxTQUFQQSxJQUFPLE9BQXlCO0FBQUEsTUFBdkJuQixRQUF1QixRQUF2QkEsUUFBdUI7QUFBQSxNQUFieUIsT0FBYSxRQUFiQSxPQUFhO0FBRXBDLHNCQUNFLDRFQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQ0x5QixNQUFBQSxlQUFlLEVBQUUsU0FEWjtBQUVMTixNQUFBQSxLQUFLLEVBQUUsT0FGRjtBQUdMSCxNQUFBQSxRQUFRLEVBQUUsTUFITDtBQUlMa0IsTUFBQUEsVUFBVSxFQUFFO0FBSlAsS0FEVDtBQU9FLGFBQVMsRUFBQztBQVBaLGtCQVNFLHdLQVRGLENBREYsZUFlRSwrREFDR2xDLE9BQU8sZ0JBQ04saURBQUMsNkNBQUQsT0FETSxnQkFHTixpSEFDRSxpREFBQyxrREFBRCxPQURGLGVBR0UsaURBQUMscURBQUQ7QUFBaUIsWUFBUSxFQUFFekI7QUFBM0IsSUFIRixlQUtFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xpRixNQUFBQSxVQUFVLEVBQ1IsOEZBRkc7QUFHTGhDLE1BQUFBLE1BQU0sRUFBRSxPQUhIO0FBSUxpQyxNQUFBQSxrQkFBa0IsRUFBRSxRQUpmO0FBS0xDLE1BQUFBLGdCQUFnQixFQUFFLFdBTGI7QUFNTEMsTUFBQUEsY0FBYyxFQUFFLE9BTlg7QUFPTHpDLE1BQUFBLFFBQVEsRUFBRTtBQVBMLEtBRFQ7QUFVRSxhQUFTLEVBQUM7QUFWWixrQkFZRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQ0UsYUFBUyxFQUFDLE1BRFo7QUFFRSxTQUFLLEVBQUU7QUFBRUMsTUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JILE1BQUFBLFFBQVEsRUFBRTtBQUE1QjtBQUZULHFCQURGLGVBT0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFRLGFBQVMsRUFBQztBQUFsQix5Q0FDbUMsR0FEbkMsQ0FERixDQVBGLENBWkYsQ0FMRixlQWdDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxhQUFTLEVBQUMscUJBRFo7QUFFRSxPQUFHLEVBQUMscUNBRk47QUFHRSxPQUFHLEVBQUM7QUFITixJQURGLENBREYsZUFRRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBQ0U7QUFDRSxTQUFLLEVBQUU7QUFDTEEsTUFBQUEsUUFBUSxFQUFFLE1BREw7QUFFTEcsTUFBQUEsS0FBSyxFQUFFLFNBRkY7QUFHTGUsTUFBQUEsVUFBVSxFQUFFO0FBSFA7QUFEVCxLQU9HLEdBUEgsOEJBREYsQ0FERixlQWFFO0FBQUcsU0FBSyxFQUFFO0FBQUVsQixNQUFBQSxRQUFRLEVBQUUsTUFBWjtBQUFvQkcsTUFBQUEsS0FBSyxFQUFFO0FBQTNCO0FBQVYsa0JBQ0UsK0VBREYsZUFFRTtBQUNFLFFBQUksRUFBQyxzQ0FEUDtBQUVFLFNBQUssRUFBQyxzQ0FGUjtBQUdFLFVBQU0sRUFBQyxRQUhUO0FBSUUsT0FBRyxFQUFDO0FBSk4sa0JBTUUsZ0dBTkYsQ0FGRixlQVVFLHFQQVZGLENBYkYsQ0FSRixDQWhDRixDQUpKLENBZkYsQ0FERjtBQWdHRCxDQWxHRDs7QUFvR0EsaUVBQWV6QixJQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pHQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUosT0FBTyxHQUFHLFNBQVZBLE9BQVUsT0FBc0I7QUFBQSxNQUFwQmhDLFFBQW9CLFFBQXBCQSxRQUFvQjtBQUFBLE1BQVY4QyxJQUFVLFFBQVZBLElBQVU7QUFFcEMsc0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRSw4RUFDRTtBQUNFLGFBQVMsRUFBQyx1QkFEWjtBQUVFLFFBQUksRUFBQyxRQUZQO0FBR0UsU0FBSyxFQUFFO0FBQ0xlLE1BQUFBLEtBQUssRUFBRSxTQURGO0FBRUx6QyxNQUFBQSxLQUFLLEVBQUUsTUFGRjtBQUdMOEMsTUFBQUEsTUFBTSxFQUFFLE1BSEg7QUFJTE4sTUFBQUEsUUFBUSxFQUFFLFVBSkw7QUFLTEcsTUFBQUEsR0FBRyxFQUFFLEtBTEE7QUFNTEQsTUFBQUEsSUFBSSxFQUFFLEtBTkQ7QUFPTHdDLE1BQUFBLE1BQU0sRUFBRTtBQVBIO0FBSFQsa0JBYUU7QUFBTSxhQUFTLEVBQUM7QUFBaEIsSUFiRixDQURGLGVBZ0JFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLGFBQVMsRUFBQyxRQURaO0FBRUUsU0FBSyxFQUFFO0FBQ0w1QyxNQUFBQSxRQUFRLEVBQUUsTUFETDtBQUVMTyxNQUFBQSxNQUFNLEVBQUUsY0FGSDtBQUdMSixNQUFBQSxLQUFLLEVBQUU7QUFIRixLQUZUO0FBT0UsV0FBTyxFQUFFMUMseUNBQU9BO0FBUGxCLGNBREYsQ0FERixlQWNFLGlEQUFDLGtEQUFEO0FBQU0sYUFBUyxFQUFDLE1BQWhCO0FBQXVCLE1BQUUsRUFBQztBQUExQixrQkFDRTtBQUFLLE9BQUcsRUFBQyxxQ0FBVDtBQUErQyxPQUFHLEVBQUM7QUFBbkQsSUFERixDQWRGLEVBaUJHMkIsSUFBSSxnQkFDTCxpREFBQyxrREFBRDtBQUNFLGFBQVMsRUFBQyxzQkFEWjtBQUVFLFNBQUssRUFBRTtBQUFFMEIsTUFBQUEsY0FBYyxFQUFFLE1BQWxCO0FBQTBCWixNQUFBQSxRQUFRLEVBQUU7QUFBcEMsS0FGVDtBQUdFLE1BQUUsRUFBQztBQUhMLGtCQUtFO0FBQUssT0FBRyxFQUFDLHVCQUFUO0FBQWlDLE9BQUcsRUFBQztBQUFyQyxJQUxGLGVBTUU7QUFDRSxhQUFTLEVBQUMsaUNBRFo7QUFFRSxTQUFLLEVBQUU7QUFBRTJDLE1BQUFBLFNBQVMsRUFBRSxPQUFiO0FBQXNCNUIsTUFBQUEsVUFBVSxFQUFFO0FBQWxDO0FBRlQsa0JBSUU7QUFDRSxhQUFTLEVBQUM7QUFEWixJQUpGLGVBT0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLGFBQVMsRUFBQztBQURaLElBREYsZUFJRTtBQUNFLFNBQUssRUFBRTtBQUFFakIsTUFBQUEsUUFBUSxFQUFFLE1BQVo7QUFBb0JHLE1BQUFBLEtBQUssRUFBRTtBQUEzQixLQURUO0FBRUUsYUFBUyxFQUFDO0FBRlosa0JBSkYsQ0FQRixDQU5GLENBREssR0EyQkQsSUE1Q04sQ0FoQkYsZUErREU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRSxpREFBQyxrREFBRDtBQUFNLGFBQVMsRUFBQyxNQUFoQjtBQUF1QixNQUFFLEVBQUM7QUFBMUIsa0JBQ0U7QUFBSyxPQUFHLEVBQUMscUNBQVQ7QUFBK0MsT0FBRyxFQUFDO0FBQW5ELElBREYsQ0FERixlQUlFLGlEQUFDLGtEQUFEO0FBQ0UsYUFBUyxFQUFDLHNCQURaO0FBRUUsU0FBSyxFQUFFO0FBQUVXLE1BQUFBLGNBQWMsRUFBRSxNQUFsQjtBQUEwQlosTUFBQUEsUUFBUSxFQUFFO0FBQXBDLEtBRlQ7QUFHRSxNQUFFLEVBQUM7QUFITCxrQkFLRTtBQUFLLE9BQUcsRUFBQyx1QkFBVDtBQUFpQyxPQUFHLEVBQUM7QUFBckMsSUFMRixlQU1FO0FBQ0UsYUFBUyxFQUFDLGlDQURaO0FBRUUsU0FBSyxFQUFFO0FBQUUyQyxNQUFBQSxTQUFTLEVBQUUsT0FBYjtBQUFzQjVCLE1BQUFBLFVBQVUsRUFBRTtBQUFsQztBQUZULGtCQUlFO0FBQU0sYUFBUyxFQUFDO0FBQWhCLElBSkYsZUFLRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQU0sYUFBUyxFQUFDO0FBQWhCLElBREYsZUFFRTtBQUNFLFNBQUssRUFBRTtBQUFFakIsTUFBQUEsUUFBUSxFQUFFLE1BQVo7QUFBb0JHLE1BQUFBLEtBQUssRUFBRTtBQUEzQixLQURUO0FBRUUsYUFBUyxFQUFDO0FBRlosa0JBRkYsQ0FMRixDQU5GLENBSkYsQ0EvREYsZUEyRkU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRSxpREFBQyxrREFBRDtBQUFNLGFBQVMsRUFBQyxTQUFoQjtBQUEwQixNQUFFLEVBQUM7QUFBN0IsdUJBREYsZUFJRSxpREFBQyxrREFBRDtBQUFNLGFBQVMsRUFBQyxTQUFoQjtBQUEwQixNQUFFLEVBQUM7QUFBN0Isc0JBSkYsZUFPRSxpREFBQyxrREFBRDtBQUNFLGFBQVMsRUFBQyxTQURaO0FBRUUsTUFBRSxFQUFDO0FBRkwsZ0JBUEYsZUFhRSxpREFBQyxrREFBRDtBQUFNLGFBQVMsRUFBQyxTQUFoQjtBQUEwQixNQUFFLEVBQUM7QUFBN0Isb0JBYkYsZUFnQkUsaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsU0FBaEI7QUFBMEIsTUFBRSxFQUFDO0FBQTdCLGNBaEJGLENBM0ZGLGVBZ0hFO0FBQUssTUFBRSxFQUFDLFdBQVI7QUFBb0IsYUFBUyxFQUFDO0FBQTlCLGtCQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQ0wyQyxNQUFBQSxVQUFVLEVBQUUsS0FEUDtBQUVMbEIsTUFBQUEsYUFBYSxFQUFFLEtBRlY7QUFHTGpCLE1BQUFBLFlBQVksRUFBRTtBQUhUO0FBRFQsa0JBT0U7QUFBRyxTQUFLLEVBQUU7QUFBRUosTUFBQUEsTUFBTSxFQUFFO0FBQVYsS0FBVjtBQUFzQyxXQUFPLEVBQUU1QywwQ0FBUUE7QUFBdkQsa0JBQ0U7QUFDRSxTQUFLLEVBQUU7QUFDTHFDLE1BQUFBLFFBQVEsRUFBRSxNQURMO0FBRUxHLE1BQUFBLEtBQUssRUFBRSxTQUZGO0FBR0x5QixNQUFBQSxhQUFhLEVBQUU7QUFIVjtBQURULFlBREYsZUFVRTtBQUFNLFNBQUssRUFBRTtBQUFFNUIsTUFBQUEsUUFBUSxFQUFFLE1BQVo7QUFBb0JHLE1BQUFBLEtBQUssRUFBRTtBQUEzQjtBQUFiLEtBQ0csR0FESCxhQUVVLEdBRlYsQ0FWRixDQVBGLENBREYsZUF3QkUsMkVBQ0UsaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsU0FBaEI7QUFBMEIsTUFBRSxFQUFDLEdBQTdCO0FBQWlDLFdBQU8sRUFBRXhDLDBDQUFRQTtBQUFsRCx1QkFERixDQXhCRixlQTZCRSwyRUFDRSxpREFBQyxrREFBRDtBQUFNLGFBQVMsRUFBQyxTQUFoQjtBQUEwQixNQUFFLEVBQUMsR0FBN0I7QUFBaUMsV0FBTyxFQUFFQSwwQ0FBUUE7QUFBbEQsc0JBREYsQ0E3QkYsZUFrQ0UsMkVBQ0UsaURBQUMsa0RBQUQ7QUFDRSxhQUFTLEVBQUMsU0FEWjtBQUVFLE1BQUUsRUFBQyxXQUZMO0FBR0UsV0FBTyxFQUFFQSwwQ0FBUUE7QUFIbkIsZ0JBREYsQ0FsQ0YsZUEyQ0UsMkVBQ0UsaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsU0FBaEI7QUFBMEIsTUFBRSxFQUFDLEdBQTdCO0FBQWlDLFdBQU8sRUFBRUEsMENBQVFBO0FBQWxELG9CQURGLENBM0NGLGVBZ0RFLDJFQUNFLGlEQUFDLGtEQUFEO0FBQU0sYUFBUyxFQUFDLFNBQWhCO0FBQTBCLE1BQUUsRUFBQyxHQUE3QjtBQUFpQyxXQUFPLEVBQUVBLDBDQUFRQTtBQUFsRCxjQURGLENBaERGLGVBcURFLDJFQUNFLGlEQUFDLGtEQUFEO0FBQU0sU0FBSyxFQUFFO0FBQUVxQyxNQUFBQSxRQUFRLEVBQUU7QUFBWixLQUFiO0FBQW1DLE1BQUUsRUFBQyxHQUF0QztBQUEwQyxXQUFPLEVBQUVyQywwQ0FBUUE7QUFBM0Qsa0JBREYsQ0FyREYsQ0FoSEYsQ0FERixFQThLR3JCLFFBOUtILGVBZ0xFLDhFQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxTQUFLLEVBQUU7QUFBRTBELE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBRFQ7QUFFRSxhQUFTLEVBQUM7QUFGWixrQkFJRSwyRUFKRixlQUtFO0FBQUssYUFBUyxFQUFDO0FBQWYsSUFMRixDQURGLGVBUUU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUcsU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQVY7QUFBZ0MsYUFBUyxFQUFDLFNBQTFDO0FBQW9ELFFBQUksRUFBQztBQUF6RCxnQ0FERixlQUlFO0FBQUcsU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQVY7QUFBZ0MsYUFBUyxFQUFDLFNBQTFDO0FBQW9ELFFBQUksRUFBQztBQUF6RCxtQkFKRixlQU9FO0FBQUcsU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQVY7QUFBZ0MsYUFBUyxFQUFDLFNBQTFDO0FBQW9ELFFBQUksRUFBQztBQUF6RCx5QkFQRixlQVVFO0FBQUcsU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQVY7QUFBZ0MsYUFBUyxFQUFDLFNBQTFDO0FBQW9ELFFBQUksRUFBQztBQUF6RCxvQkFWRixlQWFFO0FBQUcsU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQVY7QUFBZ0MsYUFBUyxFQUFDLFNBQTFDO0FBQW9ELFFBQUksRUFBQztBQUF6RCxjQWJGLENBREYsQ0FSRixlQTJCRTtBQUNFLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxRQUFRLEVBQUU7QUFBWixLQURUO0FBRUUsYUFBUyxFQUFDO0FBRlosa0JBSUUsOEVBSkYsZUFLRTtBQUFLLGFBQVMsRUFBQztBQUFmLElBTEYsQ0EzQkYsZUFrQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsa0JBQ0U7QUFBSyxPQUFHLEVBQUMsV0FBVDtBQUFxQixPQUFHLEVBQUM7QUFBekIsSUFERixDQURGLENBREYsQ0FsQ0YsQ0FERixlQTRDRTtBQUNFLFNBQUssRUFBRTtBQUFFUyxNQUFBQSxlQUFlLEVBQUU7QUFBbkIsS0FEVDtBQUVFLGFBQVMsRUFBQztBQUZaLGtCQUlFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0UsaURBQUMsa0RBQUQ7QUFDRSxhQUFTLEVBQUMsV0FEWjtBQUVFLFNBQUssRUFBRTtBQUFFVCxNQUFBQSxRQUFRLEVBQUUsTUFBWjtBQUFvQkcsTUFBQUEsS0FBSyxFQUFFO0FBQTNCLEtBRlQ7QUFHRSxNQUFFLEVBQUM7QUFITCxvQkFERixlQVFFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0UsaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsU0FBaEI7QUFBMEIsU0FBSyxFQUFFO0FBQUVILE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQWpDO0FBQXVELE1BQUUsRUFBQztBQUExRCxnQ0FERixlQUlFLGlEQUFDLGtEQUFEO0FBQU0sYUFBUyxFQUFDLFNBQWhCO0FBQTBCLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxRQUFRLEVBQUU7QUFBWixLQUFqQztBQUF1RCxNQUFFLEVBQUM7QUFBMUQsbUJBSkYsZUFPRSxpREFBQyxrREFBRDtBQUFNLGFBQVMsRUFBQyxTQUFoQjtBQUEwQixTQUFLLEVBQUU7QUFBRUEsTUFBQUEsUUFBUSxFQUFFO0FBQVosS0FBakM7QUFBdUQsTUFBRSxFQUFDO0FBQTFELHlCQVBGLGVBVUUsaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsU0FBaEI7QUFBMEIsU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFFBQVEsRUFBRTtBQUFaLEtBQWpDO0FBQXVELE1BQUUsRUFBQztBQUExRCxvQkFWRixlQWFFLGlEQUFDLGtEQUFEO0FBQU0sYUFBUyxFQUFDLFNBQWhCO0FBQTBCLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxRQUFRLEVBQUU7QUFBWixLQUFqQztBQUF1RCxNQUFFLEVBQUM7QUFBMUQsY0FiRixDQVJGLENBSkYsZUE4QkU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxRQUFRLEVBQUUsTUFBWjtBQUFvQkcsTUFBQUEsS0FBSyxFQUFFO0FBQTNCLEtBRFQ7QUFFRSxhQUFTLEVBQUM7QUFGWixrQkFJRSw4RUFKRixDQURGLGVBT0UsaURBQUMsa0RBQUQ7QUFBTSxNQUFFLEVBQUM7QUFBVCxrQkFDRTtBQUNFLE9BQUcsRUFBQyxXQUROO0FBRUUsU0FBSyxFQUFFO0FBQUV6QyxNQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQjhDLE1BQUFBLE1BQU0sRUFBRTtBQUF6QixLQUZUO0FBR0UsT0FBRyxFQUFDO0FBSE4sSUFERixDQVBGLENBOUJGLENBNUNGLGVBMkZFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xDLE1BQUFBLGVBQWUsRUFBRSxTQURaO0FBRUxOLE1BQUFBLEtBQUssRUFBRSxPQUZGO0FBR0xILE1BQUFBLFFBQVEsRUFBRTtBQUhMLEtBRFQ7QUFNRSxhQUFTLEVBQUM7QUFOWixtREFRZ0MsaURBQUMsa0RBQUQ7QUFBTSxNQUFFLEVBQUM7QUFBVCw0QkFSaEMsRUFReUUsR0FSekUsd0JBU1MsaURBQUMsa0RBQUQ7QUFBTSxNQUFFLEVBQUM7QUFBVCxrQ0FUVCx3QkFVUyxpREFBQyxrREFBRDtBQUFNLE1BQUUsRUFBQztBQUFULDBCQVZULENBM0ZGLENBaExGLENBREY7QUEyUkQsQ0E3UkQ7O0FBOFJBLGlFQUFlMUIsT0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsU0E7QUFDQTtBQUNBOztBQUVBLElBQU1DLE9BQU8sR0FBRyxTQUFWQSxPQUFVLENBQUN3RSxLQUFEO0FBQUEsc0JBQ2QsOEVBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUFDdkMsTUFBQUEsTUFBTSxFQUFDLE1BQVI7QUFBZ0I5QyxNQUFBQSxLQUFLLEVBQUMsTUFBdEI7QUFBOEIrQyxNQUFBQSxlQUFlLEVBQUMsU0FBOUM7QUFBeUROLE1BQUFBLEtBQUssRUFBQztBQUEvRCxLQURUO0FBRUUsYUFBUyxFQUFDO0FBRlosa0JBSUUsaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUUsWUFBakI7QUFBK0IsU0FBSyxFQUFFO0FBQUNHLE1BQUFBLFVBQVUsRUFBQztBQUFaLEtBQXRDO0FBQTJELE1BQUUsRUFBQztBQUE5RCxrQkFDQTtBQUNJLFNBQUssRUFBRTtBQUFDRSxNQUFBQSxNQUFNLEVBQUM7QUFBUixLQURYO0FBRUksYUFBUyxFQUFDLFdBRmQ7QUFHSSxPQUFHLEVBQUMsc0NBSFI7QUFJSSxPQUFHLEVBQUM7QUFKUixJQURBLENBSkYsZUFZRTtBQUFNLGFBQVMsRUFBQyxvQkFBaEI7QUFBcUMsU0FBSyxFQUFFO0FBQUNGLE1BQUFBLFVBQVUsRUFBQztBQUFaO0FBQTVDLGdCQVpGLGVBZUU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDQSxpREFBQyxrREFBRDtBQUFNLFNBQUssRUFBRTtBQUFDSCxNQUFBQSxLQUFLLEVBQUMsT0FBUDtBQUFnQkgsTUFBQUEsUUFBUSxFQUFDLE1BQXpCO0FBQWlDYyxNQUFBQSxjQUFjLEVBQUMsTUFBaEQ7QUFBd0RSLE1BQUFBLFVBQVUsRUFBQztBQUFuRSxLQUFiO0FBQXlGLE1BQUUsRUFBQztBQUE1RixzQkFEQSxlQUlFO0FBQ0UsU0FBSyxFQUFDLDRCQURSO0FBRUUsU0FBSyxFQUFDLElBRlI7QUFHRSxVQUFNLEVBQUMsSUFIVDtBQUlFLFFBQUksRUFBQyxjQUpQO0FBS0UsYUFBUyxFQUFDLDhCQUxaO0FBTUUsV0FBTyxFQUFDO0FBTlYsa0JBUUU7QUFDRSxZQUFRLEVBQUMsU0FEWDtBQUVFLEtBQUMsRUFBQztBQUZKLElBUkYsQ0FKRixDQWZGLENBREYsQ0FERixlQXFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQUNFLE1BQUFBLE1BQU0sRUFBQyxPQUFSO0FBQWlCOUMsTUFBQUEsS0FBSyxFQUFDLE1BQXZCO0FBQStCK0MsTUFBQUEsZUFBZSxFQUFDLFNBQS9DO0FBQTBETixNQUFBQSxLQUFLLEVBQUM7QUFBaEUsS0FEVDtBQUVFLGFBQVMsRUFBQztBQUZaLGtCQUlFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFNBQUssRUFBQyw0QkFEUjtBQUVFLFNBQUssRUFBQyxJQUZSO0FBR0UsVUFBTSxFQUFDLElBSFQ7QUFJRSxRQUFJLEVBQUMsY0FKUDtBQUtFLGFBQVMsRUFBQyxvQkFMWjtBQU1FLFdBQU8sRUFBQztBQU5WLGtCQVFFO0FBQ0UsWUFBUSxFQUFDLFNBRFg7QUFFRSxLQUFDLEVBQUM7QUFGSixJQVJGLENBREYsZUFjRSxpREFBQyxrREFBRDtBQUFNLFNBQUssRUFBRTtBQUFDQSxNQUFBQSxLQUFLLEVBQUMsT0FBUDtBQUFnQkgsTUFBQUEsUUFBUSxFQUFDLE1BQXpCO0FBQWlDYyxNQUFBQSxjQUFjLEVBQUMsTUFBaEQ7QUFBd0RSLE1BQUFBLFVBQVUsRUFBQztBQUFuRSxLQUFiO0FBQXlGLE1BQUUsRUFBQztBQUE1RixzQkFkRixDQURGLENBSkYsZUF3QkUsMkVBQ0ksaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsaUJBQWhCO0FBQWtDLFNBQUssRUFBRTtBQUFDQSxNQUFBQSxVQUFVLEVBQUM7QUFBWixLQUF6QztBQUE4RCxNQUFFLEVBQUM7QUFBakUsa0JBQ0E7QUFDRSxTQUFLLEVBQUU7QUFBQ0UsTUFBQUEsTUFBTSxFQUFDO0FBQVIsS0FEVDtBQUVFLGFBQVMsRUFBQyxNQUZaO0FBR0UsT0FBRyxFQUFDLHNDQUhOO0FBSUUsT0FBRyxFQUFDO0FBSk4sSUFEQSxDQURKLGVBU0U7QUFBSyxhQUFTLEVBQUMsMEJBQWY7QUFBMEMsU0FBSyxFQUFFO0FBQUNGLE1BQUFBLFVBQVUsRUFBQztBQUFaO0FBQWpELGdCQVRGLENBeEJGLGVBcUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxhQUFTLEVBQUMsU0FEWjtBQUVFLFNBQUssRUFBRTtBQUFDQyxNQUFBQSxNQUFNLEVBQUM7QUFBUixLQUZUO0FBR0UsV0FBTyxFQUFFckgsZ0VBQThCQTtBQUh6QyxrQkFLRTtBQUFNLFNBQUssRUFBRTtBQUFDaUgsTUFBQUEsS0FBSyxFQUFDLE9BQVA7QUFBZ0JILE1BQUFBLFFBQVEsRUFBQyxNQUF6QjtBQUFpQ2MsTUFBQUEsY0FBYyxFQUFDLE1BQWhEO0FBQXdEUixNQUFBQSxVQUFVLEVBQUM7QUFBbkU7QUFBYixrQkFMRixlQVFFO0FBQ0UsU0FBSyxFQUFDLDRCQURSO0FBRUUsU0FBSyxFQUFDLElBRlI7QUFHRSxVQUFNLEVBQUMsSUFIVDtBQUlFLFFBQUksRUFBQyxjQUpQO0FBS0UsYUFBUyxFQUFDLDhCQUxaO0FBTUUsV0FBTyxFQUFDO0FBTlYsa0JBUUU7QUFDRSxZQUFRLEVBQUMsU0FEWDtBQUVFLEtBQUMsRUFBQztBQUZKLElBUkYsQ0FSRixDQURGLENBckNGLENBREYsQ0FyQ0YsRUFxR0d5QyxLQUFLLENBQUN6RyxRQXJHVCxDQURjO0FBQUEsQ0FBaEI7O0FBMEdBLGlFQUFlaUMsT0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7O0FDOUdBOztBQUVBLElBQU1zQixPQUFPLEdBQUcsU0FBVkEsT0FBVTtBQUFBLHNCQUNkO0FBQU0sU0FBSyxFQUFFO0FBQUNXLE1BQUFBLE1BQU0sRUFBQyxPQUFSO0FBQWlCOUMsTUFBQUEsS0FBSyxFQUFDO0FBQXZCO0FBQWIsa0JBQ0U7QUFDQSxhQUFTLEVBQUMsZ0JBRFY7QUFFQSxRQUFJLEVBQUMsUUFGTDtBQUdBLFNBQUssRUFBRTtBQUNMeUMsTUFBQUEsS0FBSyxFQUFFLFNBREY7QUFFTHpDLE1BQUFBLEtBQUssRUFBRSxNQUZGO0FBR0w4QyxNQUFBQSxNQUFNLEVBQUUsTUFISDtBQUlMTixNQUFBQSxRQUFRLEVBQUUsVUFKTDtBQUtMRyxNQUFBQSxHQUFHLEVBQUUsS0FMQTtBQU1MRCxNQUFBQSxJQUFJLEVBQUUsS0FORDtBQU9Md0MsTUFBQUEsTUFBTSxFQUFFO0FBUEg7QUFIUCxrQkFhQTtBQUFNLGFBQVMsRUFBQztBQUFoQixJQWJBLENBREYsQ0FEYztBQUFBLENBQWhCOztBQW9CQSxpRUFBZS9DLE9BQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0QkE7QUFDQTs7QUFFQSxJQUFNeUMsWUFBWSxHQUFHLFNBQWZBLFlBQWU7QUFBQSxzQkFDbkI7QUFDRSxNQUFFLEVBQUMsY0FETDtBQUVFLGFBQVMsRUFBQywwQkFGWjtBQUdFLG9CQUFhO0FBSGYsa0JBS0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFFBQUksRUFBQyxRQURQO0FBRUUsc0JBQWUsZUFGakI7QUFHRSx3QkFBaUIsR0FIbkI7QUFJRSxhQUFTLEVBQUMsUUFKWjtBQUtFLG9CQUFhLE1BTGY7QUFNRSxrQkFBVztBQU5iLElBREYsZUFTRTtBQUNFLFFBQUksRUFBQyxRQURQO0FBRUUsc0JBQWUsZUFGakI7QUFHRSx3QkFBaUIsR0FIbkI7QUFJRSxrQkFBVztBQUpiLElBVEYsZUFlRTtBQUNFLFFBQUksRUFBQyxRQURQO0FBRUUsc0JBQWUsZUFGakI7QUFHRSx3QkFBaUIsR0FIbkI7QUFJRSxrQkFBVztBQUpiLElBZkYsZUFxQkU7QUFDRSxRQUFJLEVBQUMsUUFEUDtBQUVFLHNCQUFlLGVBRmpCO0FBR0Usd0JBQWlCLEdBSG5CO0FBSUUsa0JBQVc7QUFKYixJQXJCRixlQTJCRTtBQUNFLFFBQUksRUFBQyxRQURQO0FBRUUsc0JBQWUsZUFGakI7QUFHRSx3QkFBaUIsR0FIbkI7QUFJRSxrQkFBVztBQUpiLElBM0JGLENBTEYsZUF3Q0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUNMRSxNQUFBQSxVQUFVLEVBQUUsMEJBRFA7QUFFTGhDLE1BQUFBLE1BQU0sRUFBRSxPQUZIO0FBR0xpQyxNQUFBQSxrQkFBa0IsRUFBRSxRQUhmO0FBSUxDLE1BQUFBLGdCQUFnQixFQUFFLFdBSmI7QUFLTEMsTUFBQUEsY0FBYyxFQUFFO0FBTFgsS0FEVDtBQVFFLGFBQVMsRUFBQztBQVJaLGtCQVVFLGlEQUFDLGtEQUFEO0FBQ0UsYUFBUyxFQUFDLDBCQURaO0FBRUUsU0FBSyxFQUFFO0FBQUVLLE1BQUFBLFlBQVksRUFBRTtBQUFoQixLQUZUO0FBR0UsTUFBRSxFQUFDO0FBSEwsa0JBS0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFJLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxZQUFZLEVBQUU7QUFBaEI7QUFBWCxrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUFFaEQsTUFBQUEsUUFBUSxFQUFFLE1BQVo7QUFBb0JHLE1BQUFBLEtBQUssRUFBRSxPQUEzQjtBQUFvQ2UsTUFBQUEsVUFBVSxFQUFFO0FBQWhEO0FBRFQsa0NBREYsQ0FERixlQVFFO0FBQVEsYUFBUyxFQUFDO0FBQWxCLEtBQ0csR0FESCw4Q0FFMkMsR0FGM0MsQ0FSRixDQUxGLENBVkYsQ0FERixlQStCRTtBQUNFLFNBQUssRUFBRTtBQUNMc0IsTUFBQUEsVUFBVSxFQUFFLDJCQURQO0FBRUxoQyxNQUFBQSxNQUFNLEVBQUUsT0FGSDtBQUdMaUMsTUFBQUEsa0JBQWtCLEVBQUUsUUFIZjtBQUlMQyxNQUFBQSxnQkFBZ0IsRUFBRSxXQUpiO0FBS0xDLE1BQUFBLGNBQWMsRUFBRTtBQUxYLEtBRFQ7QUFRRSxhQUFTLEVBQUM7QUFSWixrQkFVRSxpREFBQyxrREFBRDtBQUNFLGFBQVMsRUFBQywwQkFEWjtBQUVFLFNBQUssRUFBRTtBQUFFSyxNQUFBQSxZQUFZLEVBQUU7QUFBaEIsS0FGVDtBQUdFLE1BQUUsRUFBQztBQUhMLGtCQUtFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSSxTQUFLLEVBQUU7QUFBRUEsTUFBQUEsWUFBWSxFQUFFO0FBQWhCO0FBQVgsa0JBQ0E7QUFDSSxTQUFLLEVBQUU7QUFBRWhELE1BQUFBLFFBQVEsRUFBRSxNQUFaO0FBQW9CRyxNQUFBQSxLQUFLLEVBQUUsT0FBM0I7QUFBb0NlLE1BQUFBLFVBQVUsRUFBRTtBQUFoRDtBQURYLGtDQURBLENBREYsZUFRRTtBQUFRLGFBQVMsRUFBQztBQUFsQixLQUNHLEdBREgsOENBRTJDLEdBRjNDLENBUkYsQ0FMRixDQVZGLENBL0JGLGVBNkRFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xzQixNQUFBQSxVQUFVLEVBQ1IsaUdBRkc7QUFHTGhDLE1BQUFBLE1BQU0sRUFBRSxPQUhIO0FBSUxpQyxNQUFBQSxrQkFBa0IsRUFBRSxRQUpmO0FBS0xDLE1BQUFBLGdCQUFnQixFQUFFLFdBTGI7QUFNTEMsTUFBQUEsY0FBYyxFQUFFO0FBTlgsS0FEVDtBQVNFLGFBQVMsRUFBQztBQVRaLGtCQVdFLGlEQUFDLGtEQUFEO0FBQ0UsYUFBUyxFQUFDLDBCQURaO0FBRUUsU0FBSyxFQUFFO0FBQUVLLE1BQUFBLFlBQVksRUFBRTtBQUFoQixLQUZUO0FBR0UsTUFBRSxFQUFDO0FBSEwsa0JBS0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFJLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxZQUFZLEVBQUU7QUFBaEI7QUFBWCxrQkFDQTtBQUNJLFNBQUssRUFBRTtBQUFFaEQsTUFBQUEsUUFBUSxFQUFFLE1BQVo7QUFBb0JHLE1BQUFBLEtBQUssRUFBRSxPQUEzQjtBQUFvQ2UsTUFBQUEsVUFBVSxFQUFFO0FBQWhEO0FBRFgsa0NBREEsQ0FERixlQVFFO0FBQVEsYUFBUyxFQUFDO0FBQWxCLEtBQ0csR0FESCw4Q0FFMkMsR0FGM0MsQ0FSRixDQUxGLENBWEYsQ0E3REYsZUE0RkU7QUFDRSxTQUFLLEVBQUU7QUFDTHNCLE1BQUFBLFVBQVUsRUFDUixvQ0FGRztBQUdMaEMsTUFBQUEsTUFBTSxFQUFFLE9BSEg7QUFJTGlDLE1BQUFBLGtCQUFrQixFQUFFLFFBSmY7QUFLTEMsTUFBQUEsZ0JBQWdCLEVBQUUsV0FMYjtBQU1MQyxNQUFBQSxjQUFjLEVBQUU7QUFOWCxLQURUO0FBU0UsYUFBUyxFQUFDO0FBVFosa0JBV0UsaURBQUMsa0RBQUQ7QUFDRSxhQUFTLEVBQUMsMEJBRFo7QUFFRSxTQUFLLEVBQUU7QUFBRUssTUFBQUEsWUFBWSxFQUFFO0FBQWhCLEtBRlQ7QUFHRSxNQUFFLEVBQUM7QUFITCxrQkFLRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUksU0FBSyxFQUFFO0FBQUVBLE1BQUFBLFlBQVksRUFBRTtBQUFoQjtBQUFYLGtCQUNBO0FBQ0ksU0FBSyxFQUFFO0FBQUVoRCxNQUFBQSxRQUFRLEVBQUUsTUFBWjtBQUFvQkcsTUFBQUEsS0FBSyxFQUFFLE9BQTNCO0FBQW9DZSxNQUFBQSxVQUFVLEVBQUU7QUFBaEQ7QUFEWCxrQ0FEQSxDQURGLGVBUUU7QUFBUSxhQUFTLEVBQUM7QUFBbEIsS0FDRyxHQURILDhDQUUyQyxHQUYzQyxDQVJGLENBTEYsQ0FYRixDQTVGRixlQTJIRTtBQUNBLFNBQUssRUFBRTtBQUNMc0IsTUFBQUEsVUFBVSxFQUNSLDRCQUZHO0FBR0xoQyxNQUFBQSxNQUFNLEVBQUUsT0FISDtBQUlMaUMsTUFBQUEsa0JBQWtCLEVBQUUsUUFKZjtBQUtMQyxNQUFBQSxnQkFBZ0IsRUFBRSxXQUxiO0FBTUxDLE1BQUFBLGNBQWMsRUFBRTtBQU5YLEtBRFA7QUFTRSxhQUFTLEVBQUM7QUFUWixrQkFXRSxpREFBQyxrREFBRDtBQUNFLGFBQVMsRUFBQywwQkFEWjtBQUVFLFNBQUssRUFBRTtBQUFFSyxNQUFBQSxZQUFZLEVBQUU7QUFBaEIsS0FGVDtBQUdFLE1BQUUsRUFBQztBQUhMLGtCQUtFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSSxTQUFLLEVBQUU7QUFBRUEsTUFBQUEsWUFBWSxFQUFFO0FBQWhCO0FBQVgsa0JBQ0E7QUFDSSxTQUFLLEVBQUU7QUFBRWhELE1BQUFBLFFBQVEsRUFBRSxNQUFaO0FBQW9CRyxNQUFBQSxLQUFLLEVBQUUsT0FBM0I7QUFBb0NlLE1BQUFBLFVBQVUsRUFBRTtBQUFoRDtBQURYLGtDQURBLENBREYsZUFRRTtBQUFRLGFBQVMsRUFBQztBQUFsQixLQUNHLEdBREgsOENBRTJDLEdBRjNDLENBUkYsQ0FMRixDQVhGLENBM0hGLENBeENGLENBRG1CO0FBQUEsQ0FBckI7O0FBdU1BLGlFQUFlb0IsWUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7O0FDMU1BOztBQUVBLElBQU14RCxRQUFRLEdBQUcsU0FBWEEsUUFBVztBQUFBLHNCQUViLHVHQUZhO0FBQUEsQ0FBakI7O0FBT0EsaUVBQWVBLFFBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVEE7QUFDQTtBQUNBO0FBQ0E7QUFTQTtBQUNBOztBQUVBLElBQU1ILE9BQU8sR0FBRyxTQUFWQSxPQUFVLE9BQWtCO0FBQUEsTUFBZmEsUUFBZSxRQUFmQSxRQUFlO0FBQ2hDLE1BQU0vRSxRQUFRLEdBQUdpSCw4REFBVyxFQUE1QjtBQUNBLE1BQU11QixRQUFRLEdBQUd4SSxRQUFRLENBQUNtRyxLQUExQjs7QUFFQSxrQkFBOEJ4QyxnREFBUSxFQUF0QztBQUFBO0FBQUEsTUFBT25CLE9BQVA7QUFBQSxNQUFnQmlHLFVBQWhCOztBQUNBLG1CQUE4QjlFLGdEQUFRLENBQUMsSUFBRCxDQUF0QztBQUFBO0FBQUEsTUFBT1ksT0FBUDtBQUFBLE1BQWdCQyxVQUFoQjs7QUFFQVosRUFBQUEsaURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSTRFLFFBQUosRUFBYztBQUNaQyxNQUFBQSxVQUFVLENBQUNELFFBQUQsQ0FBVjtBQUNBakcsTUFBQUEsa0RBQVcsQ0FBQ2lHLFFBQUQsQ0FBWDtBQUNBaEUsTUFBQUEsVUFBVSxDQUFDLEtBQUQsQ0FBVjtBQUNELEtBSkQsTUFJTztBQUNMckosTUFBQUEsaURBQUEsdUJBQ3NCNEUsTUFBTSxDQUFDQyxRQUFQLENBQWdCMEksSUFBaEIsQ0FBcUJ4SSxLQUFyQixDQUEyQixHQUEzQixFQUFnQ0MsR0FBaEMsRUFEdEIsR0FFR2hDLElBRkgsQ0FFUSxVQUFDOEcsUUFBRCxFQUFjO0FBQ2xCd0QsUUFBQUEsVUFBVSxDQUFDeEQsUUFBUSxDQUFDL0gsSUFBVixDQUFWO0FBQ0FzSCxRQUFBQSxVQUFVLENBQUMsS0FBRCxDQUFWO0FBQ0QsT0FMSDtBQU1EOztBQUVELFdBQU8sWUFBTTtBQUNYaUUsTUFBQUEsVUFBVTtBQUNWakUsTUFBQUEsVUFBVTtBQUNYLEtBSEQ7QUFJRCxHQWxCUSxFQWtCTixFQWxCTSxDQUFUOztBQW9CQSxNQUFJRCxPQUFKLEVBQWE7QUFDWCx3QkFBTyxrREFBQyxrREFBRCxPQUFQO0FBQ0QsR0FGRCxNQUVPLElBQUkvQixPQUFKLEVBQWE7QUFDbEIsd0JBQ0U7QUFBTSxlQUFTLEVBQUM7QUFBaEIsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUNFLGVBQVMsRUFBQyxXQURaO0FBRUUsV0FBSyxFQUFFO0FBQ0xrRCxRQUFBQSxLQUFLLEVBQUUsU0FERjtBQUVMVyxRQUFBQSxjQUFjLEVBQUUsTUFGWDtBQUdMZCxRQUFBQSxRQUFRLEVBQUU7QUFITCxPQUZUO0FBT0UsVUFBSSxFQUFDO0FBUFAsaUJBREYsOEJBYUU7QUFDRSxlQUFTLEVBQUMsV0FEWjtBQUVFLFdBQUssRUFBRTtBQUNMRyxRQUFBQSxLQUFLLEVBQUUsU0FERjtBQUVMVyxRQUFBQSxjQUFjLEVBQUUsTUFGWDtBQUdMZCxRQUFBQSxRQUFRLEVBQUU7QUFITDtBQUZULE9BUUcvQyxPQUFPLENBQUM4RCxLQVJYLENBYkYsQ0FERixlQXlCRTtBQUNFLGVBQVMsRUFBQyw0RUFEWjtBQUVFLFdBQUssRUFBRTtBQUFFZixRQUFBQSxRQUFRLEVBQUUsTUFBWjtBQUFvQkMsUUFBQUEsTUFBTSxFQUFFO0FBQTVCO0FBRlQsb0JBSUUsNEVBQ0U7QUFDRSxXQUFLLEVBQUU7QUFBRUUsUUFBQUEsS0FBSyxFQUFFLFdBQVQ7QUFBc0JELFFBQUFBLFFBQVEsRUFBRTtBQUFoQyxPQURUO0FBRUUsV0FBSyxFQUFDLDRCQUZSO0FBR0UsV0FBSyxFQUFDLElBSFI7QUFJRSxZQUFNLEVBQUMsSUFKVDtBQUtFLFVBQUksRUFBQyxjQUxQO0FBTUUsZUFBUyxFQUFDLG1CQU5aO0FBT0UsYUFBTyxFQUFDO0FBUFYsb0JBU0U7QUFBUSxRQUFFLEVBQUMsR0FBWDtBQUFlLFFBQUUsRUFBQyxHQUFsQjtBQUFzQixPQUFDLEVBQUM7QUFBeEIsTUFURixDQURGLGVBWUU7QUFDRSxXQUFLLEVBQUU7QUFBRUMsUUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JELFFBQUFBLFFBQVEsRUFBRTtBQUE1QixPQURUO0FBRUUsV0FBSyxFQUFDLDRCQUZSO0FBR0UsV0FBSyxFQUFDLElBSFI7QUFJRSxZQUFNLEVBQUMsSUFKVDtBQUtFLFVBQUksRUFBQyxjQUxQO0FBTUUsZUFBUyxFQUFDLGFBTlo7QUFPRSxhQUFPLEVBQUM7QUFQVixvQkFTRTtBQUFNLE9BQUMsRUFBQztBQUFSLE1BVEYsQ0FaRixlQXVCRTtBQUFNLFdBQUssRUFBRTtBQUFFQyxRQUFBQSxLQUFLLEVBQUU7QUFBVDtBQUFiLDRDQUN1Q2xELE9BQU8sQ0FBQzhELEtBRC9DLENBdkJGLGVBMEJFO0FBQ0UsV0FBSyxFQUFFO0FBQ0xaLFFBQUFBLEtBQUssRUFBRSxTQURGO0FBRUxXLFFBQUFBLGNBQWMsRUFBRSxNQUZYO0FBR0xJLFFBQUFBLFVBQVUsRUFBRTtBQUhQLE9BRFQ7QUFNRSxVQUFJLEVBQUM7QUFOUCx1QkExQkYsQ0FKRixlQXlDRSxrREFBQyxtREFBRDtBQUNFLFFBQUUsRUFBRTtBQUFFeEcsUUFBQUEsUUFBUSxFQUFFdUMsT0FBTyxDQUFDeEUsR0FBcEI7QUFBeUJtSSxRQUFBQSxLQUFLLEVBQUUzRDtBQUFoQyxPQUROO0FBRUUsV0FBSyxFQUFFO0FBQUVzRCxRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUZUO0FBR0UsYUFBTyxFQUFFLG1CQUFNO0FBQ2J0SixRQUFBQSxDQUFDLENBQUMscUJBQUQsQ0FBRCxDQUF5QixDQUF6QixFQUE0QlosU0FBNUIsQ0FBc0MwRCxHQUF0QyxDQUEwQyxRQUExQztBQUNEO0FBTEgsb0JBT0U7QUFDRSxXQUFLLEVBQUU7QUFBRW9HLFFBQUFBLEtBQUssRUFBRTtBQUFULE9BRFQ7QUFFRSxXQUFLLEVBQUMsNEJBRlI7QUFHRSxXQUFLLEVBQUMsSUFIUjtBQUlFLFlBQU0sRUFBQyxJQUpUO0FBS0UsVUFBSSxFQUFDLGNBTFA7QUFNRSxlQUFTLEVBQUMsU0FOWjtBQU9FLGFBQU8sRUFBQztBQVBWLG9CQVNFO0FBQU0sT0FBQyxFQUFDO0FBQVIsTUFURixDQVBGLENBekNGLENBekJGLGVBc0ZFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBSyxXQUFLLEVBQUU7QUFBRWlELFFBQUFBLFdBQVcsRUFBRSxPQUFmO0FBQXdCMUYsUUFBQUEsS0FBSyxFQUFFO0FBQS9CO0FBQVosb0JBQ0U7QUFBRyxVQUFJLEVBQUM7QUFBUixvQkFDRTtBQUFLLFNBQUcsRUFBRVQsT0FBTyxDQUFDNEQsUUFBbEI7QUFBNEIsZUFBUyxFQUFDO0FBQXRDLE1BREYsQ0FERixDQURGLGVBTUU7QUFDRSxXQUFLLEVBQUU7QUFBRW5ELFFBQUFBLEtBQUssRUFBRSxLQUFUO0FBQWdCMkYsUUFBQUEsWUFBWSxFQUFFO0FBQTlCLE9BRFQ7QUFFRSxlQUFTLEVBQUM7QUFGWixvQkFJRSw0RUFDRTtBQUFNLFdBQUssRUFBRTtBQUFFbEQsUUFBQUEsS0FBSyxFQUFFLFNBQVQ7QUFBb0JILFFBQUFBLFFBQVEsRUFBRTtBQUE5QjtBQUFiLDJCQUVHL0MsT0FBTyxDQUFDNEQsUUFBUixDQUFpQnlDLE9BQWpCLENBQXlCLE1BQXpCLEVBQWlDLEVBQWpDLEVBQXFDQSxPQUFyQyxDQUE2QyxPQUE3QyxFQUFzRCxFQUF0RCxDQUZILENBREYsZUFLRTtBQUNFLFdBQUssRUFBRTtBQUFFbkQsUUFBQUEsS0FBSyxFQUFFO0FBQVQsT0FEVDtBQUVFLGVBQVMsRUFBQztBQUZaLE9BSUdsRCxPQUFPLENBQUM4RCxLQUFSLENBQWN3QyxXQUFkLEVBSkgsQ0FMRixDQUpGLGVBZ0JFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFDRSxXQUFLLEVBQUU7QUFBRXBELFFBQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CSyxRQUFBQSxNQUFNLEVBQUU7QUFBNUIsT0FEVDtBQUVFLGVBQVMsK0JBQXdCdkQsT0FBTyxDQUFDeEUsR0FBaEM7QUFGWCxNQURGLGVBS0U7QUFDRSxXQUFLLEVBQUU7QUFBRXVILFFBQUFBLFFBQVEsRUFBRTtBQUFaLE9BRFQ7QUFFRSxlQUFTLDJCQUFvQi9DLE9BQU8sQ0FBQ0csR0FBNUI7QUFGWCxNQUxGLENBaEJGLGVBMEJFO0FBQUksV0FBSyxFQUFFO0FBQUV5RixRQUFBQSxTQUFTLEVBQUUsS0FBYjtBQUFvQkcsUUFBQUEsWUFBWSxFQUFFO0FBQWxDO0FBQVgsTUExQkYsZUEyQkU7QUFDRSxXQUFLLEVBQUU7QUFBRXhDLFFBQUFBLE1BQU0sRUFBRTtBQUFWLE9BRFQ7QUFFRSxlQUFTLEVBQUM7QUFGWixvQkFJRTtBQUNFLGVBQVMsRUFBQyxXQURaO0FBRUUsVUFBSSxFQUFDLFFBRlA7QUFHRSxrQkFBWSxFQUFFdkQsT0FBTyxDQUFDeEU7QUFIeEIsTUFKRixlQVNFO0FBQ0UsZUFBUyxFQUFDLGlDQURaO0FBRUUsV0FBSyxFQUFFO0FBQUV1SSxRQUFBQSxXQUFXLEVBQUU7QUFBZixPQUZUO0FBR0UsYUFBTyxFQUFFOUkseURBQXNCQTtBQUhqQyxXQVRGLGVBZ0JFO0FBQ0UsVUFBSSxFQUFDLFFBRFA7QUFFRSxrQkFBWSxFQUFDLEdBRmY7QUFHRSxlQUFTLEVBQUMsa0RBSFo7QUFJRSxXQUFLLEVBQUU7QUFBRThILFFBQUFBLFFBQVEsRUFBRTtBQUFaO0FBSlQsTUFoQkYsZUFzQkU7QUFDRSxlQUFTLEVBQUMsaUNBRFo7QUFFRSxXQUFLLEVBQUU7QUFBRWlCLFFBQUFBLFVBQVUsRUFBRTtBQUFkLE9BRlQ7QUFHRSxhQUFPLEVBQUUxSSwyREFBd0JBO0FBSG5DLFdBdEJGLGVBNkJFO0FBQ0UsYUFBTyxFQUFFO0FBQUEsZUFBTU8sdURBQWdCLEtBQUswRyxRQUFRLENBQUMsSUFBRCxDQUFiLEdBQW9CLElBQTFDO0FBQUEsT0FEWDtBQUVFLGVBQVMsRUFBQyxzQ0FGWjtBQUdFLFdBQUssRUFBRTtBQUNMeUIsUUFBQUEsVUFBVSxFQUFFLE1BRFA7QUFFTHVDLFFBQUFBLFVBQVUsRUFBRTtBQUZQO0FBSFQsb0JBUUU7QUFDRSxXQUFLLEVBQUU7QUFDTGxELFFBQUFBLFVBQVUsRUFBRSxNQURQO0FBRUxDLFFBQUFBLE1BQU0sRUFBRSxTQUZIO0FBR0xMLFFBQUFBLFFBQVEsRUFBRTtBQUhMLE9BRFQ7QUFNRSxlQUFTLEVBQUU7QUFOYiw4Q0FTRTtBQUNFLGVBQVMsRUFBQyxpQ0FEWjtBQUVFLFVBQUksRUFBQyxRQUZQO0FBR0UsV0FBSyxFQUFFO0FBQ0xDLFFBQUFBLEtBQUssRUFBRSxTQURGO0FBRUx6QyxRQUFBQSxLQUFLLEVBQUUsTUFGRjtBQUdMOEMsUUFBQUEsTUFBTSxFQUFFLE1BSEg7QUFJTE4sUUFBQUEsUUFBUSxFQUFFO0FBSkw7QUFIVCxvQkFVRTtBQUFNLGVBQVMsRUFBQztBQUFoQixNQVZGLENBVEYsQ0FSRixDQTdCRixDQTNCRixlQXdGRTtBQUNFLFdBQUssRUFBRTtBQUFFQyxRQUFBQSxLQUFLLEVBQUUsT0FBVDtBQUFrQkgsUUFBQUEsUUFBUSxFQUFFLE1BQTVCO0FBQW9DUSxRQUFBQSxNQUFNLEVBQUU7QUFBNUMsT0FEVDtBQUVFLGVBQVMsRUFBQztBQUZaLG9CQUlFO0FBQ0UsV0FBSyxFQUFFO0FBQUVnQixRQUFBQSxPQUFPLEVBQUUsR0FBWDtBQUFnQmdDLFFBQUFBLFVBQVUsRUFBRTtBQUE1QixPQURUO0FBRUUsZUFBUyxFQUFDO0FBRlosTUFKRixlQVFFO0FBQ0UsV0FBSyxFQUFFO0FBQUVoQyxRQUFBQSxPQUFPLEVBQUUsR0FBWDtBQUFnQmdDLFFBQUFBLFVBQVUsRUFBRTtBQUE1QixPQURUO0FBRUUsZUFBUyxFQUFDO0FBRlosTUFSRixDQXhGRixlQXFHRTtBQUNFLGVBQVMsRUFBQyxvREFEWjtBQUVFLFVBQUksRUFBQyxRQUZQO0FBR0UsV0FBSyxFQUFFO0FBQUVyRCxRQUFBQSxLQUFLLEVBQUUsU0FBVDtBQUFvQnpDLFFBQUFBLEtBQUssRUFBRSxNQUEzQjtBQUFtQzhDLFFBQUFBLE1BQU0sRUFBRTtBQUEzQztBQUhULG9CQUtFO0FBQU0sZUFBUyxFQUFDO0FBQWhCLE1BTEYsQ0FyR0YsZUE0R0U7QUFDRSxXQUFLLEVBQUU7QUFBRVIsUUFBQUEsUUFBUSxFQUFFLE9BQVo7QUFBcUJHLFFBQUFBLEtBQUssRUFBRSxTQUE1QjtBQUF1QzBDLFFBQUFBLFNBQVMsRUFBRTtBQUFsRDtBQURULE9BR0c1RixPQUFPLENBQUN3RyxnQkFIWCxDQTVHRixlQWlIRSw2REFqSEYsRUFtSEk7QUFBQSxhQUNBeEcsT0FBTyxDQUFDeUcsYUFBUixDQUFzQmxHLEdBQXRCLENBQTBCLFVBQUNtRyxZQUFELEVBQWV4TSxLQUFmO0FBQUEsNEJBQ3hCO0FBQUssYUFBRyxFQUFFQTtBQUFWLHdCQUNFO0FBQUssbUJBQVMsRUFBQztBQUFmLHdCQUNFO0FBQUssbUJBQVMsRUFBQztBQUFmLHdCQUNFO0FBQ0UsZUFBSyxFQUFFO0FBQUVnSixZQUFBQSxLQUFLLEVBQUUsU0FBVDtBQUFvQkgsWUFBQUEsUUFBUSxFQUFFO0FBQTlCLFdBRFQ7QUFFRSxtQkFBUyxFQUFDO0FBRlosVUFERixDQURGLGVBT0U7QUFDRSxlQUFLLEVBQUU7QUFBRUEsWUFBQUEsUUFBUSxFQUFFLE9BQVo7QUFBcUJHLFlBQUFBLEtBQUssRUFBRTtBQUE1QixXQURUO0FBRUUsbUJBQVMsRUFBQztBQUZaLHdCQUlFLGdFQUFPd0QsWUFBUCxDQUpGLENBUEYsQ0FERixDQUR3QjtBQUFBLE9BQTFCLENBREE7QUFBQSxLQUFELEVBbkhILGVBdUlFLDZEQXZJRixlQXdJRTtBQUNFLFdBQUssRUFBRTtBQUFFakcsUUFBQUEsS0FBSyxFQUFFLE1BQVQ7QUFBaUI4QyxRQUFBQSxNQUFNLEVBQUU7QUFBekIsT0FEVDtBQUVFLGVBQVMsRUFBQyxlQUZaO0FBR0UsU0FBRyxFQUFDLGtCQUhOO0FBSUUsU0FBRyxFQUFDO0FBSk4sTUF4SUYsZUE4SUU7QUFDRSxXQUFLLEVBQUU7QUFBRTlDLFFBQUFBLEtBQUssRUFBRSxNQUFUO0FBQWlCOEMsUUFBQUEsTUFBTSxFQUFFO0FBQXpCLE9BRFQ7QUFFRSxlQUFTLEVBQUMsZUFGWjtBQUdFLFNBQUcsRUFBQyxtQkFITjtBQUlFLFNBQUcsRUFBQztBQUpOLE1BOUlGLGVBb0pFO0FBQ0UsV0FBSyxFQUFFO0FBQUU5QyxRQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQjhDLFFBQUFBLE1BQU0sRUFBRTtBQUF6QixPQURUO0FBRUUsZUFBUyxFQUFDLGVBRlo7QUFHRSxTQUFHLEVBQUMsaUJBSE47QUFJRSxTQUFHLEVBQUM7QUFKTixNQXBKRixlQTBKRTtBQUNFLFdBQUssRUFBRTtBQUFFOUMsUUFBQUEsS0FBSyxFQUFFLE1BQVQ7QUFBaUI4QyxRQUFBQSxNQUFNLEVBQUU7QUFBekIsT0FEVDtBQUVFLGVBQVMsRUFBQyxVQUZaO0FBR0UsU0FBRyxFQUFDLGVBSE47QUFJRSxTQUFHLEVBQUM7QUFKTixNQTFKRixDQU5GLENBdEZGLGVBOFBFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFNLFdBQUssRUFBRTtBQUFFTCxRQUFBQSxLQUFLLEVBQUUsU0FBVDtBQUFvQkgsUUFBQUEsUUFBUSxFQUFFO0FBQTlCO0FBQWIsMkJBRUcvQyxPQUFPLENBQUM0RCxRQUFSLENBQWlCeUMsT0FBakIsQ0FBeUIsTUFBekIsRUFBaUMsRUFBakMsRUFBcUNBLE9BQXJDLENBQTZDLE9BQTdDLEVBQXNELEVBQXRELENBRkgsQ0FERixlQUtFO0FBQUksV0FBSyxFQUFFO0FBQUVuRCxRQUFBQSxLQUFLLEVBQUU7QUFBVCxPQUFYO0FBQWlDLGVBQVMsRUFBQztBQUEzQyxPQUNHbEQsT0FBTyxDQUFDOEQsS0FBUixDQUFjd0MsV0FBZCxFQURILENBTEYsQ0FERixlQVVFO0FBQUssV0FBSyxFQUFFO0FBQUVILFFBQUFBLFdBQVcsRUFBRSxLQUFmO0FBQXNCQyxRQUFBQSxZQUFZLEVBQUU7QUFBcEM7QUFBWixvQkFDRTtBQUFHLFVBQUksRUFBQztBQUFSLG9CQUNFO0FBQUssU0FBRyxFQUFFcEcsT0FBTyxDQUFDNEQsUUFBbEI7QUFBNEIsZUFBUyxFQUFDO0FBQXRDLE1BREYsQ0FERixDQVZGLGVBZUU7QUFBSyxXQUFLLEVBQUU7QUFBRXVDLFFBQUFBLFdBQVcsRUFBRSxLQUFmO0FBQXNCQyxRQUFBQSxZQUFZLEVBQUU7QUFBcEM7QUFBWixvQkFDRTtBQUNFLFdBQUssRUFBRTtBQUFFN0MsUUFBQUEsTUFBTSxFQUFFO0FBQVYsT0FEVDtBQUVFLGVBQVMsRUFBQztBQUZaLG9CQUlFO0FBQ0UsV0FBSyxFQUFFO0FBQUVMLFFBQUFBLEtBQUssRUFBRTtBQUFULE9BRFQ7QUFFRSxlQUFTLCtCQUF3QmxELE9BQU8sQ0FBQ3hFLEdBQWhDO0FBRlgsTUFKRixlQVFFO0FBQ0UsV0FBSyxFQUFFO0FBQUV1SCxRQUFBQSxRQUFRLEVBQUU7QUFBWixPQURUO0FBRUUsZUFBUywyQkFBb0IvQyxPQUFPLENBQUNHLEdBQTVCO0FBRlgsTUFSRixDQURGLGVBY0U7QUFBSSxXQUFLLEVBQUU7QUFBRXlGLFFBQUFBLFNBQVMsRUFBRSxLQUFiO0FBQW9CRyxRQUFBQSxZQUFZLEVBQUU7QUFBbEM7QUFBWCxNQWRGLGVBZUU7QUFDRSxXQUFLLEVBQUU7QUFBRXhDLFFBQUFBLE1BQU0sRUFBRTtBQUFWLE9BRFQ7QUFFRSxlQUFTLEVBQUM7QUFGWixvQkFJRTtBQUNFLGVBQVMsRUFBQyxXQURaO0FBRUUsVUFBSSxFQUFDLFFBRlA7QUFHRSxrQkFBWSxFQUFFdkQsT0FBTyxDQUFDeEU7QUFIeEIsTUFKRixlQVNFO0FBQ0UsZUFBUyxFQUFDLGlDQURaO0FBRUUsV0FBSyxFQUFFO0FBQUV1SSxRQUFBQSxXQUFXLEVBQUU7QUFBZixPQUZUO0FBR0UsYUFBTyxFQUFFLGlCQUFDNEMsS0FBRDtBQUFBLGVBQVcxTCw2REFBc0IsQ0FBQzBMLEtBQUQsQ0FBakM7QUFBQTtBQUhYLFdBVEYsZUFnQkU7QUFDRSxVQUFJLEVBQUMsUUFEUDtBQUVFLGtCQUFZLEVBQUMsR0FGZjtBQUdFLGVBQVMsRUFBQyxrREFIWjtBQUlFLFdBQUssRUFBRTtBQUFFNUQsUUFBQUEsUUFBUSxFQUFFO0FBQVo7QUFKVCxNQWhCRixlQXNCRTtBQUNFLGVBQVMsRUFBQyxpQ0FEWjtBQUVFLFdBQUssRUFBRTtBQUFFaUIsUUFBQUEsVUFBVSxFQUFFO0FBQWQsT0FGVDtBQUdFLGFBQU8sRUFBRSxpQkFBQzJDLEtBQUQ7QUFBQSxlQUFXckwsK0RBQXdCLENBQUNxTCxLQUFELENBQW5DO0FBQUE7QUFIWCxXQXRCRixlQTZCRTtBQUNFLGFBQU8sRUFBRTtBQUFBLGVBQU05Syx1REFBZ0IsS0FBSzBHLFFBQVEsQ0FBQyxJQUFELENBQWIsR0FBc0IsSUFBNUM7QUFBQSxPQURYO0FBRUUsZUFBUyxFQUFDLHNDQUZaO0FBR0UsV0FBSyxFQUFFO0FBQUV5QixRQUFBQSxVQUFVLEVBQUUsTUFBZDtBQUFzQnVDLFFBQUFBLFVBQVUsRUFBRTtBQUFsQztBQUhULG9CQUtFO0FBQ0UsZUFBUyxFQUFDLGlDQURaO0FBRUUsVUFBSSxFQUFDLFFBRlA7QUFHRSxXQUFLLEVBQUU7QUFBRXJELFFBQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CekMsUUFBQUEsS0FBSyxFQUFFLE1BQTNCO0FBQW1DOEMsUUFBQUEsTUFBTSxFQUFFO0FBQTNDO0FBSFQsb0JBS0U7QUFBTSxlQUFTLEVBQUM7QUFBaEIsTUFMRixDQUxGLGVBWUU7QUFBTSxXQUFLLEVBQUU7QUFBRUYsUUFBQUEsVUFBVSxFQUFFLE1BQWQ7QUFBc0JDLFFBQUFBLE1BQU0sRUFBRTtBQUE5QjtBQUFiLGdDQVpGLENBN0JGLENBZkYsZUE2REU7QUFDRSxXQUFLLEVBQUU7QUFBRUosUUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JILFFBQUFBLFFBQVEsRUFBRSxNQUE1QjtBQUFvQ1EsUUFBQUEsTUFBTSxFQUFFO0FBQTVDLE9BRFQ7QUFFRSxlQUFTLEVBQUM7QUFGWixvQkFJRTtBQUNFLFdBQUssRUFBRTtBQUFFZ0IsUUFBQUEsT0FBTyxFQUFFLEdBQVg7QUFBZ0JnQyxRQUFBQSxVQUFVLEVBQUU7QUFBNUIsT0FEVDtBQUVFLGVBQVMsRUFBQztBQUZaLE1BSkYsZUFRRTtBQUNFLFdBQUssRUFBRTtBQUFFaEMsUUFBQUEsT0FBTyxFQUFFLEdBQVg7QUFBZ0JnQyxRQUFBQSxVQUFVLEVBQUU7QUFBNUIsT0FEVDtBQUVFLGVBQVMsRUFBQztBQUZaLE1BUkYsQ0E3REYsZUEwRUU7QUFDRSxXQUFLLEVBQUU7QUFBRXhELFFBQUFBLFFBQVEsRUFBRSxPQUFaO0FBQXFCRyxRQUFBQSxLQUFLLEVBQUUsU0FBNUI7QUFBdUMwQyxRQUFBQSxTQUFTLEVBQUU7QUFBbEQ7QUFEVCxPQUdHNUYsT0FBTyxDQUFDd0csZ0JBSFgsQ0ExRUYsZUErRUUsNkRBL0VGLEVBaUZJO0FBQUEsYUFDQXhHLE9BQU8sQ0FBQ3lHLGFBQVIsQ0FBc0JsRyxHQUF0QixDQUEwQixVQUFDbUcsWUFBRCxFQUFleE0sS0FBZjtBQUFBLDRCQUN4QjtBQUFLLGFBQUcsRUFBRUEsS0FBVjtBQUFpQixtQkFBUyxFQUFDO0FBQTNCLHdCQUNFO0FBQUssbUJBQVMsRUFBQztBQUFmLHdCQUNFO0FBQ0UsZUFBSyxFQUFFO0FBQUVnSixZQUFBQSxLQUFLLEVBQUUsU0FBVDtBQUFvQkgsWUFBQUEsUUFBUSxFQUFFO0FBQTlCLFdBRFQ7QUFFRSxtQkFBUyxFQUFDO0FBRlosVUFERixDQURGLGVBT0U7QUFDRSxlQUFLLEVBQUU7QUFBRUEsWUFBQUEsUUFBUSxFQUFFLE9BQVo7QUFBcUJHLFlBQUFBLEtBQUssRUFBRTtBQUE1QixXQURUO0FBRUUsbUJBQVMsRUFBQztBQUZaLHdCQUlFLGdFQUFPd0QsWUFBUCxDQUpGLENBUEYsQ0FEd0I7QUFBQSxPQUExQixDQURBO0FBQUEsS0FBRCxFQWpGSCxlQW1HRSw2REFuR0YsZUFvR0UsNEVBQ0U7QUFDRSxXQUFLLEVBQUU7QUFBRWpHLFFBQUFBLEtBQUssRUFBRSxNQUFUO0FBQWlCOEMsUUFBQUEsTUFBTSxFQUFFO0FBQXpCLE9BRFQ7QUFFRSxlQUFTLEVBQUMsZUFGWjtBQUdFLFNBQUcsRUFBQyxrQkFITjtBQUlFLFNBQUcsRUFBQztBQUpOLE1BREYsZUFPRTtBQUNFLFdBQUssRUFBRTtBQUFFOUMsUUFBQUEsS0FBSyxFQUFFLE1BQVQ7QUFBaUI4QyxRQUFBQSxNQUFNLEVBQUU7QUFBekIsT0FEVDtBQUVFLGVBQVMsRUFBQyxlQUZaO0FBR0UsU0FBRyxFQUFDLG1CQUhOO0FBSUUsU0FBRyxFQUFDO0FBSk4sTUFQRixlQWFFO0FBQ0UsV0FBSyxFQUFFO0FBQUU5QyxRQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQjhDLFFBQUFBLE1BQU0sRUFBRTtBQUF6QixPQURUO0FBRUUsZUFBUyxFQUFDLGVBRlo7QUFHRSxTQUFHLEVBQUMsaUJBSE47QUFJRSxTQUFHLEVBQUM7QUFKTixNQWJGLGVBbUJFO0FBQ0UsV0FBSyxFQUFFO0FBQUU5QyxRQUFBQSxLQUFLLEVBQUUsTUFBVDtBQUFpQjhDLFFBQUFBLE1BQU0sRUFBRTtBQUF6QixPQURUO0FBRUUsZUFBUyxFQUFDLGVBRlo7QUFHRSxTQUFHLEVBQUMsZUFITjtBQUlFLFNBQUcsRUFBQztBQUpOLE1BbkJGLENBcEdGLENBZkYsQ0E5UEYsZUE2WUU7QUFDRSxXQUFLLEVBQUU7QUFBRVIsUUFBQUEsUUFBUSxFQUFFLE1BQVo7QUFBb0JHLFFBQUFBLEtBQUssRUFBRTtBQUEzQixPQURUO0FBRUUsZUFBUyxFQUFDO0FBRlosb0JBSUU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLFFBQUUsRUFBQyxVQUFSO0FBQW1CLGVBQVMsRUFBQztBQUE3QixvQkFDRTtBQUNFLFdBQUssRUFBRTtBQUFFYSxRQUFBQSxXQUFXLEVBQUUsTUFBZjtBQUF1QlQsUUFBQUEsTUFBTSxFQUFFO0FBQS9CLE9BRFQ7QUFFRSxlQUFTLEVBQUMsNEJBRlo7QUFHRSxhQUFPLEVBQUU7QUFBQSxlQUFNakssNkNBQU0sQ0FBQyxzQkFBRCxDQUFaO0FBQUE7QUFIWCxxQkFERixlQVFFO0FBQ0UsV0FBSyxFQUFFO0FBQUUwSyxRQUFBQSxXQUFXLEVBQUUsTUFBZjtBQUF1QlQsUUFBQUEsTUFBTSxFQUFFO0FBQS9CLE9BRFQ7QUFFRSxlQUFTLEVBQUMsNEJBRlo7QUFHRSxhQUFPLEVBQUU7QUFBQSxlQUFNakssNkNBQU0sQ0FBQyxjQUFELENBQVo7QUFBQTtBQUhYLHVCQVJGLGVBZUU7QUFDRSxXQUFLLEVBQUU7QUFBRTBLLFFBQUFBLFdBQVcsRUFBRSxNQUFmO0FBQXVCVCxRQUFBQSxNQUFNLEVBQUU7QUFBL0IsT0FEVDtBQUVFLGVBQVMsRUFBQyw0QkFGWjtBQUdFLGFBQU8sRUFBRTtBQUFBLGVBQU1qSyw2Q0FBTSxDQUFDLHNCQUFELENBQVo7QUFBQTtBQUhYLHVCQWZGLGVBc0JFO0FBQ0UsV0FBSyxFQUFFO0FBQUUwSyxRQUFBQSxXQUFXLEVBQUUsTUFBZjtBQUF1QlQsUUFBQUEsTUFBTSxFQUFFO0FBQS9CLE9BRFQ7QUFFRSxlQUFTLEVBQUMsNEJBRlo7QUFHRSxhQUFPLEVBQUU7QUFBQSxlQUFNakssNkNBQU0sQ0FBQyxnQkFBRCxDQUFaO0FBQUE7QUFIWCxvQ0F0QkYsRUE4QkksWUFBTTtBQUNOLFVBQUkyRyxPQUFPLENBQUNwQyxRQUFaLEVBQXNCO0FBQ3BCLDRCQUNFO0FBQ0UsZUFBSyxFQUFFO0FBQUUwRixZQUFBQSxNQUFNLEVBQUU7QUFBVixXQURUO0FBRUUsbUJBQVMsRUFBQyw0QkFGWjtBQUdFLGlCQUFPLEVBQUU7QUFBQSxtQkFBTWpLLDZDQUFNLENBQUMsY0FBRCxDQUFaO0FBQUE7QUFIWCwwQkFERjtBQVNEO0FBQ0YsS0FaQSxFQTlCSCxDQURGLENBSkYsZUFrREU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLFdBQUssRUFBRTtBQUFFMEssUUFBQUEsV0FBVyxFQUFFO0FBQWY7QUFBWixvQkFDRTtBQUFHLFFBQUUsRUFBQyxzQkFBTjtBQUE2QixlQUFTLEVBQUM7QUFBdkMsT0FDRy9ELE9BQU8sQ0FBQzRHLGVBRFgsQ0FERixDQURGLGVBT0U7QUFBSyxXQUFLLEVBQUU7QUFBRTdDLFFBQUFBLFdBQVcsRUFBRTtBQUFmO0FBQVosb0JBQ0U7QUFDRSxRQUFFLEVBQUMsY0FETDtBQUVFLGVBQVMsRUFBQztBQUZaLG9CQUlFLGlFQUNJO0FBQUEsYUFDQThDLE1BQU0sQ0FBQ0MsSUFBUCxDQUFZOUcsT0FBTyxDQUFDK0csUUFBcEIsRUFBOEJ4RyxHQUE5QixDQUFrQyxVQUFDMUMsR0FBRCxFQUFNM0QsS0FBTjtBQUFBLDRCQUNoQztBQUFJLGFBQUcsRUFBRUE7QUFBVCx3QkFDRSw4REFBSzJELEdBQUwsQ0FERixlQUVFLDhEQUFLbUMsT0FBTyxDQUFDK0csUUFBUixDQUFpQmxKLEdBQWpCLENBQUwsQ0FGRixDQURnQztBQUFBLE9BQWxDLENBREE7QUFBQSxLQUFELEVBREgsQ0FKRixDQURGLENBUEYsZUF3QkU7QUFBSyxXQUFLLEVBQUU7QUFBRWtHLFFBQUFBLFdBQVcsRUFBRTtBQUFmO0FBQVosb0JBQ0U7QUFDRSxRQUFFLEVBQUMsc0JBREw7QUFFRSxlQUFTLEVBQUM7QUFGWixvQkFJRSxpRUFDSTtBQUFBLGFBQ0E4QyxNQUFNLENBQUNDLElBQVAsQ0FBWTlHLE9BQU8sQ0FBQ2dILGVBQXBCLEVBQXFDekcsR0FBckMsQ0FBeUMsVUFBQzFDLEdBQUQsRUFBTTNELEtBQU47QUFBQSw0QkFDdkM7QUFBSSxhQUFHLEVBQUVBO0FBQVQsd0JBQ0UsOERBQUsyRCxHQUFMLENBREYsZUFFRSw4REFBS21DLE9BQU8sQ0FBQ2dILGVBQVIsQ0FBd0JuSixHQUF4QixDQUFMLENBRkYsQ0FEdUM7QUFBQSxPQUF6QyxDQURBO0FBQUEsS0FBRCxFQURILENBSkYsQ0FERixDQXhCRixlQXlDRTtBQUFLLFdBQUssRUFBRTtBQUFFa0csUUFBQUEsV0FBVyxFQUFFO0FBQWY7QUFBWixvQkFDRTtBQUNFLFFBQUUsRUFBQyxnQkFETDtBQUVFLGVBQVMsRUFBQztBQUZaLG9CQUlFLGlFQUNJO0FBQUEsYUFDQThDLE1BQU0sQ0FBQ0ksT0FBUCxDQUFlakgsT0FBTyxDQUFDa0gsU0FBdkIsRUFBa0MzRyxHQUFsQyxDQUFzQyxVQUFDMUMsR0FBRCxFQUFNM0QsS0FBTjtBQUFBLDRCQUNwQztBQUFJLGFBQUcsRUFBRUE7QUFBVCx3QkFDRSw4REFBSzJELEdBQUwsQ0FERixlQUVFLDhEQUFLbUMsT0FBTyxDQUFDa0gsU0FBUixDQUFrQnJKLEdBQWxCLENBQUwsQ0FGRixDQURvQztBQUFBLE9BQXRDLENBREE7QUFBQSxLQUFELEVBREgsQ0FKRixDQURGLENBekNGLEVBMERJLFlBQU07QUFDTixVQUFJbUMsT0FBTyxDQUFDcEMsUUFBWixFQUFzQjtBQUNwQiw0QkFDRTtBQUFLLFlBQUUsRUFBQyxjQUFSO0FBQXVCLG1CQUFTLEVBQUM7QUFBakMsd0JBQ0UsNEVBQ0U7QUFDRSxtQkFBUyxFQUFDLDBFQURaO0FBRUUsaUJBQU8sRUFBRSxpQkFBQytJLEtBQUQ7QUFBQSxtQkFDUHBLLDBFQUFtQyxDQUFDb0ssS0FBSyxDQUFDUSxNQUFQLENBRDVCO0FBQUE7QUFGWCwrQ0FPRTtBQUFLLG1CQUFTLEVBQUM7QUFBZixVQVBGLENBREYsZUFVRTtBQUFLLG1CQUFTLEVBQUM7QUFBZix3QkFDRTtBQUNFLG1CQUFTLEVBQUMsZ0RBRFo7QUFFRSxjQUFJLEVBQUMsUUFGUDtBQUdFLGVBQUssRUFBRTtBQUNMakUsWUFBQUEsS0FBSyxFQUFFLFNBREY7QUFFTHpDLFlBQUFBLEtBQUssRUFBRSxNQUZGO0FBR0w4QyxZQUFBQSxNQUFNLEVBQUU7QUFISDtBQUhULHdCQVNFO0FBQU0sbUJBQVMsRUFBQztBQUFoQixVQVRGLENBREYsZUFZRTtBQUFPLG1CQUFTLEVBQUM7QUFBakIsVUFaRixDQVZGLENBREYsQ0FERjtBQTZCRDtBQUNGLEtBaENBLEVBMURILENBbERGLENBN1lGLGVBNGhCRTtBQUFLLFdBQUssRUFBRTtBQUFFcUMsUUFBQUEsU0FBUyxFQUFFO0FBQWIsT0FBWjtBQUFrQyxlQUFTLEVBQUM7QUFBNUMsb0JBQ0U7QUFDRSxRQUFFLEVBQUMsMkNBREw7QUFFRSxlQUFTLEVBQUMsb0RBRlo7QUFHRSxhQUFPLEVBQUUsaUJBQUNlLEtBQUQ7QUFBQSxlQUFXdEssMkRBQW9CLENBQUNzSyxLQUFLLENBQUNRLE1BQVAsQ0FBL0I7QUFBQTtBQUhYLG1DQU1FO0FBQ0UsV0FBSyxFQUFDLDRCQURSO0FBRUUsV0FBSyxFQUFDLElBRlI7QUFHRSxZQUFNLEVBQUMsSUFIVDtBQUlFLFVBQUksRUFBQyxjQUpQO0FBS0UsZUFBUyxFQUFDLG9CQUxaO0FBTUUsYUFBTyxFQUFDO0FBTlYsb0JBUUU7QUFDRSxjQUFRLEVBQUMsU0FEWDtBQUVFLE9BQUMsRUFBQztBQUZKLE1BUkYsQ0FORixDQURGLGVBcUJFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRSw2REFBSW5ILE9BQU8sQ0FBQzRHLGVBQVosQ0FERixDQURGLENBckJGLGVBMEJFO0FBQ0UsUUFBRSxFQUFDLG1DQURMO0FBRUUsZUFBUyxFQUFDLG9EQUZaO0FBR0UsYUFBTyxFQUFFLGlCQUFDRCxLQUFEO0FBQUEsZUFBV3RLLDJEQUFvQixDQUFDc0ssS0FBSyxDQUFDUSxNQUFQLENBQS9CO0FBQUE7QUFIWCxvQkFLRSw2RUFMRixlQU1FO0FBQ0UsV0FBSyxFQUFDLDRCQURSO0FBRUUsV0FBSyxFQUFDLElBRlI7QUFHRSxZQUFNLEVBQUMsSUFIVDtBQUlFLFVBQUksRUFBQyxjQUpQO0FBS0UsZUFBUyxFQUFDLG9CQUxaO0FBTUUsYUFBTyxFQUFDO0FBTlYsb0JBUUU7QUFDRSxjQUFRLEVBQUMsU0FEWDtBQUVFLE9BQUMsRUFBQztBQUZKLE1BUkYsQ0FORixDQTFCRixlQThDRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFBTyxlQUFTLEVBQUM7QUFBakIsb0JBQ0UsaUVBQ0k7QUFBQSxhQUNBTixNQUFNLENBQUNDLElBQVAsQ0FBWTlHLE9BQU8sQ0FBQytHLFFBQXBCLEVBQThCeEcsR0FBOUIsQ0FBa0MsVUFBQzFDLEdBQUQsRUFBTTNELEtBQU47QUFBQSw0QkFDaEM7QUFBSSxhQUFHLEVBQUVBO0FBQVQsd0JBQ0UsOERBQUsyRCxHQUFMLENBREYsZUFFRSw4REFBS21DLE9BQU8sQ0FBQytHLFFBQVIsQ0FBaUJsSixHQUFqQixDQUFMLENBRkYsQ0FEZ0M7QUFBQSxPQUFsQyxDQURBO0FBQUEsS0FBRCxFQURILENBREYsQ0FERixDQURGLENBOUNGLGVBNkRFO0FBQ0UsUUFBRSxFQUFDLDJDQURMO0FBRUUsZUFBUyxFQUFDLG9EQUZaO0FBR0UsYUFBTyxFQUFFLGlCQUFDOEksS0FBRDtBQUFBLGVBQVd0SywyREFBb0IsQ0FBQ3NLLEtBQUssQ0FBQ1EsTUFBUCxDQUEvQjtBQUFBO0FBSFgsb0JBS0UsNkVBTEYsZUFNRTtBQUNFLFdBQUssRUFBQyw0QkFEUjtBQUVFLFdBQUssRUFBQyxJQUZSO0FBR0UsWUFBTSxFQUFDLElBSFQ7QUFJRSxVQUFJLEVBQUMsY0FKUDtBQUtFLGVBQVMsRUFBQyxvQkFMWjtBQU1FLGFBQU8sRUFBQztBQU5WLG9CQVFFO0FBQ0UsY0FBUSxFQUFDLFNBRFg7QUFFRSxPQUFDLEVBQUM7QUFGSixNQVJGLENBTkYsQ0E3REYsZUFpRkU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQ0UsUUFBRSxFQUFDLDRCQURMO0FBRUUsZUFBUyxFQUFDLHdCQUZaO0FBR0UsYUFBTyxFQUFFLGlCQUFDUixLQUFEO0FBQUEsZUFBV1MsMkJBQTJCLENBQUNULEtBQUssQ0FBQ1EsTUFBUCxDQUF0QztBQUFBO0FBSFgsb0JBS0UsaUVBQ0k7QUFBQSxhQUNBTixNQUFNLENBQUNDLElBQVAsQ0FBWTlHLE9BQU8sQ0FBQ2dILGVBQXBCLEVBQXFDekcsR0FBckMsQ0FBeUMsVUFBQzFDLEdBQUQsRUFBTTNELEtBQU47QUFBQSw0QkFDdkM7QUFBSSxhQUFHLEVBQUVBO0FBQVQsd0JBQ0UsOERBQUsyRCxHQUFMLENBREYsZUFFRSw4REFBS21DLE9BQU8sQ0FBQ2dILGVBQVIsQ0FBd0JuSixHQUF4QixDQUFMLENBRkYsQ0FEdUM7QUFBQSxPQUF6QyxDQURBO0FBQUEsS0FBRCxFQURILENBTEYsQ0FERixDQURGLENBakZGLGVBb0dFO0FBQ0UsUUFBRSxFQUFDLHFDQURMO0FBRUUsZUFBUyxFQUFDLG9EQUZaO0FBR0UsYUFBTyxFQUFFLGlCQUFDOEksS0FBRDtBQUFBLGVBQVd0SywyREFBb0IsQ0FBQ3NLLEtBQUssQ0FBQ1EsTUFBUCxDQUEvQjtBQUFBO0FBSFgsb0JBS0UsMEZBTEYsZUFNRTtBQUNFLFdBQUssRUFBQyw0QkFEUjtBQUVFLFdBQUssRUFBQyxJQUZSO0FBR0UsWUFBTSxFQUFDLElBSFQ7QUFJRSxVQUFJLEVBQUMsY0FKUDtBQUtFLGVBQVMsRUFBQyxvQkFMWjtBQU1FLGFBQU8sRUFBQztBQU5WLG9CQVFFO0FBQ0UsY0FBUSxFQUFDLFNBRFg7QUFFRSxPQUFDLEVBQUM7QUFGSixNQVJGLENBTkYsQ0FwR0YsZUF3SEU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQU8sZUFBUyxFQUFDO0FBQWpCLG9CQUNFLGlFQUNJO0FBQUEsYUFDQU4sTUFBTSxDQUFDQyxJQUFQLENBQVk5RyxPQUFPLENBQUNrSCxTQUFwQixFQUErQjNHLEdBQS9CLENBQW1DLFVBQUMxQyxHQUFELEVBQU0zRCxLQUFOO0FBQUEsNEJBQ2pDO0FBQUksYUFBRyxFQUFFQTtBQUFULHdCQUNFLDhEQUFLMkQsR0FBTCxDQURGLGVBRUUsOERBQUttQyxPQUFPLENBQUNrSCxTQUFSLENBQWtCckosR0FBbEIsQ0FBTCxDQUZGLENBRGlDO0FBQUEsT0FBbkMsQ0FEQTtBQUFBLEtBQUQsRUFESCxDQURGLENBREYsQ0FERixDQXhIRixFQXdJSSxZQUFNO0FBQ04sVUFBSW1DLE9BQU8sQ0FBQ3BDLFFBQVosRUFBc0I7QUFDcEIsNEJBQ0UsbUhBQ0U7QUFDRSxZQUFFLEVBQUMsbUNBREw7QUFFRSxtQkFBUyxFQUFDLG9EQUZaO0FBR0UsaUJBQU8sRUFBRSxpQkFBQytJLEtBQUQ7QUFBQSxtQkFBV3RLLDJEQUFvQixDQUFDc0ssS0FBSyxDQUFDUSxNQUFQLENBQS9CO0FBQUE7QUFIWCx3QkFLRSw0RUFMRixlQU1FO0FBQ0UsZUFBSyxFQUFDLDRCQURSO0FBRUUsZUFBSyxFQUFDLElBRlI7QUFHRSxnQkFBTSxFQUFDLElBSFQ7QUFJRSxjQUFJLEVBQUMsY0FKUDtBQUtFLG1CQUFTLEVBQUMsb0JBTFo7QUFNRSxpQkFBTyxFQUFDO0FBTlYsd0JBUUU7QUFDRSxrQkFBUSxFQUFDLFNBRFg7QUFFRSxXQUFDLEVBQUM7QUFGSixVQVJGLENBTkYsQ0FERixlQXFCRTtBQUFLLG1CQUFTLEVBQUM7QUFBZix3QkFDRTtBQUFLLG1CQUFTLEVBQUM7QUFBZix3QkFDRTtBQUNFLGVBQUssRUFBRTtBQUFFcEUsWUFBQUEsUUFBUSxFQUFFO0FBQVosV0FEVDtBQUVFLG1CQUFTLEVBQUMsMEVBRlo7QUFHRSxpQkFBTyxFQUFFLGlCQUFDNEQsS0FBRDtBQUFBLG1CQUNQcEssMEVBQW1DLENBQUNvSyxLQUFLLENBQUNRLE1BQVAsQ0FENUI7QUFBQTtBQUhYLCtDQVFFO0FBQUssbUJBQVMsRUFBQztBQUFmLFVBUkYsQ0FERixlQVdFO0FBQUssbUJBQVMsRUFBQztBQUFmLHdCQUNFO0FBQ0UsbUJBQVMsRUFBQyxnREFEWjtBQUVFLGNBQUksRUFBQyxRQUZQO0FBR0UsZUFBSyxFQUFFO0FBQ0xqRSxZQUFBQSxLQUFLLEVBQUUsU0FERjtBQUVMekMsWUFBQUEsS0FBSyxFQUFFLE1BRkY7QUFHTDhDLFlBQUFBLE1BQU0sRUFBRTtBQUhIO0FBSFQsd0JBU0U7QUFBTSxtQkFBUyxFQUFDO0FBQWhCLFVBVEYsQ0FERixlQVlFO0FBQU8sbUJBQVMsRUFBQztBQUFqQixVQVpGLENBWEYsQ0FERixDQXJCRixDQURGO0FBb0REO0FBQ0YsS0F2REEsRUF4SUgsQ0E1aEJGLENBREY7QUFndUJELEdBanVCTSxNQWl1QkE7QUFDTCx3QkFBTyxrREFBQyxtREFBRCxPQUFQO0FBQ0Q7QUFDRixDQWp3QkQ7O0FBbXdCQSxpRUFBZTdCLE9BQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbHhCQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTTJGLFdBQVcsR0FBRyxTQUFkQSxXQUFjLE9BQWU7QUFBQSxNQUFickgsT0FBYSxRQUFiQSxPQUFhO0FBRS9Cb0IsRUFBQUEsZ0RBQVMsQ0FBQztBQUFBLFdBQUtyQixpREFBVyxDQUFDQyxPQUFELENBQWhCO0FBQUEsR0FBRCxFQUEyQixFQUEzQixDQUFUO0FBRUEsc0JBQ0E7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDUTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNJO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0ksaURBQUMsa0RBQUQ7QUFBTSxNQUFFLEVBQUU7QUFBQ3ZDLE1BQUFBLFFBQVEsRUFBRXVDLE9BQU8sQ0FBQ3hFLEdBQW5CO0FBQXdCbUksTUFBQUEsS0FBSyxFQUFFM0Q7QUFBL0I7QUFBVixrQkFDQTtBQUFLLFNBQUssRUFBRTtBQUFDdUQsTUFBQUEsTUFBTSxFQUFDO0FBQVIsS0FBWjtBQUE4QixPQUFHLEVBQUV2RCxPQUFPLENBQUM0RCxRQUEzQztBQUFxRCxhQUFTLEVBQUM7QUFBL0QsSUFEQSxDQURKLENBREosZUFNSTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNJLGlEQUFDLGtEQUFEO0FBQU0sYUFBUyxFQUFDLFlBQWhCO0FBQTZCLE1BQUUsRUFBRTtBQUFDbkcsTUFBQUEsUUFBUSxFQUFFdUMsT0FBTyxDQUFDeEUsR0FBbkI7QUFBd0JtSSxNQUFBQSxLQUFLLEVBQUUzRDtBQUEvQjtBQUFqQyxrQkFDQTtBQUFJLFNBQUssRUFBRTtBQUFDa0QsTUFBQUEsS0FBSyxFQUFDO0FBQVAsS0FBWDtBQUE4QixhQUFTLEVBQUM7QUFBeEMsS0FBa0VsRCxPQUFPLENBQUM4RCxLQUExRSxDQURBLENBREosZUFJSTtBQUFHLFNBQUssRUFBRTtBQUFDZixNQUFBQSxRQUFRLEVBQUMsTUFBVjtBQUFrQkcsTUFBQUEsS0FBSyxFQUFDO0FBQXhCO0FBQVYsS0FBK0NsRCxPQUFPLENBQUN3RyxnQkFBdkQsQ0FKSixlQUtJO0FBQUssYUFBUyxFQUFDO0FBQWYsSUFMSixlQU1JO0FBQUssU0FBSyxFQUFFO0FBQUNqRCxNQUFBQSxNQUFNLEVBQUM7QUFBUixLQUFaO0FBQTZCLGFBQVMsRUFBQztBQUF2QyxrQkFDSTtBQUFNLFNBQUssRUFBRTtBQUFDTCxNQUFBQSxLQUFLLEVBQUM7QUFBUCxLQUFiO0FBQWdDLGFBQVMsRUFBQztBQUExQyxJQURKLGVBRUk7QUFBTSxTQUFLLEVBQUU7QUFBQ0gsTUFBQUEsUUFBUSxFQUFDO0FBQVYsS0FBYjtBQUFtQyxhQUFTLEVBQUM7QUFBN0MsSUFGSixDQU5KLGVBVUk7QUFBSyxTQUFLLEVBQUU7QUFBQ1EsTUFBQUEsTUFBTSxFQUFDO0FBQVIsS0FBWjtBQUE2QixhQUFTLEVBQUM7QUFBdkMsa0JBQ0ksaURBQUMsa0RBQUQ7QUFBTSxhQUFTLEVBQUMsVUFBaEI7QUFBMkIsU0FBSyxFQUFFO0FBQUNNLE1BQUFBLGNBQWMsRUFBQztBQUFoQixLQUFsQztBQUEyRCxNQUFFLEVBQUU7QUFBQ3BHLE1BQUFBLFFBQVEsRUFBRXVDLE9BQU8sQ0FBQ3hFLEdBQW5CO0FBQXdCbUksTUFBQUEsS0FBSyxFQUFFM0Q7QUFBL0I7QUFBL0QsdUJBREosQ0FWSixlQWVJO0FBQUssU0FBSyxFQUFFO0FBQUNrRCxNQUFBQSxLQUFLLEVBQUMsT0FBUDtBQUFnQkgsTUFBQUEsUUFBUSxFQUFDLE1BQXpCO0FBQWlDUSxNQUFBQSxNQUFNLEVBQUM7QUFBeEMsS0FBWjtBQUE2RCxhQUFTLEVBQUM7QUFBdkUsa0JBQ0k7QUFBRyxTQUFLLEVBQUU7QUFBQ2dCLE1BQUFBLE9BQU8sRUFBQyxHQUFUO0FBQWNnQyxNQUFBQSxVQUFVLEVBQUU7QUFBMUIsS0FBVjtBQUFxRCxhQUFTLEVBQUM7QUFBL0QsSUFESixlQUVJO0FBQU0sU0FBSyxFQUFFO0FBQUNoQyxNQUFBQSxPQUFPLEVBQUMsR0FBVDtBQUFjZ0MsTUFBQUEsVUFBVSxFQUFFO0FBQTFCLEtBQWI7QUFBd0QsYUFBUyxFQUFDO0FBQWxFLElBRkosQ0FmSixDQU5KLENBRFIsQ0FEQTtBQStCQyxDQW5DTDs7QUFxQ0EsaUVBQWVjLFdBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU0vQixlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLE9BQWdCO0FBQUEsTUFBZGhGLFFBQWMsUUFBZEEsUUFBYztBQUV0QyxNQUFNdEMsUUFBUSxHQUFHc0osNkNBQU0sQ0FBQyxJQUFELENBQXZCO0FBQ0EsTUFBTUUsWUFBWSxHQUFHRiw2Q0FBTSxDQUFDLElBQUQsQ0FBM0I7O0FBRUEsV0FBU0csbUJBQVQsR0FBOEI7QUFFNUJ6SixJQUFBQSxRQUFRLENBQUMwSixPQUFULENBQWlCdE8sU0FBakIsQ0FBMkIyRCxNQUEzQixDQUFrQyxVQUFsQztBQUNBeUssSUFBQUEsWUFBWSxDQUFDRSxPQUFiLENBQXFCQyxJQUFyQjtBQUNEOztBQUVELFdBQVNDLGVBQVQsR0FBMEI7QUFFeEI3SixJQUFBQSxvREFBYyxDQUFDQyxRQUFRLENBQUMwSixPQUFWLENBQWQ7QUFDQTFKLElBQUFBLFFBQVEsQ0FBQzBKLE9BQVQsQ0FBaUJDLElBQWpCO0FBQ0Q7O0FBRUR2RyxFQUFBQSxnREFBUyxDQUFDLFlBQU07QUFFWmYsSUFBQUEsa0RBQVksQ0FBQ0MsUUFBRCxDQUFaO0FBQ0FuQyxJQUFBQSxnREFBVTtBQUNiLEdBSlEsRUFJTixFQUpNLENBQVQ7QUFNQSxzQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQ0UsTUFBRSxFQUFDLGlCQURMO0FBRUUsYUFBUyxFQUFDLGdCQUZaO0FBR0Usb0JBQWEsVUFIZjtBQUlFLHdCQUFpQixPQUpuQjtBQUtFLG9CQUFhO0FBTGYsa0JBT0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNJLFlBQU07QUFFTixRQUFJbUMsUUFBSixFQUFjO0FBRVosYUFBT0EsUUFBUSxDQUFDQyxHQUFULENBQWEsVUFBQ1AsT0FBRCxFQUFVOUYsS0FBVjtBQUFBLDRCQUFvQixpREFBQyx5REFBRDtBQUFxQixpQkFBTyxFQUFFOEYsT0FBOUI7QUFBdUMsYUFBRyxFQUFFOUY7QUFBNUMsVUFBcEI7QUFBQSxPQUFiLENBQVA7QUFDRDtBQUNGLEdBTkEsRUFESCxDQVBGLGVBaUJFO0FBQ0UsUUFBSSxFQUFDLGtCQURQO0FBRUUsYUFBUyxFQUFDLHNDQUZaO0FBR0UscUJBQWMsTUFIaEI7QUFJRSxRQUFJLEVBQUMsUUFKUDtBQUtFLFdBQU8sRUFBRXVOLG1CQUxYO0FBTUUsWUFBUSxFQUFFLENBQUMsQ0FOYjtBQU9FLE9BQUcsRUFBRUQ7QUFQUCxrQkFTRTtBQUNFLGFBQVMsRUFBQyw0QkFEWjtBQUVFLG1CQUFZO0FBRmQsSUFURixDQWpCRixlQStCRSwyRUFDRTtBQUNFLFFBQUksRUFBQyxrQkFEUDtBQUVFLGFBQVMsRUFBQyxzQ0FGWjtBQUdFLHFCQUFjLE1BSGhCO0FBSUUsUUFBSSxFQUFDLFFBSlA7QUFLRSxXQUFPLEVBQUVJLGVBTFg7QUFNRSxZQUFRLEVBQUUsQ0FBQyxDQU5iO0FBT0UsT0FBRyxFQUFFNUo7QUFQUCxrQkFTRTtBQUNFLGFBQVMsRUFBQyw0QkFEWjtBQUVFLG1CQUFZO0FBRmQsSUFURixDQURGLENBL0JGLENBREYsQ0FERjtBQW9ERCxDQTNFRDs7QUE0RUEsaUVBQWVzSCxlQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakZBO0FBQ0E7O0FBRUEsSUFBTWlDLG1CQUFtQixHQUFHLFNBQXRCQSxtQkFBc0IsT0FBaUI7QUFBQSxNQUFkdkgsT0FBYyxRQUFkQSxPQUFjO0FBRTNDLE1BQUk4QyxPQUFPLEdBQUdILDREQUFVLEVBQXhCO0FBRUF2QixFQUFBQSxnREFBUyxDQUFFO0FBQUEsV0FBTXBILENBQUMsQ0FBQyxvQ0FBRCxDQUFELENBQXdDNk4sS0FBeEMsQ0FBOEMsVUFBQWxCLEtBQUssRUFBSTtBQUN0RSxVQUFJWCxRQUFRLEdBQUc4QixJQUFJLENBQUNDLEtBQUwsQ0FBV3BCLEtBQUssQ0FBQ1EsTUFBTixDQUFhbkssRUFBeEIsQ0FBZjs7QUFDQThGLE1BQUFBLE9BQU8sQ0FBQzBCLElBQVIsQ0FBYTtBQUFDL0csUUFBQUEsUUFBUSxFQUFFdUksUUFBUSxDQUFDeEssR0FBcEI7QUFBeUJtSSxRQUFBQSxLQUFLLEVBQUVxQztBQUFoQyxPQUFiO0FBQ0QsS0FIZ0IsQ0FBTjtBQUFBLEdBQUYsRUFHTCxFQUhLLENBQVQ7QUFLQSxzQkFDRTtBQUFLLGFBQVMsRUFBQyxlQUFmO0FBQStCLFNBQUssRUFBRTtBQUFFMUMsTUFBQUEsTUFBTSxFQUFFO0FBQVY7QUFBdEMsa0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxTQUFLLEVBQUU7QUFBRUMsTUFBQUEsTUFBTSxFQUFFO0FBQVYsS0FEVDtBQUVFLE9BQUcsRUFBRXZELE9BQU8sQ0FBQzRELFFBRmY7QUFHRSxhQUFTLEVBQUMsV0FIWjtBQUlFLE1BQUUsRUFBRWtFLElBQUksQ0FBQ0UsU0FBTCxDQUFlaEksT0FBZjtBQUpOLElBREYsQ0FERixlQVNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFBSSxNQUFFLEVBQUU4SCxJQUFJLENBQUNFLFNBQUwsQ0FBZWhJLE9BQWYsQ0FBUjtBQUFpQyxTQUFLLEVBQUU7QUFBRWtELE1BQUFBLEtBQUssRUFBRTtBQUFULEtBQXhDO0FBQThELGFBQVMsRUFBQztBQUF4RSxLQUNHbEQsT0FBTyxDQUFDOEQsS0FEWCxDQURGLGVBSUU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUFFWixNQUFBQSxLQUFLLEVBQUU7QUFBVCxLQURUO0FBRUUsYUFBUyx1QkFBZ0JsRCxPQUFPLENBQUN4RSxHQUF4QjtBQUZYLElBREYsZUFLRTtBQUNFLFNBQUssRUFBRTtBQUFFdUgsTUFBQUEsUUFBUSxFQUFFO0FBQVosS0FEVDtBQUVFLGFBQVMsMkJBQW9CL0MsT0FBTyxDQUFDeEUsR0FBNUI7QUFGWCxJQUxGLENBSkYsZUFjSTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQU0sTUFBRSxFQUFFc00sSUFBSSxDQUFDRSxTQUFMLENBQWVoSSxPQUFmLENBQVY7QUFBbUMsU0FBSyxFQUFFO0FBQUU2RCxNQUFBQSxjQUFjLEVBQUU7QUFBbEIsS0FBMUM7QUFBc0UsYUFBUyxFQUFDO0FBQWhGLHVCQURGLENBZEosQ0FURixDQURGLENBREYsQ0FERjtBQW9DRCxDQTdDRDs7QUE4Q0EsaUVBQWUwRCxtQkFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU01RixRQUFRLEdBQUcsU0FBWEEsUUFBVyxPQUF5QjtBQUFBLE1BQXZCckIsUUFBdUIsUUFBdkJBLFFBQXVCO0FBQUEsTUFBYnlCLE9BQWEsUUFBYkEsT0FBYTtBQUV4QyxzQkFDRSwrREFDR0EsT0FBTyxnQkFDTixpREFBQyw2Q0FBRCxPQURNLEdBRUp6QixRQUFRLGdCQUNWLGlIQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0UsaURBQUMsa0RBQUQ7QUFBTSxNQUFFLEVBQUM7QUFBVCxrQkFDRTtBQUNFLGFBQVMsRUFBQyxxQkFEWjtBQUVFLFNBQUssRUFBRTtBQUFFNEMsTUFBQUEsS0FBSyxFQUFFO0FBQVQ7QUFGVCxJQURGLENBREYsZUFPRTtBQUNFLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxLQUFLLEVBQUUsU0FBVDtBQUFvQkgsTUFBQUEsUUFBUSxFQUFFLE1BQTlCO0FBQXNDa0IsTUFBQUEsVUFBVSxFQUFFO0FBQWxEO0FBRFQsZ0JBUEYsQ0FERixlQWVFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxTQUFLLEVBQUU7QUFDTHhELE1BQUFBLEtBQUssRUFBRSxrQkFERjtBQUVMdUQsTUFBQUEsVUFBVSxFQUFFLEtBRlA7QUFHTEQsTUFBQUEsV0FBVyxFQUFFO0FBSFIsS0FEVDtBQU1FLGFBQVMsRUFBQztBQU5aLGtCQVFFLDJFQUNFLGlEQUFDLGtEQUFEO0FBQ0UsU0FBSyxFQUFFO0FBQ0xiLE1BQUFBLEtBQUssRUFBRSxTQURGO0FBRUxXLE1BQUFBLGNBQWMsRUFBRSxNQUZYO0FBR0xkLE1BQUFBLFFBQVEsRUFBRTtBQUhMLEtBRFQ7QUFNRSxNQUFFLEVBQUM7QUFOTCwyQkFERixlQVlFO0FBQU0sU0FBSyxFQUFFO0FBQUVHLE1BQUFBLEtBQUssRUFBRSxTQUFUO0FBQW9CSCxNQUFBQSxRQUFRLEVBQUU7QUFBOUI7QUFBYixnQkFaRixDQVJGLGVBd0JFO0FBQ0UsYUFBUyxFQUFDLE1BRFo7QUFFRSxTQUFLLEVBQUU7QUFDTEcsTUFBQUEsS0FBSyxFQUFFLFNBREY7QUFFTEgsTUFBQUEsUUFBUSxFQUFFLFNBRkw7QUFHTGtCLE1BQUFBLFVBQVUsRUFBRTtBQUhQO0FBRlQsZ0JBeEJGLENBREYsZUFvQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNHM0QsUUFBUSxDQUFDQyxHQUFULENBQWEsVUFBQ1AsT0FBRCxFQUFVOUYsS0FBVjtBQUFBLHdCQUNaLGlEQUFDLGlEQUFEO0FBQWEsYUFBTyxFQUFFOEYsT0FBdEI7QUFBK0IsU0FBRyxFQUFFOUY7QUFBcEMsTUFEWTtBQUFBLEdBQWIsQ0FESCxDQXBDRixDQWZGLENBRFUsZ0JBNERWLGlEQUFDLDhDQUFELE9BL0RKLENBREY7QUFvRUQsQ0F0RUQ7O0FBd0VBLGlFQUFleUgsUUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU1DLFFBQVEsR0FBRyxTQUFYQSxRQUFXLE9BQXFCO0FBQUEsTUFBbkJPLElBQW1CLFFBQW5CQSxJQUFtQjtBQUFBLE1BQWJKLE9BQWEsUUFBYkEsT0FBYTtBQUVwQyxTQUFPQSxPQUFPLGdCQUFHLGlEQUFDLDZDQUFELE9BQUgsZ0JBQ1osNEVBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUssU0FBSyxFQUFFO0FBQUVrQixNQUFBQSxRQUFRLEVBQUU7QUFBWixLQUFaO0FBQXNDLGFBQVMsRUFBQztBQUFoRCxrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUFFQyxNQUFBQSxLQUFLLEVBQUU7QUFBVCxLQURUO0FBRUUsU0FBSyxFQUFDLDRCQUZSO0FBR0UsU0FBSyxFQUFDLElBSFI7QUFJRSxVQUFNLEVBQUMsSUFKVDtBQUtFLFFBQUksRUFBQyxjQUxQO0FBTUUsYUFBUyxFQUFDLG1CQU5aO0FBT0UsV0FBTyxFQUFDO0FBUFYsa0JBU0U7QUFBUSxNQUFFLEVBQUMsR0FBWDtBQUFlLE1BQUUsRUFBQyxHQUFsQjtBQUFzQixLQUFDLEVBQUM7QUFBeEIsSUFURixDQURGLGVBWUU7QUFDRSxTQUFLLEVBQUU7QUFDTEEsTUFBQUEsS0FBSyxFQUFFLE9BREY7QUFFTEQsTUFBQUEsUUFBUSxFQUFFLFVBRkw7QUFHTEUsTUFBQUEsSUFBSSxFQUFFLEtBSEQ7QUFJTEMsTUFBQUEsR0FBRyxFQUFFLEtBSkE7QUFLTEwsTUFBQUEsUUFBUSxFQUFFO0FBTEw7QUFEVCxTQVpGLGVBdUJFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xBLE1BQUFBLFFBQVEsRUFBRSxNQURMO0FBRUxHLE1BQUFBLEtBQUssRUFBRSxTQUZGO0FBR0xLLE1BQUFBLE1BQU0sRUFBRSxNQUhIO0FBSUxGLE1BQUFBLFVBQVUsRUFBRTtBQUpQLEtBRFQ7QUFPRSxhQUFTLEVBQUM7QUFQWixxQkF2QkYsQ0FERixlQXFDRTtBQUNFLGFBQVMsRUFBQywrQkFEWjtBQUVFLFNBQUssRUFBRTtBQUFFRSxNQUFBQSxNQUFNLEVBQUUsTUFBVjtBQUFrQkYsTUFBQUEsVUFBVSxFQUFFO0FBQTlCO0FBRlQsa0JBSUU7QUFBSyxPQUFHLEVBQUMsZUFBVDtBQUF5QixPQUFHLEVBQUM7QUFBN0IsSUFKRixDQXJDRixlQTJDRTtBQUFLLFNBQUssRUFBRTtBQUFFSixNQUFBQSxRQUFRLEVBQUU7QUFBWixLQUFaO0FBQXNDLGFBQVMsRUFBQztBQUFoRCxrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUFFQyxNQUFBQSxLQUFLLEVBQUU7QUFBVCxLQURUO0FBRUUsU0FBSyxFQUFDLDRCQUZSO0FBR0UsU0FBSyxFQUFDLElBSFI7QUFJRSxVQUFNLEVBQUMsSUFKVDtBQUtFLFFBQUksRUFBQyxjQUxQO0FBTUUsYUFBUyxFQUFDLGNBTlo7QUFPRSxXQUFPLEVBQUM7QUFQVixrQkFTRTtBQUFNLEtBQUMsRUFBQztBQUFSLElBVEYsQ0FERixlQVlFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xBLE1BQUFBLEtBQUssRUFBRSxTQURGO0FBRUxELE1BQUFBLFFBQVEsRUFBRSxVQUZMO0FBR0xFLE1BQUFBLElBQUksRUFBRSxLQUhEO0FBSUxDLE1BQUFBLEdBQUcsRUFBRSxLQUpBO0FBS0xMLE1BQUFBLFFBQVEsRUFBRTtBQUxMO0FBRFQsU0FaRixlQXVCRTtBQUNFLFNBQUssRUFBRTtBQUNMQSxNQUFBQSxRQUFRLEVBQUUsTUFETDtBQUVMRyxNQUFBQSxLQUFLLEVBQUUsU0FGRjtBQUdMSyxNQUFBQSxNQUFNLEVBQUUsTUFISDtBQUlMRixNQUFBQSxVQUFVLEVBQUU7QUFKUCxLQURUO0FBT0UsYUFBUyxFQUFDO0FBUFosdUJBdkJGLENBM0NGLENBREYsQ0FERixlQWtGRTtBQUNFLGFBQVMsRUFBQyxvREFEWjtBQUVFLFNBQUssRUFBRTtBQUFFSCxNQUFBQSxLQUFLLEVBQUU7QUFBVDtBQUZULGtCQUlFO0FBQUssYUFBUyxFQUFDO0FBQWYsa0JBQ0U7QUFDRSxhQUFTLEVBQUMsOEJBRFo7QUFFRSxTQUFLLEVBQUU7QUFBRVEsTUFBQUEsWUFBWSxFQUFFO0FBQWhCO0FBRlQsc0JBREYsZUFPRSxpREFBQyxrREFBRDtBQUFjLFFBQUksRUFBRXZCO0FBQXBCLElBUEYsQ0FKRixlQWFFO0FBQ0UsU0FBSyxFQUFFO0FBQUVvQixNQUFBQSxNQUFNLEVBQUU7QUFBVixLQURUO0FBRUUsYUFBUyxFQUFDO0FBRlosa0JBSUU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFJLGFBQVMsRUFBQztBQUFkLGtCQURGLENBSkYsZUFPRSw0REFQRixlQVNFLGlEQUFDLHNEQUFEO0FBQ0UsYUFBUyxFQUFFLDJCQURiO0FBRUUsUUFBSSxFQUFFcEI7QUFGUixJQVRGLGVBY0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUNFLFNBQUssRUFBRTtBQUNMWSxNQUFBQSxRQUFRLEVBQUUsTUFETDtBQUVMUyxNQUFBQSxlQUFlLEVBQUUsT0FGWjtBQUdMTixNQUFBQSxLQUFLLEVBQUUsU0FIRjtBQUlMb0IsTUFBQUEsU0FBUyxFQUFFLHFCQUpOO0FBS0w2RCxNQUFBQSxVQUFVLEVBQUUscUJBTFA7QUFNTEMsTUFBQUEsV0FBVyxFQUFFO0FBTlIsS0FEVDtBQVNFLGFBQVMsRUFBQyw0Q0FUWjtBQVVFLFdBQU8sRUFBRXpPLHlEQUF1QkE7QUFWbEMsa0JBWUUsNERBQUl3SSxJQUFJLENBQUN4SCxLQUFMLENBQVd6QixNQUFmLHdCQVpGLGVBYUU7QUFBSyxhQUFTLEVBQUM7QUFBZixJQWJGLENBREYsZUFnQkU7QUFDRSxTQUFLLEVBQUU7QUFDTGlQLE1BQUFBLFVBQVUsRUFBRSxxQkFEUDtBQUVMQyxNQUFBQSxXQUFXLEVBQUUscUJBRlI7QUFHTDFFLE1BQUFBLFlBQVksRUFBRSxxQkFIVDtBQUlMRixNQUFBQSxlQUFlLEVBQUUsT0FKWjtBQUtMTixNQUFBQSxLQUFLLEVBQUU7QUFMRixLQURUO0FBUUUsYUFBUyxFQUFDO0FBUlosS0FVR2YsSUFBSSxDQUFDeEgsS0FBTCxDQUFXNEYsR0FBWCxDQUFlLFVBQUNrRCxJQUFELEVBQU92SixLQUFQO0FBQUEsd0JBQ2Q7QUFBSyxTQUFHLEVBQUVBLEtBQVY7QUFBaUIsZUFBUyxFQUFDO0FBQTNCLG9CQUNFO0FBQUssZUFBUyxFQUFDO0FBQWYsb0JBQ0U7QUFDRSxXQUFLLEVBQUU7QUFBRXFKLFFBQUFBLE1BQU0sRUFBRTtBQUFWLE9BRFQ7QUFFRSxTQUFHLEVBQUVFLElBQUksQ0FBQ3pELE9BQUwsQ0FBYTRELFFBRnBCO0FBR0UsU0FBRyxFQUFDO0FBSE4sTUFERixDQURGLGVBUUU7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUFLLFdBQUssRUFBRTtBQUFFYixRQUFBQSxRQUFRLEVBQUU7QUFBWixPQUFaO0FBQWtDLGVBQVMsRUFBQztBQUE1QyxPQUNHVSxJQUFJLENBQUN6RCxPQUFMLENBQWE4RCxLQURoQixDQURGLGVBSUU7QUFBSyxXQUFLLEVBQUU7QUFBRWYsUUFBQUEsUUFBUSxFQUFFO0FBQVo7QUFBWixtQkFDV1UsSUFBSSxDQUFDL0gsUUFEaEIsQ0FKRixlQU9FO0FBQUssV0FBSyxFQUFFO0FBQUVxSCxRQUFBQSxRQUFRLEVBQUU7QUFBWjtBQUFaLHVCQUNlVSxJQUFJLENBQUMzSSxLQUFMLENBQVdDLE9BQVgsQ0FBbUIsQ0FBbkIsQ0FEZixZQVBGLGVBVUU7QUFBSyxXQUFLLEVBQUU7QUFBRWdJLFFBQUFBLFFBQVEsRUFBRTtBQUFaO0FBQVosbUJBQ1csR0FEWCxFQUVHLENBQUNVLElBQUksQ0FBQzNJLEtBQUwsSUFBYyxJQUFJMkksSUFBSSxDQUFDekQsT0FBTCxDQUFhRyxHQUEvQixDQUFELEVBQXNDcEYsT0FBdEMsQ0FBOEMsQ0FBOUMsQ0FGSCxZQVZGLENBUkYsQ0FEYztBQUFBLEdBQWYsQ0FWSCxDQWhCRixDQWRGLENBYkYsZUFrRkU7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLFNBQUssRUFBRTtBQUFFd0ksTUFBQUEsTUFBTSxFQUFFO0FBQVYsS0FBWjtBQUF1QyxhQUFTLEVBQUM7QUFBakQsa0JBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixrQkFDRTtBQUFLLGFBQVMsRUFBQztBQUFmLGtCQUNFO0FBQUksYUFBUyxFQUFDO0FBQWQsa0JBREYsQ0FERixlQUlFO0FBQ0UsYUFBUyxFQUFDLHVCQURaO0FBRUUsV0FBTyxFQUFFdEgsZ0VBQThCQTtBQUZ6QyxrQkFJRTtBQUNFLFNBQUssRUFBQyw0QkFEUjtBQUVFLFNBQUssRUFBQyxJQUZSO0FBR0UsVUFBTSxFQUFDLElBSFQ7QUFJRSxRQUFJLEVBQUMsY0FKUDtBQUtFLGFBQVMsRUFBQyxvQkFMWjtBQU1FLFdBQU8sRUFBQztBQU5WLGtCQVFFO0FBQU0sS0FBQyxFQUFDO0FBQVIsSUFSRixDQUpGLENBSkYsQ0FERixlQXFCRSw0REFyQkYsZUFzQkUsaURBQUMsc0RBQUQ7QUFDRSxhQUFTLEVBQUUsbUNBRGI7QUFFRSxRQUFJLEVBQUVrRztBQUZSLElBdEJGLGVBMkJFLDJFQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xZLE1BQUFBLFFBQVEsRUFBRSxNQURMO0FBRUxTLE1BQUFBLGVBQWUsRUFBRSxPQUZaO0FBR0xOLE1BQUFBLEtBQUssRUFBRSxTQUhGO0FBSUxvQixNQUFBQSxTQUFTLEVBQUUscUJBSk47QUFLTDZELE1BQUFBLFVBQVUsRUFBRSxxQkFMUDtBQU1MQyxNQUFBQSxXQUFXLEVBQUU7QUFOUixLQURUO0FBU0UsYUFBUyxFQUFDLDRDQVRaO0FBVUUsV0FBTyxFQUFFek8seURBQXVCQTtBQVZsQyxrQkFZRSw0REFBSXdJLElBQUksQ0FBQ3hILEtBQUwsQ0FBV3pCLE1BQWYsd0JBWkYsZUFhRTtBQUFLLGFBQVMsRUFBQztBQUFmLElBYkYsQ0FERixlQWdCRTtBQUNFLFNBQUssRUFBRTtBQUNMaVAsTUFBQUEsVUFBVSxFQUFFLHFCQURQO0FBRUxDLE1BQUFBLFdBQVcsRUFBRSxxQkFGUjtBQUdMMUUsTUFBQUEsWUFBWSxFQUFFLHFCQUhUO0FBSUxGLE1BQUFBLGVBQWUsRUFBRSxPQUpaO0FBS0xOLE1BQUFBLEtBQUssRUFBRTtBQUxGLEtBRFQ7QUFRRSxhQUFTLEVBQUM7QUFSWixLQVVHZixJQUFJLENBQUN4SCxLQUFMLENBQVc0RixHQUFYLENBQWUsVUFBQ2tELElBQUQsRUFBT3ZKLEtBQVA7QUFBQSx3QkFDZDtBQUFLLFNBQUcsRUFBRUEsS0FBVjtBQUFpQixlQUFTLEVBQUM7QUFBM0Isb0JBQ0U7QUFBSyxlQUFTLEVBQUM7QUFBZixvQkFDRTtBQUNFLFdBQUssRUFBRTtBQUFFcUosUUFBQUEsTUFBTSxFQUFFO0FBQVYsT0FEVDtBQUVFLFNBQUcsRUFBRUUsSUFBSSxDQUFDekQsT0FBTCxDQUFhNEQsUUFGcEI7QUFHRSxTQUFHLEVBQUM7QUFITixNQURGLENBREYsZUFRRTtBQUFLLGVBQVMsRUFBQztBQUFmLG9CQUNFO0FBQUssV0FBSyxFQUFFO0FBQUViLFFBQUFBLFFBQVEsRUFBRTtBQUFaLE9BQVo7QUFBa0MsZUFBUyxFQUFDO0FBQTVDLE9BQ0dVLElBQUksQ0FBQ3pELE9BQUwsQ0FBYThELEtBRGhCLENBREYsZUFJRTtBQUFLLFdBQUssRUFBRTtBQUFFZixRQUFBQSxRQUFRLEVBQUU7QUFBWjtBQUFaLG1CQUNXVSxJQUFJLENBQUMvSCxRQURoQixDQUpGLGVBT0U7QUFBSyxXQUFLLEVBQUU7QUFBRXFILFFBQUFBLFFBQVEsRUFBRTtBQUFaO0FBQVosdUJBQ2VVLElBQUksQ0FBQzNJLEtBQUwsQ0FBV0MsT0FBWCxDQUFtQixDQUFuQixDQURmLFlBUEYsZUFVRTtBQUFLLFdBQUssRUFBRTtBQUFFZ0ksUUFBQUEsUUFBUSxFQUFFO0FBQVo7QUFBWixtQkFDVyxHQURYLEVBRUcsQ0FBQ1UsSUFBSSxDQUFDM0ksS0FBTCxJQUFjLElBQUkySSxJQUFJLENBQUN6RCxPQUFMLENBQWFHLEdBQS9CLENBQUQsRUFBc0NwRixPQUF0QyxDQUE4QyxDQUE5QyxDQUZILEVBRXFELEdBRnJELFdBVkYsQ0FSRixDQURjO0FBQUEsR0FBZixDQVZILENBaEJGLENBM0JGLENBREYsQ0FsRkYsQ0FsRkYsQ0FERjtBQTZQRCxDQS9QRDs7QUFpUUEsaUVBQWU2RyxRQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZRQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTXFHLFlBQVksR0FBRyxTQUFmQSxZQUFlLE9BQVk7QUFBQSxNQUFWOUYsSUFBVSxRQUFWQSxJQUFVO0FBQy9CLE1BQUlXLE9BQU8sR0FBR0gsNERBQVUsRUFBeEI7O0FBRUEsV0FBUzBGLFlBQVQsR0FBd0I7QUFFdEIsUUFBSXRPLEtBQUssR0FBR0Qsa0RBQVksRUFBeEI7O0FBRUEsUUFBSUMsS0FBSyxJQUFJb0ksSUFBSSxDQUFDeEgsS0FBTCxDQUFXekIsTUFBWCxHQUFvQixDQUFqQyxFQUFvQztBQUVsQyxVQUFJd0wsWUFBWSxHQUFHO0FBRWpCRyxRQUFBQSxTQUFTLEVBQUU3SyxDQUFDLENBQUMsWUFBRCxDQUFELENBQWdCbUIsR0FBaEIsRUFGTTtBQUdqQjJKLFFBQUFBLFFBQVEsRUFBRTlLLENBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZW1CLEdBQWYsRUFITztBQUlqQnlKLFFBQUFBLE9BQU8sRUFBRTVLLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY21CLEdBQWQsRUFKUTtBQUtqQjRKLFFBQUFBLGNBQWMsRUFBRS9LLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCbUIsR0FBckIsRUFMQztBQU1qQjZKLFFBQUFBLGNBQWMsRUFBRWhMLENBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCbUIsR0FBckIsRUFOQztBQU9qQjhKLFFBQUFBLE9BQU8sRUFBRWpMLENBQUMsQ0FBQyxVQUFELENBQUQsQ0FBY21CLEdBQWQsRUFQUTtBQVFqQitKLFFBQUFBLElBQUksRUFBRWxMLENBQUMsQ0FBQyxPQUFELENBQUQsQ0FBV21CLEdBQVgsRUFSVztBQVNqQmdLLFFBQUFBLFNBQVMsRUFBRW5MLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JtQixHQUFoQixFQVRNO0FBVWpCaUssUUFBQUEsS0FBSyxFQUFFcEwsQ0FBQyxDQUFDLFFBQUQsQ0FBRCxDQUFZbUIsR0FBWixFQVZVO0FBV2pCbU4sUUFBQUEsU0FBUyxFQUFFdE8sQ0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQm1CLEdBQWhCLEVBWE07QUFZakJvTixRQUFBQSxZQUFZLEVBQUV2TyxDQUFDLENBQUMsZUFBRCxDQUFELENBQW1CbUIsR0FBbkI7QUFaRyxPQUFuQjtBQWVBMkgsTUFBQUEsT0FBTyxDQUFDMEIsSUFBUixDQUFhO0FBQUMvRyxRQUFBQSxRQUFRLEVBQUMsd0JBQVY7QUFBb0NrRyxRQUFBQSxLQUFLLEVBQUNlO0FBQTFDLE9BQWI7QUFDRDtBQUNGOztBQUFBO0FBRUQsc0JBQ0U7QUFBTSxhQUFTLEVBQUM7QUFBaEIsa0JBQ0UsMkVBQ0UsNEVBREYsZUFFRTtBQUFPLFFBQUksRUFBQyxNQUFaO0FBQW1CLE1BQUUsRUFBQyxXQUF0QjtBQUFrQyxhQUFTLEVBQUMsY0FBNUM7QUFBMkQsWUFBUTtBQUFuRSxJQUZGLGVBR0UsMkRBSEYsQ0FERixlQU1FLDJFQUNFLDZFQURGLGVBRUU7QUFBTyxRQUFJLEVBQUMsTUFBWjtBQUFtQixhQUFTLEVBQUMsY0FBN0I7QUFBNEMsTUFBRSxFQUFDLFVBQS9DO0FBQTBELFlBQVE7QUFBbEUsSUFGRixlQUdFLDJEQUhGLENBTkYsZUFXRSwyRUFDRSx5RUFERixlQUVFO0FBQU8sUUFBSSxFQUFDLE1BQVo7QUFBbUIsYUFBUyxFQUFDLGNBQTdCO0FBQTRDLE1BQUUsRUFBQztBQUEvQyxJQUZGLENBWEYsZUFlRSwyRUFDRSwrRUFERixlQUVFO0FBQ0UsUUFBSSxFQUFDLE1BRFA7QUFFRSxhQUFTLEVBQUMsY0FGWjtBQUdFLE1BQUUsRUFBQyxnQkFITDtBQUlFLFlBQVE7QUFKVixJQUZGLGVBUUUsMkRBUkYsQ0FmRixlQXlCRSwyRUFDRSwrREFERixlQUVFO0FBQU8sUUFBSSxFQUFDLE1BQVo7QUFBbUIsYUFBUyxFQUFDLGNBQTdCO0FBQTRDLE1BQUUsRUFBQztBQUEvQyxJQUZGLENBekJGLGVBNkJFLDJFQUNFLGdGQURGLGVBRUU7QUFBTyxRQUFJLEVBQUMsTUFBWjtBQUFtQixhQUFTLEVBQUMsY0FBN0I7QUFBNEMsTUFBRSxFQUFDLFNBQS9DO0FBQXlELFlBQVE7QUFBakUsSUFGRixlQUdFLDJEQUhGLENBN0JGLGVBa0NFLDJFQUNFLGdGQURGLGVBRUU7QUFBTyxRQUFJLEVBQUMsTUFBWjtBQUFtQixhQUFTLEVBQUMsY0FBN0I7QUFBNEMsTUFBRSxFQUFDLE1BQS9DO0FBQXNELFlBQVE7QUFBOUQsSUFGRixlQUdFLDJEQUhGLENBbENGLGVBdUNFLDJFQUNFLGlGQURGLGVBRUU7QUFDRSxRQUFJLEVBQUMsTUFEUDtBQUVFLGFBQVMsRUFBQyxjQUZaO0FBR0UsTUFBRSxFQUFDLFdBSEw7QUFJRSxnQkFBWSxFQUFDLE1BSmY7QUFLRSxZQUFRO0FBTFYsSUFGRixlQVNFLDJEQVRGLENBdkNGLGVBa0RFLDJFQUNFLDJGQURGLGVBRUU7QUFBTyxRQUFJLEVBQUMsTUFBWjtBQUFtQixhQUFTLEVBQUMsY0FBN0I7QUFBNEMsTUFBRSxFQUFDLE9BQS9DO0FBQXVELFlBQVE7QUFBL0QsSUFGRixlQUdFLDJEQUhGLENBbERGLGVBdURFLDJFQUNFO0FBQ0UsU0FBSyxFQUFFO0FBQ0xqRSxNQUFBQSxLQUFLLEVBQUUsTUFERjtBQUVMOEMsTUFBQUEsTUFBTSxFQUFFLE1BRkg7QUFHTEQsTUFBQUEsTUFBTSxFQUFFLFNBSEg7QUFJTGEsTUFBQUEsWUFBWSxFQUFFO0FBSlQsS0FEVDtBQU9FLFFBQUksRUFBQyxVQVBQO0FBUUUsTUFBRSxFQUFDO0FBUkwsSUFERixlQVdFO0FBQU8sU0FBSyxFQUFFO0FBQUVqQixNQUFBQSxLQUFLLEVBQUU7QUFBVCxLQUFkO0FBQW9DLGFBQVMsRUFBQztBQUE5QywwRUFYRixDQXZERixlQXNFRSwyRUFDRTtBQUFPLFNBQUssRUFBRTtBQUFFQSxNQUFBQSxLQUFLLEVBQUU7QUFBVDtBQUFkLGtDQURGLGVBRUU7QUFDRSxTQUFLLEVBQUU7QUFBRWlCLE1BQUFBLFlBQVksRUFBRTtBQUFoQixLQURUO0FBRUUsYUFBUyxFQUFDLGNBRlo7QUFHRSxNQUFFLEVBQUM7QUFITCxJQUZGLENBdEVGLGVBOEVFLDZHQTlFRixlQStFRTtBQUNFLFNBQUssRUFBRTtBQUNMMUQsTUFBQUEsS0FBSyxFQUFFLE9BREY7QUFFTDhDLE1BQUFBLE1BQU0sRUFBRSxNQUZIO0FBR0xELE1BQUFBLE1BQU0sRUFBRSxTQUhIO0FBSUxjLE1BQUFBLFNBQVMsRUFBRTtBQUpOLEtBRFQ7QUFPRSxhQUFTLEVBQUMsc0NBUFo7QUFRRSxXQUFPLEVBQUVpRTtBQVJYLGdCQS9FRixDQURGO0FBOEZELENBMUhEOztBQTJIQSxpRUFBZUosWUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0hBOztBQUVBLElBQU1DLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUI7QUFBQSxNQUFHOU8sU0FBSCxRQUFHQSxTQUFIO0FBQUEsTUFBYytJLElBQWQsUUFBY0EsSUFBZDtBQUFBLHNCQUN2QjtBQUFPLGFBQVMsRUFBRS9JO0FBQWxCLGtCQUNFLDZFQUNFLDBFQUNFLDZGQURGLGVBRUUsNkRBQUssQ0FBQytJLElBQUksQ0FBQ3JILEtBQUwsR0FBYXFILElBQUksQ0FBQ2tDLFFBQW5CLEVBQTZCdEosT0FBN0IsQ0FBcUMsQ0FBckMsQ0FBTCxZQUZGLENBREYsZUFLRSwwRUFDRSw4RkFERixlQUVFLDZEQUFLb0gsSUFBSSxDQUFDckgsS0FBTCxDQUFXQyxPQUFYLENBQW1CLENBQW5CLENBQUwsWUFGRixDQUxGLGVBU0UsMEVBQ0UscUVBREYsZUFFRSw2REFBS29ILElBQUksQ0FBQ2tDLFFBQUwsQ0FBY3RKLE9BQWQsQ0FBc0IsQ0FBdEIsQ0FBTCxZQUZGLENBVEYsZUFhRSwwRUFDRSw2RUFERixlQUVFLHdFQUZGLENBYkYsZUFpQkU7QUFBSSxhQUFTLEVBQUM7QUFBZCxrQkFDRSw4RkFERixlQUVFLDZEQUFLb0gsSUFBSSxDQUFDckgsS0FBTCxDQUFXQyxPQUFYLENBQW1CLENBQW5CLENBQUwsWUFGRixDQWpCRixDQURGLENBRHVCO0FBQUEsQ0FBekI7O0FBMkJBLGlFQUFlbU4sZ0JBQWYsRTs7Ozs7Ozs7Ozs7O0FDN0JBIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3wvXFwuKGp8dClzeCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY29udHJvbGxlcnMuanNvbiIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY29udHJvbGxlcnMvaGVsbG9fY29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvYXBwLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9ib290c3RyYXAuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvQXBwLmpzeCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY29tcG9uZW50cy9DYXJ0LmpzeCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY29tcG9uZW50cy9Db25maXJtYXRpb24uanN4Iiwid2VicGFjazovLy8uL2Fzc2V0cy9jb21wb25lbnRzL0VtcHR5Q2FydC5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvSG9tZS5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvTGF5b3V0MS5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvTGF5b3V0Mi5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvTG9hZGluZy5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvTWFpbkNhcm91c2VsLmpzeCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY29tcG9uZW50cy9Ob3RGb3VuZC5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvUHJvZHVjdC5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvUHJvZHVjdENhcmQuanN4Iiwid2VicGFjazovLy8uL2Fzc2V0cy9jb21wb25lbnRzL1Byb2R1Y3RDYXJvdXNlbC5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvUHJvZHVjdENhcm91c2VsSXRlbS5qc3giLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2NvbXBvbmVudHMvUHJvZHVjdHMuanN4Iiwid2VicGFjazovLy8uL2Fzc2V0cy9jb21wb25lbnRzL1NoaXBwaW5nLmpzeCIsIndlYnBhY2s6Ly8vLi9hc3NldHMvY29tcG9uZW50cy9TaGlwcGluZ0Zvcm0uanN4Iiwid2VicGFjazovLy8uL2Fzc2V0cy9jb21wb25lbnRzL1RvdGFsQW5kVGF4VGFibGUuanN4Iiwid2VicGFjazovLy8uL2Fzc2V0cy9zdHlsZXMvYXBwLmNzcz8zZmJhIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBtYXAgPSB7XG5cdFwiLi9oZWxsb19jb250cm9sbGVyLmpzXCI6IFwiLi9ub2RlX21vZHVsZXMvQHN5bWZvbnkvc3RpbXVsdXMtYnJpZGdlL2xhenktY29udHJvbGxlci1sb2FkZXIuanMhLi9hc3NldHMvY29udHJvbGxlcnMvaGVsbG9fY29udHJvbGxlci5qc1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1hcCwgcmVxKSkge1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gbWFwW3JlcV07XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuL2Fzc2V0cy9jb250cm9sbGVycyBzeW5jIHJlY3Vyc2l2ZSAuL25vZGVfbW9kdWxlcy9Ac3ltZm9ueS9zdGltdWx1cy1icmlkZ2UvbGF6eS1jb250cm9sbGVyLWxvYWRlci5qcyEgXFxcXC4oanx0KXN4PyRcIjsiLCJleHBvcnQgZGVmYXVsdCB7XG59OyIsImltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tICdzdGltdWx1cyc7XG5cbi8qXG4gKiBUaGlzIGlzIGFuIGV4YW1wbGUgU3RpbXVsdXMgY29udHJvbGxlciFcbiAqXG4gKiBBbnkgZWxlbWVudCB3aXRoIGEgZGF0YS1jb250cm9sbGVyPVwiaGVsbG9cIiBhdHRyaWJ1dGUgd2lsbCBjYXVzZVxuICogdGhpcyBjb250cm9sbGVyIHRvIGJlIGV4ZWN1dGVkLiBUaGUgbmFtZSBcImhlbGxvXCIgY29tZXMgZnJvbSB0aGUgZmlsZW5hbWU6XG4gKiBoZWxsb19jb250cm9sbGVyLmpzIC0+IFwiaGVsbG9cIlxuICpcbiAqIERlbGV0ZSB0aGlzIGZpbGUgb3IgYWRhcHQgaXQgZm9yIHlvdXIgdXNlIVxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIENvbnRyb2xsZXIge1xuICAgIGNvbm5lY3QoKSB7XG4gICAgICAgIHRoaXMuZWxlbWVudC50ZXh0Q29udGVudCA9ICdIZWxsbyBTdGltdWx1cyEgRWRpdCBtZSBpbiBhc3NldHMvY29udHJvbGxlcnMvaGVsbG9fY29udHJvbGxlci5qcyc7XG4gICAgfVxufVxuIiwiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IGltcG9ydCB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcbmltcG9ydCAnLi9zdHlsZXMvYXBwLmNzcyc7XG5cbi8vIHN0YXJ0IHRoZSBTdGltdWx1cyBhcHBsaWNhdGlvblxuaW1wb3J0ICcuL2Jvb3RzdHJhcC5qcyc7XG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcbmltcG9ydCBBcHAgZnJvbSAnLi9jb21wb25lbnRzL0FwcC5qc3gnO1xuXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXG5cblJlYWN0RE9NLnJlbmRlcig8QXBwIC8+LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncm9vdCcpKTtcblxuLy9Gb290ZXIgYWNjb3JkaW9uXG5cbmxldCBhY2MgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwiYWNjb3JkaW9uX2J1dHRvblwiKTtcblxuaWYgKGFjYykge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYWNjLmxlbmd0aDsgaSsrKSB7XG5cbiAgICAgICAgYWNjW2ldLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMuY2xhc3NMaXN0LnRvZ2dsZShcIm9wZW5cIilcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgbGV0IHBhbmVsID0gdGhpcy5uZXh0RWxlbWVudFNpYmxpbmdcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHBhbmVsLnN0eWxlLm1heEhlaWdodCkge1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgIHBhbmVsLnN0eWxlLm1heEhlaWdodCA9IG51bGxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBwYW5lbC5zdHlsZS5tYXhIZWlnaHQgPSBwYW5lbC5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG4vL1NoaXBwaW5nIGFjY29yZGlvblxuXG5leHBvcnQgZnVuY3Rpb24gdG9nZ2xlU2hpcHBpbmdBY2NvcmRpb24oKSB7XG5cbiAgICBsZXQgYWNjb3JkaW9uQnV0dG9ucyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2FjY29yZGlvbl9idXR0b24nKVxuXG4gICAgbGV0IHBhbmVscyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3NoaXBwaW5nLWFjY29yZGlvbi1wYW5lbCcpXG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFjY29yZGlvbkJ1dHRvbnMubGVuZ3RoOyBpKyspIHtcblxuICAgICAgICBhY2NvcmRpb25CdXR0b25zW2ldLmNsYXNzTGlzdC50b2dnbGUoXCJvcGVuXCIpXG5cbiAgICAgICAgaWYgKHBhbmVsc1tpXS5zdHlsZS5tYXhIZWlnaHQpIHtcblxuICAgICAgICAgICAgcGFuZWxzW2ldLnN0eWxlLm1heEhlaWdodCA9IG51bGxcbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgcGFuZWxzW2ldLnN0eWxlLm1heEhlaWdodCA9IHBhbmVsc1tpXS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLy8gU2hpcHBpbmcgZm9ybSB2YWxpZGF0aW9uXG5cbmV4cG9ydCBmdW5jdGlvbiB2YWxpZGF0ZUZvcm0oKSB7XG5cbiAgICBsZXQgdmFsaWQgPSB0cnVlXG5cbiAgICAkKCdpbnB1dCcpLmVhY2goKGluZGV4LCBlbGVtZW50KSA9PiB7XG5cbiAgICAgICAgaWYgKCFlbGVtZW50LmNoZWNrVmFsaWRpdHkoKSkge1xuXG4gICAgICAgICAgICB2YWxpZCA9IGZhbHNlXG5cbiAgICAgICAgICAgICQoZWxlbWVudCkubmV4dCgpLnRleHQoJ1ZhYWRpdHR1IGtlbnR0w6QuJylcblxuICAgICAgICAgICAgJChlbGVtZW50KS5jc3MoJ2JvcmRlcicsICcxcHggc29saWQgI2UxNzcxZicpXG5cbiAgICAgICAgICAgICQoZWxlbWVudCkucHJldigpLmNzcygnY29sb3InLCAnI2UxNzcxZicpXG5cbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgJChlbGVtZW50KS5uZXh0KCkudGV4dCgnJylcblxuICAgICAgICAgICAgJChlbGVtZW50KS5jc3MoJ2JvcmRlcicsICcxcHggc29saWQgbGlnaHRncmF5JylcblxuICAgICAgICAgICAgJChlbGVtZW50KS5wcmV2KCkuY3NzKCdjb2xvcicsICcjNWY1ZjVmJylcbiAgICAgICAgfVxuICAgIH0pXG5cbiAgICByZXR1cm4gdmFsaWRcbn1cblxuLy8gU2hvcHBpbmcgY2FydFxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q2FydCgpIHtcblxuICAgICQuZ2V0KFwiL2FwaS9jYXJ0L2dldFwiLCBkYXRhID0+IHtcblxuICAgICAgICBpZiAoZGF0YS5pdGVtcy5sZW5ndGggPiAwKSB7XG5cbiAgICAgICAgICAgICQoXCIuc2hvcHBpbmctY2FydC1zdW1tYXJ5XCIpLnJlbW92ZUNsYXNzKFwiaW52aXNpYmxlXCIpXG5cbiAgICAgICAgICAgICQoXCIuc2hvcHBpbmctY2FydC1pdGVtcy1jb3VudFwiKS50ZXh0KGRhdGEuaXRlbXMubGVuZ3RoKVxuXG4gICAgICAgICAgICAkKFwiLnNob3BwaW5nLWNhcnQtdG90YWxcIikuaHRtbChkYXRhLnRvdGFsLnRvRml4ZWQoMikgKyBcIiZuYnNwOyZldXJvO1wiKVxuXG4gICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICQoXCIuc2hvcHBpbmctY2FydC1zdW1tYXJ5XCIpLmFkZENsYXNzKFwiaW52aXNpYmxlXCIpXG4gICAgICAgIH1cblxuICAgIH0sICdqc29uJylcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHZhbGlkYXRlUmVkdWNlUXVhbnRpdHkoKSB7XG5cbiAgICBsZXQgaW5wdXQgPSAkKCcucXVhbnRpdHktaW5wdXQnKS52YWwoKVxuXG4gICAgaWYgKHBhcnNlSW50KGlucHV0KSA+IDApIHtcblxuICAgICAgICAkKCcucXVhbnRpdHktaW5wdXQnKS52YWwocGFyc2VJbnQoaW5wdXQpIC0gMSlcblxuICAgICAgICByZXR1cm4gdHJ1ZVxuICAgIH1cbiAgICBpZiAocGFyc2VJbnQoaW5wdXQudmFsdWUpID09PSAwKSB7XG5cbiAgICAgICAgJCgnLm5vbnBvc2l0aXZlLXF1YW50aXR5LXdhcm5pbmcnKS5yZW1vdmVDbGFzcygnZC1ub25lJylcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB2YWxpZGF0ZUluY3JlYXNlUXVhbnRpdHkoKSB7XG5cbiAgICBsZXQgaW5wdXQgPSAkKCcucXVhbnRpdHktaW5wdXQnKS52YWwoKVxuXG4gICAgaWYgKHBhcnNlSW50KGlucHV0KSA8IDEwMCkge1xuXG4gICAgICAgICQoJy5xdWFudGl0eS1pbnB1dCcpLnZhbChwYXJzZUludChpbnB1dCkgKyAxKVxuXG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gY2hhbmdlUXVhbnRpdHkoKSB7XG5cbiAgICBsZXQgaW5wdXQgPSAkKCcucXVhbnRpdHktaW5wdXQnKS52YWwoKVxuXG4gICAgbGV0IHVybCA9ICQoJy51cmwtaW5wdXQnKS52YWwoKVxuXG4gICAgJC5wb3N0KCdhcGkvY2FydC9jaGFuZ2UnLCB7IHF1YW50aXR5OiBwYXJzZUludChpbnB1dCksIHVybDogdXJsIH0pXG4gICAgLnRoZW4oKCkgPT4gZm9jdXNQYWdlKCkpXG59XG5cbmV4cG9ydCBmdW5jdGlvbiB2YWxpZGF0ZVF1YW50aXR5KCkge1xuXG4gICAgbGV0IGlucHV0ID0gJCgnLnF1YW50aXR5LWlucHV0JykudmFsKClcblxuICAgIGlmIChwYXJzZUludChpbnB1dCkgPiAwICYmIHBhcnNlSW50KGlucHV0KSA8IDEwMCkge1xuXG4gICAgICAgIGFkZFRvQ2FydCgpXG5cbiAgICAgICAgJCgnLm5vbnBvc2l0aXZlLXF1YW50aXR5LXdhcm5pbmcnKS5hZGRDbGFzcygnZC1ub25lJylcbiAgICAgICAgJCgnLnF1YW50aXR5LXdhcm5pbmcnKS5hZGRDbGFzcygnZC1ub25lJylcblxuICAgICAgICByZXR1cm4gdHJ1ZVxuXG4gICAgfSBlbHNlIGlmIChwYXJzZUludChpbnB1dCkgPCAxKSB7XG5cbiAgICAgICAgJCgnLm5vbnBvc2l0aXZlLXF1YW50aXR5LXdhcm5pbmcnKS5yZW1vdmVDbGFzcygnZC1ub25lJylcbiAgICAgICAgJChcIi5hZGQtY2FydC1jb25maXJtZWRcIikuYWRkQ2xhc3MoXCJkLW5vbmVcIilcblxuICAgIH0gZWxzZSB7XG5cbiAgICAgICAgJCgnLnF1YW50aXR5LXdhcm5pbmcnKS5yZW1vdmVDbGFzcygnZC1ub25lJylcbiAgICAgICAgJChcIi5hZGQtY2FydC1jb25maXJtZWRcIikuYWRkQ2xhc3MoXCJkLW5vbmVcIilcblxuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGJsdXJQYWdlKCkge1xuXG4gICAgJCgnLnBhZ2UtY29udGFpbmVyJykuY3NzKCdvcGFjaXR5JywgMC41KVxuXG4gICAgJCgnLnNwaW5uZXItYm9yZGVyJykucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpXG59XG5cbmV4cG9ydCBmdW5jdGlvbiBmb2N1c1BhZ2UoKSB7XG5cbiAgICAkKCcucGFnZS1jb250YWluZXInKS5jc3MoJ29wYWNpdHknLCAxKVxuICAgICQoJy5zcGlubmVyLWJvcmRlcicpLmFkZENsYXNzKCdkLW5vbmUnKVxufVxuXG5mdW5jdGlvbiBhZGRUb0NhcnQoKSB7XG5cbiAgICAkKFwiLm9yZGVyLWJ0blwiKS5jc3MoXCJvcGFjaXR5XCIsIDAuNSlcbiAgICAkKFwiLm9yZGVyLWJ0bi1zcGlubmVyLWJvcmRlclwiKS5yZW1vdmVDbGFzcyhcImQtbm9uZVwiKVxuXG4gICAgJC5wb3N0KFwiL2FwaS9wcm9kdWN0L2FkZFwiLFxuICAgICAgICB7ICdwcm9kdWN0JzogJCgnLnVybC1pbnB1dCcpLnZhbCgpLCAncXVhbnRpdHknOiAkKCcucXVhbnRpdHktaW5wdXQnKS52YWwoKSB9LFxuICAgICAgICAoKSA9PiB7XG5cbiAgICAgICAgICAgICQoXCIuYWRkLWNhcnQtY29uZmlybWVkXCIpLnJlbW92ZUNsYXNzKFwiZC1ub25lXCIpXG4gICAgICAgICAgICAkKFwiLm9yZGVyLWJ0blwiKS5jc3MoXCJvcGFjaXR5XCIsIDEpXG4gICAgICAgICAgICAkKFwiLm9yZGVyLWJ0bi1zcGlubmVyLWJvcmRlclwiKS5hZGRDbGFzcyhcImQtbm9uZVwiKVxuXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxuICAgICAgICB9KVxufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlSXRlbSh1cmwpIHtcblxuICAgIGF4aW9zLmdldChcImFwaS9jYXJ0L3JlbW92ZS9cIiArIHVybCkudGhlbigoKSA9PiBmb2N1c1BhZ2UoKSlcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHRvZ2dsZUV4cGFuZGVkVG90YWxBbmRUYXhUYWJsZSgpIHtcblxuICAgIGlmICgkKCcuZXhwYW5kZWQtdG90YWwtYW5kLXRheC10YWJsZS1jb250YWluZXInKS5oYXNDbGFzcygnZC1ub25lJykpIHtcblxuICAgICAgICAkKCcuZXhwYW5kZWQtdG90YWwtYW5kLXRheC10YWJsZS1jb250YWluZXInKS5yZW1vdmVDbGFzcygnZC1ub25lJylcbiAgICAgICAgJCgnLnNoaXBwaW5nLWZvcm0tY29udGFpbmVyJykuYWRkQ2xhc3MoJ2Qtbm9uZScpXG4gICAgICAgICQoJy5zaGlwcGluZy1wcm9ncmVzcy1pbmRpY2F0b3ItY29udGFpbmVyJykuYWRkQ2xhc3MoJ2Qtbm9uZScpXG4gICAgICAgICQoJy5zaGlwcGluZy1tb2JpbGUtaGVhZGVyLWNvbnRhaW5lcicpLmFkZENsYXNzKCdkLW5vbmUnKVxuICAgICAgICAkKCcuc2hpcHBpbmctbWFpbi1jb250ZW50LWNvbnRhaW5lcicpLnJlbW92ZUNsYXNzKCdtdC00JylcblxuICAgIH0gZWxzZSB7XG5cbiAgICAgICAgJCgnLmV4cGFuZGVkLXRvdGFsLWFuZC10YXgtdGFibGUtY29udGFpbmVyJykuYWRkQ2xhc3MoJ2Qtbm9uZScpXG4gICAgICAgICQoJy5zaGlwcGluZy1mb3JtLWNvbnRhaW5lcicpLnJlbW92ZUNsYXNzKCdkLW5vbmUnKVxuICAgICAgICAkKCcuc2hpcHBpbmctcHJvZ3Jlc3MtaW5kaWNhdG9yLWNvbnRhaW5lcicpLnJlbW92ZUNsYXNzKCdkLW5vbmUnKVxuICAgICAgICAkKCcuc2hpcHBpbmctbW9iaWxlLWhlYWRlci1jb250YWluZXInKS5yZW1vdmVDbGFzcygnZC1ub25lJylcbiAgICAgICAgJCgnLnNoaXBwaW5nLW1haW4tY29udGVudC1jb250YWluZXInKS5hZGRDbGFzcygnbXQtNCcpXG4gICAgfVxufVxuXG4vLyBQcm9kdWN0IGFjY29yZGlvbiBhbmQgdGFic1xuXG5leHBvcnQgZnVuY3Rpb24gcHJvZHVjdEFjY29yZGlvbigpIHtcblxuICAgIGxldCBwcm9kdWN0QWNjID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcbiAgICAgICAgXCJwcm9kdWN0X2FjY29yZGlvbl9idXR0b25cIlxuICAgICk7XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHByb2R1Y3RBY2MubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgcHJvZHVjdEFjY1tpXS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgYWNjb3JkaW9uQnV0dG9uQ2xpY2spO1xuICAgIH1cblxuICAgIGxldCBwcm9kdWN0RmVhdHVyZXNBY2MgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFxuICAgICAgICBcInByb2R1Y3RfZmVhdHVyZXNfYWNjb3JkaW9uX2J1dHRvblwiXG4gICAgKTtcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcHJvZHVjdEZlYXR1cmVzQWNjLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHByb2R1Y3RGZWF0dXJlc0FjY1tpXS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgKCkgPT5cbiAgICAgICAgICAgIHByb2R1Y3RGZWF0dXJlc0FjY29yZGlvbkJ1dHRvbkNsaWNrKGkpKTtcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBhY2NvcmRpb25CdXR0b25DbGljayhhY2NvcmRpb25CdXR0b24pIHtcblxuICAgIGxldCBwcm9kdWN0QWNjID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcbiAgICAgICAgXCJwcm9kdWN0X2FjY29yZGlvbl9idXR0b25cIlxuICAgICk7XG5cbiAgICBsZXQgdGFiTGlzdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0YWItbGlzdCcpXG4gICAgdGFiTGlzdC5jbGFzc05hbWUgPSAnJ1xuICAgIGxldCBwcm9kdWN0UGFuZWxzID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInByb2R1Y3RfcGFuZWxcIik7XG4gICAgbGV0IHRhYnMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwidGFiXCIpO1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwcm9kdWN0QWNjLmxlbmd0aDsgaSsrKSB7XG5cbiAgICAgICAgaWYgKGFjY29yZGlvbkJ1dHRvbiAhPT0gcHJvZHVjdEFjY1tpXSAmJiBhY2NvcmRpb25CdXR0b24ucGFyZW50RWxlbWVudCAhPT0gcHJvZHVjdEFjY1tpXSkge1xuXG4gICAgICAgICAgICB0YWJzW2ldLmNsYXNzTGlzdC5hZGQoJ2Qtbm9uZScpXG4gICAgICAgICAgICBwcm9kdWN0UGFuZWxzW2ldLnN0eWxlLm1heEhlaWdodCA9IG51bGxcbiAgICAgICAgICAgIHByb2R1Y3RBY2NbaV0uY2xhc3NMaXN0LnJlbW92ZSgnb3BlbicpXG5cbiAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgdGFiTGlzdC5jbGFzc0xpc3QuYWRkKCd0YWInICsgcHJvZHVjdEFjY1tpXS5pZC5zdWJzdHJpbmcoJ3Byb2R1Y3QtYWNjb3JkaW9uLWJ1dHRvbicubGVuZ3RoKSlcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0YWInICsgcHJvZHVjdEFjY1tpXS5pZC5zdWJzdHJpbmcoJ3Byb2R1Y3QtYWNjb3JkaW9uLWJ1dHRvbicubGVuZ3RoKSkuY2xhc3NMaXN0LnJlbW92ZSgnZC1ub25lJylcbiAgICAgICAgICAgIHByb2R1Y3RQYW5lbHNbaV0uc3R5bGUubWF4SGVpZ2h0ID0gcHJvZHVjdFBhbmVsc1tpXS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgICAgIHByb2R1Y3RBY2NbaV0uY2xhc3NMaXN0LmFkZCgnb3BlbicpXG4gICAgICAgIH1cbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB0b2dnbGUodGFiKSB7XG5cbiAgICBsZXQgdGFiTGlzdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0YWItbGlzdCcpXG4gICAgbGV0IHByb2R1Y3RQYW5lbHMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdwcm9kdWN0X3BhbmVsJylcbiAgICBsZXQgdGFicyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ3RhYicpXG4gICAgbGV0IHByb2R1Y3RBY2MgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFxuICAgICAgICBcInByb2R1Y3RfYWNjb3JkaW9uX2J1dHRvblwiXG4gICAgKTtcblxuICAgIHRhYkxpc3QuY2xhc3NOYW1lID0gXCJcIlxuICAgIHRhYkxpc3QuY2xhc3NMaXN0LmFkZCh0YWIpXG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRhYnMubGVuZ3RoOyBpKyspIHtcblxuICAgICAgICB0YWJzW2ldLmNsYXNzTGlzdC5hZGQoJ2Qtbm9uZScpXG5cbiAgICAgICAgaWYgKHByb2R1Y3RBY2NbaV0uaWQgIT09ICdwcm9kdWN0LWFjY29yZGlvbi1idXR0b24nICsgdGFiLnN1YnN0cmluZygndGFiJy5sZW5ndGgpKSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHByb2R1Y3RQYW5lbHNbaV0uc3R5bGUubWF4SGVpZ2h0ID0gbnVsbFxuICAgICAgICAgICAgcHJvZHVjdEFjY1tpXS5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJylcbiAgICAgICAgXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHByb2R1Y3RQYW5lbHNbaV0uc3R5bGUubWF4SGVpZ2h0ID0gcHJvZHVjdFBhbmVsc1tpXS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgICAgIHByb2R1Y3RBY2NbaV0uY2xhc3NMaXN0LmFkZCgnb3BlbicpXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCh0YWIpLmNsYXNzTGlzdC5yZW1vdmUoJ2Qtbm9uZScpXG59XG5cblxuLy8gUHJvZHVjdCBmZWF0dXJlcyBhY2NvcmRpb25cblxuZXhwb3J0IGZ1bmN0aW9uIHByb2R1Y3RGZWF0dXJlc0FjY29yZGlvbkJ1dHRvbkNsaWNrKCkge1xuXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInByb2R1Y3RfZmVhdHVyZXNfYWNjb3JkaW9uX2J1dHRvblwiKVswXS5jbGFzc0xpc3QudG9nZ2xlKFwib3BlblwiKVxuXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInByb2R1Y3RfZmVhdHVyZXNfYWNjb3JkaW9uX2J1dHRvblwiKVsxXS5jbGFzc0xpc3QudG9nZ2xlKFwib3BlblwiKVxuXG4gICAgbGV0IHByb2R1Y3RGZWF0dXJlc1BhbmVsMSA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJwcm9kdWN0X2ZlYXR1cmVzX2FjY29yZGlvbl9idXR0b25cIilbMF0ubmV4dEVsZW1lbnRTaWJsaW5nXG4gICAgbGV0IHByb2R1Y3RQYW5lbDEgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwicHJvZHVjdF9mZWF0dXJlc19hY2NvcmRpb25fYnV0dG9uXCIpWzBdLnBhcmVudEVsZW1lbnQucGFyZW50RWxlbWVudFxuICAgIGxldCBwcm9kdWN0RmVhdHVyZXNQYW5lbDIgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKFwicHJvZHVjdF9mZWF0dXJlc19hY2NvcmRpb25fYnV0dG9uXCIpWzFdLm5leHRFbGVtZW50U2libGluZ1xuICAgIGxldCBwcm9kdWN0UGFuZWwyID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcInByb2R1Y3RfZmVhdHVyZXNfYWNjb3JkaW9uX2J1dHRvblwiKVsxXS5wYXJlbnRFbGVtZW50LnBhcmVudEVsZW1lbnRcblxuICAgIGlmIChwcm9kdWN0RmVhdHVyZXNQYW5lbDEuc3R5bGUubWF4SGVpZ2h0KSB7XG5cbiAgICAgICAgcHJvZHVjdEZlYXR1cmVzUGFuZWwxLnN0eWxlLm1heEhlaWdodCA9IG51bGxcbiAgICAgICAgcHJvZHVjdEZlYXR1cmVzUGFuZWwyLnN0eWxlLm1heEhlaWdodCA9IG51bGxcblxuICAgIH0gZWxzZSB7XG5cbiAgICAgICAgaWYgKCQoXCIucHJvZHVjdC1mZWF0dXJlcy10YWJsZVwiKS5odG1sKCkgPT09IFwiXCIpIHtcblxuICAgICAgICAgICAgJChcIi5wcm9kdWN0LWZlYXR1cmVzLXNwaW5uZXItYm9yZGVyXCIpLnJlbW92ZUNsYXNzKFwiZC1ub25lXCIpXG5cbiAgICAgICAgICAgIGxldCB1cmwgPSB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUuc3BsaXQoXCIvXCIpLnBvcCgpXG5cbiAgICAgICAgICAgICQuZ2V0KFwiYXBpL3Byb2R1Y3QvXCIgKyB1cmwsIHt9LCBmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgJChcIi5wcm9kdWN0LWZlYXR1cmVzLXNwaW5uZXItYm9yZGVyXCIpLmFkZENsYXNzKFwiZC1ub25lXCIpXG5cbiAgICAgICAgICAgICAgICAkLmVhY2goZGF0YS5mZWF0dXJlcywgZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcblxuICAgICAgICAgICAgICAgICAgICAkKFwiLnByb2R1Y3QtZmVhdHVyZXMtdGFibGVcIikuaHRtbChgPHRyPjx0ZD4ke2tleX08L3RkPjx0ZD4ke3ZhbHVlfTwvdGQ+PC90cj5gKVxuICAgICAgICAgICAgICAgIH0pXG5cbiAgICAgICAgICAgICAgICBwcm9kdWN0RmVhdHVyZXNQYW5lbDEuc3R5bGUubWF4SGVpZ2h0ID0gcHJvZHVjdEZlYXR1cmVzUGFuZWwxLnNjcm9sbEhlaWdodCArIFwicHhcIlxuICAgICAgICAgICAgICAgIHByb2R1Y3RQYW5lbDEuc3R5bGUubWF4SGVpZ2h0ID0gcHJvZHVjdEZlYXR1cmVzUGFuZWwxLnNjcm9sbEhlaWdodCArIHByb2R1Y3RQYW5lbDEuc2Nyb2xsSGVpZ2h0ICsgXCJweFwiXG4gICAgICAgICAgICAgICAgcHJvZHVjdEZlYXR1cmVzUGFuZWwyLnN0eWxlLm1heEhlaWdodCA9IHByb2R1Y3RGZWF0dXJlc1BhbmVsMS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgICAgICAgICBwcm9kdWN0UGFuZWwyLnN0eWxlLm1heEhlaWdodCA9IHByb2R1Y3RGZWF0dXJlc1BhbmVsMS5zY3JvbGxIZWlnaHQgKyBwcm9kdWN0UGFuZWwxLnNjcm9sbEhlaWdodCArIFwicHhcIlxuICAgICAgICAgICAgfSwgJ2pzb24nKVxuICAgICAgICB9XG5cbiAgICAgICAgcHJvZHVjdEZlYXR1cmVzUGFuZWwxLnN0eWxlLm1heEhlaWdodCA9IHByb2R1Y3RGZWF0dXJlc1BhbmVsMS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgcHJvZHVjdFBhbmVsMS5zdHlsZS5tYXhIZWlnaHQgPSBwcm9kdWN0RmVhdHVyZXNQYW5lbDEuc2Nyb2xsSGVpZ2h0ICsgcHJvZHVjdFBhbmVsMS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgcHJvZHVjdEZlYXR1cmVzUGFuZWwyLnN0eWxlLm1heEhlaWdodCA9IHByb2R1Y3RGZWF0dXJlc1BhbmVsMS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICAgICAgcHJvZHVjdFBhbmVsMi5zdHlsZS5tYXhIZWlnaHQgPSBwcm9kdWN0RmVhdHVyZXNQYW5lbDEuc2Nyb2xsSGVpZ2h0ICsgcHJvZHVjdFBhbmVsMS5zY3JvbGxIZWlnaHQgKyBcInB4XCJcbiAgICB9XG59XG5cbi8vUHJvZHVjdCBjYXJvdXNlbFxuXG5sZXQgbWluUGVyU2xpZGVcblxuZXhwb3J0IGZ1bmN0aW9uIGhhbmRsZU5leHRMaW5rKG5leHRMaW5rKSB7XG5cbiAgICBsZXQgaXRlbXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcjcHJvZHVjdENhcm91c2VsIC5jYXJvdXNlbC1pdGVtJylcblxuICAgIGxldCBjdXJyZW50SW5kZXggPSAkKCcjcHJvZHVjdENhcm91c2VsIC5hY3RpdmUnKS5pbmRleCgpXG5cbiAgICBpZiAoY3VycmVudEluZGV4ICsgMSA9PT0gaXRlbXMubGVuZ3RoIC0gbWluUGVyU2xpZGUpIHtcblxuICAgICAgICBuZXh0TGluay5jbGFzc0xpc3QuYWRkKFwiZGlzYWJsZWRcIilcbiAgICB9IGVsc2Uge1xuXG4gICAgICAgIG5leHRMaW5rLmNsYXNzTGlzdC5yZW1vdmUoXCJkaXNhYmxlZFwiKVxuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG15RnVuY3Rpb24oKSB7XG5cbiAgICBsZXQgeHMgPSB3aW5kb3cubWF0Y2hNZWRpYShcIihtYXgtd2lkdGg6IDU3NXB4KVwiKVxuICAgIGxldCBzbSA9IHdpbmRvdy5tYXRjaE1lZGlhKFwiKG1pbi13aWR0aDogNTc2cHgpIGFuZCAobWF4LXdpZHRoOiA3NjdweClcIilcbiAgICBsZXQgbWQgPSB3aW5kb3cubWF0Y2hNZWRpYShcIihtaW4td2lkdGg6IDc2OHB4KSBhbmQgKG1heC13aWR0aDogOTkxcHgpXCIpXG4gICAgbGV0IGxnID0gd2luZG93Lm1hdGNoTWVkaWEoXCIobWluLXdpZHRoOiA5OTJweClcIilcblxuICAgIHhzLmFkZExpc3RlbmVyKG15RnVuY3Rpb24yKVxuICAgIHNtLmFkZExpc3RlbmVyKG15RnVuY3Rpb24yKVxuICAgIG1kLmFkZExpc3RlbmVyKG15RnVuY3Rpb24yKVxuICAgIGxnLmFkZExpc3RlbmVyKG15RnVuY3Rpb24yKVxuXG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3Byb2R1Y3RDYXJvdXNlbCAuY2Fyb3VzZWwtaXRlbScpLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXG5cbiAgICBteUZ1bmN0aW9uMih4cylcbiAgICBteUZ1bmN0aW9uMihzbSlcbiAgICBteUZ1bmN0aW9uMihtZClcbiAgICBteUZ1bmN0aW9uMihsZylcbn1cblxuZnVuY3Rpb24gbXlGdW5jdGlvbjIoeCkge1xuXG4gICAgaWYgKHgubWF0Y2hlcykge1xuXG4gICAgICAgIGlmICh4Lm1lZGlhID09IFwiKG1heC13aWR0aDogNTc1cHgpXCIpIHtcblxuICAgICAgICAgICAgbWluUGVyU2xpZGUgPSAyXG5cbiAgICAgICAgfSBlbHNlIGlmICh4Lm1lZGlhID09IFwiKG1pbi13aWR0aDogNTc2cHgpIGFuZCAobWF4LXdpZHRoOiA3NjdweClcIikge1xuXG4gICAgICAgICAgICBtaW5QZXJTbGlkZSA9IDNcblxuICAgICAgICB9IGVsc2UgaWYgKHgubWVkaWEgPT0gXCIobWluLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtd2lkdGg6IDk5MXB4KVwiKSB7XG5cbiAgICAgICAgICAgIG1pblBlclNsaWRlID0gNFxuXG4gICAgICAgIH0gZWxzZSBpZiAoeC5tZWRpYSA9PSBcIihtaW4td2lkdGg6IDk5MnB4KVwiKSB7XG5cbiAgICAgICAgICAgIG1pblBlclNsaWRlID0gNVxuXG4gICAgICAgIH1cblxuICAgICAgICBteUZ1bmN0aW9uMyhtaW5QZXJTbGlkZSlcbiAgICB9XG59XG5cbmZ1bmN0aW9uIG15RnVuY3Rpb24zKG1pblBlclNsaWRlKSB7XG5cbiAgICBsZXQgbmV4dExpbmsgPSAkKCcuY2Fyb3VzZWwtY29udHJvbC1uZXh0JylcblxuICAgIGxldCBpdGVtcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJyNwcm9kdWN0Q2Fyb3VzZWwgLmNhcm91c2VsLWl0ZW0nKVxuXG4gICAgbGV0IG51bVBlclNsaWRlID0gaXRlbXNbMF0uY2hpbGRFbGVtZW50Q291bnRcblxuICAgIGlmIChudW1QZXJTbGlkZSA9PT0gMSkge1xuXG4gICAgICAgIGl0ZW1zLmZvckVhY2goKGVsKSA9PiB7XG5cbiAgICAgICAgICAgIGxldCBuZXh0ID0gZWwubmV4dEVsZW1lbnRTaWJsaW5nXG5cbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAxOyBpIDwgbWluUGVyU2xpZGU7IGkrKykge1xuXG4gICAgICAgICAgICAgICAgaWYgKCFuZXh0KSB7XG5cbiAgICAgICAgICAgICAgICAgICAgbmV4dCA9IGl0ZW1zWzBdXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgbGV0IGNsb25lQ2hpbGRyZW4gPSAkKG5leHQpLmNoaWxkcmVuKClcbiAgICAgICAgICAgICAgICBsZXQgY2xvbmVDaGlsZCA9ICQoY2xvbmVDaGlsZHJlbikuZmlyc3QoKS5jbG9uZSh0cnVlKVxuICAgICAgICAgICAgICAgIGNsb25lQ2hpbGQuYXBwZW5kVG8oZWwpXG4gICAgICAgICAgICAgICAgbmV4dCA9IG5leHQubmV4dEVsZW1lbnRTaWJsaW5nXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfSBlbHNlIGlmIChudW1QZXJTbGlkZSA8IG1pblBlclNsaWRlKSB7XG5cbiAgICAgICAgaXRlbXMuZm9yRWFjaCgoZWwpID0+IHtcbiAgICAgICAgICAgIGxldCBuZXh0ID0gZWwubmV4dEVsZW1lbnRTaWJsaW5nXG5cbiAgICAgICAgICAgIGlmICghbmV4dCkge1xuXG4gICAgICAgICAgICAgICAgbmV4dCA9IGl0ZW1zWzBdXG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGxldCBpID0gbnVtUGVyU2xpZGVcblxuICAgICAgICAgICAgd2hpbGUgKGkgPCBtaW5QZXJTbGlkZSkge1xuXG4gICAgICAgICAgICAgICAgbGV0IGNsb25lQ2hpbGQgPSBuZXh0LmNsb25lTm9kZSh0cnVlKVxuICAgICAgICAgICAgICAgIGxldCBqID0gbnVtUGVyU2xpZGUgLSAxXG4gICAgICAgICAgICAgICAgbGV0IG5leHRDaGlsZCA9IGNsb25lQ2hpbGQuY2hpbGRyZW5bal1cblxuICAgICAgICAgICAgICAgIHdoaWxlIChuZXh0Q2hpbGQgJiYgaSA8IG1pblBlclNsaWRlKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgZWwuYXBwZW5kQ2hpbGQobmV4dENoaWxkKVxuICAgICAgICAgICAgICAgICAgICBuZXh0Q2hpbGQgPSBjbG9uZUNoaWxkLmNoaWxkcmVuWysral1cbiAgICAgICAgICAgICAgICAgICAgaSsrXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgbmV4dCA9IG5leHQubmV4dEVsZW1lbnRTaWJsaW5nXG5cbiAgICAgICAgICAgICAgICBpZiAoIW5leHQpIHtcblxuICAgICAgICAgICAgICAgICAgICBuZXh0ID0gaXRlbXNbMF1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG5cbiAgICAgICAgJCgnI3Byb2R1Y3RDYXJvdXNlbCAuYWN0aXZlJylbMF0uY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJylcbiAgICAgICAgJCgnI3Byb2R1Y3RDYXJvdXNlbCAuY2Fyb3VzZWwtaXRlbScpWzBdLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXG5cbiAgICAgICAgbmV4dExpbmtbMF0uY2xhc3NMaXN0LnJlbW92ZShcImRpc2FibGVkXCIpXG5cbiAgICB9IGVsc2UgaWYgKG51bVBlclNsaWRlID4gbWluUGVyU2xpZGUpIHtcblxuICAgICAgICBpdGVtcy5mb3JFYWNoKChlbCkgPT4ge1xuXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gbnVtUGVyU2xpZGUgLSAxOyBpID4gbWluUGVyU2xpZGUgLSAxOyBpLS0pIHtcblxuICAgICAgICAgICAgICAgIGVsLnJlbW92ZUNoaWxkKGVsLmNoaWxkcmVuW2ldKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuXG4gICAgICAgICQoJyNwcm9kdWN0Q2Fyb3VzZWwgLmFjdGl2ZScpWzBdLmNsYXNzTGlzdC5yZW1vdmUoJ2FjdGl2ZScpXG4gICAgICAgICQoJyNwcm9kdWN0Q2Fyb3VzZWwgLmNhcm91c2VsLWl0ZW0nKVswXS5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxuXG4gICAgICAgIG5leHRMaW5rWzBdLmNsYXNzTGlzdC5yZW1vdmUoXCJkaXNhYmxlZFwiKVxuICAgIH1cbn1cblxuLy8gUHJvZHVjdCBkZXRhaWxzXG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkUHJvZHVjdChwcm9kdWN0KSB7XG5cbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcblxuICAgICAgICAkKFwiLmFkZC10by1jYXJ0LWZvcm1cIikucmVtb3ZlQ2xhc3MoXCJpbnZpc2libGVcIilcbiAgICAgICAgJChcIi5vcmRlci1idG5cIikuY3NzKFwib3BhY2l0eVwiLCAxKVxuXG4gICAgICAgIGlmICgkKFwiLmluLXN0b3JlXCIpKSB7XG5cbiAgICAgICAgICAgICQoXCIuaW4tc3RvcmUtY2lyY2xlXCIpLmFkZENsYXNzKFwiZmFzIGZhLWNpcmNsZVwiKS5jc3MoXCJvcGFjaXR5XCIsIDEpXG4gICAgICAgICAgICAkKFwiLmluLXN0b3JlXCIpLnRleHQocHJvZHVjdC5pblN0b3JlKS5jc3MoXCJvcGFjaXR5XCIsIDEpXG4gICAgICAgICAgICAkKFwiLnByb2R1Y3QtZGV0YWlscy13YWl0LXNwaW5uZXItYm9yZGVyXCIpLmFkZENsYXNzKFwiZC1ub25lXCIpXG4gICAgICAgIH1cblxuICAgICAgICAkKFwiLnZhdFwiKS50ZXh0KGBTaXMuIEFMVjpuICgke3BhcnNlSW50KHByb2R1Y3QudmF0ICogMTAwKX0lKWApXG4gICAgICAgICQoXCIucHJpY2VcIikuaHRtbChwcm9kdWN0LnByaWNlICsgXCIgJmV1cm87XCIpXG5cbiAgICB9LCAxMDAwKVxuXG59XG5cbi8vIFByb2R1Y3QgbGlzdCBkZXRhaWxzXG5cbmV4cG9ydCBmdW5jdGlvbiBsb2FkUHJvZHVjdHMocHJvZHVjdHMpIHtcblxuICAgIHByb2R1Y3RzLm1hcCgocHJvZHVjdCA9PiB7XG5cbiAgICAgICAgJChcIi5wcmljZS5cIiArIHByb2R1Y3QudXJsKS5odG1sKHByb2R1Y3QucHJpY2UgKyBcIiAmZXVybztcIilcbiAgICAgICAgJChcIi52YXQuXCIgKyBwcm9kdWN0LnVybCkudGV4dChgU2lzLiBBTFY6biAoJHtwYXJzZUludChwcm9kdWN0LnZhdCAqIDEwMCl9JSlgKVxuXG4gICAgICAgIGlmICgkKFwiLmluLXN0b3JlXCIpKSB7XG5cbiAgICAgICAgICAgICQoXCIuaW4tc3RvcmUtY2lyY2xlLlwiICsgcHJvZHVjdC51cmwpLmFkZENsYXNzKFwiZmFzIGZhLWNpcmNsZVwiKS5jc3MoXCJvcGFjaXR5XCIsIDEpXG4gICAgICAgICAgICAkKFwiLmluLXN0b3JlLlwiICsgcHJvZHVjdC51cmwpLnRleHQocHJvZHVjdC5pblN0b3JlKS5jc3MoXCJvcGFjaXR5XCIsIDEpXG4gICAgICAgIH1cbiAgICB9KSlcbn1cblxuLy8gU2lkZSBuYXZpZ2F0aW9uXG5cbmV4cG9ydCBmdW5jdGlvbiBvcGVuTmF2KCkge1xuXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJteVNpZGVuYXZcIikuc3R5bGUud2lkdGggPSBcIjEwMCVcIlxuICAgICQoXCJib2R5XCIpLmNzcyhcIm92ZXJmbG93LXlcIiwgXCJoaWRkZW5cIilcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGNsb3NlTmF2KCkge1xuXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJteVNpZGVuYXZcIikuc3R5bGUud2lkdGggPSBcIjBcIlxuICAgICQoXCJib2R5XCIpLmNzcyhcIm92ZXJmbG93LXlcIiwgXCJhdXRvXCIpXG59IiwiaW1wb3J0IHsgc3RhcnRTdGltdWx1c0FwcCB9IGZyb20gJ0BzeW1mb255L3N0aW11bHVzLWJyaWRnZSc7XG5cbi8vIFJlZ2lzdGVycyBTdGltdWx1cyBjb250cm9sbGVycyBmcm9tIGNvbnRyb2xsZXJzLmpzb24gYW5kIGluIHRoZSBjb250cm9sbGVycy8gZGlyZWN0b3J5XG5leHBvcnQgY29uc3QgYXBwID0gc3RhcnRTdGltdWx1c0FwcChyZXF1aXJlLmNvbnRleHQoXG4gICAgJ0BzeW1mb255L3N0aW11bHVzLWJyaWRnZS9sYXp5LWNvbnRyb2xsZXItbG9hZGVyIS4vY29udHJvbGxlcnMnLFxuICAgIHRydWUsXG4gICAgL1xcLihqfHQpc3g/JC9cbikpO1xuXG4vLyByZWdpc3RlciBhbnkgY3VzdG9tLCAzcmQgcGFydHkgY29udHJvbGxlcnMgaGVyZVxuLy8gYXBwLnJlZ2lzdGVyKCdzb21lX2NvbnRyb2xsZXJfbmFtZScsIFNvbWVJbXBvcnRlZENvbnRyb2xsZXIpO1xuIiwiaW1wb3J0IHsgQnJvd3NlclJvdXRlciBhcyBSb3V0ZXIsIFJvdXRlLCBTd2l0Y2ggfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBMYXlvdXQxIGZyb20gXCIuL0xheW91dDFcIjtcbmltcG9ydCBMYXlvdXQyIGZyb20gXCIuL0xheW91dDJcIjtcbmltcG9ydCBDYXJ0IGZyb20gXCIuL0NhcnRcIjtcbmltcG9ydCBDb25maXJtYXRpb24gZnJvbSBcIi4vQ29uZmlybWF0aW9uXCI7XG5pbXBvcnQgSG9tZSBmcm9tIFwiLi9Ib21lXCI7XG5pbXBvcnQgUHJvZHVjdCBmcm9tIFwiLi9Qcm9kdWN0XCI7XG5pbXBvcnQgUHJvZHVjdHMgZnJvbSBcIi4vUHJvZHVjdHNcIjtcbmltcG9ydCBTaGlwcGluZyBmcm9tIFwiLi9TaGlwcGluZ1wiO1xuaW1wb3J0IE5vdEZvdW5kIGZyb20gXCIuL05vdEZvdW5kXCI7XG5pbXBvcnQgYXhpb3MgZnJvbSBcImF4aW9zXCI7XG5cbmNvbnN0IEFwcCA9ICgpID0+IHtcbiAgY29uc3QgW3Byb2R1Y3RzLCBzZXRQcm9kdWN0c10gPSB1c2VTdGF0ZSgpO1xuICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ10gPSB1c2VTdGF0ZSh0cnVlKTtcbiAgY29uc3QgW2xvYWRpbmdDYXJ0LCBzZXRMb2FkaW5nQ2FydF0gPSB1c2VTdGF0ZSh0cnVlKTtcbiAgY29uc3QgW2NhcnQsIHNldENhcnRdID0gdXNlU3RhdGUoKTtcbiAgY29uc3QgW3VwZGF0ZSwgc2V0VXBkYXRlXSA9IHVzZVN0YXRlKGZhbHNlKTtcblxuICBmdW5jdGlvbiBjYWxsYmFjayhfdXBkYXRlKSB7XG4gICAgc2V0VXBkYXRlKF91cGRhdGUpO1xuICB9XG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBheGlvcy5nZXQoXCIvYXBpL3Byb2R1Y3QvcHJvZHVjdHNcIikudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgIHNldFByb2R1Y3RzKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgc2V0TG9hZGluZyhmYWxzZSk7XG4gICAgfSk7XG5cbiAgICByZXR1cm4gKCkgPT4ge1xuICAgICAgc2V0TG9hZGluZygpO1xuICAgICAgc2V0UHJvZHVjdHMoKTtcbiAgICAgIHNldENhcnQoKTtcbiAgICB9O1xuICB9LCBbXSk7XG5cbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBheGlvcy5nZXQoXCIvYXBpL2NhcnQvZ2V0XCIpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICBzZXRDYXJ0KHJlc3BvbnNlLmRhdGEpO1xuICAgICAgc2V0TG9hZGluZ0NhcnQoZmFsc2UpO1xuXG4gICAgICBpZiAocmVzcG9uc2UuZGF0YS5pdGVtcy5sZW5ndGggPiAwKSB7XG4gICAgICAgICQoXCIuc2hvcHBpbmctY2FydC1zdW1tYXJ5XCIpLnJlbW92ZUNsYXNzKFwiaW52aXNpYmxlXCIpO1xuXG4gICAgICAgICQoXCIuc2hvcHBpbmctY2FydC1pdGVtcy1jb3VudFwiKS50ZXh0KHJlc3BvbnNlLmRhdGEuaXRlbXMubGVuZ3RoKTtcblxuICAgICAgICAkKFwiLnNob3BwaW5nLWNhcnQtdG90YWxcIikuaHRtbChcbiAgICAgICAgICByZXNwb25zZS5kYXRhLnRvdGFsLnRvRml4ZWQoMikgKyBcIiZuYnNwOyZldXJvO1wiXG4gICAgICAgICk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAkKFwiLnNob3BwaW5nLWNhcnQtc3VtbWFyeVwiKS5hZGRDbGFzcyhcImludmlzaWJsZVwiKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHJldHVybiAoXG4gICAgICAoKSA9PiB7XG4gICAgICAgIHNldExvYWRpbmcoKTtcbiAgICAgICAgc2V0TG9hZGluZ0NhcnQoKTtcbiAgICAgICAgc2V0VXBkYXRlKCk7XG4gICAgICAgIHNldENhcnQoKTtcbiAgICAgIH0sXG4gICAgICBbdXBkYXRlXVxuICAgICk7XG4gIH0pO1xuXG4gIHJldHVybiAoXG4gICAgPFJvdXRlcj5cbiAgICAgIDxTd2l0Y2g+XG4gICAgICAgIDxSb3V0ZSBleGFjdCBwYXRoPXtbXCIvXCIsIFwiL2NoZWNrb3V0XCIsIFwiL3Byb2R1Y3RzXCIsIFwiLzp1cmxcIl19PlxuICAgICAgICAgIDxMYXlvdXQxIGNhcnQ9e2NhcnR9PlxuICAgICAgICAgICAgPFN3aXRjaD5cbiAgICAgICAgICAgICAgPFJvdXRlIGV4YWN0IHBhdGg9XCIvXCI+XG4gICAgICAgICAgICAgICAgPEhvbWUgcHJvZHVjdHM9e3Byb2R1Y3RzfSBsb2FkaW5nPXtsb2FkaW5nfSAvPlxuICAgICAgICAgICAgICA8L1JvdXRlPlxuICAgICAgICAgICAgICA8Um91dGUgZXhhY3QgcGF0aD1cIi9wcm9kdWN0c1wiPlxuICAgICAgICAgICAgICAgIDxQcm9kdWN0cyBwcm9kdWN0cz17cHJvZHVjdHN9IGxvYWRpbmc9e2xvYWRpbmd9IC8+XG4gICAgICAgICAgICAgIDwvUm91dGU+XG4gICAgICAgICAgICAgIDxSb3V0ZSBleGFjdCBwYXRoPVwiL2NoZWNrb3V0XCI+XG4gICAgICAgICAgICAgICAgPENhcnQgY2FydD17Y2FydH0gbG9hZGluZz17bG9hZGluZ0NhcnR9IGNhbGxiYWNrPXtjYWxsYmFja30gLz5cbiAgICAgICAgICAgICAgPC9Sb3V0ZT5cbiAgICAgICAgICAgICAgPFJvdXRlIGV4YWN0IHBhdGg9XCIvOnVybFwiPlxuICAgICAgICAgICAgICAgIDxQcm9kdWN0IGNhbGxiYWNrPXtjYWxsYmFja30gLz5cbiAgICAgICAgICAgICAgPC9Sb3V0ZT5cbiAgICAgICAgICAgICAgPFJvdXRlIHBhdGg9XCIqXCIgY29tcG9uZW50PXtOb3RGb3VuZH0gLz5cbiAgICAgICAgICAgIDwvU3dpdGNoPlxuICAgICAgICAgIDwvTGF5b3V0MT5cbiAgICAgICAgPC9Sb3V0ZT5cbiAgICAgICAgPFJvdXRlIHBhdGg9e1tcIi9jaGVja291dC9zaGlwcGluZ1wiLCBcIi9jaGVja291dC9jb25maXJtYXRpb25cIl19PlxuICAgICAgICAgIDxMYXlvdXQyPlxuICAgICAgICAgICAgPFN3aXRjaD5cbiAgICAgICAgICAgICAgPFJvdXRlIGV4YWN0IHBhdGg9XCIvY2hlY2tvdXQvc2hpcHBpbmdcIj5cbiAgICAgICAgICAgICAgICA8U2hpcHBpbmcgY2FydD17Y2FydH0gbG9hZGluZz17bG9hZGluZ0NhcnR9IC8+XG4gICAgICAgICAgICAgIDwvUm91dGU+XG4gICAgICAgICAgICAgIDxSb3V0ZVxuICAgICAgICAgICAgICAgIGV4YWN0XG4gICAgICAgICAgICAgICAgcGF0aD1cIi9jaGVja291dC9jb25maXJtYXRpb25cIlxuICAgICAgICAgICAgICAgIGNvbXBvbmVudD17Q29uZmlybWF0aW9ufVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9Td2l0Y2g+XG4gICAgICAgICAgPC9MYXlvdXQyPlxuICAgICAgICA8L1JvdXRlPlxuICAgICAgPC9Td2l0Y2g+XG4gICAgPC9Sb3V0ZXI+XG4gICk7XG59O1xuZXhwb3J0IGRlZmF1bHQgQXBwO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgTGluaywgdXNlSGlzdG9yeSB9IGZyb20gXCJyZWFjdC1yb3V0ZXItZG9tXCI7XG5pbXBvcnQge1xuICB2YWxpZGF0ZVJlZHVjZVF1YW50aXR5LFxuICB2YWxpZGF0ZUluY3JlYXNlUXVhbnRpdHksXG4gIGJsdXJQYWdlLFxuICByZW1vdmVJdGVtLFxuICBjaGFuZ2VRdWFudGl0eSxcbn0gZnJvbSBcIi4uL2FwcFwiO1xuaW1wb3J0IExvYWRpbmcgZnJvbSBcIi4vTG9hZGluZ1wiO1xuaW1wb3J0IEVtcHR5Q2FydCBmcm9tIFwiLi9FbXB0eUNhcnRcIjtcblxuY29uc3QgQ2FydCA9ICh7IGNhcnQsIGxvYWRpbmcsIGNhbGxiYWNrIH0pID0+IHtcbiAgbGV0IGhpc3RvcnkgPSB1c2VIaXN0b3J5KCk7XG5cbiAgcmV0dXJuIGxvYWRpbmcgPyAoXG4gICAgPExvYWRpbmcgLz5cbiAgKSA6IGNhcnQuaXRlbXMubGVuZ3RoID09PSAwID8gKFxuICAgIDxFbXB0eUNhcnQgLz5cbiAgKSA6IChcbiAgICA8bWFpbj5cbiAgICAgIHtjYXJ0LnRvdGFsIDwgMzAgPyAoXG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9XCJtaW4tb3JkZXItYW1vdW50LXdhcm5pbmcgYmctd2hpdGUgcC00IG1iLTIgbXMtMyBtZS0zIG10LTQgZC1mbGV4IGp1c3RpZnktY29udGVudC1iZXR3ZWVuXCJcbiAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgZm9udFNpemU6IFwiMTJweFwiLFxuICAgICAgICAgICAgYm9yZGVyOiBcIjFweCBzb2xpZCAjZTc3MTEwXCIsXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJyZWxhdGl2ZVwiLFxuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICBjb2xvcjogXCIjZTc3MTEwXCIsXG4gICAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXG4gICAgICAgICAgICAgIGxlZnQ6IFwiMzBweFwiLFxuICAgICAgICAgICAgICB0b3A6IFwiMjdweFwiLFxuICAgICAgICAgICAgfX1cbiAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICAgICAgd2lkdGg9XCIyMFwiXG4gICAgICAgICAgICBoZWlnaHQ9XCIyMFwiXG4gICAgICAgICAgICBmaWxsPVwiY3VycmVudENvbG9yXCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImJpIGJpLWV4Y2xhbWF0aW9uLXRyaWFuZ2xlXCJcbiAgICAgICAgICAgIHZpZXdCb3g9XCIwIDAgMTYgMTZcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxwYXRoIGQ9XCJNNy45MzggMi4wMTZBLjEzLjEzIDAgMCAxIDguMDAyIDJhLjEzLjEzIDAgMCAxIC4wNjMuMDE2LjE0Ni4xNDYgMCAwIDEgLjA1NC4wNTdsNi44NTcgMTEuNjY3Yy4wMzYuMDYuMDM1LjEyNC4wMDIuMTgzYS4xNjMuMTYzIDAgMCAxLS4wNTQuMDYuMTE2LjExNiAwIDAgMS0uMDY2LjAxN0gxLjE0NmEuMTE1LjExNSAwIDAgMS0uMDY2LS4wMTcuMTYzLjE2MyAwIDAgMS0uMDU0LS4wNi4xNzYuMTc2IDAgMCAxIC4wMDItLjE4M0w3Ljg4NCAyLjA3M2EuMTQ3LjE0NyAwIDAgMSAuMDU0LS4wNTd6bTEuMDQ0LS40NWExLjEzIDEuMTMgMCAwIDAtMS45NiAwTC4xNjUgMTMuMjMzYy0uNDU3Ljc3OC4wOTEgMS43NjcuOTggMS43NjdoMTMuNzEzYy44ODkgMCAxLjQzOC0uOTkuOTgtMS43NjdMOC45ODIgMS41NjZ6XCIgLz5cbiAgICAgICAgICAgIDxwYXRoIGQ9XCJNNy4wMDIgMTJhMSAxIDAgMSAxIDIgMCAxIDEgMCAwIDEtMiAwek03LjEgNS45OTVhLjkwNS45MDUgMCAxIDEgMS44IDBsLS4zNSAzLjUwN2EuNTUyLjU1MiAwIDAgMS0xLjEgMEw3LjEgNS45OTV6XCIgLz5cbiAgICAgICAgICA8L3N2Zz5cbiAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCJsaWdodGdyYXlcIiB9fVxuICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICB3aWR0aD1cIjMyXCJcbiAgICAgICAgICAgIGhlaWdodD1cIjMyXCJcbiAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiYmkgYmktY2lyY2xlLWZpbGxcIlxuICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGNpcmNsZSBjeD1cIjhcIiBjeT1cIjhcIiByPVwiOFwiIC8+XG4gICAgICAgICAgPC9zdmc+XG4gICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgIGNsYXNzTmFtZT1cInBzLTRcIlxuICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgY29sb3I6IFwiI2U3NzExMFwiLFxuICAgICAgICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxuICAgICAgICAgICAgICBsZWZ0OiBcIjYwcHhcIixcbiAgICAgICAgICAgICAgbGluZUhlaWdodDogXCIzMnB4XCIsXG4gICAgICAgICAgICB9fVxuICAgICAgICAgID5cbiAgICAgICAgICAgIFBpZW5pbiB0aWxhdXNtw6TDpHLDpCBvbiAzMCwwMCAmZXVybztcbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPExpbmtcbiAgICAgICAgICAgIHRvPVwiL2NoZWNrb3V0XCJcbiAgICAgICAgICAgIHN0eWxlPXt7IGN1cnNvcjogXCJwb2ludGVyXCIgfX1cbiAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgZG9jdW1lbnRcbiAgICAgICAgICAgICAgICAuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZShcIm1pbi1vcmRlci1hbW91bnQtd2FybmluZ1wiKVswXVxuICAgICAgICAgICAgICAgIC5jbGFzc0xpc3QuYWRkKFwiZC1ub25lXCIpO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiNlNzcxMTBcIiB9fVxuICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgd2lkdGg9XCIzMlwiXG4gICAgICAgICAgICAgIGhlaWdodD1cIjMyXCJcbiAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJpIGJpLXhcIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGggZD1cIk00LjY0NiA0LjY0NmEuNS41IDAgMCAxIC43MDggMEw4IDcuMjkzbDIuNjQ2LTIuNjQ3YS41LjUgMCAwIDEgLjcwOC43MDhMOC43MDcgOGwyLjY0NyAyLjY0NmEuNS41IDAgMCAxLS43MDguNzA4TDggOC43MDdsLTIuNjQ2IDIuNjQ3YS41LjUgMCAwIDEtLjcwOC0uNzA4TDcuMjkzIDggNC42NDYgNS4zNTRhLjUuNSAwIDAgMSAwLS43MDh6XCIgLz5cbiAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApIDogbnVsbH1cbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPVwiY2hlY2tvdXQtdG9wLWNvbnRhaW5lclwiXG4gICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEycHhcIiwgY29sb3I6IFwiIzVmNWY1ZlwiIH19XG4gICAgICA+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZS1ibG9jayBjaGVja291dC1tYWluLWNvbnRhaW5lclwiXG4gICAgICAgICAgc3R5bGU9e3sgaGVpZ2h0OiBcImZpdENvbnRlbnRcIiB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGgyIGNsYXNzTmFtZT1cImNhcnQtZGV0YWlscy10aXRsZSBtdC0yIHAtNCBtYi0wIGJnLXdoaXRlIGZ3LWJvbGRcIj5cbiAgICAgICAgICAgIE9TVE9LT1JJXG4gICAgICAgICAgPC9oMj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcnQtZGV0YWlscy1jb250YWluZXIgcC00IGJnLXdoaXRlXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcnQtZGV0YWlsc1wiPlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlciBwLTNcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmRDb2xvcjogXCJsaWdodGdyYXlcIiwgaGVpZ2h0OiBcIjcwcHhcIiB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogXCI0MCVcIiB9fSBjbGFzc05hbWU9XCJjYXJ0LWNvbnRlbnRzLWhlYWRpbmdcIj5cbiAgICAgICAgICAgICAgICAgIFR1b3RlXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogXCIxNSVcIiB9fSBjbGFzc05hbWU9XCJjYXJ0LWNvbnRlbnRzLWhlYWRpbmdcIj5cbiAgICAgICAgICAgICAgICAgIFlrc2lra8O2aGludGFcbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IHdpZHRoOiBcIjMwJVwiIH19IGNsYXNzTmFtZT1cImNhcnQtY29udGVudHMtaGVhZGluZ1wiPlxuICAgICAgICAgICAgICAgICAgS3BsL3RuXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogXCIxNSVcIiB9fSBjbGFzc05hbWU9XCJjYXJ0LWNvbnRlbnRzLWhlYWRpbmdcIj5cbiAgICAgICAgICAgICAgICAgIEtva29uYWlzaGludGEgaWxtYW4gQUxWOnTDpFxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAge2NhcnQuaXRlbXMubWFwKChpdGVtLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtZmxleCBhbGlnbi1pdGVtcy10b3AgcHQtMyBwYi0zXCJcbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGJvcmRlckJvdHRvbTogXCIxcHggc29saWQgbGlnaHRncmF5XCIgfX1cbiAgICAgICAgICAgICAgICAgIGtleT17aW5kZXh9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCI0MCVcIiB9fVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJjYXJ0LWNvbnRlbnRzLWl0ZW0gZC1mbGV4XCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPExpbmtcbiAgICAgICAgICAgICAgICAgICAgICB0bz17eyBwYXRobmFtZTogaXRlbS5wcm9kdWN0LnVybCwgc3RhdGU6IGl0ZW0ucHJvZHVjdCB9fVxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiNzJweFwiLCBoZWlnaHQ6IFwiNzJweFwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBzcmM9e2l0ZW0ucHJvZHVjdC5pbWFnZVVybH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtaW5saW5lXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPExpbmtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvPXt7IHBhdGhuYW1lOiBpdGVtLnByb2R1Y3QudXJsLCBzdGF0ZTogaXRlbS5wcm9kdWN0IH19XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB0ZXh0RGVjb3JhdGlvbjogXCJub25lXCIsIGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+e2l0ZW0ucHJvZHVjdC50aXRsZX08L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgY29sb3I6IFwibGlnaHRncmF5XCIsIGZvbnRTaXplOiBcIjEwcHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAge2l0ZW0ucHJvZHVjdC5pbWFnZVVybH1cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6IFwiMTUlXCIgfX0gY2xhc3NOYW1lPVwiY2FydC1jb250ZW50cy1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgICAgeyhpdGVtLnByb2R1Y3QucHJpY2UgKiAoMSAtIGl0ZW0ucHJvZHVjdC52YXQpKS50b0ZpeGVkKDIpfXtcIiBcIn1cbiAgICAgICAgICAgICAgICAgICAgICAmZXVybztcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgY29sb3I6IFwibGlnaHRncmF5XCIsIGZvbnRTaXplOiBcIjEwcHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgICBJbG1hbiBBTFY6dMOkXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PntpdGVtLnByb2R1Y3QucHJpY2V9ICZldXJvOzwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGNvbG9yOiBcImxpZ2h0Z3JheVwiLCBmb250U2l6ZTogXCIxMHB4XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgU2lzLiBBTFY6biAoMTQgJSlcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6IFwiMzAlXCIgfX0gY2xhc3NOYW1lPVwiY2FydC1jb250ZW50cy1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzTmFtZT1cImQtZmxleFwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwidXJsLWlucHV0XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJ1cmxcIlxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cImhpZGRlblwiXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU9e2l0ZW0ucHJvZHVjdC51cmx9XG4gICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicXVhbnRpdHktY2hhbmdlLWJ0biB0ZXh0LWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBtYXJnaW5SaWdodDogXCIxMHB4XCIsIGxpbmVIZWlnaHQ6IFwiNDBweFwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGJsdXJQYWdlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHZhbGlkYXRlUmVkdWNlUXVhbnRpdHkoKSA/IGNoYW5nZVF1YW50aXR5KCkgOiBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgLVxuICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJxdWFudGl0eVwiXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwibnVtYmVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT17aXRlbS5xdWFudGl0eX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInF1YW50aXR5LWlucHV0IGQtaW5saW5lIGZvcm0tY29udHJvbCB0ZXh0LWNlbnRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIxMnB4XCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJxdWFudGl0eS1jaGFuZ2UtYnRuIHRleHQtY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IG1hcmdpbkxlZnQ6IFwiMTBweFwiLCBsaW5lSGVpZ2h0OiBcIjQwcHhcIiB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBibHVyUGFnZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0ZUluY3JlYXNlUXVhbnRpdHkoKSA/IGNoYW5nZVF1YW50aXR5KCkgOiBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgICAgK1xuICAgICAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCI0MCVcIiB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXIgYWxpZ24taXRlbXMtY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiwgY3Vyc29yOiBcInBvaW50ZXJcIiB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg9XCIxNlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodD1cIjE2XCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJpIGJpLXRyYXNoXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBibHVyUGFnZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbW92ZUl0ZW0oaXRlbS5wcm9kdWN0LnVybCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9XCJNNS41IDUuNUEuNS41IDAgMCAxIDYgNnY2YS41LjUgMCAwIDEtMSAwVjZhLjUuNSAwIDAgMSAuNS0uNXptMi41IDBhLjUuNSAwIDAgMSAuNS41djZhLjUuNSAwIDAgMS0xIDBWNmEuNS41IDAgMCAxIC41LS41em0zIC41YS41LjUgMCAwIDAtMSAwdjZhLjUuNSAwIDAgMCAxIDBWNnpcIiAvPlxuICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGxSdWxlPVwiZXZlbm9kZFwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZD1cIk0xNC41IDNhMSAxIDAgMCAxLTEgMUgxM3Y5YTIgMiAwIDAgMS0yIDJINWEyIDIgMCAwIDEtMi0yVjRoLS41YTEgMSAwIDAgMS0xLTFWMmExIDEgMCAwIDEgMS0xSDZhMSAxIDAgMCAxIDEtMWgyYTEgMSAwIDAgMSAxIDFoMy41YTEgMSAwIDAgMSAxIDF2MXpNNC4xMTggNCA0IDQuMDU5VjEzYTEgMSAwIDAgMCAxIDFoNmExIDEgMCAwIDAgMS0xVjQuMDU5TDExLjg4MiA0SDQuMTE4ek0yLjUgM1YyaDExdjFoLTExelwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L3N2Zz5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9mb3JtPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiBcIjEwcHhcIixcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogXCI0OHB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIxNDBweFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcImxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6IFwiI2I1NWYxOFwiLFxuICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibm9ucG9zaXRpdmUtcXVhbnRpdHktd2FybmluZyB0ZXh0LWNlbnRlciBwLTIgbXQtMiBtYi0yIGQtbm9uZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2Pk3DpMOkcsOkIGVpIG9sZSBzYWxsaXR1bjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXY+cmFqYW4gc2lzw6RsbMOkLjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogXCIxNSVcIiB9fSBjbGFzc05hbWU9XCJjYXJ0LWNvbnRlbnRzLWl0ZW1cIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICAgICAgICB7KGl0ZW0udG90YWwgKiAoMSAtIGl0ZW0ucHJvZHVjdC52YXQpKS50b0ZpeGVkKDIpfSAmZXVybztcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgY29sb3I6IFwibGlnaHRncmF5XCIsIGZvbnRTaXplOiBcIjEwcHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgICBJbG1hbiBBTFY6dMOkXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PntpdGVtLnRvdGFsLnRvRml4ZWQoMil9ICZldXJvOzwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGNvbG9yOiBcImxpZ2h0Z3JheVwiLCBmb250U2l6ZTogXCIxMHB4XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgU2lzLiBBTFY6biAoMTQgJSlcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvbW8tY29kZS1jb250YWluZXIgcHMtMiBwZS0yIHB0LTMgcGItMyBtdC0yXCJcbiAgICAgICAgICAgICAgc3R5bGU9e3sgYm9yZGVyOiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIiB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm1iLTNcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiwgZm9udFdlaWdodDogXCJib2xkXCIgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIE9ua28gc2ludWxsYSBhbGVubnVza29vZGk/XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGZvcm0+XG4gICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBib3hTaGFkb3c6IFwibm9uZVwiLFxuICAgICAgICAgICAgICAgICAgICBib3JkZXJSYWRpdXM6IFwiMFwiLFxuICAgICAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIxM3B4XCIsXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiBcImxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbCBmdy1ib2xkXCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT1cIlN5w7Z0w6QgYWxlbm51c2tvb2RpXCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImNoZWNrb3V0LXBhZ2UtYnRuIHctMTAwIG10LTFcIlxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgICAgICAgdGV4dEFsaWduOiBcImNlbnRlclwiLFxuICAgICAgICAgICAgICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICBLw6R5dGUgYWxlbm51c3RhXG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiwgaGVpZ2h0OiBcImZpdC1jb250ZW50XCIgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJ0b3RhbC1hbmQtdGF4LWNvbnRhaW5lciBtYi0yIG1lLTMgcC00IGJnLXdoaXRlXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxoNiBjbGFzc05hbWU9XCJmdy1ib2xkIHRleHQtbGVmdFwiPlRJTEFVS1NFU0k8L2g2PlxuICAgICAgICAgIDxociAvPlxuICAgICAgICAgIDx0YWJsZSBjbGFzc05hbWU9XCJ0b3RhbC1hbmQtdGF4LXRhYmxlIHctMTAwXCI+XG4gICAgICAgICAgICA8dGJvZHk+XG4gICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICA8dGQ+S29rb25haXNoaW50YSBpbG1hbiBBTFY6dMOkPC90ZD5cbiAgICAgICAgICAgICAgICA8dGQ+eyhjYXJ0LnRvdGFsIC0gY2FydC50b3RhbFZhdCkudG9GaXhlZCgyKX0gJmV1cm87PC90ZD5cbiAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgIDx0ZD5Lb2tvbmFpc2hpbnRhIEFMVjrDpG4ga2Fuc3NhPC90ZD5cbiAgICAgICAgICAgICAgICA8dGQ+e2NhcnQudG90YWwudG9GaXhlZCgyKX0gJmV1cm87PC90ZD5cbiAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgPHRyPlxuICAgICAgICAgICAgICAgIDx0ZD5WZXJvdDwvdGQ+XG4gICAgICAgICAgICAgICAgPHRkPntjYXJ0LnRvdGFsVmF0LnRvRml4ZWQoMil9ICZldXJvOzwvdGQ+XG4gICAgICAgICAgICAgIDwvdHI+XG4gICAgICAgICAgICAgIDx0cj5cbiAgICAgICAgICAgICAgICA8dGQ+VG9pbWl0dXNrdWx1dDwvdGQ+XG4gICAgICAgICAgICAgICAgPHRkPklsbWFpbmVuPC90ZD5cbiAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgPHRyXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgYm9yZGVyVG9wOiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIiwgZm9udFdlaWdodDogXCJib2xkXCIgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDx0ZD5Lb2tvbmFpc2hpbnRhIEFMVjrDpG4ga2Fuc3NhPC90ZD5cbiAgICAgICAgICAgICAgICA8dGQ+e2NhcnQudG90YWwudG9GaXhlZCgyKX0gJmV1cm87PC90ZD5cbiAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgPHRyIGNsYXNzTmFtZT1cImQtbGctbm9uZVwiPlxuICAgICAgICAgICAgICAgIDx0ZD48L3RkPlxuICAgICAgICAgICAgICAgIDx0ZD5cbiAgICAgICAgICAgICAgICAgIHtjYXJ0LnRvdGFsIDwgMzAgPyAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBvcGFjaXR5OiBcIjAuNVwiLCBjdXJzb3I6IFwicG9pbnRlclwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiY2hlY2tvdXQtcGFnZS1idG4gcHJvY2VlZC10by1jaGVja291dC1idG4gdy0xMDAgbXQtMVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBTbGlycnkga2Fzc2FsbGVcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgIGhpc3RvcnkucHVzaChcIi9jaGVja291dC9zaGlwcGluZ1wiKVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBjdXJzb3I6IFwicG9pbnRlclwiLCB0ZXh0QWxpZ246IFwiY2VudGVyXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJjaGVja291dC1wYWdlLWJ0biBwcm9jZWVkLXRvLWNoZWNrb3V0LWJ0biB3LTEwMCBtdC0xXCJcbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgIFNsaXJyeSBrYXNzYWxsZVxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgPC90ZD5cbiAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgIDwvdGJvZHk+XG4gICAgICAgICAgPC90YWJsZT5cblxuICAgICAgICAgIHtjYXJ0LnRvdGFsIDwgMzAgPyAoXG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgIHN0eWxlPXt7IGN1cnNvcjogXCJwb2ludGVyXCIsIG9wYWNpdHk6IFwiMC41XCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1ub25lIGQtbGctYmxvY2sgY2hlY2tvdXQtcGFnZS1idG4gcHJvY2VlZC10by1jaGVja291dC1idG4gbXQtM1wiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIFNsaXJyeSBrYXNzYWxsZVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKSA6IChcbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT5cbiAgICAgICAgICAgICAgICBoaXN0b3J5LnB1c2goXCIvY2hlY2tvdXQvc2hpcHBpbmdcIilcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBzdHlsZT17eyBjdXJzb3I6IFwicG9pbnRlclwiLCB0ZXh0QWxpZ246IFwiY2VudGVyXCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1ub25lIGQtbGctYmxvY2sgY2hlY2tvdXQtcGFnZS1idG4gcHJvY2VlZC10by1jaGVja291dC1idG4gbXQtM1wiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIFNsaXJyeSBrYXNzYWxsZVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgKX1cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDtcbiAgICA8L21haW4+XG4gICk7XG59O1xuZXhwb3J0IGRlZmF1bHQgQ2FydDtcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IHVzZUxvY2F0aW9uIH0gZnJvbSBcInJlYWN0LXJvdXRlci1kb21cIjtcbmltcG9ydCBheGlvcyBmcm9tIFwiYXhpb3NcIjtcblxuY29uc3QgQ29uZmlybWF0aW9uID0gKCkgPT4ge1xuICBjb25zdCBsb2NhdGlvbiA9IHVzZUxvY2F0aW9uKCk7XG4gIGNvbnN0IHNoaXBwaW5nSW5mbyA9IGxvY2F0aW9uLnN0YXRlO1xuXG4gIHVzZUVmZmVjdCgoKSA9PiBheGlvcy5nZXQoXCIvYXBpL2NhcnQvY2xlYXJcIiksIFtdKTtcblxuICByZXR1cm4gKFxuICAgIDxtYWluPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJzaGlwcGluZy1wcm9ncmVzcy1pbmRpY2F0b3ItY29udGFpbmVyIGQtbGctYmxvY2tcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWZsZXggbXQtNCBwcy00IHBlLTQganVzdGlmeS1jb250ZW50LWFyb3VuZCBqdXN0aWZ5LWNvbnRlbnQtbGctY2VudGVyXCI+XG4gICAgICAgICAgPGRpdiBzdHlsZT17eyBwb3NpdGlvbjogXCJyZWxhdGl2ZVwiIH19IGNsYXNzTmFtZT1cIm1zLWF1dG8gbXMtbGctMVwiPlxuICAgICAgICAgICAgPHN2Z1xuICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIgfX1cbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMjRcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIyNFwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS1jaXJjbGVcIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGggZD1cIk04IDE1QTcgNyAwIDEgMSA4IDFhNyA3IDAgMCAxIDAgMTR6bTAgMUE4IDggMCAxIDAgOCAwYTggOCAwIDAgMCAwIDE2elwiIC8+XG4gICAgICAgICAgICA8L3N2Zz5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXG4gICAgICAgICAgICAgICAgbGVmdDogXCI4cHhcIixcbiAgICAgICAgICAgICAgICB0b3A6IFwiNHB4XCIsXG4gICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMTJweFwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAxXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIGZvbnRTaXplOiBcIjEzcHhcIixcbiAgICAgICAgICAgICAgICBjb2xvcjogXCIjNWY1ZjVmXCIsXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBcIjI0cHhcIixcbiAgICAgICAgICAgICAgICBsaW5lSGVpZ2h0OiBcIjI0cHhcIixcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwibXMtMyBtZS0zXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgVG9pbWl0dXNrdWx1dFxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtbm9uZSBkLW1kLWlubGluZS1ibG9jayBtZS0zXCJcbiAgICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogXCIyNHB4XCIsIGxpbmVIZWlnaHQ6IFwiMjRweFwiIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1nL2xpbmUucG5nXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IHN0eWxlPXt7IHBvc2l0aW9uOiBcInJlbGF0aXZlXCIgfX0gY2xhc3NOYW1lPVwibWUtYXV0byBtZS1sZy0xXCI+XG4gICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgd2lkdGg9XCIyNFwiXG4gICAgICAgICAgICAgIGhlaWdodD1cIjI0XCJcbiAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJpIGJpLWNpcmNsZS1maWxsXCJcbiAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxjaXJjbGUgY3g9XCI4XCIgY3k9XCI4XCIgcj1cIjhcIiAvPlxuICAgICAgICAgICAgPC9zdmc+XG4gICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIGNvbG9yOiBcIndoaXRlXCIsXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcbiAgICAgICAgICAgICAgICBsZWZ0OiBcIjhweFwiLFxuICAgICAgICAgICAgICAgIHRvcDogXCI0cHhcIixcbiAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIxMnB4XCIsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDJcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMTNweFwiLFxuICAgICAgICAgICAgICAgIGNvbG9yOiBcIiM1ZjVmNWZcIixcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IFwiMjRweFwiLFxuICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IFwiMjRweFwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtcy0zIG1lLTMgZnctYm9sZFwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIFRpbGF1c3ZhaHZpc3R1c1xuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtdC00IGJnLXdoaXRlXCIgc3R5bGU9e3sgcGFkZGluZ0JvdHRvbTogXCIyNTBweFwiIH19PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctMTAwIHRleHQtY2VudGVyIGZzLTMgcC00XCI+XG4gICAgICAgICAgS2lpdG9zIHRpbGF1a3Nlc3Rhc2khIFRpbGF1a3Nlc2kgbMOkaGV0ZXTDpMOkbiBvc29pdHRlZXNlZW46XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luTGVmdDogXCI1MCVcIiB9fT5cbiAgICAgICAgICB7c2hpcHBpbmdJbmZvLmNvbXBhbnkgPyA8ZGl2PntzaGlwcGluZ0luZm8uY29tcGFueX08L2Rpdj4gOiBudWxsfVxuXG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIHtzaGlwcGluZ0luZm8uZmlyc3ROYW1lfSB7c2hpcHBpbmdJbmZvLmxhc3ROYW1lfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXY+e3NoaXBwaW5nSW5mby5zdHJlZXRBZGRyZXNzMX08L2Rpdj5cblxuICAgICAgICAgIHtzaGlwcGluZ0luZm8uc3RyZWV0QWRkcmVzczIgPyAoXG4gICAgICAgICAgICA8ZGl2PntzaGlwcGluZ0luZm8uc3RyZWV0QWRkcmVzczJ9PC9kaXY+XG4gICAgICAgICAgKSA6IG51bGx9XG5cbiAgICAgICAgICA8ZGl2PntzaGlwcGluZ0luZm8uemlwQ29kZX08L2Rpdj5cbiAgICAgICAgICA8ZGl2PntzaGlwcGluZ0luZm8uY2l0eX08L2Rpdj5cbiAgICAgICAgICA8ZGl2PntzaGlwcGluZ0luZm8udGVsZXBob25lfTwvZGl2PlxuICAgICAgICAgIDxkaXY+e3NoaXBwaW5nSW5mby5lbWFpbH08L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L21haW4+XG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBDb25maXJtYXRpb247XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5pbXBvcnQge0xpbmt9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXG5jb25zdCBFbXB0eUNhcnQgPSAoe3Byb2R1Y3RzfSkgPT4gKFxuXG4gICAgPG1haW4gY2xhc3NOYW1lPVwicC00XCIgc3R5bGU9e3t3aWR0aDonMTAwJScsIGhlaWdodDonNTAwcHgnLCBiYWNrZ3JvdW5kQ29sb3I6J2xpZ2h0Z3JheScsIGxpbmVIZWlnaHQ6JzZweCd9fT5cbiAgICAgICA8cD5Pc3Rvc2tvcmlzaSBvbiB0eWhqw6QuPC9wPlxuICAgICAgIDxwPkphdGthIDxMaW5rIHRvPVwiL1wiIHN0eWxlPXt7dGV4dERlY29yYXRpb246J25vbmUnLCBjb2xvcjonYmxhY2snfX0+dMOkc3TDpDwvTGluaz4gb3N0b2tzaWFzaS48L3A+XG4gICAgPC9tYWluPlxuKVxuXG5leHBvcnQgZGVmYXVsdCBFbXB0eUNhcnQiLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgTWFpbkNhcm91c2VsIGZyb20gXCIuL01haW5DYXJvdXNlbFwiO1xuaW1wb3J0IFByb2R1Y3RDYXJvdXNlbCBmcm9tIFwiLi9Qcm9kdWN0Q2Fyb3VzZWxcIjtcbmltcG9ydCBMb2FkaW5nIGZyb20gXCIuL0xvYWRpbmdcIjtcblxuY29uc3QgSG9tZSA9ICh7cHJvZHVjdHMsIGxvYWRpbmd9KSA9PiB7XG5cbiAgcmV0dXJuIChcbiAgICA8bWFpbj5cbiAgICAgIDxkaXZcbiAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwiI2U3NzExMFwiLFxuICAgICAgICAgIGNvbG9yOiBcIndoaXRlXCIsXG4gICAgICAgICAgZm9udFNpemU6IFwiMTNweFwiLFxuICAgICAgICAgIGZvbnRXZWlnaHQ6IFwiYm9sZFwiLFxuICAgICAgICB9fVxuICAgICAgICBjbGFzc05hbWU9XCJiYW5uZXIgdGV4dC1jZW50ZXIgdGV4dC1icmVha1wiXG4gICAgICA+XG4gICAgICAgIDxzcGFuPlxuICAgICAgICAgIFJlaHV0IHN1b3JhYW4gb3ZlbGxlc2kgLSBtYWtzdXRvbiB0b2ltaXR1cyBrYWlra2lpbiB0aWxhdWtzaWluIVxuICAgICAgICAgIE1pbmltaXRpbGF1c23DpMOkcsOkIDIgdHVvdGV0dGEuXG4gICAgICAgIDwvc3Bhbj5cbiAgICAgIDwvZGl2PlxuICAgICAgPG1haW4+XG4gICAgICAgIHtsb2FkaW5nID8gKFxuICAgICAgICAgIDxMb2FkaW5nIC8+XG4gICAgICAgICkgOiAoXG4gICAgICAgICAgPD5cbiAgICAgICAgICAgIDxNYWluQ2Fyb3VzZWwgLz5cblxuICAgICAgICAgICAgPFByb2R1Y3RDYXJvdXNlbCBwcm9kdWN0cz17cHJvZHVjdHN9IC8+XG5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOlxuICAgICAgICAgICAgICAgICAgXCJ1cmwoJ2ltZy9waWtrdXNhbmthcmlfbm9ycmFob3JzZV9iYW5uZXJpXzE4MDB4NTAwXzNmOTkzNmNkLWI0OGYtNDc1OC04OWJlLTQyYTY5OTEzZmUyNi5qcGcnKVwiLFxuICAgICAgICAgICAgICAgIGhlaWdodDogXCIzMDBweFwiLFxuICAgICAgICAgICAgICAgIGJhY2tncm91bmRQb3NpdGlvbjogXCJjZW50ZXJcIixcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kUmVwZWF0OiBcIm5vLXJlcGVhdFwiLFxuICAgICAgICAgICAgICAgIGJhY2tncm91bmRTaXplOiBcImNvdmVyXCIsXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IFwicmVsYXRpdmVcIixcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicm93IG10LTUgZnJvbnQtcGFnZS1pbWFnZS0xIGZyb250LXBhZ2UtaW1hZ2UtMS1jb250YWluZXJcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZyb250LXBhZ2UtaW1hZ2UtMS1vdmVybGF5IHctMTAwXCI+XG4gICAgICAgICAgICAgICAgPGgyXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtYi0zXCJcbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIndoaXRlXCIsIGZvbnRTaXplOiBcIjQwcHhcIiB9fVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIFBJS0tVIFNBTktBUklcbiAgICAgICAgICAgICAgICA8L2gyPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4IGFsaWduLWl0ZW1zLWVuZFwiPlxuICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJjYXJvdXNlbC1idG4gbXQzXCI+XG4gICAgICAgICAgICAgICAgICAgIFBPTklFTiBPTUEgUkVIVSwgVFVUVVNUVSBKQSBPU1RBe1wiIFwifVxuICAgICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG10LTUgbWItNSBmcm9udC1wYWdlLWltYWdlLTItY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTZcIj5cbiAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJpbWctZmx1aWQgbXQtMyBtYi0zXCJcbiAgICAgICAgICAgICAgICAgIHNyYz1cImltZy9ub3JyYV9tZW51X2xvZ29fdmFha2FfMTA4MHgucG5nXCJcbiAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29sLWxnLTYgaGlkZGVuXCI+XG4gICAgICAgICAgICAgICAgPGgzIGNsYXNzTmFtZT1cIm10LTQgbWItNFwiPlxuICAgICAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIyMnB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICAgICAgICAgIGZvbnRXZWlnaHQ6IFwiYm9sZFwiLFxuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICB7XCIgXCJ9XG4gICAgICAgICAgICAgICAgICAgIE9QVElNT0kgSEVWT1NFU0kgUlVPS0lOVEFcbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L2gzPlxuICAgICAgICAgICAgICAgIDxwIHN0eWxlPXt7IGZvbnRTaXplOiBcIjE2cHhcIiwgY29sb3I6IFwiIzVmNWY1ZlwiIH19PlxuICAgICAgICAgICAgICAgICAgPHNwYW4+TWFrc3V0dG9tYWxsYTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL25vcnJhbWVudS5sYW50bWFubmVuYWdyby5maS9cIlxuICAgICAgICAgICAgICAgICAgICB0aXRsZT1cImh0dHBzOi8vbm9ycmFtZW51LmxhbnRtYW5uZW5mZWVkLmZpL1wiXG4gICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyXCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4+Tm9ycmEgTWVudSAtcnVva2ludGFsYXNrdXJpbGxhPC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgICAgIHRlZXQgaGVscG9zdGkganV1cmkgc2ludW4gaGV2b3NlbGxlc2kgc29waXZhbiwgdHVydmFsbGlzZW5cbiAgICAgICAgICAgICAgICAgICAgamEgdMOkeXNpcGFpbm9pc2VuIHJ1b2tpbnRhc3V1bm5pdGVsbWFuLiBIYXJraXRlbiBzdXVubml0ZWx0dVxuICAgICAgICAgICAgICAgICAgICBydW9raW50YSB0dWtlZSBoZXZvc2VuIGh5dmludm9pbnRpYSBqYSBzdW9yaXR1c2t5a3nDpC5cbiAgICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC8+XG4gICAgICAgICl9XG4gICAgICA8L21haW4+XG4gICAgPC9tYWluPlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgSG9tZTtcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IExpbmsgfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuaW1wb3J0IHsgY2xvc2VOYXYsIG9wZW5OYXYgfSBmcm9tIFwiLi4vYXBwXCI7XG5cbmNvbnN0IExheW91dDEgPSAoe2NoaWxkcmVuLCBjYXJ0fSkgPT4ge1xuXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJwYWdlLWNvbnRhaW5lclwiPlxuICAgICAgPGhlYWRlcj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT1cInNwaW5uZXItYm9yZGVyIGQtbm9uZVwiXG4gICAgICAgICAgcm9sZT1cInN0YXR1c1wiXG4gICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgIGNvbG9yOiBcIiNlNzcxMTBcIixcbiAgICAgICAgICAgIHdpZHRoOiBcIjUwcHhcIixcbiAgICAgICAgICAgIGhlaWdodDogXCI1MHB4XCIsXG4gICAgICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxuICAgICAgICAgICAgdG9wOiBcIjUwJVwiLFxuICAgICAgICAgICAgbGVmdDogXCI1MCVcIixcbiAgICAgICAgICAgIHpJbmRleDogXCIxMDQwXCIsXG4gICAgICAgICAgfX1cbiAgICAgICAgPlxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInNyLW9ubHlcIj48L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlci1tb2JpbGVcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInRvZ2dsZS1jb250YWluZXJcIj5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cInRvZ2dsZVwiXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMjVweFwiLFxuICAgICAgICAgICAgICAgIGN1cnNvcjogXCJjb250ZXh0LW1lbnVcIixcbiAgICAgICAgICAgICAgICBjb2xvcjogXCJ3aGl0ZVwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBvbkNsaWNrPXtvcGVuTmF2fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAmIzk3NzY7XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPExpbmsgY2xhc3NOYW1lPVwibG9nb1wiIHRvPVwiL1wiPlxuICAgICAgICAgICAgPGltZyBzcmM9XCJpbWcvbm9ycmFfbG9nb19tdXN0YV9sYWF0aWtrb18xLnBuZ1wiIGFsdD1cIlwiIC8+XG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIHtjYXJ0ID8gKFxuICAgICAgICAgIDxMaW5rXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJzaG9wcGluZy1jYXJ0IGQtZmxleFwiXG4gICAgICAgICAgICBzdHlsZT17eyB0ZXh0RGVjb3JhdGlvbjogXCJub25lXCIsIHBvc2l0aW9uOiBcInJlbGF0aXZlXCIgfX1cbiAgICAgICAgICAgIHRvPVwiL2NoZWNrb3V0XCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8aW1nIHNyYz1cImltZy9zaG9wcGluZy1jYXJ0LnBuZ1wiIGFsdD1cIlwiIC8+XG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNob3BwaW5nLWNhcnQtc3VtbWFyeSBpbnZpc2libGVcIlxuICAgICAgICAgICAgICBzdHlsZT17eyBtYXJnaW5Ub3A6IFwiLTEwcHhcIiwgbWFyZ2luTGVmdDogXCIzMHB4XCIgfX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzaG9wcGluZy1jYXJ0LWl0ZW1zLWNvdW50IHRleHQtY2VudGVyXCJcbiAgICAgICAgICAgICAgPjwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWlubGluZVwiPlxuICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJzaG9wcGluZy1jYXJ0LXRvdGFsIGZ3LWJvbGRcIlxuICAgICAgICAgICAgICAgID48L3NwYW4+XG4gICAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEwcHhcIiwgY29sb3I6IFwid2hpdGVcIiB9fVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1ibG9jayB0ZXh0LWVuZFwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgU2lzLiBBTFY6blxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVhZGVyLWRlc2t0b3BcIj5cbiAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJsb2dvXCIgdG89XCIvXCI+XG4gICAgICAgICAgICA8aW1nIHNyYz1cImltZy9ub3JyYV9sb2dvX211c3RhX2xhYXRpa2tvXzEucG5nXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPExpbmtcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cInNob3BwaW5nLWNhcnQgZC1mbGV4XCJcbiAgICAgICAgICAgIHN0eWxlPXt7IHRleHREZWNvcmF0aW9uOiBcIm5vbmVcIiwgcG9zaXRpb246IFwicmVsYXRpdmVcIiB9fVxuICAgICAgICAgICAgdG89XCIvY2hlY2tvdXRcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxpbWcgc3JjPVwiaW1nL3Nob3BwaW5nLWNhcnQucG5nXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2hvcHBpbmctY2FydC1zdW1tYXJ5IGludmlzaWJsZVwiXG4gICAgICAgICAgICAgIHN0eWxlPXt7IG1hcmdpblRvcDogXCItMTBweFwiLCBtYXJnaW5MZWZ0OiBcIjMwcHhcIiB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJzaG9wcGluZy1jYXJ0LWl0ZW1zLWNvdW50IHRleHQtY2VudGVyXCI+PC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtaW5saW5lXCI+XG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwic2hvcHBpbmctY2FydC10b3RhbCBmdy1ib2xkXCI+PC9zcGFuPlxuICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIxMHB4XCIsIGNvbG9yOiBcIndoaXRlXCIgfX1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtYmxvY2sgdGV4dC1lbmRcIlxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIFNpcy4gQUxWOm5cbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5hdmlnYXRpb24tYmFyXCI+XG4gICAgICAgICAgPExpbmsgY2xhc3NOYW1lPVwiZnctYm9sZFwiIHRvPVwiL1wiPlxuICAgICAgICAgICAgUlVPS0lOVEFMQVNLVVJJXG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDxMaW5rIGNsYXNzTmFtZT1cImZ3LWJvbGRcIiB0bz1cIi9cIj5cbiAgICAgICAgICAgIFJVT0tJTlRBVklOS0lUXG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDxMaW5rXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJmdy1ib2xkXCJcbiAgICAgICAgICAgIHRvPVwiL3Byb2R1Y3RzXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICBUVU9UVEVFVFxuICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJmdy1ib2xkXCIgdG89XCIvXCI+XG4gICAgICAgICAgICBZSFRFWVNUSUVET1RcbiAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPExpbmsgY2xhc3NOYW1lPVwiZnctYm9sZFwiIHRvPVwiL1wiPlxuICAgICAgICAgICAgWVJJVFlTXG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8ZGl2IGlkPVwibXlTaWRlbmF2XCIgY2xhc3NOYW1lPVwic2lkZW5hdiBkLWxnLW5vbmVcIj5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICBwYWRkaW5nVG9wOiBcIjZweFwiLFxuICAgICAgICAgICAgICBwYWRkaW5nQm90dG9tOiBcIjZweFwiLFxuICAgICAgICAgICAgICBib3JkZXJCb3R0b206IFwiMnB4IHNvbGlkIGxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgfX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8YSBzdHlsZT17eyBjdXJzb3I6IFwiY29udGV4dC1tZW51XCIgfX0gb25DbGljaz17Y2xvc2VOYXZ9PlxuICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIzMnB4XCIsXG4gICAgICAgICAgICAgICAgICBjb2xvcjogXCIjZTc3MTEwXCIsXG4gICAgICAgICAgICAgICAgICBwYWRkaW5nQm90dG9tOiBcIjZweFwiLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAmdGltZXM7XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgZm9udFNpemU6IFwiMTJweFwiLCBjb2xvcjogXCIjNWY1ZjVmXCIgfX0+XG4gICAgICAgICAgICAgICAge1wiIFwifVxuICAgICAgICAgICAgICAgIFZBTElLS097XCIgXCJ9XG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPExpbmsgY2xhc3NOYW1lPVwiZnctYm9sZFwiIHRvPVwiL1wiIG9uQ2xpY2s9e2Nsb3NlTmF2fT5cbiAgICAgICAgICAgICAgUlVPS0lOVEFMQVNLVVJJXG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxMaW5rIGNsYXNzTmFtZT1cImZ3LWJvbGRcIiB0bz1cIi9cIiBvbkNsaWNrPXtjbG9zZU5hdn0+XG4gICAgICAgICAgICAgIFJVT0tJTlRBVklOS0lUXG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxMaW5rXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImZ3LWJvbGRcIlxuICAgICAgICAgICAgICB0bz1cIi9wcm9kdWN0c1wiXG4gICAgICAgICAgICAgIG9uQ2xpY2s9e2Nsb3NlTmF2fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBUVU9UVEVFVFxuICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJmdy1ib2xkXCIgdG89XCIvXCIgb25DbGljaz17Y2xvc2VOYXZ9PlxuICAgICAgICAgICAgICBZSFRFWVNUSUVET1RcbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPExpbmsgY2xhc3NOYW1lPVwiZnctYm9sZFwiIHRvPVwiL1wiIG9uQ2xpY2s9e2Nsb3NlTmF2fT5cbiAgICAgICAgICAgICAgWVJJVFlTXG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxMaW5rIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEycHhcIiB9fSB0bz1cIi9cIiBvbkNsaWNrPXtjbG9zZU5hdn0+XG4gICAgICAgICAgICAgIFBJS0FUSUxBVVNcbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2hlYWRlcj5cblxuICAgICAge2NoaWxkcmVufVxuXG4gICAgICA8Zm9vdGVyPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtbGctbm9uZVwiPlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEzcHhcIiB9fVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiYWNjb3JkaW9uX2J1dHRvbiBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Yj5WRVJLS09LQVVQUEE8L2I+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBsdXMtaWNvbiB3aGl0ZS1wbHVzLWljb25cIj48L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBhbmVsXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInB0LTQgcHItNCBwYi00XCI+XG4gICAgICAgICAgICAgIDxhIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEzcHhcIiB9fSBjbGFzc05hbWU9XCJkLWJsb2NrXCIgaHJlZj1cIi9cIj5cbiAgICAgICAgICAgICAgICBNeXludGktIGphIHRvaW1pdHVzZWhkb3RcbiAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8YSBzdHlsZT17eyBmb250U2l6ZTogXCIxM3B4XCIgfX0gY2xhc3NOYW1lPVwiZC1ibG9ja1wiIGhyZWY9XCIvXCI+XG4gICAgICAgICAgICAgICAgUGFsYXV0dWtzZXRcbiAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8YSBzdHlsZT17eyBmb250U2l6ZTogXCIxM3B4XCIgfX0gY2xhc3NOYW1lPVwiZC1ibG9ja1wiIGhyZWY9XCIvXCI+XG4gICAgICAgICAgICAgICAgS8OkeXR0w7ZlaGRvdFxuICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDxhIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEzcHhcIiB9fSBjbGFzc05hbWU9XCJkLWJsb2NrXCIgaHJlZj1cIi9cIj5cbiAgICAgICAgICAgICAgICBZaHRleXN0aWVkb3RcbiAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8YSBzdHlsZT17eyBmb250U2l6ZTogXCIxM3B4XCIgfX0gY2xhc3NOYW1lPVwiZC1ibG9ja1wiIGhyZWY9XCIvXCI+XG4gICAgICAgICAgICAgICAgWXJpdHlzXG4gICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEzcHhcIiB9fVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiYWNjb3JkaW9uX2J1dHRvbiBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Yj5TRVVSQUEgTUVJVMOEPC9iPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwbHVzLWljb24gd2hpdGUtcGx1cy1pY29uXCI+PC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwYW5lbFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwdC00IHByLTQgcGItNFwiPlxuICAgICAgICAgICAgICA8YSBocmVmPVwiXCI+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9XCJpbWcvZi5wbmdcIiBhbHQ9XCJcIiAvPlxuICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmRDb2xvcjogXCIjMDIwYzEwXCIgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJ3LTEwMCByb3cgcC0zIG0tMCBkLW5vbmUgZC1sZy1ibG9ja1wiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtaW5saW5lLWJsb2NrIGNvbC01XCI+XG4gICAgICAgICAgICA8TGlua1xuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtdC0zIHBiLTJcIlxuICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIxM3B4XCIsIGNvbG9yOiBcIndoaXRlXCIgfX1cbiAgICAgICAgICAgICAgdG89XCIvXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgVkVSS0tPS0FVUFBBXG4gICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByLTQgcGItNFwiPlxuICAgICAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJkLWJsb2NrXCIgc3R5bGU9e3sgZm9udFNpemU6IFwiMTNweFwiIH19IHRvPVwiL1wiPlxuICAgICAgICAgICAgICAgIE15eW50aS0gamEgdG9pbWl0dXNlaGRvdFxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIDxMaW5rIGNsYXNzTmFtZT1cImQtYmxvY2tcIiBzdHlsZT17eyBmb250U2l6ZTogXCIxM3B4XCIgfX0gdG89XCIvXCI+XG4gICAgICAgICAgICAgICAgUGFsYXV0dWtzZXRcbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJkLWJsb2NrXCIgc3R5bGU9e3sgZm9udFNpemU6IFwiMTNweFwiIH19IHRvPVwiL1wiPlxuICAgICAgICAgICAgICAgIEvDpHl0dMO2ZWhkb3RcbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJkLWJsb2NrXCIgc3R5bGU9e3sgZm9udFNpemU6IFwiMTNweFwiIH19IHRvPVwiL1wiPlxuICAgICAgICAgICAgICAgIFlodGV5c3RpZWRvdFxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIDxMaW5rIGNsYXNzTmFtZT1cImQtYmxvY2tcIiBzdHlsZT17eyBmb250U2l6ZTogXCIxM3B4XCIgfX0gdG89XCIvXCI+XG4gICAgICAgICAgICAgICAgWXJpdHlzXG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1pbmxpbmUtYmxvY2sgY29sLTIgYWxpZ24tdG9wXCI+XG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEzcHhcIiwgY29sb3I6IFwid2hpdGVcIiB9fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtdC0zIHBiLTJcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8Yj5TRVVSQUEgTUVJVMOEPC9iPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8TGluayB0bz1cIi9cIj5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIHNyYz1cImltZy9mLnBuZ1wiXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMTVweFwiLCBoZWlnaHQ6IFwiMTVweFwiIH19XG4gICAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPHBcbiAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIiMwMjBjMTBcIixcbiAgICAgICAgICAgIGNvbG9yOiBcIndoaXRlXCIsXG4gICAgICAgICAgICBmb250U2l6ZTogXCIxMnB4XCIsXG4gICAgICAgICAgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJ3LTEwMCB0ZXh0LWNlbnRlciBwdC0zIHBiLTQgbWItMFwiXG4gICAgICAgID5cbiAgICAgICAgICAmY29weTsgMjAyMCBMYW50bcOkbm5lbiAmbmJzcDsgPExpbmsgdG89XCIvXCI+VGlldG9zdW9qYXBvbGl0aWlra2E8L0xpbms+e1wiIFwifVxuICAgICAgICAgICZuYnNwOyA8TGluayB0bz1cIi9cIj5FdsOkc3Rla8OkeXTDpG50w7Y8L0xpbms+XG4gICAgICAgICAgJm5ic3A7IDxMaW5rIHRvPVwiL1wiPkV2w6RzdGVhc2V0dWtzZXQ8L0xpbms+XG4gICAgICAgIDwvcD5cbiAgICAgIDwvZm9vdGVyPlxuICAgIDwvZGl2PlxuICApO1xufTtcbmV4cG9ydCBkZWZhdWx0IExheW91dDE7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCJcbmltcG9ydCB7TGlua30gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSdcbmltcG9ydCB7dG9nZ2xlRXhwYW5kZWRUb3RhbEFuZFRheFRhYmxlfSBmcm9tICcuLi9hcHAnXG5cbmNvbnN0IExheW91dDIgPSAocHJvcHMpID0+IChcbiAgPGhlYWRlcj5cbiAgICA8ZGl2IGNsYXNzTmFtZT1cImQtbm9uZSBkLWxnLWJsb2NrXCI+XG4gICAgICA8ZGl2XG4gICAgICAgIHN0eWxlPXt7aGVpZ2h0Oic3NHB4Jywgd2lkdGg6JzEwMCUnLCBiYWNrZ3JvdW5kQ29sb3I6JyMwMjBjMTAnLCBjb2xvcjond2hpdGUnfX1cbiAgICAgICAgY2xhc3NOYW1lPVwiZC1mbGV4IGp1c3RpZnktY29udGVudC1iZXR3ZWVuXCJcbiAgICAgID5cbiAgICAgICAgPExpbmsgY2xhc3NOYW1lPXtcImxvZ28gaC0xMDBcIn0gc3R5bGU9e3tsaW5lSGVpZ2h0Oic3NHB4J319IHRvPVwiL1wiPlxuICAgICAgICA8aW1nXG4gICAgICAgICAgICBzdHlsZT17e2hlaWdodDonNjBweCd9fVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwibWwtMyBtdC0xXCJcbiAgICAgICAgICAgIHNyYz1cIi9pbWcvbm9ycmFfbG9nb19tdXN0YV9sYWF0aWtrb18xLnBuZ1wiXG4gICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgIC8+XG4gICAgICAgIDwvTGluaz5cbiAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiZnMtMyBmdy1ib2xkIGgtMTAwXCIgc3R5bGU9e3tsaW5lSGVpZ2h0Oic3MHB4J319PlxuICAgICAgICAgIEtBU1NBTExFXG4gICAgICAgIDwvc3Bhbj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWlubGluZSBtZS0zXCI+XG4gICAgICAgIDxMaW5rIHN0eWxlPXt7Y29sb3I6J3doaXRlJywgZm9udFNpemU6JzEzcHgnLCB0ZXh0RGVjb3JhdGlvbjonbm9uZScsIGxpbmVIZWlnaHQ6Jzc0cHgnfX0gdG89XCIvXCI+XG4gICAgICAgICAgSmF0a2Egb3N0b2tzaWFcbiAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPHN2Z1xuICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICB3aWR0aD1cIjE2XCJcbiAgICAgICAgICAgIGhlaWdodD1cIjE2XCJcbiAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgYmkgYmktY2hldnJvbi1yaWdodFwiXG4gICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8cGF0aFxuICAgICAgICAgICAgICBmaWxsUnVsZT1cImV2ZW5vZGRcIlxuICAgICAgICAgICAgICBkPVwiTTQuNjQ2IDEuNjQ2YS41LjUgMCAwIDEgLjcwOCAwbDYgNmEuNS41IDAgMCAxIDAgLjcwOGwtNiA2YS41LjUgMCAwIDEtLjcwOC0uNzA4TDEwLjI5MyA4IDQuNjQ2IDIuMzU0YS41LjUgMCAwIDEgMC0uNzA4elwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvc3ZnPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICAgIDxkaXYgY2xhc3NOYW1lPVwic2hpcHBpbmctbW9iaWxlLWhlYWRlci1jb250YWluZXIgZC1sZy1ub25lXCI+XG4gICAgICA8ZGl2XG4gICAgICAgIHN0eWxlPXt7aGVpZ2h0OicxNDhweCcsIHdpZHRoOicxMDAlJywgYmFja2dyb3VuZENvbG9yOicjMDIwYzEwJywgY29sb3I6J3doaXRlJ319XG4gICAgICAgIGNsYXNzTmFtZT1cImQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlblwiXG4gICAgICA+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1pbmxpbmUgZC1mbGV4IG1zLTNcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm10LWF1dG9cIj5cbiAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS1jaGV2cm9uLWxlZnRcIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGhcbiAgICAgICAgICAgICAgICBmaWxsUnVsZT1cImV2ZW5vZGRcIlxuICAgICAgICAgICAgICAgIGQ9XCJNMTEuMzU0IDEuNjQ2YS41LjUgMCAwIDEgMCAuNzA4TDUuNzA3IDhsNS42NDcgNS42NDZhLjUuNSAwIDAgMS0uNzA4LjcwOGwtNi02YS41LjUgMCAwIDEgMC0uNzA4bDYtNmEuNS41IDAgMCAxIC43MDggMHpcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9zdmc+XG4gICAgICAgICAgICA8TGluayBzdHlsZT17e2NvbG9yOid3aGl0ZScsIGZvbnRTaXplOicxM3B4JywgdGV4dERlY29yYXRpb246J25vbmUnLCBsaW5lSGVpZ2h0Oic3NHB4J319IHRvPVwiL1wiPlxuICAgICAgICAgICAgSmF0a2Egb3N0b2tzaWFcbiAgICAgICAgICAgIDwvTGluaz4gICBcbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8TGluayBjbGFzc05hbWU9XCJsb2dvIGgtMTAwIHBzLTJcIiBzdHlsZT17e2xpbmVIZWlnaHQ6Jzc0cHgnfX0gdG89XCIvXCI+XG4gICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgIHN0eWxlPXt7aGVpZ2h0Oic2MHB4J319XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm10LTFcIlxuICAgICAgICAgICAgICBzcmM9XCIvaW1nL25vcnJhX2xvZ29fbXVzdGFfbGFhdGlra29fMS5wbmdcIlxuICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZzLTYgZnctYm9sZCB0ZXh0LWNlbnRlclwiIHN0eWxlPXt7bGluZUhlaWdodDonNzBweCd9fT5cbiAgICAgICAgICAgIEtBU1NBTExFXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtaW5saW5lIGQtZmxleCBtZS0zXCI+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwibXQtYXV0b1wiXG4gICAgICAgICAgICBzdHlsZT17e2N1cnNvcjoncG9pbnRlcid9fVxuICAgICAgICAgICAgb25DbGljaz17dG9nZ2xlRXhwYW5kZWRUb3RhbEFuZFRheFRhYmxlfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxzcGFuIHN0eWxlPXt7Y29sb3I6J3doaXRlJywgZm9udFNpemU6JzEzcHgnLCB0ZXh0RGVjb3JhdGlvbjonbm9uZScsIGxpbmVIZWlnaHQ6Jzc0cHgnfX0+XG4gICAgICAgICAgICAgIFRpbGF1a3Nlc2lcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBiaSBiaS1jaGV2cm9uLXJpZ2h0XCJcbiAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxwYXRoXG4gICAgICAgICAgICAgICAgZmlsbFJ1bGU9XCJldmVub2RkXCJcbiAgICAgICAgICAgICAgICBkPVwiTTQuNjQ2IDEuNjQ2YS41LjUgMCAwIDEgLjcwOCAwbDYgNmEuNS41IDAgMCAxIDAgLjcwOGwtNiA2YS41LjUgMCAwIDEtLjcwOC0uNzA4TDEwLjI5MyA4IDQuNjQ2IDIuMzU0YS41LjUgMCAwIDEgMC0uNzA4elwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L3N2Zz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgICB7cHJvcHMuY2hpbGRyZW59XG4gIDwvaGVhZGVyPlxuKTtcblxuZXhwb3J0IGRlZmF1bHQgTGF5b3V0MiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcblxuY29uc3QgTG9hZGluZyA9ICgpID0+IChcbiAgPG1haW4gc3R5bGU9e3toZWlnaHQ6JzUwMHB4Jywgd2lkdGg6JzEwMCUnfX0+XG4gICAgPGRpdlxuICAgIGNsYXNzTmFtZT1cInNwaW5uZXItYm9yZGVyXCJcbiAgICByb2xlPVwic3RhdHVzXCJcbiAgICBzdHlsZT17e1xuICAgICAgY29sb3I6IFwiI2U3NzExMFwiLFxuICAgICAgd2lkdGg6IFwiNTBweFwiLFxuICAgICAgaGVpZ2h0OiBcIjUwcHhcIixcbiAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXG4gICAgICB0b3A6IFwiNTAlXCIsXG4gICAgICBsZWZ0OiBcIjUwJVwiLFxuICAgICAgekluZGV4OiBcIjEwNDBcIixcbiAgICB9fVxuICA+XG4gICAgPHNwYW4gY2xhc3NOYW1lPVwic3Itb25seVwiPjwvc3Bhbj5cbiAgPC9kaXY+XG4gIDwvbWFpbj5cbik7XG5cbmV4cG9ydCBkZWZhdWx0IExvYWRpbmc7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBMaW5rIH0gZnJvbSBcInJlYWN0LXJvdXRlci1kb21cIjtcblxuY29uc3QgTWFpbkNhcm91c2VsID0gKCkgPT4gKFxuICA8ZGl2XG4gICAgaWQ9XCJtYWluQ2Fyb3VzZWxcIlxuICAgIGNsYXNzTmFtZT1cImNhcm91c2VsIHNsaWRlIG1sLTIgbXItMlwiXG4gICAgZGF0YS1icy1yaWRlPVwiY2Fyb3VzZWxcIlxuICA+XG4gICAgPGRpdiBjbGFzc05hbWU9XCJjYXJvdXNlbC1pbmRpY2F0b3JzXCI+XG4gICAgICA8YnV0dG9uXG4gICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICBkYXRhLWJzLXRhcmdldD1cIiNtYWluQ2Fyb3VzZWxcIlxuICAgICAgICBkYXRhLWJzLXNsaWRlLXRvPVwiMFwiXG4gICAgICAgIGNsYXNzTmFtZT1cImFjdGl2ZVwiXG4gICAgICAgIGFyaWEtY3VycmVudD1cInRydWVcIlxuICAgICAgICBhcmlhLWxhYmVsPVwiU2xpZGUgMVwiXG4gICAgICA+PC9idXR0b24+XG4gICAgICA8YnV0dG9uXG4gICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICBkYXRhLWJzLXRhcmdldD1cIiNtYWluQ2Fyb3VzZWxcIlxuICAgICAgICBkYXRhLWJzLXNsaWRlLXRvPVwiMVwiXG4gICAgICAgIGFyaWEtbGFiZWw9XCJTbGlkZSAyXCJcbiAgICAgID48L2J1dHRvbj5cbiAgICAgIDxidXR0b25cbiAgICAgICAgdHlwZT1cImJ1dHRvblwiXG4gICAgICAgIGRhdGEtYnMtdGFyZ2V0PVwiI21haW5DYXJvdXNlbFwiXG4gICAgICAgIGRhdGEtYnMtc2xpZGUtdG89XCIyXCJcbiAgICAgICAgYXJpYS1sYWJlbD1cIlNsaWRlIDNcIlxuICAgICAgPjwvYnV0dG9uPlxuICAgICAgPGJ1dHRvblxuICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgZGF0YS1icy10YXJnZXQ9XCIjbWFpbkNhcm91c2VsXCJcbiAgICAgICAgZGF0YS1icy1zbGlkZS10bz1cIjNcIlxuICAgICAgICBhcmlhLWxhYmVsPVwiU2xpZGUgNFwiXG4gICAgICA+PC9idXR0b24+XG4gICAgICA8YnV0dG9uXG4gICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICBkYXRhLWJzLXRhcmdldD1cIiNtYWluQ2Fyb3VzZWxcIlxuICAgICAgICBkYXRhLWJzLXNsaWRlLXRvPVwiNFwiXG4gICAgICAgIGFyaWEtbGFiZWw9XCJTbGlkZSA1XCJcbiAgICAgID48L2J1dHRvbj5cbiAgICA8L2Rpdj5cblxuICAgIDxkaXYgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtaW5uZXJcIj5cbiAgICAgIDxkaXZcbiAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBcInVybCgnaW1nL25vcnJhLW9sZC5qcGcnKVwiLFxuICAgICAgICAgIGhlaWdodDogXCI1MDRweFwiLFxuICAgICAgICAgIGJhY2tncm91bmRQb3NpdGlvbjogXCJjZW50ZXJcIixcbiAgICAgICAgICBiYWNrZ3JvdW5kUmVwZWF0OiBcIm5vLXJlcGVhdFwiLFxuICAgICAgICAgIGJhY2tncm91bmRTaXplOiBcImNvdmVyXCIsXG4gICAgICAgIH19XG4gICAgICAgIGNsYXNzTmFtZT1cIm1haW4tY2Fyb3VzZWwtaW1hZ2UgY2Fyb3VzZWwtaXRlbSBhY3RpdmVcIlxuICAgICAgPlxuICAgICAgICA8TGlua1xuICAgICAgICAgIGNsYXNzTmFtZT1cImNhcm91c2VsLWNhcHRpb24gZC1ibG9ja1wiXG4gICAgICAgICAgc3R5bGU9e3sgbWFyZ2luQm90dG9tOiBcIjMwcHhcIiB9fVxuICAgICAgICAgIHRvPVwiL1wiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcm91c2VsLWNhcHRpb24gZC1ibG9ja1wiPlxuICAgICAgICAgICAgPGgxIHN0eWxlPXt7IG1hcmdpbkJvdHRvbTogXCIzMHB4XCIgfX0+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwiMzZweFwiLCBjb2xvcjogXCJ3aGl0ZVwiLCBmb250V2VpZ2h0OiBcImJvbGRcIiB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgUlVPS0kgSEVWT1NFU0kgSFlWSU5WT05USUFcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPC9oMT5cbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiY2Fyb3VzZWwtYnRuXCI+XG4gICAgICAgICAgICAgIHtcIiBcIn1cbiAgICAgICAgICAgICAgS09USU1BSVNFVCBIRVZPU1JFSFVUIFBPSEpPSVNJSU4gT0xPSUhJTntcIiBcIn1cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0xpbms+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXZcbiAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBcInVybCgnaW1nL25vcnJzbGlkZTIuanBnJylcIixcbiAgICAgICAgICBoZWlnaHQ6IFwiNTA0cHhcIixcbiAgICAgICAgICBiYWNrZ3JvdW5kUG9zaXRpb246IFwiY2VudGVyXCIsXG4gICAgICAgICAgYmFja2dyb3VuZFJlcGVhdDogXCJuby1yZXBlYXRcIixcbiAgICAgICAgICBiYWNrZ3JvdW5kU2l6ZTogXCJjb3ZlclwiLFxuICAgICAgICB9fVxuICAgICAgICBjbGFzc05hbWU9XCJtYWluLWNhcm91c2VsLWltYWdlIGNhcm91c2VsLWl0ZW1cIlxuICAgICAgPlxuICAgICAgICA8TGlua1xuICAgICAgICAgIGNsYXNzTmFtZT1cImNhcm91c2VsLWNhcHRpb24gZC1ibG9ja1wiXG4gICAgICAgICAgc3R5bGU9e3sgbWFyZ2luQm90dG9tOiBcIjMwcHhcIiB9fVxuICAgICAgICAgIHRvPVwiL1wiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcm91c2VsLWNhcHRpb24gZC1ibG9ja1wiPlxuICAgICAgICAgICAgPGgxIHN0eWxlPXt7IG1hcmdpbkJvdHRvbTogXCIzMHB4XCIgfX0+XG4gICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjM2cHhcIiwgY29sb3I6IFwid2hpdGVcIiwgZm9udFdlaWdodDogXCJib2xkXCIgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIFJVT0tJIEhFVk9TRVNJIEhZVklOVk9OVElBXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDwvaDE+XG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImNhcm91c2VsLWJ0blwiPlxuICAgICAgICAgICAgICB7XCIgXCJ9XG4gICAgICAgICAgICAgIEtPVElNQUlTRVQgSEVWT1NSRUhVVCBQT0hKT0lTSUlOIE9MT0lISU57XCIgXCJ9XG4gICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9MaW5rPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2XG4gICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgYmFja2dyb3VuZDpcbiAgICAgICAgICAgIFwidXJsKCdpbWcvbm9ycmFfa2FydXNlbGxpa3V2YV9udW9yaXNvXzMwMDB4MTEwMF9mZWQ0ODVkMy1mMmUxLTQyZDktODRjMS0wYWZlOWFiYWM2ZjZfNDQ3MnguanBnJylcIixcbiAgICAgICAgICBoZWlnaHQ6IFwiNTA0cHhcIixcbiAgICAgICAgICBiYWNrZ3JvdW5kUG9zaXRpb246IFwiY2VudGVyXCIsXG4gICAgICAgICAgYmFja2dyb3VuZFJlcGVhdDogXCJuby1yZXBlYXRcIixcbiAgICAgICAgICBiYWNrZ3JvdW5kU2l6ZTogXCJjb3ZlclwiLFxuICAgICAgICB9fVxuICAgICAgICBjbGFzc05hbWU9XCJtYWluLWNhcm91c2VsLWltYWdlIGNhcm91c2VsLWl0ZW1cIlxuICAgICAgPlxuICAgICAgICA8TGlua1xuICAgICAgICAgIGNsYXNzTmFtZT1cImNhcm91c2VsLWNhcHRpb24gZC1ibG9ja1wiXG4gICAgICAgICAgc3R5bGU9e3sgbWFyZ2luQm90dG9tOiBcIjMwcHhcIiB9fVxuICAgICAgICAgIHRvPVwiL1wiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcm91c2VsLWNhcHRpb24gZC1ibG9ja1wiPlxuICAgICAgICAgICAgPGgxIHN0eWxlPXt7IG1hcmdpbkJvdHRvbTogXCIzMHB4XCIgfX0+XG4gICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjM2cHhcIiwgY29sb3I6IFwid2hpdGVcIiwgZm9udFdlaWdodDogXCJib2xkXCIgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIFJVT0tJIEhFVk9TRVNJIEhZVklOVk9OVElBXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDwvaDE+XG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImNhcm91c2VsLWJ0blwiPlxuICAgICAgICAgICAgICB7XCIgXCJ9XG4gICAgICAgICAgICAgIEtPVElNQUlTRVQgSEVWT1NSRUhVVCBQT0hKT0lTSUlOIE9MT0lISU57XCIgXCJ9XG4gICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9MaW5rPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2XG4gICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgYmFja2dyb3VuZDpcbiAgICAgICAgICAgIFwidXJsKCdpbWcvbm9ycmFfbWVudV8xOTIweDcwNC5qcGcnKVwiLFxuICAgICAgICAgIGhlaWdodDogXCI1MDRweFwiLFxuICAgICAgICAgIGJhY2tncm91bmRQb3NpdGlvbjogXCJjZW50ZXJcIixcbiAgICAgICAgICBiYWNrZ3JvdW5kUmVwZWF0OiBcIm5vLXJlcGVhdFwiLFxuICAgICAgICAgIGJhY2tncm91bmRTaXplOiBcImNvdmVyXCIsXG4gICAgICAgIH19XG4gICAgICAgIGNsYXNzTmFtZT1cIm1haW4tY2Fyb3VzZWwtaW1hZ2UgY2Fyb3VzZWwtaXRlbVwiXG4gICAgICA+XG4gICAgICAgIDxMaW5rXG4gICAgICAgICAgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtY2FwdGlvbiBkLWJsb2NrXCJcbiAgICAgICAgICBzdHlsZT17eyBtYXJnaW5Cb3R0b206IFwiMzBweFwiIH19XG4gICAgICAgICAgdG89XCIvXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtY2FwdGlvbiBkLWJsb2NrXCI+XG4gICAgICAgICAgICA8aDEgc3R5bGU9e3sgbWFyZ2luQm90dG9tOiBcIjMwcHhcIiB9fT5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwiMzZweFwiLCBjb2xvcjogXCJ3aGl0ZVwiLCBmb250V2VpZ2h0OiBcImJvbGRcIiB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgUlVPS0kgSEVWT1NFU0kgSFlWSU5WT05USUFcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPC9oMT5cbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiY2Fyb3VzZWwtYnRuXCI+XG4gICAgICAgICAgICAgIHtcIiBcIn1cbiAgICAgICAgICAgICAgS09USU1BSVNFVCBIRVZPU1JFSFVUIFBPSEpPSVNJSU4gT0xPSUhJTntcIiBcIn1cbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0xpbms+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXZcbiAgICAgIHN0eWxlPXt7XG4gICAgICAgIGJhY2tncm91bmQ6XG4gICAgICAgICAgXCJ1cmwoJ2ltZy9ibGFjay1ob3JzZS5qcGcnKVwiLFxuICAgICAgICBoZWlnaHQ6IFwiNTA0cHhcIixcbiAgICAgICAgYmFja2dyb3VuZFBvc2l0aW9uOiBcImNlbnRlclwiLFxuICAgICAgICBiYWNrZ3JvdW5kUmVwZWF0OiBcIm5vLXJlcGVhdFwiLFxuICAgICAgICBiYWNrZ3JvdW5kU2l6ZTogXCJjb3ZlclwiLFxuICAgICAgfX1cbiAgICAgICAgY2xhc3NOYW1lPVwibWFpbi1jYXJvdXNlbC1pbWFnZSBjYXJvdXNlbC1pdGVtXCJcbiAgICAgID5cbiAgICAgICAgPExpbmtcbiAgICAgICAgICBjbGFzc05hbWU9XCJjYXJvdXNlbC1jYXB0aW9uIGQtYmxvY2tcIlxuICAgICAgICAgIHN0eWxlPXt7IG1hcmdpbkJvdHRvbTogXCIzMHB4XCIgfX1cbiAgICAgICAgICB0bz1cIi9cIlxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJvdXNlbC1jYXB0aW9uIGQtYmxvY2tcIj5cbiAgICAgICAgICAgIDxoMSBzdHlsZT17eyBtYXJnaW5Cb3R0b206IFwiMzBweFwiIH19PlxuICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIzNnB4XCIsIGNvbG9yOiBcIndoaXRlXCIsIGZvbnRXZWlnaHQ6IFwiYm9sZFwiIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBSVU9LSSBIRVZPU0VTSSBIWVZJTlZPTlRJQVxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8L2gxPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJjYXJvdXNlbC1idG5cIj5cbiAgICAgICAgICAgICAge1wiIFwifVxuICAgICAgICAgICAgICBLT1RJTUFJU0VUIEhFVk9TUkVIVVQgUE9ISk9JU0lJTiBPTE9JSElOe1wiIFwifVxuICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvTGluaz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICA8L2Rpdj5cbik7XG5cbmV4cG9ydCBkZWZhdWx0IE1haW5DYXJvdXNlbDtcbiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcblxuY29uc3QgTm90Rm91bmQgPSAoKSA9PiAoXG5cbiAgICA8bWFpbj5cbiAgICAgICAgVGhlIHJlcXVlc3RlZCByZXNvdXJjZSB3YXMgbm90IGZvdW5kLlxuICAgIDwvbWFpbj5cbilcblxuZXhwb3J0IGRlZmF1bHQgTm90Rm91bmQiLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgTGluaywgdXNlTG9jYXRpb24gfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuaW1wb3J0IGF4aW9zIGZyb20gXCJheGlvc1wiO1xuaW1wb3J0IHtcbiAgdmFsaWRhdGVSZWR1Y2VRdWFudGl0eSxcbiAgdmFsaWRhdGVJbmNyZWFzZVF1YW50aXR5LFxuICB2YWxpZGF0ZVF1YW50aXR5LFxuICBsb2FkUHJvZHVjdCxcbiAgdG9nZ2xlLFxuICBwcm9kdWN0RmVhdHVyZXNBY2NvcmRpb25CdXR0b25DbGljayxcbiAgYWNjb3JkaW9uQnV0dG9uQ2xpY2ssXG59IGZyb20gXCIuLi9hcHBcIjtcbmltcG9ydCBOb3RGb3VuZCBmcm9tIFwiLi9Ob3RGb3VuZC5qc3hcIjtcbmltcG9ydCBMb2FkaW5nIGZyb20gXCIuL0xvYWRpbmcuanN4XCI7XG5cbmNvbnN0IFByb2R1Y3QgPSAoeyBjYWxsYmFjayB9KSA9PiB7XG4gIGNvbnN0IGxvY2F0aW9uID0gdXNlTG9jYXRpb24oKTtcbiAgY29uc3QgX3Byb2R1Y3QgPSBsb2NhdGlvbi5zdGF0ZTtcblxuICBjb25zdCBbcHJvZHVjdCwgc2V0UHJvZHVjdF0gPSB1c2VTdGF0ZSgpO1xuICBjb25zdCBbbG9hZGluZywgc2V0TG9hZGluZ10gPSB1c2VTdGF0ZSh0cnVlKTtcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmIChfcHJvZHVjdCkge1xuICAgICAgc2V0UHJvZHVjdChfcHJvZHVjdCk7XG4gICAgICBsb2FkUHJvZHVjdChfcHJvZHVjdCk7XG4gICAgICBzZXRMb2FkaW5nKGZhbHNlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgYXhpb3NcbiAgICAgICAgLmdldChgYXBpL3Byb2R1Y3QvJHt3aW5kb3cubG9jYXRpb24uaHJlZi5zcGxpdChcIi9cIikucG9wKCl9YClcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgc2V0UHJvZHVjdChyZXNwb25zZS5kYXRhKTtcbiAgICAgICAgICBzZXRMb2FkaW5nKGZhbHNlKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcmV0dXJuICgpID0+IHtcbiAgICAgIHNldFByb2R1Y3QoKTtcbiAgICAgIHNldExvYWRpbmcoKTtcbiAgICB9O1xuICB9LCBbXSk7XG5cbiAgaWYgKGxvYWRpbmcpIHtcbiAgICByZXR1cm4gPExvYWRpbmcgLz47XG4gIH0gZWxzZSBpZiAocHJvZHVjdCkge1xuICAgIHJldHVybiAoXG4gICAgICA8bWFpbiBjbGFzc05hbWU9XCJtYWluLWNvbnRhaW5lciBwYi00XCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1ub25lIGQtbGctYmxvY2sgbWItMlwiPlxuICAgICAgICAgIDxhXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJhbGlnbi10b3BcIlxuICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICB0ZXh0RGVjb3JhdGlvbjogXCJub25lXCIsXG4gICAgICAgICAgICAgIGZvbnRTaXplOiBcInNtYWxsXCIsXG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgaHJlZj1cIiNcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIEV0dXNpdnVcbiAgICAgICAgICA8L2E+XG4gICAgICAgICAgJm5ic3A7IC8gJm5ic3A7XG4gICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImFsaWduLXRvcFwiXG4gICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICBjb2xvcjogXCIjNWY1ZjVmXCIsXG4gICAgICAgICAgICAgIHRleHREZWNvcmF0aW9uOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgZm9udFNpemU6IFwic21hbGxcIixcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAge3Byb2R1Y3QudGl0bGV9XG4gICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGNsYXNzTmFtZT1cImFkZC1jYXJ0LWNvbmZpcm1lZCBiZy13aGl0ZSBwLTQgbWItMSBkLWZsZXgganVzdGlmeS1jb250ZW50LWJldHdlZW4gZC1ub25lXCJcbiAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIxMnB4XCIsIGJvcmRlcjogXCIxcHggc29saWQgZ3JlZW5cIiB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwibGlnaHRncmF5XCIsIHBvc2l0aW9uOiBcImFic29sdXRlXCIgfX1cbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMzJcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIzMlwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS1jaXJjbGUtZmlsbFwiXG4gICAgICAgICAgICAgIHZpZXdCb3g9XCIwIDAgMTYgMTZcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8Y2lyY2xlIGN4PVwiOFwiIGN5PVwiOFwiIHI9XCI4XCIgLz5cbiAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgICAgPHN2Z1xuICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCJncmVlblwiLCBwb3NpdGlvbjogXCJyZWxhdGl2ZVwiIH19XG4gICAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICAgICAgICB3aWR0aD1cIjMyXCJcbiAgICAgICAgICAgICAgaGVpZ2h0PVwiMzJcIlxuICAgICAgICAgICAgICBmaWxsPVwiY3VycmVudENvbG9yXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYmkgYmktY2hlY2tcIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGggZD1cIk0xMC45NyA0Ljk3YS43NS43NSAwIDAgMSAxLjA3IDEuMDVsLTMuOTkgNC45OWEuNzUuNzUgMCAwIDEtMS4wOC4wMkw0LjMyNCA4LjM4NGEuNzUuNzUgMCAxIDEgMS4wNi0xLjA2bDIuMDk0IDIuMDkzIDMuNDczLTQuNDI1YS4yNjcuMjY3IDAgMCAxIC4wMi0uMDIyelwiIC8+XG4gICAgICAgICAgICA8L3N2Zz5cbiAgICAgICAgICAgIDxzcGFuIHN0eWxlPXt7IGNvbG9yOiBcImdyZWVuXCIgfX0+XG4gICAgICAgICAgICAgICZuYnNwOyZuYnNwOyZuYnNwOyBMaXPDpHNpdCB0dW90dGVlbiB7cHJvZHVjdC50aXRsZX1cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICAgIHRleHREZWNvcmF0aW9uOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgICBmb250V2VpZ2h0OiBcImJvbGRcIixcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgaHJlZj1cImNoZWNrb3V0L2NhcnRcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBvc3Rvc2tvcmlpc2kuXG4gICAgICAgICAgICA8L2E+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPExpbmtcbiAgICAgICAgICAgIHRvPXt7IHBhdGhuYW1lOiBwcm9kdWN0LnVybCwgc3RhdGU6IHByb2R1Y3QgfX1cbiAgICAgICAgICAgIHN0eWxlPXt7IGN1cnNvcjogXCJwb2ludGVyXCIgfX1cbiAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHtcbiAgICAgICAgICAgICAgJChcIi5hZGQtY2FydC1jb25maXJtZWRcIilbMF0uY2xhc3NMaXN0LmFkZChcImQtbm9uZVwiKTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPHN2Z1xuICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCJncmVlblwiIH19XG4gICAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICAgICAgICB3aWR0aD1cIjMyXCJcbiAgICAgICAgICAgICAgaGVpZ2h0PVwiMzJcIlxuICAgICAgICAgICAgICBmaWxsPVwiY3VycmVudENvbG9yXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYmkgYmkteFwiXG4gICAgICAgICAgICAgIHZpZXdCb3g9XCIwIDAgMTYgMTZcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8cGF0aCBkPVwiTTQuNjQ2IDQuNjQ2YS41LjUgMCAwIDEgLjcwOCAwTDggNy4yOTNsMi42NDYtMi42NDdhLjUuNSAwIDAgMSAuNzA4LjcwOEw4LjcwNyA4bDIuNjQ3IDIuNjQ2YS41LjUgMCAwIDEtLjcwOC43MDhMOCA4LjcwN2wtMi42NDYgMi42NDdhLjUuNSAwIDAgMS0uNzA4LS43MDhMNy4yOTMgOCA0LjY0NiA1LjM1NGEuNS41IDAgMCAxIDAtLjcwOHpcIiAvPlxuICAgICAgICAgICAgPC9zdmc+XG4gICAgICAgICAgPC9MaW5rPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWZsZXggdy0xMDAganVzdGlmeS1jb250ZW50LWJldHdlZW4gcC00IGJnLXdoaXRlIGQtbm9uZSBkLWxnLWZsZXhcIj5cbiAgICAgICAgICA8ZGl2IHN0eWxlPXt7IHBhZGRpbmdMZWZ0OiBcIjEwMHB4XCIsIHdpZHRoOiBcIjU1JVwiIH19PlxuICAgICAgICAgICAgPGEgaHJlZj1cIlwiPlxuICAgICAgICAgICAgICA8aW1nIHNyYz17cHJvZHVjdC5pbWFnZVVybH0gY2xhc3NOYW1lPVwiaW1nLWZsdWlkIHctMTAwXCIgLz5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCI0NSVcIiwgcGFkZGluZ1JpZ2h0OiBcIjQwcHhcIiB9fVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgcHQtMlwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgY29sb3I6IFwiIzVmNWY1ZlwiLCBmb250U2l6ZTogXCIxMnB4XCIgfX0+XG4gICAgICAgICAgICAgICAgVHVvdGVudW1lcm8gJm5ic3A7XG4gICAgICAgICAgICAgICAge3Byb2R1Y3QuaW1hZ2VVcmwucmVwbGFjZShcIi5qcGdcIiwgXCJcIikucmVwbGFjZShcIi9pbWcvXCIsIFwiXCIpfVxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxoNFxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3QtdGl0bGUgZnctYm9sZFwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICB7cHJvZHVjdC50aXRsZS50b1VwcGVyQ2FzZSgpfVxuICAgICAgICAgICAgICA8L2g0PlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByaWNlLWJveCBkLWZsZXgganVzdGlmeS1jb250ZW50LWJldHdlZW4gYWxpZ24taXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwiIzVmNWY1ZlwiLCBoZWlnaHQ6IFwiNDZweFwiIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgZnMtMiBmdy1ib2xkIHByaWNlICR7cHJvZHVjdC51cmx9YH1cbiAgICAgICAgICAgICAgPjwvc3Bhbj5cbiAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCJ4LXNtYWxsXCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2B0ZXh0LW11dGVkIHZhdCAke3Byb2R1Y3QudmF0fWB9XG4gICAgICAgICAgICAgID48L3NwYW4+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxociBzdHlsZT17eyBtYXJnaW5Ub3A6IFwiNnB4XCIsIG1hcmdpbkJvdHRvbTogXCI0OHB4XCIgfX0gLz5cbiAgICAgICAgICAgIDxmb3JtXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogXCI0MHB4XCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1mbGV4IGludmlzaWJsZSBhZGQtdG8tY2FydC1mb3JtXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwidXJsLWlucHV0XCJcbiAgICAgICAgICAgICAgICB0eXBlPVwiaGlkZGVuXCJcbiAgICAgICAgICAgICAgICBkZWZhdWx0VmFsdWU9e3Byb2R1Y3QudXJsfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInF1YW50aXR5LWNoYW5nZS1idG4gdGV4dC1jZW50ZXJcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IG1hcmdpblJpZ2h0OiBcIjEwcHhcIiB9fVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3ZhbGlkYXRlUmVkdWNlUXVhbnRpdHl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAtXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgdHlwZT1cIm51bWJlclwiXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlPVwiMVwiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicXVhbnRpdHktaW5wdXQgZC1pbmxpbmUgZm9ybS1jb250cm9sIHRleHQtY2VudGVyXCJcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCIxMnB4XCIgfX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJxdWFudGl0eS1jaGFuZ2UtYnRuIHRleHQtY2VudGVyXCJcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBtYXJnaW5MZWZ0OiBcIjEwcHhcIiB9fVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3ZhbGlkYXRlSW5jcmVhc2VRdWFudGl0eX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICtcbiAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdmFsaWRhdGVRdWFudGl0eSgpID8gY2FsbGJhY2sodHJ1ZSk6bnVsbH1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJvcmRlci1idG4gZC1pbmxpbmUgdGV4dC1jZW50ZXIgdy0xMDBcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBtYXJnaW5MZWZ0OiBcIjIwcHhcIixcbiAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IFwiMC41cyBlYXNlLWluXCIsXG4gICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBsaW5lSGVpZ2h0OiBcIjQwcHhcIixcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yOiBcInBvaW50ZXJcIixcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IFwicmVsYXRpdmVcIixcbiAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1wiZC1mbGV4IGp1c3RpZnktY29udGVudC1jZW50ZXJcIn1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICBMaXPDpMOkIG9zdG9rb3JpaW5cbiAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwib3JkZXItYnRuLXNwaW5uZXItYm9yZGVyIGQtbm9uZVwiXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJzdGF0dXNcIlxuICAgICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiBcIiNlNzcxMTBcIixcbiAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIyMHB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBcIjIwcHhcIixcbiAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogXCJhYnNvbHV0ZVwiLFxuICAgICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJzci1vbmx5XCI+PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwiZ3JlZW5cIiwgZm9udFNpemU6IFwiMTJweFwiLCBoZWlnaHQ6IFwiMThweFwiIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm10LTMgaW4tc3RvcmUtYm94XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGlcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBvcGFjaXR5OiBcIjBcIiwgdHJhbnNpdGlvbjogXCIwLjVzIGVhc2UtaW5cIiB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImluLXN0b3JlLWNpcmNsZVwiXG4gICAgICAgICAgICAgID48L2k+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgb3BhY2l0eTogXCIwXCIsIHRyYW5zaXRpb246IFwiMC41cyBlYXNlLWluXCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJpbi1zdG9yZVwiXG4gICAgICAgICAgICAgID48L3NwYW4+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic3Bpbm5lci1ib3JkZXIgcHJvZHVjdC1kZXRhaWxzLXdhaXQtc3Bpbm5lci1ib3JkZXJcIlxuICAgICAgICAgICAgICByb2xlPVwic3RhdHVzXCJcbiAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwiI2U3NzExMFwiLCB3aWR0aDogXCIyMHB4XCIsIGhlaWdodDogXCIyMHB4XCIgfX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwic3Itb25seVwiPjwvc3Bhbj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPHBcbiAgICAgICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwic21hbGxcIiwgY29sb3I6IFwiIzVmNWY1ZlwiLCBtYXJnaW5Ub3A6IFwiMTZweFwiIH19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIHtwcm9kdWN0LnNob3J0RGVzY3JpcHRpb259XG4gICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICA8aHIgLz5cblxuICAgICAgICAgICAgeygoKSA9PlxuICAgICAgICAgICAgICBwcm9kdWN0LnNlbGxpbmdQb2ludHMubWFwKChzZWxsaW5nUG9pbnQsIGluZGV4KSA9PiAoXG4gICAgICAgICAgICAgICAgPGRpdiBrZXk9e2luZGV4fT5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4XCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxpXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCIjZTc3MTEwXCIsIGZvbnRTaXplOiBcInNtYWxsXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImZhcyBmYS1jaGVja1wiXG4gICAgICAgICAgICAgICAgICAgICAgPjwvaT5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBmb250U2l6ZTogXCJzbWFsbFwiLCBjb2xvcjogXCIjNWY1ZjVmXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBwLTEgZnctYm9sZFwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57c2VsbGluZ1BvaW50fTwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgKSkpKCl9XG5cbiAgICAgICAgICAgIDxociAvPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIyMnB4XCIsIGhlaWdodDogXCIyMnB4XCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgbWUtMVwiXG4gICAgICAgICAgICAgIHNyYz1cImltZy9mYWNlYm9vay5qcGdcIlxuICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMjJweFwiLCBoZWlnaHQ6IFwiMjJweFwiIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtaW5saW5lIG1lLTFcIlxuICAgICAgICAgICAgICBzcmM9XCJpbWcvcGludGVyZXN0LmpwZ1wiXG4gICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIyMnB4XCIsIGhlaWdodDogXCIyMnB4XCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgbWUtMVwiXG4gICAgICAgICAgICAgIHNyYz1cImltZy90d2l0dGVyLmpwZ1wiXG4gICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIyMnB4XCIsIGhlaWdodDogXCIyMnB4XCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmVcIlxuICAgICAgICAgICAgICBzcmM9XCJpbWcvZW1haWwucG5nXCJcbiAgICAgICAgICAgICAgYWx0PVwiXCJcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1kLXByb2R1Y3QtY29udGFpbmVyIHctMTAwIGJnLXdoaXRlIHBiLTRcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInB0LTQgcHMtM1wiPlxuICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3sgY29sb3I6IFwiIzVmNWY1ZlwiLCBmb250U2l6ZTogXCIxMnB4XCIgfX0+XG4gICAgICAgICAgICAgIFR1b3RlbnVtZXJvICZuYnNwO1xuICAgICAgICAgICAgICB7cHJvZHVjdC5pbWFnZVVybC5yZXBsYWNlKFwiLmpwZ1wiLCBcIlwiKS5yZXBsYWNlKFwiL2ltZy9cIiwgXCJcIil9XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8aDQgc3R5bGU9e3sgY29sb3I6IFwiIzVmNWY1ZlwiIH19IGNsYXNzTmFtZT1cImZ3LWJvbGRcIj5cbiAgICAgICAgICAgICAge3Byb2R1Y3QudGl0bGUudG9VcHBlckNhc2UoKX1cbiAgICAgICAgICAgIDwvaDQ+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBzdHlsZT17eyBwYWRkaW5nTGVmdDogXCIyMCVcIiwgcGFkZGluZ1JpZ2h0OiBcIjIwJVwiIH19PlxuICAgICAgICAgICAgPGEgaHJlZj1cIlwiPlxuICAgICAgICAgICAgICA8aW1nIHNyYz17cHJvZHVjdC5pbWFnZVVybH0gY2xhc3NOYW1lPVwiaW1nLWZsdWlkIHctMTAwXCIgLz5cbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IHN0eWxlPXt7IHBhZGRpbmdMZWZ0OiBcIjIwJVwiLCBwYWRkaW5nUmlnaHQ6IFwiMjAlXCIgfX0+XG4gICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogXCI0M3B4XCIgfX1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJpY2UtYm94IGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlbiBhbGlnbi1pdGVtcy1jZW50ZXJcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17YGZzLTIgZnctYm9sZCBwcmljZSAke3Byb2R1Y3QudXJsfWB9XG4gICAgICAgICAgICAgID48L3NwYW4+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwieC1zbWFsbFwiIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgdGV4dC1tdXRlZCB2YXQgJHtwcm9kdWN0LnZhdH1gfVxuICAgICAgICAgICAgICA+PC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8aHIgc3R5bGU9e3sgbWFyZ2luVG9wOiBcIjZweFwiLCBtYXJnaW5Cb3R0b206IFwiNDhweFwiIH19IC8+XG4gICAgICAgICAgICA8Zm9ybVxuICAgICAgICAgICAgICBzdHlsZT17eyBoZWlnaHQ6IFwiNDBweFwiIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtZmxleCBpbnZpc2libGUgYWRkLXRvLWNhcnQtZm9ybVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInVybC1pbnB1dFwiXG4gICAgICAgICAgICAgICAgdHlwZT1cImhpZGRlblwiXG4gICAgICAgICAgICAgICAgZGVmYXVsdFZhbHVlPXtwcm9kdWN0LnVybH1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJxdWFudGl0eS1jaGFuZ2UtYnRuIHRleHQtY2VudGVyXCJcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBtYXJnaW5SaWdodDogXCIxMHB4XCIgfX1cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZXZlbnQpID0+IHZhbGlkYXRlUmVkdWNlUXVhbnRpdHkoZXZlbnQpfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgLVxuICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgIHR5cGU9XCJudW1iZXJcIlxuICAgICAgICAgICAgICAgIGRlZmF1bHRWYWx1ZT1cIjFcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInF1YW50aXR5LWlucHV0IGQtaW5saW5lIGZvcm0tY29udHJvbCB0ZXh0LWNlbnRlclwiXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwiMTJweFwiIH19XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicXVhbnRpdHktY2hhbmdlLWJ0biB0ZXh0LWNlbnRlclwiXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgbWFyZ2luTGVmdDogXCIxMHB4XCIgfX1cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZXZlbnQpID0+IHZhbGlkYXRlSW5jcmVhc2VRdWFudGl0eShldmVudCl9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICArXG4gICAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHZhbGlkYXRlUXVhbnRpdHkoKSA/IGNhbGxiYWNrKHRydWUpIDogbnVsbH1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJvcmRlci1idG4gZC1pbmxpbmUgdGV4dC1jZW50ZXIgdy0xMDBcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IG1hcmdpbkxlZnQ6IFwiMjBweFwiLCB0cmFuc2l0aW9uOiBcIjAuNXMgZWFzZS1pblwiIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJvcmRlci1idG4tc3Bpbm5lci1ib3JkZXIgZC1ub25lXCJcbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJzdGF0dXNcIlxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwiI2U3NzExMFwiLCB3aWR0aDogXCIyMHB4XCIsIGhlaWdodDogXCIyMHB4XCIgfX1cbiAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJzci1vbmx5XCI+PC9zcGFuPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxzcGFuIHN0eWxlPXt7IGxpbmVIZWlnaHQ6IFwiNDBweFwiLCBjdXJzb3I6IFwicG9pbnRlclwiIH19PlxuICAgICAgICAgICAgICAgICAgTGlzw6TDpCBvc3Rva29yaWluXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZm9ybT5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwiZ3JlZW5cIiwgZm9udFNpemU6IFwiMTJweFwiLCBoZWlnaHQ6IFwiMThweFwiIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm10LTMgaW4tc3RvcmUtYm94XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGlcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBvcGFjaXR5OiBcIjBcIiwgdHJhbnNpdGlvbjogXCIwLjVzIGVhc2UtaW5cIiB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImluLXN0b3JlLWNpcmNsZVwiXG4gICAgICAgICAgICAgID48L2k+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgb3BhY2l0eTogXCIwXCIsIHRyYW5zaXRpb246IFwiMC41cyBlYXNlLWluXCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJpbi1zdG9yZVwiXG4gICAgICAgICAgICAgID48L3NwYW4+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxwXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcInNtYWxsXCIsIGNvbG9yOiBcIiM1ZjVmNWZcIiwgbWFyZ2luVG9wOiBcIjE2cHhcIiB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICB7cHJvZHVjdC5zaG9ydERlc2NyaXB0aW9ufVxuICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgPGhyIC8+XG5cbiAgICAgICAgICAgIHsoKCkgPT5cbiAgICAgICAgICAgICAgcHJvZHVjdC5zZWxsaW5nUG9pbnRzLm1hcCgoc2VsbGluZ1BvaW50LCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgIDxkaXYga2V5PXtpbmRleH0gY2xhc3NOYW1lPVwiZC1mbGV4XCI+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICAgICAgPGlcbiAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCIjZTc3MTEwXCIsIGZvbnRTaXplOiBcInNtYWxsXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmYXMgZmEtc29saWQgZmEtY2hlY2tcIlxuICAgICAgICAgICAgICAgICAgICA+PC9pPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcInNtYWxsXCIsIGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBwLTEgZnctYm9sZFwiXG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIDxzcGFuPntzZWxsaW5nUG9pbnR9PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpKSgpfVxuXG4gICAgICAgICAgICA8aHIgLz5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIyMnB4XCIsIGhlaWdodDogXCIyMnB4XCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBtZS0xXCJcbiAgICAgICAgICAgICAgICBzcmM9XCJpbWcvZmFjZWJvb2suanBnXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgd2lkdGg6IFwiMjJweFwiLCBoZWlnaHQ6IFwiMjJweFwiIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgbWUtMVwiXG4gICAgICAgICAgICAgICAgc3JjPVwiaW1nL3BpbnRlcmVzdC5qcGdcIlxuICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIyMnB4XCIsIGhlaWdodDogXCIyMnB4XCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBtZS0xXCJcbiAgICAgICAgICAgICAgICBzcmM9XCJpbWcvdHdpdHRlci5qcGdcIlxuICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBzdHlsZT17eyB3aWR0aDogXCIyMnB4XCIsIGhlaWdodDogXCIyMnB4XCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBtZS0xXCJcbiAgICAgICAgICAgICAgICBzcmM9XCJpbWcvZW1haWwucG5nXCJcbiAgICAgICAgICAgICAgICBhbHQ9XCJcIlxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwiMTJweFwiLCBjb2xvcjogXCIjNWY1ZjVmXCIgfX1cbiAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0LXRhYi1saXN0XCJcbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicC0zIGJnLXdoaXRlIHctMTAwIG10LTJcIj5cbiAgICAgICAgICAgIDxkaXYgaWQ9XCJ0YWItbGlzdFwiIGNsYXNzTmFtZT1cInRhYi1sb25nLWRlc2NyaXB0aW9uXCI+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBtYXJnaW5SaWdodDogXCI0MHB4XCIsIGN1cnNvcjogXCJwb2ludGVyXCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBmdy1ib2xkIHB0LTMgcGItM1wiXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdG9nZ2xlKFwidGFiLWxvbmctZGVzY3JpcHRpb25cIil9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBUVU9URUtVVkFVU1xuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IG1hcmdpblJpZ2h0OiBcIjQwcHhcIiwgY3Vyc29yOiBcInBvaW50ZXJcIiB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtaW5saW5lIGZ3LWJvbGQgcHQtMyBwYi0zXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0b2dnbGUoXCJ0YWItY29udGVudHNcIil9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBTSVPDhExUw5ZcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBtYXJnaW5SaWdodDogXCI0MHB4XCIsIGN1cnNvcjogXCJwb2ludGVyXCIgfX1cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJkLWlubGluZSBmdy1ib2xkIHB0LTMgcGItM1wiXG4gICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdG9nZ2xlKFwidGFiLW1vcmUtaW5mb3JtYXRpb25cIil9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBMSVPDhFRJRVRPQVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IG1hcmdpblJpZ2h0OiBcIjQwcHhcIiwgY3Vyc29yOiBcInBvaW50ZXJcIiB9fVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtaW5saW5lIGZ3LWJvbGQgcHQtMyBwYi0zXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0b2dnbGUoXCJ0YWItdXNhZ2UtcmF0ZVwiKX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIEvDhFlUVMOWTcOEw4RSw4RcbiAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgeygoKSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKHByb2R1Y3QuZmVhdHVyZXMpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBjdXJzb3I6IFwicG9pbnRlclwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1pbmxpbmUgZnctYm9sZCBwdC0zIHBiLTNcIlxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRvZ2dsZShcInRhYi1mZWF0dXJlc1wiKX1cbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgIE9NSU5BSVNVVURFVFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KSgpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwLTMgYmctd2hpdGVcIj5cbiAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luUmlnaHQ6IFwiMzcwcHhcIiB9fT5cbiAgICAgICAgICAgICAgPHAgaWQ9XCJ0YWItbG9uZy1kZXNjcmlwdGlvblwiIGNsYXNzTmFtZT1cInRhYlwiPlxuICAgICAgICAgICAgICAgIHtwcm9kdWN0LmxvbmdEZXNjcmlwdGlvbn1cbiAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luUmlnaHQ6IFwiMzcwcHhcIiB9fT5cbiAgICAgICAgICAgICAgPHRhYmxlXG4gICAgICAgICAgICAgICAgaWQ9XCJ0YWItY29udGVudHNcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInRhYiB0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmxlc3MgZC1ub25lXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICAgICAgICAgIHsoKCkgPT5cbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMocHJvZHVjdC5jb250ZW50cykubWFwKChrZXksIGluZGV4KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgPHRyIGtleT17aW5kZXh9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRkPntrZXl9PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57cHJvZHVjdC5jb250ZW50c1trZXldfTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgKSkpKCl9XG4gICAgICAgICAgICAgICAgPC90Ym9keT5cbiAgICAgICAgICAgICAgPC90YWJsZT5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IG1hcmdpblJpZ2h0OiBcIjM3MHB4XCIgfX0+XG4gICAgICAgICAgICAgIDx0YWJsZVxuICAgICAgICAgICAgICAgIGlkPVwidGFiLW1vcmUtaW5mb3JtYXRpb25cIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInRhYmxlIHRhYmxlLWJvcmRlcmxlc3MgdGFiIGQtbm9uZVwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8dGJvZHk+XG4gICAgICAgICAgICAgICAgICB7KCgpID0+XG4gICAgICAgICAgICAgICAgICAgIE9iamVjdC5rZXlzKHByb2R1Y3QubW9yZUluZm9ybWF0aW9uKS5tYXAoKGtleSwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICAgICAgICA8dHIga2V5PXtpbmRleH0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e2tleX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRkPntwcm9kdWN0Lm1vcmVJbmZvcm1hdGlvbltrZXldfTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgICAgICAgKSkpKCl9XG4gICAgICAgICAgICAgICAgPC90Ym9keT5cbiAgICAgICAgICAgICAgPC90YWJsZT5cbiAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IG1hcmdpblJpZ2h0OiBcIjM3MHB4XCIgfX0+XG4gICAgICAgICAgICAgIDx0YWJsZVxuICAgICAgICAgICAgICAgIGlkPVwidGFiLXVzYWdlLXJhdGVcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVybGVzcyB0YWIgZC1ub25lXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICAgICAgICAgIHsoKCkgPT5cbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmVudHJpZXMocHJvZHVjdC51c2FnZVJhdGUpLm1hcCgoa2V5LCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgIDx0ciBrZXk9e2luZGV4fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57a2V5fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3Byb2R1Y3QudXNhZ2VSYXRlW2tleV19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICApKSkoKX1cbiAgICAgICAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICAgICAgICA8L3RhYmxlPlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIHsoKCkgPT4ge1xuICAgICAgICAgICAgICBpZiAocHJvZHVjdC5mZWF0dXJlcykge1xuICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICA8ZGl2IGlkPVwidGFiLWZlYXR1cmVzXCIgY2xhc3NOYW1lPVwidGFiIGQtbm9uZVwiPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3RfZmVhdHVyZXNfYWNjb3JkaW9uX2J1dHRvbiBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyIGZ3LWJvbGQgcHQtNFwiXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZXZlbnQpID0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHByb2R1Y3RGZWF0dXJlc0FjY29yZGlvbkJ1dHRvbkNsaWNrKGV2ZW50LnRhcmdldClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICBUdW90ZW4gb21pbmFpc3V1ZGV0XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3RfZmVhdHVyZXNfcGx1c19pY29uXCI+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0X2ZlYXR1cmVzX3BhbmVsXCI+XG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNwaW5uZXItYm9yZGVyIHByb2R1Y3QtZmVhdHVyZXMtc3Bpbm5lci1ib3JkZXJcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICByb2xlPVwic3RhdHVzXCJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogXCIjZTc3MTEwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IFwiMjBweFwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogXCIyMHB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInNyLW9ubHlcIj48L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0YWJsZSBjbGFzc05hbWU9XCJ0YWJsZSB0YWJsZS1zdHJpcGVkIHRhYmxlLWJvcmRlcmxlc3MgcHJvZHVjdC1mZWF0dXJlcy10YWJsZVwiPjwvdGFibGU+XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSkoKX1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3sgbWFyZ2luVG9wOiBcIjFweFwiIH19IGNsYXNzTmFtZT1cInByb2R1Y3QtYWNjb3JkaW9uXCI+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJwcm9kdWN0LWFjY29yZGlvbi1idXR0b24tbG9uZy1kZXNjcmlwdGlvblwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0X2FjY29yZGlvbl9idXR0b24gZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoZXZlbnQpID0+IGFjY29yZGlvbkJ1dHRvbkNsaWNrKGV2ZW50LnRhcmdldCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgVHVvdGVrdXZhdXNcbiAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS1jaGV2cm9uLWRvd25cIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGhcbiAgICAgICAgICAgICAgICBmaWxsUnVsZT1cImV2ZW5vZGRcIlxuICAgICAgICAgICAgICAgIGQ9XCJNMS42NDYgNC42NDZhLjUuNSAwIDAgMSAuNzA4IDBMOCAxMC4yOTNsNS42NDYtNS42NDdhLjUuNSAwIDAgMSAuNzA4LjcwOGwtNiA2YS41LjUgMCAwIDEtLjcwOCAwbC02LTZhLjUuNSAwIDAgMSAwLS43MDh6XCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdF9wYW5lbFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwdC00IHByLTQgcGItNFwiPlxuICAgICAgICAgICAgICA8cD57cHJvZHVjdC5sb25nRGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJwcm9kdWN0LWFjY29yZGlvbi1idXR0b24tY29udGVudHNcIlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdF9hY2NvcmRpb25fYnV0dG9uIGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXJcIlxuICAgICAgICAgICAgb25DbGljaz17KGV2ZW50KSA9PiBhY2NvcmRpb25CdXR0b25DbGljayhldmVudC50YXJnZXQpfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxiPlNpc8OkbHTDtjwvYj5cbiAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS1jaGV2cm9uLWRvd25cIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGhcbiAgICAgICAgICAgICAgICBmaWxsUnVsZT1cImV2ZW5vZGRcIlxuICAgICAgICAgICAgICAgIGQ9XCJNMS42NDYgNC42NDZhLjUuNSAwIDAgMSAuNzA4IDBMOCAxMC4yOTNsNS42NDYtNS42NDdhLjUuNSAwIDAgMSAuNzA4LjcwOGwtNiA2YS41LjUgMCAwIDEtLjcwOCAwbC02LTZhLjUuNSAwIDAgMSAwLS43MDh6XCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdF9wYW5lbFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwdC00IHByLTQgcGItNFwiPlxuICAgICAgICAgICAgICA8dGFibGUgY2xhc3NOYW1lPVwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJsZXNzXCI+XG4gICAgICAgICAgICAgICAgPHRib2R5PlxuICAgICAgICAgICAgICAgICAgeygoKSA9PlxuICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhwcm9kdWN0LmNvbnRlbnRzKS5tYXAoKGtleSwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICAgICAgICA8dHIga2V5PXtpbmRleH0+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e2tleX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRkPntwcm9kdWN0LmNvbnRlbnRzW2tleV19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICApKSkoKX1cbiAgICAgICAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICAgICAgICA8L3RhYmxlPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJwcm9kdWN0LWFjY29yZGlvbi1idXR0b24tbW9yZS1pbmZvcm1hdGlvblwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0X2FjY29yZGlvbl9idXR0b24gZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoZXZlbnQpID0+IGFjY29yZGlvbkJ1dHRvbkNsaWNrKGV2ZW50LnRhcmdldCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGI+TGlzw6R0aWV0b2E8L2I+XG4gICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICAgICAgICAgICAgICB3aWR0aD1cIjE2XCJcbiAgICAgICAgICAgICAgaGVpZ2h0PVwiMTZcIlxuICAgICAgICAgICAgICBmaWxsPVwiY3VycmVudENvbG9yXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYmkgYmktY2hldnJvbi1kb3duXCJcbiAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxwYXRoXG4gICAgICAgICAgICAgICAgZmlsbFJ1bGU9XCJldmVub2RkXCJcbiAgICAgICAgICAgICAgICBkPVwiTTEuNjQ2IDQuNjQ2YS41LjUgMCAwIDEgLjcwOCAwTDggMTAuMjkzbDUuNjQ2LTUuNjQ3YS41LjUgMCAwIDEgLjcwOC43MDhsLTYgNmEuNS41IDAgMCAxLS43MDggMGwtNi02YS41LjUgMCAwIDEgMC0uNzA4elwiXG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L3N2Zz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3RfcGFuZWxcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHQtNCBwci00IHBiLTRcIj5cbiAgICAgICAgICAgICAgPHRhYmxlXG4gICAgICAgICAgICAgICAgaWQ9XCJhY2NvcmRpb24tbW9yZS1pbmZvcm1hdGlvblwiXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwidGFibGUgdGFibGUtYm9yZGVybGVzc1wiXG4gICAgICAgICAgICAgICAgb25DbGljaz17KGV2ZW50KSA9PiBwcm9kdWN0QWNjb3JkaW9uQnV0dG9uQ2xpY2soZXZlbnQudGFyZ2V0KX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICAgICAgICAgIHsoKCkgPT5cbiAgICAgICAgICAgICAgICAgICAgT2JqZWN0LmtleXMocHJvZHVjdC5tb3JlSW5mb3JtYXRpb24pLm1hcCgoa2V5LCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgICAgICAgIDx0ciBrZXk9e2luZGV4fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57a2V5fTwvdGQ+XG4gICAgICAgICAgICAgICAgICAgICAgICA8dGQ+e3Byb2R1Y3QubW9yZUluZm9ybWF0aW9uW2tleV19PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICA8L3RyPlxuICAgICAgICAgICAgICAgICAgICApKSkoKX1cbiAgICAgICAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICAgICAgICA8L3RhYmxlPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJwcm9kdWN0LWFjY29yZGlvbi1idXR0b24tdXNhZ2UtcmF0ZVwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJwcm9kdWN0X2FjY29yZGlvbl9idXR0b24gZC1mbGV4IGFsaWduLWl0ZW1zLWNlbnRlclwiXG4gICAgICAgICAgICBvbkNsaWNrPXsoZXZlbnQpID0+IGFjY29yZGlvbkJ1dHRvbkNsaWNrKGV2ZW50LnRhcmdldCl9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGI+S8OkeXR0w7Ztw6TDpHLDpDwvYj5cbiAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgIGZpbGw9XCJjdXJyZW50Q29sb3JcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS1jaGV2cm9uLWRvd25cIlxuICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPHBhdGhcbiAgICAgICAgICAgICAgICBmaWxsUnVsZT1cImV2ZW5vZGRcIlxuICAgICAgICAgICAgICAgIGQ9XCJNMS42NDYgNC42NDZhLjUuNSAwIDAgMSAuNzA4IDBMOCAxMC4yOTNsNS42NDYtNS42NDdhLjUuNSAwIDAgMSAuNzA4LjcwOGwtNiA2YS41LjUgMCAwIDEtLjcwOCAwbC02LTZhLjUuNSAwIDAgMSAwLS43MDh6XCJcbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdF9wYW5lbFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwdC00IHByLTQgcGItNFwiPlxuICAgICAgICAgICAgICA8dGFibGUgY2xhc3NOYW1lPVwidGFibGUgdGFibGUtc3RyaXBlZCB0YWJsZS1ib3JkZXJsZXNzXCI+XG4gICAgICAgICAgICAgICAgPHRib2R5PlxuICAgICAgICAgICAgICAgICAgeygoKSA9PlxuICAgICAgICAgICAgICAgICAgICBPYmplY3Qua2V5cyhwcm9kdWN0LnVzYWdlUmF0ZSkubWFwKChrZXksIGluZGV4KSA9PiAoXG4gICAgICAgICAgICAgICAgICAgICAgPHRyIGtleT17aW5kZXh9PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRkPntrZXl9PC90ZD5cbiAgICAgICAgICAgICAgICAgICAgICAgIDx0ZD57cHJvZHVjdC51c2FnZVJhdGVba2V5XX08L3RkPlxuICAgICAgICAgICAgICAgICAgICAgIDwvdHI+XG4gICAgICAgICAgICAgICAgICAgICkpKSgpfVxuICAgICAgICAgICAgICAgIDwvdGJvZHk+XG4gICAgICAgICAgICAgIDwvdGFibGU+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIHsoKCkgPT4ge1xuICAgICAgICAgICAgaWYgKHByb2R1Y3QuZmVhdHVyZXMpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICA8PlxuICAgICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgICBpZD1cInByb2R1Y3QtYWNjb3JkaW9uLWJ1dHRvbi1mZWF0dXJlc1wiXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInByb2R1Y3RfYWNjb3JkaW9uX2J1dHRvbiBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGV2ZW50KSA9PiBhY2NvcmRpb25CdXR0b25DbGljayhldmVudC50YXJnZXQpfVxuICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICA8Yj5PbWluYWlzdXVkZXQ8L2I+XG4gICAgICAgICAgICAgICAgICAgIDxzdmdcbiAgICAgICAgICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgICAgICAgICB3aWR0aD1cIjE2XCJcbiAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9XCIxNlwiXG4gICAgICAgICAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYmkgYmktY2hldnJvbi1kb3duXCJcbiAgICAgICAgICAgICAgICAgICAgICB2aWV3Qm94PVwiMCAwIDE2IDE2XCJcbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgIDxwYXRoXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxsUnVsZT1cImV2ZW5vZGRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgZD1cIk0xLjY0NiA0LjY0NmEuNS41IDAgMCAxIC43MDggMEw4IDEwLjI5M2w1LjY0Ni01LjY0N2EuNS41IDAgMCAxIC43MDguNzA4bC02IDZhLjUuNSAwIDAgMS0uNzA4IDBsLTYtNmEuNS41IDAgMCAxIDAtLjcwOHpcIlxuICAgICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3RfcGFuZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwdC00IHByLTQgcGItNFwiPlxuICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGZvbnRTaXplOiBcIjEycHhcIiB9fVxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicHJvZHVjdF9mZWF0dXJlc19hY2NvcmRpb25fYnV0dG9uIGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXIgZnctYm9sZCBwdC00XCJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhldmVudCkgPT5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdEZlYXR1cmVzQWNjb3JkaW9uQnV0dG9uQ2xpY2soZXZlbnQudGFyZ2V0KVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgIFR1b3RlbiBvbWluYWlzdXVkZXRcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdF9mZWF0dXJlc19wbHVzX2ljb25cIj48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3RfZmVhdHVyZXNfcGFuZWxcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic3Bpbm5lci1ib3JkZXIgcHJvZHVjdC1mZWF0dXJlcy1zcGlubmVyLWJvcmRlclwiXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJvbGU9XCJzdGF0dXNcIlxuICAgICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiBcIiNlNzcxMTBcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogXCIyMHB4XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBcIjIwcHhcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwic3Itb25seVwiPjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgPHRhYmxlIGNsYXNzTmFtZT1cInRhYmxlIHRhYmxlLXN0cmlwZWQgdGFibGUtYm9yZGVybGVzcyBwYi0yIHByb2R1Y3QtZmVhdHVyZXMtdGFibGVcIj48L3RhYmxlPlxuICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDwvPlxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pKCl9XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9tYWluPlxuICAgICk7XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIDxOb3RGb3VuZCAvPjtcbiAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgUHJvZHVjdDtcbiIsImltcG9ydCBSZWFjdCwge3VzZUVmZmVjdH0gZnJvbSAncmVhY3QnXG5pbXBvcnQge0xpbmt9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nXG5pbXBvcnQgeyBsb2FkUHJvZHVjdCB9IGZyb20gJy4uL2FwcCdcblxuY29uc3QgUHJvZHVjdENhcmQgPSAoe3Byb2R1Y3R9KSA9PiB7XG5cbiAgICB1c2VFZmZlY3QoKCk9PiBsb2FkUHJvZHVjdChwcm9kdWN0KSxbXSlcblxuICAgIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJjb2wtNiBjb2wtbGctNCBwLTAgbWItMVwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkIGgtMTAwXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWltZyBkLWZsZXgganVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxuICAgICAgICAgICAgICAgICAgICA8TGluayB0bz17e3BhdGhuYW1lOiBwcm9kdWN0LnVybCwgc3RhdGU6IHByb2R1Y3R9fT5cbiAgICAgICAgICAgICAgICAgICAgPGltZyBzdHlsZT17e2hlaWdodDonMTUwcHgnfX0gc3JjPXtwcm9kdWN0LmltYWdlVXJsfSBjbGFzc05hbWU9XCJpbWctZmx1aWRcIiAvPlxuICAgICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWJvZHkgZC1mbGV4IGZsZXgtY29sdW1uXCI+XG4gICAgICAgICAgICAgICAgICAgIDxMaW5rIGNsYXNzTmFtZT1cImNhcmQtdGl0bGVcIiB0bz17e3BhdGhuYW1lOiBwcm9kdWN0LnVybCwgc3RhdGU6IHByb2R1Y3R9fT5cbiAgICAgICAgICAgICAgICAgICAgPGg1IHN0eWxlPXt7Y29sb3I6JyM1ZjVmNWYnfX0gY2xhc3NOYW1lPVwiY2FyZC10aXRsZSB0ZXh0LWNlbnRlclwiPntwcm9kdWN0LnRpdGxlfTwvaDU+XG4gICAgICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgICAgICAgPHAgc3R5bGU9e3tmb250U2l6ZTonMTJweCcsIGNvbG9yOicjNWY1ZjVmJ319Pntwcm9kdWN0LnNob3J0RGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImgtMTAwXCI+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3toZWlnaHQ6JzM1cHgnfX0gY2xhc3NOYW1lPVwicHJpY2UtYm94IGQtZmxleCBqdXN0aWZ5LWNvbnRlbnQtYmV0d2VlbiBhbGlnbi1pdGVtcy1jZW50ZXIgbWItM1wiPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3tjb2xvcjonIzVmNWY1Zid9fSBjbGFzc05hbWU9XCJmcy00IHByaWNlIGZ3LWJvbGRcIj48L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBzdHlsZT17e2ZvbnRTaXplOid4LXNtYWxsJ319IGNsYXNzTmFtZT1cInRleHQtbXV0ZWQgdmF0XCI+PC9zcGFuPlxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e2hlaWdodDonNDJweCd9fSBjbGFzc05hbWU9XCJidG4tYm94IGQtZmxleCBhbGlnbi1pdGVtcy1lbmRcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxMaW5rIGNsYXNzTmFtZT1cImNhcmQtYnRuXCIgc3R5bGU9e3t0ZXh0RGVjb3JhdGlvbjonbm9uZSd9fSB0bz17e3BhdGhuYW1lOiBwcm9kdWN0LnVybCwgc3RhdGU6IHByb2R1Y3R9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIEx1ZSBsaXPDpMOkXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7Y29sb3I6J2dyZWVuJywgZm9udFNpemU6JzEycHgnLCBoZWlnaHQ6JzE4cHgnfX0gY2xhc3NOYW1lPVwibXQtMyBpbi1zdG9yZS1ib3hcIj5cbiAgICAgICAgICAgICAgICAgICAgICAgIDxpIHN0eWxlPXt7b3BhY2l0eTonMCcsIHRyYW5zaXRpb246ICcwLjVzIGVhc2UtaW4nfX0gY2xhc3NOYW1lPVwiaW4tc3RvcmUtY2lyY2xlXCI+PC9pPlxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gc3R5bGU9e3tvcGFjaXR5OicwJywgdHJhbnNpdGlvbjogJzAuNXMgZWFzZS1pbid9fSBjbGFzc05hbWU9XCJpbi1zdG9yZVwiPjwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4pXG4gICAgfVxuXG5leHBvcnQgZGVmYXVsdCBQcm9kdWN0Q2FyZCIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QsIHVzZVJlZiB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IFByb2R1Y3RDYXJvdXNlbEl0ZW0gZnJvbSBcIi4vUHJvZHVjdENhcm91c2VsSXRlbVwiO1xuaW1wb3J0IHsgaGFuZGxlTmV4dExpbmssIG15RnVuY3Rpb24gfSBmcm9tIFwiLi4vYXBwXCI7XG5pbXBvcnQge2xvYWRQcm9kdWN0c30gZnJvbSAnLi4vYXBwJ1xuXG5jb25zdCBQcm9kdWN0Q2Fyb3VzZWwgPSAoe3Byb2R1Y3RzfSkgPT4ge1xuXG4gIGNvbnN0IG5leHRMaW5rID0gdXNlUmVmKG51bGwpXG4gIGNvbnN0IHByZXZpb3VzTGluayA9IHVzZVJlZihudWxsKVxuXG4gIGZ1bmN0aW9uIGhhbmRsZVByZXZpb3VzQ2xpY2soKXtcblxuICAgIG5leHRMaW5rLmN1cnJlbnQuY2xhc3NMaXN0LnJlbW92ZShcImRpc2FibGVkXCIpXG4gICAgcHJldmlvdXNMaW5rLmN1cnJlbnQuYmx1cigpXG4gIH1cblxuICBmdW5jdGlvbiBoYW5kbGVOZXh0Q2xpY2soKXtcblxuICAgIGhhbmRsZU5leHRMaW5rKG5leHRMaW5rLmN1cnJlbnQpXG4gICAgbmV4dExpbmsuY3VycmVudC5ibHVyKClcbiAgfVxuXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG5cbiAgICAgIGxvYWRQcm9kdWN0cyhwcm9kdWN0cylcbiAgICAgIG15RnVuY3Rpb24oKVxuICB9LCBbXSlcbiAgXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJyb3cgbXgtYXV0byBteS1hdXRvIGp1c3RpZnktY29udGVudC1jZW50ZXIgbXQtNVwiPlxuICAgICAgPGRpdlxuICAgICAgICBpZD1cInByb2R1Y3RDYXJvdXNlbFwiXG4gICAgICAgIGNsYXNzTmFtZT1cImNhcm91c2VsIHNsaWRlXCJcbiAgICAgICAgZGF0YS1icy1yaWRlPVwiY2Fyb3VzZWxcIlxuICAgICAgICBkYXRhLWJzLWludGVydmFsPVwiZmFsc2VcIlxuICAgICAgICBkYXRhLWJzLXdyYXA9XCJmYWxzZVwiXG4gICAgICA+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtaW5uZXJcIj5cbiAgICAgICAgICB7KCgpID0+IHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaWYgKHByb2R1Y3RzKSB7XG4gICAgICAgICAgICAgIFxuICAgICAgICAgICAgICByZXR1cm4gcHJvZHVjdHMubWFwKChwcm9kdWN0LCBpbmRleCkgPT4gPFByb2R1Y3RDYXJvdXNlbEl0ZW0gcHJvZHVjdD17cHJvZHVjdH0ga2V5PXtpbmRleH0gLz4pXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSkoKX1cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGFcbiAgICAgICAgICBocmVmPVwiI3Byb2R1Y3RDYXJvdXNlbFwiXG4gICAgICAgICAgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtY29udHJvbC1wcmV2IGJnLXRyYW5zcGFyZW50XCJcbiAgICAgICAgICBkYXRhLWJzLXNsaWRlPVwicHJldlwiXG4gICAgICAgICAgcm9sZT1cImJ1dHRvblwiXG4gICAgICAgICAgb25DbGljaz17aGFuZGxlUHJldmlvdXNDbGlja31cbiAgICAgICAgICB0YWJJbmRleD17LTF9XG4gICAgICAgICAgcmVmPXtwcmV2aW91c0xpbmt9XG4gICAgICAgID5cbiAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtY29udHJvbC1wcmV2LWljb25cIlxuICAgICAgICAgICAgYXJpYS1oaWRkZW49XCJ0cnVlXCJcbiAgICAgICAgICA+PC9zcGFuPlxuICAgICAgICA8L2E+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGFcbiAgICAgICAgICAgIGhyZWY9XCIjcHJvZHVjdENhcm91c2VsXCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImNhcm91c2VsLWNvbnRyb2wtbmV4dCBiZy10cmFuc3BhcmVudFwiXG4gICAgICAgICAgICBkYXRhLWJzLXNsaWRlPVwibmV4dFwiXG4gICAgICAgICAgICByb2xlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgIG9uQ2xpY2s9e2hhbmRsZU5leHRDbGlja31cbiAgICAgICAgICAgIHRhYkluZGV4PXstMX1cbiAgICAgICAgICAgIHJlZj17bmV4dExpbmt9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiY2Fyb3VzZWwtY29udHJvbC1uZXh0LWljb25cIlxuICAgICAgICAgICAgICBhcmlhLWhpZGRlbj1cInRydWVcIlxuICAgICAgICAgICAgPjwvc3Bhbj5cbiAgICAgICAgICA8L2E+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gICk7XG59O1xuZXhwb3J0IGRlZmF1bHQgUHJvZHVjdENhcm91c2VsO1xuIiwiaW1wb3J0IFJlYWN0LCB7IHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgdXNlSGlzdG9yeSB9IGZyb20gXCJyZWFjdC1yb3V0ZXItZG9tXCI7XG5cbmNvbnN0IFByb2R1Y3RDYXJvdXNlbEl0ZW0gPSAoeyBwcm9kdWN0IH0pID0+IHtcbiAgXG4gIGxldCBoaXN0b3J5ID0gdXNlSGlzdG9yeSgpXG5cbiAgdXNlRWZmZWN0KCAoKSA9PiAkKCcuY2FyZC1idG4sIC5jYXJkLXRpdGxlLCAuaW1nLWZsdWlkJykuY2xpY2soZXZlbnQgPT4ge1xuICAgIGxldCBfcHJvZHVjdCA9IEpTT04ucGFyc2UoZXZlbnQudGFyZ2V0LmlkKVxuICAgIGhpc3RvcnkucHVzaCh7cGF0aG5hbWU6IF9wcm9kdWN0LnVybCwgc3RhdGU6IF9wcm9kdWN0fSlcbiAgfSksIFtdKVxuICBcbiAgcmV0dXJuIChcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImNhcm91c2VsLWl0ZW1cIiBzdHlsZT17eyBjdXJzb3I6IFwicG9pbnRlclwiIH19PlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb2xcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWltZyBkLWZsZXgganVzdGlmeS1jb250ZW50LWNlbnRlclwiPlxuICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICBzdHlsZT17eyBoZWlnaHQ6IFwiMTUwcHhcIiB9fVxuICAgICAgICAgICAgICBzcmM9e3Byb2R1Y3QuaW1hZ2VVcmx9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImltZy1mbHVpZFwiXG4gICAgICAgICAgICAgIGlkPXtKU09OLnN0cmluZ2lmeShwcm9kdWN0KX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkLWJvZHkgZC1mbGV4IGZsZXgtY29sdW1uXCI+XG4gICAgICAgICAgICA8aDUgaWQ9e0pTT04uc3RyaW5naWZ5KHByb2R1Y3QpfSBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIgfX0gY2xhc3NOYW1lPVwiY2FyZC10aXRsZSB0ZXh0LWNlbnRlclwiPlxuICAgICAgICAgICAgICB7cHJvZHVjdC50aXRsZX1cbiAgICAgICAgICAgIDwvaDU+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInByaWNlLWJveCBkLWZsZXgganVzdGlmeS1jb250ZW50LWJldHdlZW4gYWxpZ24taXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgY29sb3I6IFwiIzVmNWY1ZlwiIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgZnMtNSBwcmljZSAke3Byb2R1Y3QudXJsfWB9XG4gICAgICAgICAgICAgID48L3NwYW4+XG4gICAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgICAgc3R5bGU9e3sgZm9udFNpemU6IFwieC1zbWFsbFwiIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtgdGV4dC1tdXRlZCB2YXQgJHtwcm9kdWN0LnVybH1gfVxuICAgICAgICAgICAgICA+PC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnRuLWJveCBkLWZsZXggYWxpZ24taXRlbXMtZW5kXCI+XG4gICAgICAgICAgICAgICAgPHNwYW4gaWQ9e0pTT04uc3RyaW5naWZ5KHByb2R1Y3QpfSBzdHlsZT17eyB0ZXh0RGVjb3JhdGlvbjogXCJub25lXCIgfX0gY2xhc3NOYW1lPVwiY2FyZC1idG5cIj5cbiAgICAgICAgICAgICAgICAgICAgTHVlIGxpc8Okw6RcbiAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xufTtcbmV4cG9ydCBkZWZhdWx0IFByb2R1Y3RDYXJvdXNlbEl0ZW07XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBMaW5rIH0gZnJvbSBcInJlYWN0LXJvdXRlci1kb21cIjtcbmltcG9ydCBQcm9kdWN0Q2FyZCBmcm9tIFwiLi9Qcm9kdWN0Q2FyZFwiO1xuaW1wb3J0IExvYWRpbmcgZnJvbSBcIi4vTG9hZGluZ1wiO1xuaW1wb3J0IE5vdEZvdW5kIGZyb20gXCIuL05vdEZvdW5kXCI7XG5cbmNvbnN0IFByb2R1Y3RzID0gKHtwcm9kdWN0cywgbG9hZGluZ30pID0+IHtcblxuICByZXR1cm4gKFxuICAgIDxtYWluPlxuICAgICAge2xvYWRpbmcgPyAoXG4gICAgICAgIDxMb2FkaW5nIC8+XG4gICAgICApIDogcHJvZHVjdHMgPyAoXG4gICAgICAgIDw+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWxnLW5vbmUgcC0zIGJnLXdoaXRlXCI+XG4gICAgICAgICAgICA8TGluayB0bz1cIi9cIj5cbiAgICAgICAgICAgICAgPGlcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJmYXMgZmEtY2hldnJvbi1sZWZ0XCJcbiAgICAgICAgICAgICAgICBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIgfX1cbiAgICAgICAgICAgICAgPjwvaT5cbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiwgZm9udFNpemU6IFwiMTZweFwiLCBmb250V2VpZ2h0OiBcImJvbGRcIiB9fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBUVU9UVEVFVFxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwLTRcIj5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICB3aWR0aDogXCJjYWxjKDEwMCUgLSA2cHgpXCIsXG4gICAgICAgICAgICAgICAgbWFyZ2luTGVmdDogXCIzcHhcIixcbiAgICAgICAgICAgICAgICBtYXJnaW5SaWdodDogXCItM3B4XCIsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm5hdmlnYXRpb24tbGlua3MgcC00IGJnLXdoaXRlIGQtbm9uZSBkLWxnLWJsb2NrIG1iLTFcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgIDxMaW5rXG4gICAgICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjogXCIjNWY1ZjVmXCIsXG4gICAgICAgICAgICAgICAgICAgIHRleHREZWNvcmF0aW9uOiBcIm5vbmVcIixcbiAgICAgICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMTJweFwiLFxuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIHRvPVwiL1wiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgRXR1c2l2dSAmbmJzcDsgLyAmbmJzcDtcbiAgICAgICAgICAgICAgICA8L0xpbms+XG5cbiAgICAgICAgICAgICAgICA8c3BhbiBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIsIGZvbnRTaXplOiBcIjEycHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgIFRVT1RURUVUXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInB0LTNcIlxuICAgICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgICBjb2xvcjogXCIjNWY1ZjVmXCIsXG4gICAgICAgICAgICAgICAgICBmb250U2l6ZTogXCJ4LWxhcmdlXCIsXG4gICAgICAgICAgICAgICAgICBmb250V2VpZ2h0OiBcImJvbGRcIixcbiAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgVFVPVFRFRVRcbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicm93IG0tMFwiPlxuICAgICAgICAgICAgICB7cHJvZHVjdHMubWFwKChwcm9kdWN0LCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICAgIDxQcm9kdWN0Q2FyZCBwcm9kdWN0PXtwcm9kdWN0fSBrZXk9e2luZGV4fSAvPlxuICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8Lz5cbiAgICAgICkgOiAoXG4gICAgICAgIDxOb3RGb3VuZCAvPlxuICAgICAgKX1cbiAgICA8L21haW4+XG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBQcm9kdWN0cztcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBTaGlwcGluZ0Zvcm0gZnJvbSBcIi4vU2hpcHBpbmdGb3JtXCI7XG5pbXBvcnQgVG90YWxBbmRUYXhUYWJsZSBmcm9tIFwiLi9Ub3RhbEFuZFRheFRhYmxlXCI7XG5pbXBvcnQgTG9hZGluZyBmcm9tIFwiLi9Mb2FkaW5nXCJcbmltcG9ydCB7IHRvZ2dsZUV4cGFuZGVkVG90YWxBbmRUYXhUYWJsZSwgdG9nZ2xlU2hpcHBpbmdBY2NvcmRpb24gfSBmcm9tIFwiLi4vYXBwXCI7XG5cbmNvbnN0IFNoaXBwaW5nID0gKHtjYXJ0LCBsb2FkaW5nfSkgPT4ge1xuXG4gIHJldHVybiBsb2FkaW5nID8gPExvYWRpbmcgLz4gOiAoXG4gICAgPG1haW4+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cInNoaXBwaW5nLXByb2dyZXNzLWluZGljYXRvci1jb250YWluZXIgZC1sZy1ibG9ja1wiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtZmxleCBtdC00IHBzLTQgcGUtNCBqdXN0aWZ5LWNvbnRlbnQtYXJvdW5kIGp1c3RpZnktY29udGVudC1sZy1jZW50ZXJcIj5cbiAgICAgICAgICA8ZGl2IHN0eWxlPXt7IHBvc2l0aW9uOiBcInJlbGF0aXZlXCIgfX0gY2xhc3NOYW1lPVwibWUtYXV0byBtZS1sZy0xXCI+XG4gICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgd2lkdGg9XCIyNFwiXG4gICAgICAgICAgICAgIGhlaWdodD1cIjI0XCJcbiAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJpIGJpLWNpcmNsZS1maWxsXCJcbiAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxjaXJjbGUgY3g9XCI4XCIgY3k9XCI4XCIgcj1cIjhcIiAvPlxuICAgICAgICAgICAgPC9zdmc+XG4gICAgICAgICAgICA8c3BhblxuICAgICAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgICAgIGNvbG9yOiBcIndoaXRlXCIsXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcbiAgICAgICAgICAgICAgICBsZWZ0OiBcIjhweFwiLFxuICAgICAgICAgICAgICAgIHRvcDogXCI0cHhcIixcbiAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIxMnB4XCIsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDFcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMTNweFwiLFxuICAgICAgICAgICAgICAgIGNvbG9yOiBcIiM1ZjVmNWZcIixcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IFwiMjRweFwiLFxuICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IFwiMjRweFwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtcy0zIG1lLTMgZnctYm9sZFwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIFRvaW1pdHVza3VsdXRcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImQtbm9uZSBkLW1kLWlubGluZS1ibG9jayBtZS0zXCJcbiAgICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogXCIyNHB4XCIsIGxpbmVIZWlnaHQ6IFwiMjRweFwiIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPGltZyBzcmM9XCIvaW1nL2xpbmUucG5nXCIgYWx0PVwiXCIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IHN0eWxlPXt7IHBvc2l0aW9uOiBcInJlbGF0aXZlXCIgfX0gY2xhc3NOYW1lPVwibXMtYXV0byBtcy1sZy0xXCI+XG4gICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGNvbG9yOiBcIiM1ZjVmNWZcIiB9fVxuICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgd2lkdGg9XCIyNFwiXG4gICAgICAgICAgICAgIGhlaWdodD1cIjI0XCJcbiAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJpIGJpLWNpcmNsZVwiXG4gICAgICAgICAgICAgIHZpZXdCb3g9XCIwIDAgMTYgMTZcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8cGF0aCBkPVwiTTggMTVBNyA3IDAgMSAxIDggMWE3IDcgMCAwIDEgMCAxNHptMCAxQTggOCAwIDEgMCA4IDBhOCA4IDAgMCAwIDAgMTZ6XCIgLz5cbiAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgICAgPHNwYW5cbiAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICBjb2xvcjogXCIjNWY1ZjVmXCIsXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcbiAgICAgICAgICAgICAgICBsZWZ0OiBcIjhweFwiLFxuICAgICAgICAgICAgICAgIHRvcDogXCI0cHhcIixcbiAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIxMnB4XCIsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDJcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuXG4gICAgICAgICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgICAgICAgZm9udFNpemU6IFwiMTNweFwiLFxuICAgICAgICAgICAgICAgIGNvbG9yOiBcIiM1ZjVmNWZcIixcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IFwiMjRweFwiLFxuICAgICAgICAgICAgICAgIGxpbmVIZWlnaHQ6IFwiMjRweFwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJtcy0zIG1lLTNcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBWaWxhdXN2YWh2aXN0dXNcbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPVwic2hpcHBpbmctbWFpbi1jb250ZW50LWNvbnRhaW5lciBkLWZsZXggbS1sZy00IG10LTRcIlxuICAgICAgICBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIgfX1cbiAgICAgID5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJzaGlwcGluZy1mb3JtLWNvbnRhaW5lciBkLWxnLWlubGluZS1ibG9jayBiZy13aGl0ZSBwLTQgbWUtbGctNCBmbGV4LWdyb3ctMVwiPlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImZ3LWJvbGQgZnMtNiB3LTEwMCBwYi00IG1iLTRcIlxuICAgICAgICAgICAgc3R5bGU9e3sgYm9yZGVyQm90dG9tOiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIiB9fVxuICAgICAgICAgID5cbiAgICAgICAgICAgIFRvaW1pdHVzb3NvaXRlXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPFNoaXBwaW5nRm9ybSBjYXJ0PXtjYXJ0fSAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogXCJmaXQtY29udGVudFwiIH19XG4gICAgICAgICAgY2xhc3NOYW1lPVwidG90YWwtYW5kLXRheC10YWJsZS1jb250YWluZXIgbWItMiBwLTQgYmctd2hpdGUgZC1ub25lIGQtbGctaW5saW5lLWJsb2NrIGQtZmxleFwiXG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtaW5saW5lLWJsb2NrXCI+XG4gICAgICAgICAgICA8aDYgY2xhc3NOYW1lPVwiZnctYm9sZCB0ZXh0LWxlZnRcIj5USUxBVUtTRVNJPC9oNj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8aHIgLz5cblxuICAgICAgICAgIDxUb3RhbEFuZFRheFRhYmxlXG4gICAgICAgICAgICBjbGFzc0xpc3Q9e1widG90YWwtYW5kLXRheC10YWJsZSB3LTEwMFwifVxuICAgICAgICAgICAgY2FydD17Y2FydH1cbiAgICAgICAgICAvPlxuXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLW5vbmUgZC1sZy1pbmxpbmUtYmxvY2tcIj5cbiAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICBmb250U2l6ZTogXCIxMnB4XCIsXG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIndoaXRlXCIsXG4gICAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICAgIGJvcmRlclRvcDogXCIxcHggc29saWQgbGlnaHRncmF5XCIsXG4gICAgICAgICAgICAgICAgYm9yZGVyTGVmdDogXCIxcHggc29saWQgbGlnaHRncmF5XCIsXG4gICAgICAgICAgICAgICAgYm9yZGVyUmlnaHQ6IFwiMXB4IHNvbGlkIGxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCJhY2NvcmRpb25fYnV0dG9uIGQtZmxleCBhbGlnbi1pdGVtcy1jZW50ZXJcIlxuICAgICAgICAgICAgICBvbkNsaWNrPXt0b2dnbGVTaGlwcGluZ0FjY29yZGlvbn1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPGI+e2NhcnQuaXRlbXMubGVuZ3RofSBUVU9URSBPU1RPU0tPUklTU0E8L2I+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGx1cy1pY29uIGdyYXktcGx1cy1pY29uXCI+PC9kaXY+XG4gICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICBib3JkZXJMZWZ0OiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIixcbiAgICAgICAgICAgICAgICBib3JkZXJSaWdodDogXCIxcHggc29saWQgbGlnaHRncmF5XCIsXG4gICAgICAgICAgICAgICAgYm9yZGVyQm90dG9tOiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIixcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwid2hpdGVcIixcbiAgICAgICAgICAgICAgICBjb2xvcjogXCIjNWY1ZjVmXCIsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cInNoaXBwaW5nLWFjY29yZGlvbi1wYW5lbCBwYW5lbFwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIHtjYXJ0Lml0ZW1zLm1hcCgoaXRlbSwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICA8ZGl2IGtleT17aW5kZXh9IGNsYXNzTmFtZT1cInB0LTMgcHItMyBwYi0zIGQtZmxleFwiPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJkLWlubGluZS1ibG9ja1wiPlxuICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgaGVpZ2h0OiBcIjc1cHhcIiB9fVxuICAgICAgICAgICAgICAgICAgICAgIHNyYz17aXRlbS5wcm9kdWN0LmltYWdlVXJsfVxuICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5saW5lLWJsb2NrXCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgZm9udFNpemU6IFwiMTJweFwiIH19IGNsYXNzTmFtZT1cImZ3LWJvbGRcIj5cbiAgICAgICAgICAgICAgICAgICAgICB7aXRlbS5wcm9kdWN0LnRpdGxlfVxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBmb250U2l6ZTogXCIxMnB4XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgS3BsL3RuOiB7aXRlbS5xdWFudGl0eX1cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3sgZm9udFNpemU6IFwiMTBweFwiIH19PlxuICAgICAgICAgICAgICAgICAgICAgIFZlcm9sbGluZW46IHtpdGVtLnRvdGFsLnRvRml4ZWQoMil9ICZldXJvO1xuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBmb250U2l6ZTogXCIxMHB4XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgVmVyb3Rvbjp7XCIgXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgeyhpdGVtLnRvdGFsICogKDEgLSBpdGVtLnByb2R1Y3QudmF0KSkudG9GaXhlZCgyKX0gJmV1cm87XG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICkpfVxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImV4cGFuZGVkLXRvdGFsLWFuZC10YXgtdGFibGUtY29udGFpbmVyIGJnLXdoaXRlIHAtNCBkLW5vbmUgZC1sZy1ub25lIHctMTAwIGgtMTAwXCI+XG4gICAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IFwiZml0LWNvbnRlbnRcIiB9fSBjbGFzc05hbWU9XCJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy0xMDAgZC1ibG9ja1wiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImQtaW5saW5lLWJsb2NrXCI+XG4gICAgICAgICAgICAgICAgPGg2IGNsYXNzTmFtZT1cImZ3LWJvbGQgdGV4dC1sZWZ0XCI+VElMQVVLU0VTSTwvaDY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZC1mbGV4IGQtaW5saW5lLWJsb2NrXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0b2dnbGVFeHBhbmRlZFRvdGFsQW5kVGF4VGFibGV9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8c3ZnXG4gICAgICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcbiAgICAgICAgICAgICAgICAgIHdpZHRoPVwiMTZcIlxuICAgICAgICAgICAgICAgICAgaGVpZ2h0PVwiMTZcIlxuICAgICAgICAgICAgICAgICAgZmlsbD1cImN1cnJlbnRDb2xvclwiXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJiaSBiaS14LWxnIG1zLWF1dG9cIlxuICAgICAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAxNiAxNlwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPHBhdGggZD1cIk0xLjI5MyAxLjI5M2ExIDEgMCAwIDEgMS40MTQgMEw4IDYuNTg2bDUuMjkzLTUuMjkzYTEgMSAwIDEgMSAxLjQxNCAxLjQxNEw5LjQxNCA4bDUuMjkzIDUuMjkzYTEgMSAwIDAgMS0xLjQxNCAxLjQxNEw4IDkuNDE0bC01LjI5MyA1LjI5M2ExIDEgMCAwIDEtMS40MTQtMS40MTRMNi41ODYgOCAxLjI5MyAyLjcwN2ExIDEgMCAwIDEgMC0xLjQxNHpcIiAvPlxuICAgICAgICAgICAgICAgIDwvc3ZnPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGhyIC8+XG4gICAgICAgICAgICA8VG90YWxBbmRUYXhUYWJsZVxuICAgICAgICAgICAgICBjbGFzc0xpc3Q9e1wiZC1ibG9jayB0b3RhbC1hbmQtdGF4LXRhYmxlIHctMTAwXCJ9XG4gICAgICAgICAgICAgIGNhcnQ9e2NhcnR9XG4gICAgICAgICAgICAvPlxuXG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGZvbnRTaXplOiBcIjEycHhcIixcbiAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogXCJ3aGl0ZVwiLFxuICAgICAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICAgICAgYm9yZGVyVG9wOiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIixcbiAgICAgICAgICAgICAgICAgIGJvcmRlckxlZnQ6IFwiMXB4IHNvbGlkIGxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICAgICAgYm9yZGVyUmlnaHQ6IFwiMXB4IHNvbGlkIGxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYWNjb3JkaW9uX2J1dHRvbiBkLWZsZXggYWxpZ24taXRlbXMtY2VudGVyXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0b2dnbGVTaGlwcGluZ0FjY29yZGlvbn1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxiPntjYXJ0Lml0ZW1zLmxlbmd0aH0gVFVPVEUgT1NUT1NLT1JJU1NBPC9iPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicGx1cy1pY29uIGdyYXktcGx1cy1pY29uXCI+PC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgICAgICAgIGJvcmRlckxlZnQ6IFwiMXB4IHNvbGlkIGxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICAgICAgYm9yZGVyUmlnaHQ6IFwiMXB4IHNvbGlkIGxpZ2h0Z3JheVwiLFxuICAgICAgICAgICAgICAgICAgYm9yZGVyQm90dG9tOiBcIjFweCBzb2xpZCBsaWdodGdyYXlcIixcbiAgICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogXCJ3aGl0ZVwiLFxuICAgICAgICAgICAgICAgICAgY29sb3I6IFwiIzVmNWY1ZlwiLFxuICAgICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwic2hpcHBpbmctYWNjb3JkaW9uLXBhbmVsIHBhbmVsXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIHtjYXJ0Lml0ZW1zLm1hcCgoaXRlbSwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgICAgIDxkaXYga2V5PXtpbmRleH0gY2xhc3NOYW1lPVwicHQtMyBwci0zIHBiLTMgZC1mbGV4XCI+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZC1pbmxpbmUtYmxvY2tcIj5cbiAgICAgICAgICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT17eyBoZWlnaHQ6IFwiNzVweFwiIH19XG4gICAgICAgICAgICAgICAgICAgICAgICBzcmM9e2l0ZW0ucHJvZHVjdC5pbWFnZVVybH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXG4gICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW5saW5lLWJsb2NrXCI+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBmb250U2l6ZTogXCIxMnB4XCIgfX0gY2xhc3NOYW1lPVwiZnctYm9sZFwiPlxuICAgICAgICAgICAgICAgICAgICAgICAge2l0ZW0ucHJvZHVjdC50aXRsZX1cbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGZvbnRTaXplOiBcIjEycHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIEtwbC90bjoge2l0ZW0ucXVhbnRpdHl9XG4gICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBmb250U2l6ZTogXCIxMHB4XCIgfX0+XG4gICAgICAgICAgICAgICAgICAgICAgICBWZXJvbGxpbmVuOiB7aXRlbS50b3RhbC50b0ZpeGVkKDIpfSAmZXVybztcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGZvbnRTaXplOiBcIjEwcHhcIiB9fT5cbiAgICAgICAgICAgICAgICAgICAgICAgIFZlcm90b246e1wiIFwifVxuICAgICAgICAgICAgICAgICAgICAgICAgeyhpdGVtLnRvdGFsICogKDEgLSBpdGVtLnByb2R1Y3QudmF0KSkudG9GaXhlZCgyKX17XCIgXCJ9XG4gICAgICAgICAgICAgICAgICAgICAgICAmZXVybztcbiAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICApKX1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L21haW4+XG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBTaGlwcGluZztcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IHZhbGlkYXRlRm9ybSB9IGZyb20gXCIuLi9hcHBcIjtcbmltcG9ydCB7IHVzZUhpc3RvcnkgfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuXG5jb25zdCBTaGlwcGluZ0Zvcm0gPSAoe2NhcnR9KSA9PiB7XG4gIGxldCBoaXN0b3J5ID0gdXNlSGlzdG9yeSgpO1xuXG4gIGZ1bmN0aW9uIGhhbmRsZVN1Ym1pdCgpIHtcbiAgICBcbiAgICBsZXQgdmFsaWQgPSB2YWxpZGF0ZUZvcm0oKTtcblxuICAgIGlmICh2YWxpZCAmJiBjYXJ0Lml0ZW1zLmxlbmd0aCA+IDApIHtcbiAgICAgIFxuICAgICAgbGV0IHNoaXBwaW5nSW5mbyA9IHtcbiAgICAgICAgXG4gICAgICAgIGZpcnN0TmFtZTogJChcIiNmaXJzdE5hbWVcIikudmFsKCksXG4gICAgICAgIGxhc3ROYW1lOiAkKFwiI2xhc3ROYW1lXCIpLnZhbCgpLFxuICAgICAgICBjb21wYW55OiAkKFwiI2NvbXBhbnlcIikudmFsKCksXG4gICAgICAgIHN0cmVldEFkZHJlc3MxOiAkKFwiI3N0cmVldEFkZHJlc3MxXCIpLnZhbCgpLFxuICAgICAgICBzdHJlZXRBZGRyZXNzMjogJChcIiNzdHJlZXRBZGRyZXNzMlwiKS52YWwoKSxcbiAgICAgICAgemlwQ29kZTogJChcIiN6aXBDb2RlXCIpLnZhbCgpLFxuICAgICAgICBjaXR5OiAkKFwiI2NpdHlcIikudmFsKCksXG4gICAgICAgIHRlbGVwaG9uZTogJChcIiN0ZWxlcGhvbmVcIikudmFsKCksXG4gICAgICAgIGVtYWlsOiAkKFwiI2VtYWlsXCIpLnZhbCgpLFxuICAgICAgICBtYXJrZXRpbmc6ICQoXCIjbWFya2V0aW5nXCIpLnZhbCgpLFxuICAgICAgICBkZWxpdmVyeUluZm86ICQoXCIjZGVsaXZlcnlJbmZvXCIpLnZhbCgpLFxuICAgICAgfTtcblxuICAgICAgaGlzdG9yeS5wdXNoKHtwYXRobmFtZTpcIi9jaGVja291dC9jb25maXJtYXRpb25cIiwgc3RhdGU6c2hpcHBpbmdJbmZvfSlcbiAgICB9XG4gIH07XG5cbiAgcmV0dXJuIChcbiAgICA8Zm9ybSBjbGFzc05hbWU9XCJzaGlwcGluZy1mb3JtXCI+XG4gICAgICA8ZGl2PlxuICAgICAgICA8bGFiZWw+RXR1bmltaSAqPC9sYWJlbD5cbiAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgaWQ9XCJmaXJzdE5hbWVcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiByZXF1aXJlZCAvPlxuICAgICAgICA8cD48L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxsYWJlbD5TdWt1bnVtaSAqPC9sYWJlbD5cbiAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJsYXN0TmFtZVwiIHJlcXVpcmVkIC8+XG4gICAgICAgIDxwPjwvcD5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdj5cbiAgICAgICAgPGxhYmVsPllyaXR5czwvbGFiZWw+XG4gICAgICAgIDxpbnB1dCB0eXBlPVwidGV4dFwiIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiIGlkPVwiY29tcGFueVwiIC8+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxsYWJlbD5LYXR1b3NvaXRlICo8L2xhYmVsPlxuICAgICAgICA8aW5wdXRcbiAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcbiAgICAgICAgICBpZD1cInN0cmVldEFkZHJlc3MxXCJcbiAgICAgICAgICByZXF1aXJlZFxuICAgICAgICAvPlxuICAgICAgICA8cD48L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxsYWJlbD48L2xhYmVsPlxuICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInN0cmVldEFkZHJlc3MyXCIgLz5cbiAgICAgIDwvZGl2PlxuICAgICAgPGRpdj5cbiAgICAgICAgPGxhYmVsPlBvc3RpbnVtZXJvICo8L2xhYmVsPlxuICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBpZD1cInppcENvZGVcIiByZXF1aXJlZCAvPlxuICAgICAgICA8cD48L3A+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxsYWJlbD5QYWlra2FrdW50YSAqPC9sYWJlbD5cbiAgICAgICAgPGlucHV0IHR5cGU9XCJ0ZXh0XCIgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCIgaWQ9XCJjaXR5XCIgcmVxdWlyZWQgLz5cbiAgICAgICAgPHA+PC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2PlxuICAgICAgICA8bGFiZWw+UHVoZWxpbnVtZXJvICo8L2xhYmVsPlxuICAgICAgICA8aW5wdXRcbiAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgY2xhc3NOYW1lPVwiZm9ybS1jb250cm9sXCJcbiAgICAgICAgICBpZD1cInRlbGVwaG9uZVwiXG4gICAgICAgICAgZGVmYXVsdFZhbHVlPVwiKzM1OFwiXG4gICAgICAgICAgcmVxdWlyZWRcbiAgICAgICAgLz5cbiAgICAgICAgPHA+PC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2PlxuICAgICAgICA8bGFiZWw+U8Oka2jDtnBvc3Rpb3NvaXRlICo8L2xhYmVsPlxuICAgICAgICA8aW5wdXQgdHlwZT1cInRleHRcIiBjbGFzc05hbWU9XCJmb3JtLWNvbnRyb2xcIiBpZD1cImVtYWlsXCIgcmVxdWlyZWQgLz5cbiAgICAgICAgPHA+PC9wPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2PlxuICAgICAgICA8aW5wdXRcbiAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgd2lkdGg6IFwiMjBweFwiLFxuICAgICAgICAgICAgaGVpZ2h0OiBcIjIwcHhcIixcbiAgICAgICAgICAgIGN1cnNvcjogXCJwb2ludGVyXCIsXG4gICAgICAgICAgICBib3JkZXJSYWRpdXM6IFwiMFwiLFxuICAgICAgICAgIH19XG4gICAgICAgICAgdHlwZT1cImNoZWNrYm94XCJcbiAgICAgICAgICBpZD1cIm1hcmtldGluZ1wiXG4gICAgICAgIC8+XG4gICAgICAgIDxsYWJlbCBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIgfX0gY2xhc3NOYW1lPVwiYWxpZ24tdG9wXCI+XG4gICAgICAgICAgTWludWxsZSBzYWEgbMOkaGV0dMOkw6QgTm9ycmEgSG9yc2VuIG1hcmtraW5vaW50aW1hdGVyaWFhbGlhXG4gICAgICAgIDwvbGFiZWw+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxsYWJlbCBzdHlsZT17eyBjb2xvcjogXCIjNWY1ZjVmXCIgfX0+VG9pbWl0dWtzZW4gbGlzw6R0aWVkb3QgPC9sYWJlbD5cbiAgICAgICAgPHRleHRhcmVhXG4gICAgICAgICAgc3R5bGU9e3sgYm9yZGVyUmFkaXVzOiBcIjBcIiB9fVxuICAgICAgICAgIGNsYXNzTmFtZT1cImZvcm0tY29udHJvbFwiXG4gICAgICAgICAgaWQ9XCJkZWxpdmVyeUluZm9cIlxuICAgICAgICA+PC90ZXh0YXJlYT5cbiAgICAgIDwvZGl2PlxuICAgICAgPHNwYW4+Vmllc3RpaW4gbWFodHV1IG1ha3NpbWlzc2FhbiAyMzUgbWVya2tpw6Q8L3NwYW4+XG4gICAgICA8c3BhblxuICAgICAgICBzdHlsZT17e1xuICAgICAgICAgIHdpZHRoOiBcIjIyMHB4XCIsXG4gICAgICAgICAgaGVpZ2h0OiBcIjUwcHhcIixcbiAgICAgICAgICBjdXJzb3I6IFwicG9pbnRlclwiLFxuICAgICAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIixcbiAgICAgICAgfX1cbiAgICAgICAgY2xhc3NOYW1lPVwiY2hlY2tvdXQtcGFnZS1idG4gZmxvYXQtZW5kIG10LTUgcC0zXCJcbiAgICAgICAgb25DbGljaz17aGFuZGxlU3VibWl0fVxuICAgICAgPlxuICAgICAgICBTZXVyYWF2YVxuICAgICAgPC9zcGFuPlxuICAgIDwvZm9ybT5cbiAgKTtcbn07XG5leHBvcnQgZGVmYXVsdCBTaGlwcGluZ0Zvcm07XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5cbmNvbnN0IFRvdGFsQW5kVGF4VGFibGUgPSAoeyBjbGFzc0xpc3QsIGNhcnQgfSkgPT4gKFxuICA8dGFibGUgY2xhc3NOYW1lPXtjbGFzc0xpc3R9PlxuICAgIDx0Ym9keT5cbiAgICAgIDx0cj5cbiAgICAgICAgPHRkPktva29uYWlzaGludGEgaWxtYW4gQUxWOnTDpDwvdGQ+XG4gICAgICAgIDx0ZD57KGNhcnQudG90YWwgLSBjYXJ0LnRvdGFsVmF0KS50b0ZpeGVkKDIpfSAmZXVybzs8L3RkPlxuICAgICAgPC90cj5cbiAgICAgIDx0cj5cbiAgICAgICAgPHRkPktva29uYWlzaGludGEgQUxWOsOkbiBrYW5zc2E8L3RkPlxuICAgICAgICA8dGQ+e2NhcnQudG90YWwudG9GaXhlZCgyKX0gJmV1cm87PC90ZD5cbiAgICAgIDwvdHI+XG4gICAgICA8dHI+XG4gICAgICAgIDx0ZD5WZXJvdDwvdGQ+XG4gICAgICAgIDx0ZD57Y2FydC50b3RhbFZhdC50b0ZpeGVkKDIpfSAmZXVybzs8L3RkPlxuICAgICAgPC90cj5cbiAgICAgIDx0cj5cbiAgICAgICAgPHRkPlRvaW1pdHVza3VsdXQ8L3RkPlxuICAgICAgICA8dGQ+SWxtYWluZW48L3RkPlxuICAgICAgPC90cj5cbiAgICAgIDx0ciBjbGFzc05hbWU9XCJmdy1ib2xkXCI+XG4gICAgICAgIDx0ZD5Lb2tvbmFpc2hpbnRhIEFMVjrDpG4ga2Fuc3NhPC90ZD5cbiAgICAgICAgPHRkPntjYXJ0LnRvdGFsLnRvRml4ZWQoMil9ICZldXJvOzwvdGQ+XG4gICAgICA8L3RyPlxuICAgIDwvdGJvZHk+XG4gIDwvdGFibGU+XG4pO1xuXG5leHBvcnQgZGVmYXVsdCBUb3RhbEFuZFRheFRhYmxlO1xuIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luXG5leHBvcnQge307Il0sIm5hbWVzIjpbIkNvbnRyb2xsZXIiLCJlbGVtZW50IiwidGV4dENvbnRlbnQiLCJSZWFjdCIsIlJlYWN0RE9NIiwiQXBwIiwiYXhpb3MiLCJyZW5kZXIiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwiYWNjIiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsImkiLCJsZW5ndGgiLCJhZGRFdmVudExpc3RlbmVyIiwiY2xhc3NMaXN0IiwidG9nZ2xlIiwicGFuZWwiLCJuZXh0RWxlbWVudFNpYmxpbmciLCJzdHlsZSIsIm1heEhlaWdodCIsInNjcm9sbEhlaWdodCIsInRvZ2dsZVNoaXBwaW5nQWNjb3JkaW9uIiwiYWNjb3JkaW9uQnV0dG9ucyIsInBhbmVscyIsInZhbGlkYXRlRm9ybSIsInZhbGlkIiwiJCIsImVhY2giLCJpbmRleCIsImNoZWNrVmFsaWRpdHkiLCJuZXh0IiwidGV4dCIsImNzcyIsInByZXYiLCJnZXRDYXJ0IiwiZ2V0IiwiZGF0YSIsIml0ZW1zIiwicmVtb3ZlQ2xhc3MiLCJodG1sIiwidG90YWwiLCJ0b0ZpeGVkIiwiYWRkQ2xhc3MiLCJ2YWxpZGF0ZVJlZHVjZVF1YW50aXR5IiwiaW5wdXQiLCJ2YWwiLCJwYXJzZUludCIsInZhbHVlIiwidmFsaWRhdGVJbmNyZWFzZVF1YW50aXR5IiwiY2hhbmdlUXVhbnRpdHkiLCJ1cmwiLCJwb3N0IiwicXVhbnRpdHkiLCJ0aGVuIiwiZm9jdXNQYWdlIiwidmFsaWRhdGVRdWFudGl0eSIsImFkZFRvQ2FydCIsImJsdXJQYWdlIiwicmVtb3ZlSXRlbSIsInRvZ2dsZUV4cGFuZGVkVG90YWxBbmRUYXhUYWJsZSIsImhhc0NsYXNzIiwicHJvZHVjdEFjY29yZGlvbiIsInByb2R1Y3RBY2MiLCJhY2NvcmRpb25CdXR0b25DbGljayIsInByb2R1Y3RGZWF0dXJlc0FjYyIsInByb2R1Y3RGZWF0dXJlc0FjY29yZGlvbkJ1dHRvbkNsaWNrIiwiYWNjb3JkaW9uQnV0dG9uIiwidGFiTGlzdCIsImNsYXNzTmFtZSIsInByb2R1Y3RQYW5lbHMiLCJ0YWJzIiwicGFyZW50RWxlbWVudCIsImFkZCIsInJlbW92ZSIsImlkIiwic3Vic3RyaW5nIiwidGFiIiwicHJvZHVjdEZlYXR1cmVzUGFuZWwxIiwicHJvZHVjdFBhbmVsMSIsInByb2R1Y3RGZWF0dXJlc1BhbmVsMiIsInByb2R1Y3RQYW5lbDIiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsInBhdGhuYW1lIiwic3BsaXQiLCJwb3AiLCJmZWF0dXJlcyIsImtleSIsIm1pblBlclNsaWRlIiwiaGFuZGxlTmV4dExpbmsiLCJuZXh0TGluayIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJjdXJyZW50SW5kZXgiLCJteUZ1bmN0aW9uIiwieHMiLCJtYXRjaE1lZGlhIiwic20iLCJtZCIsImxnIiwiYWRkTGlzdGVuZXIiLCJteUZ1bmN0aW9uMiIsInF1ZXJ5U2VsZWN0b3IiLCJ4IiwibWF0Y2hlcyIsIm1lZGlhIiwibXlGdW5jdGlvbjMiLCJudW1QZXJTbGlkZSIsImNoaWxkRWxlbWVudENvdW50IiwiZm9yRWFjaCIsImVsIiwiY2xvbmVDaGlsZHJlbiIsImNoaWxkcmVuIiwiY2xvbmVDaGlsZCIsImZpcnN0IiwiY2xvbmUiLCJhcHBlbmRUbyIsImNsb25lTm9kZSIsImoiLCJuZXh0Q2hpbGQiLCJhcHBlbmRDaGlsZCIsInJlbW92ZUNoaWxkIiwibG9hZFByb2R1Y3QiLCJwcm9kdWN0Iiwic2V0VGltZW91dCIsImluU3RvcmUiLCJ2YXQiLCJwcmljZSIsImxvYWRQcm9kdWN0cyIsInByb2R1Y3RzIiwibWFwIiwib3Blbk5hdiIsIndpZHRoIiwiY2xvc2VOYXYiLCJzdGFydFN0aW11bHVzQXBwIiwiYXBwIiwicmVxdWlyZSIsImNvbnRleHQiLCJCcm93c2VyUm91dGVyIiwiUm91dGVyIiwiUm91dGUiLCJTd2l0Y2giLCJ1c2VTdGF0ZSIsInVzZUVmZmVjdCIsIkxheW91dDEiLCJMYXlvdXQyIiwiQ2FydCIsIkNvbmZpcm1hdGlvbiIsIkhvbWUiLCJQcm9kdWN0IiwiUHJvZHVjdHMiLCJTaGlwcGluZyIsIk5vdEZvdW5kIiwic2V0UHJvZHVjdHMiLCJsb2FkaW5nIiwic2V0TG9hZGluZyIsImxvYWRpbmdDYXJ0Iiwic2V0TG9hZGluZ0NhcnQiLCJjYXJ0Iiwic2V0Q2FydCIsInVwZGF0ZSIsInNldFVwZGF0ZSIsImNhbGxiYWNrIiwiX3VwZGF0ZSIsInJlc3BvbnNlIiwiTGluayIsInVzZUhpc3RvcnkiLCJMb2FkaW5nIiwiRW1wdHlDYXJ0IiwiaGlzdG9yeSIsImZvbnRTaXplIiwiYm9yZGVyIiwicG9zaXRpb24iLCJjb2xvciIsImxlZnQiLCJ0b3AiLCJsaW5lSGVpZ2h0IiwiY3Vyc29yIiwiaGVpZ2h0IiwiYmFja2dyb3VuZENvbG9yIiwiaXRlbSIsImJvcmRlckJvdHRvbSIsInN0YXRlIiwiaW1hZ2VVcmwiLCJ0ZXh0RGVjb3JhdGlvbiIsInRpdGxlIiwibWFyZ2luUmlnaHQiLCJtYXJnaW5MZWZ0IiwiZm9udFdlaWdodCIsImJveFNoYWRvdyIsImJvcmRlclJhZGl1cyIsInRleHRBbGlnbiIsInRvdGFsVmF0IiwiYm9yZGVyVG9wIiwib3BhY2l0eSIsInB1c2giLCJ1c2VMb2NhdGlvbiIsInNoaXBwaW5nSW5mbyIsInBhZGRpbmdCb3R0b20iLCJjb21wYW55IiwiZmlyc3ROYW1lIiwibGFzdE5hbWUiLCJzdHJlZXRBZGRyZXNzMSIsInN0cmVldEFkZHJlc3MyIiwiemlwQ29kZSIsImNpdHkiLCJ0ZWxlcGhvbmUiLCJlbWFpbCIsIk1haW5DYXJvdXNlbCIsIlByb2R1Y3RDYXJvdXNlbCIsImJhY2tncm91bmQiLCJiYWNrZ3JvdW5kUG9zaXRpb24iLCJiYWNrZ3JvdW5kUmVwZWF0IiwiYmFja2dyb3VuZFNpemUiLCJ6SW5kZXgiLCJtYXJnaW5Ub3AiLCJwYWRkaW5nVG9wIiwicHJvcHMiLCJtYXJnaW5Cb3R0b20iLCJfcHJvZHVjdCIsInNldFByb2R1Y3QiLCJocmVmIiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJyZXBsYWNlIiwidG9VcHBlckNhc2UiLCJ0cmFuc2l0aW9uIiwic2hvcnREZXNjcmlwdGlvbiIsInNlbGxpbmdQb2ludHMiLCJzZWxsaW5nUG9pbnQiLCJldmVudCIsImxvbmdEZXNjcmlwdGlvbiIsIk9iamVjdCIsImtleXMiLCJjb250ZW50cyIsIm1vcmVJbmZvcm1hdGlvbiIsImVudHJpZXMiLCJ1c2FnZVJhdGUiLCJ0YXJnZXQiLCJwcm9kdWN0QWNjb3JkaW9uQnV0dG9uQ2xpY2siLCJQcm9kdWN0Q2FyZCIsInVzZVJlZiIsIlByb2R1Y3RDYXJvdXNlbEl0ZW0iLCJwcmV2aW91c0xpbmsiLCJoYW5kbGVQcmV2aW91c0NsaWNrIiwiY3VycmVudCIsImJsdXIiLCJoYW5kbGVOZXh0Q2xpY2siLCJjbGljayIsIkpTT04iLCJwYXJzZSIsInN0cmluZ2lmeSIsIlNoaXBwaW5nRm9ybSIsIlRvdGFsQW5kVGF4VGFibGUiLCJib3JkZXJMZWZ0IiwiYm9yZGVyUmlnaHQiLCJoYW5kbGVTdWJtaXQiLCJtYXJrZXRpbmciLCJkZWxpdmVyeUluZm8iXSwic291cmNlUm9vdCI6IiJ9